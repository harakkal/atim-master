<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */

/**
 * Class StructureValueDomain
 */
class StructureValueDomain extends AppModel
{

    public $name = 'StructureValueDomain';

    public $hasManySave = array(
        'StructureValueDomainsPermissibleValue' => array(
            'className' => 'StructureValueDomainsPermissibleValue',
            'foreignKey' => 'structure_value_domain_id'
        )
    );
    
    private $studiedListValues = array();

    /**
     * StructureValueDomain constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->hasMany = $this->hasManySave;
    }


    /**
     *
     * @param mixed $results
     * @param bool $primary
     * @return mixed
     */
    public function afterFind($results, $primary = false)
    {
        if (isset($results[0])) {
            $permissibleValues = array();
            foreach ($results as &$subResult) {
                if (isset($subResult['StructureValueDomainsPermissibleValue'])) {
                    $oldResult = $subResult;
                    $svd = $oldResult['StructureValueDomain'];
                    $subResult = array(
                        "id" => $svd['id'],
                        "domain_name" => $svd['domain_name'],
                        "overrive" => $svd['override'],
                        "category" => $svd['category'],
                        "source" => $svd['source']
                    );
                    foreach ($oldResult['StructureValueDomainsPermissibleValue'] as $svdpv) {
                        $permissibleValues[] = array(
                            "id" => $svdpv['id'],
                            "value" => $svdpv['StructurePermissibleValue']['value'],
                            "language_alias" => $svdpv['StructurePermissibleValue']['language_alias'],
                            "display_order" => $svdpv['display_order'],
                            "flag_active" => $svdpv['flag_active'],
                            "use_as_input" => $svdpv['use_as_input']
                        );
                    }
                    $subResult['StructurePermissibleValue'] = $permissibleValues;
                } else {
                    break;
                }
            }
            $results['StructurePermissibleValue'] = $permissibleValues;
        }
        return $results;
    }

    /**
     *
     * @param array $structureValueDomain
     * @param $dropdownResult
     */
    public function updateDropdownResult(array $structureValueDomain, &$dropdownResult)
    {
        if (strlen($structureValueDomain['source']) > 0) {
            // load source
            $tmpDropdownResult = StructuresComponent::getPulldownFromSource($structureValueDomain['source']);
            if (array_key_exists('defined', $tmpDropdownResult)) {
                $dropdownResult['defined'] += $tmpDropdownResult['defined'];
                if (array_key_exists('previously_defined', $tmpDropdownResult)) {
                    $dropdownResult['previously_defined'] += $tmpDropdownResult['previously_defined'];
                }
            } else {
                $dropdownResult['defined'] += $tmpDropdownResult;
            }
        } else {
            $this->cacheQueries = true;
            $tmpDropdownResult = $this->find('first', array(
                'recursive' => 2, // cakephp has a memory leak when recursive = 2
                'conditions' => array(
                    'StructureValueDomain.id' => $structureValueDomain['id']
                )
            ));
            if (isset($tmpDropdownResult['StructurePermissibleValue']) && count($tmpDropdownResult['StructurePermissibleValue']) > 0) {
                $tmpResult = array(
                    'defined' => array(),
                    'previously_defined' => array()
                );
                // sort based on flag and on order
                foreach ($tmpDropdownResult['StructurePermissibleValue'] as $tmpEntry) {
                    if ($tmpEntry['flag_active']) {
                        if ($tmpEntry['use_as_input']) {
                            $tmpResult['defined'][$tmpEntry['value']] = sprintf("%04d", $tmpEntry['display_order']) . __($tmpEntry['language_alias'], null);
                        } else {
                            $tmpResult['previously_defined'][$tmpEntry['value']] = sprintf("%04d", $tmpEntry['display_order']) . __($tmpEntry['language_alias'], null);
                        }
                    }
                }
                asort($tmpResult['defined']);
                asort($tmpResult['previously_defined']);
                $substr4Func = create_function('$str', 'return substr($str, 4);');
                $tmpResult['defined'] = array_map($substr4Func, $tmpResult['defined']);
                $tmpResult['previously_defined'] = array_map($substr4Func, $tmpResult['previously_defined']);

                $dropdownResult['defined'] += $tmpResult['defined']; // merging arrays and keeping numeric keys intact
                $dropdownResult['previously_defined'] += $tmpResult['previously_defined'];
            }
        }
    }
    
    /**
     * Return the value to display to the user based on the value passed in parameter (as recorded into the database), 
     * the list domain_name where the value comes from and the language of the user.
     * 
     * Functions works whatever the type of list is : fix, variable or generated by function. 
     *
     * @param string $domainName Domain Name of the list.
     * @param string $value Value to format for display.
     * 
     * @return string Value to display.
     */
    function getValueToDisplay($domainName, $studiedValue) {
        // Check list has already been set to get value to display or set list
        if (!array_key_exists($domainName, $this->studiedListValues))  {
            $domain = $this->find('first', array('conditions' => array("StructureValueDomain.domain_name" => $domainName), 'recursive' => 2));
            if (empty($domain)) {
                if (Configure::read('debug') > 0 && strlen($studiedValue)) {
                    AppController::addWarningMsg(__("unknown list of values (domain_name) [%s]", $studiedValue, $domainName));
                }
                $this->studiedListValues[$domainName] = array();
            } else {
                if (empty($domain['source'])) {
                    foreach($domain['StructurePermissibleValue'] as $newListValue) {
                        if ($newListValue['flag_active']) {
                            $this->studiedListValues[$domainName][$newListValue['value']] = __($newListValue['language_alias']);
                        } 
                    }
                } else {
                    $this->studiedListValues[$domainName] = StructuresComponent::getPulldownFromSource($domain['source']);
                    if (strpos($domain['source'], 'getCustomDropdown')) {
                        $this->studiedListValues[$domainName] = array_merge($this->studiedListValues[$domainName]['defined'], $this->studiedListValues[$domainName]['previously_defined']); 
                    }
                }
            }
        }
        
        //Get value to display
        if (isset($this->studiedListValues[$domainName]) && array_key_exists($studiedValue, $this->studiedListValues[$domainName])) {
            return $this->studiedListValues[$domainName][$studiedValue];
        } else {
            if (Configure::read('debug') > 0 && strlen($studiedValue)) {
                AppController::addWarningMsg(__("missing list value [%s] for list of values (domain_name) [%s]", $studiedValue, $domainName));
            }
            return $studiedValue;
        }
    }

    /**
     * Return the list of values of a any type of list based on the domain name of the list. Values are retruned in an array with
     * keys equal to the value in database and values equal to the translated displayed values.
     *
     * Functions works whatever the type of list is : fix, variable or generated by function
     * 
     * @param string $domainName Domain Name of the list.
     *
     * @return array Keys equal to the value in database and values equal to the translated displayed values.
     */
    function getDropDown($domainName) {
        // Check list has already been set to get value to display or set list
        if (!array_key_exists($domainName, $this->studiedListValues))  {
            $domain = $this->find('first', array('conditions' => array("StructureValueDomain.domain_name" => $domainName), 'recursive' => 2));
            if (empty($domain)) {
                if (Configure::read('debug') > 0) {
                    AppController::addWarningMsg(__("unknown list of values (domain_name = [%s]", $domainName));
                }
                $this->studiedListValues[$domainName] = array();
            } else {
                if (empty($domain['source'])) {
                    foreach($domain['StructurePermissibleValue'] as $newListValue) {
                        if ($newListValue['flag_active']) {
                            $this->studiedListValues[$domainName][$newListValue['value']] = __($newListValue['language_alias']);
                        }
                    }
                } else {
                    $this->studiedListValues[$domainName] = StructuresComponent::getPulldownFromSource($domain['source']);
                    if (strpos($domain['source'], 'getCustomDropdown')) {
                        $this->studiedListValues[$domainName] = array_merge($this->studiedListValues[$domainName]['defined'], $this->studiedListValues[$domainName]['previously_defined']);
                    }
                }
            }
        }
        return $this->studiedListValues[$domainName];
    }
    
    public function find($type = 'first', $query = array())
    {
        $this->hasMany = $this->hasManySave;

        if (!isset($query['recursive'])){
            $query['recursive'] = 2;
        }
        return parent::find($type, $query);
    }


}