<?php
 /**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link http://cakephp.org CakePHP(tm) Project
 * @package app.Controller
 * @since CakePHP(tm) v 0.2.9
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    private static $missingTranslations = array();

    private static $me = null;

    private static $acl = null;

    public static $beignFlash = false;

    const CONFIRM = 1;

    const ERROR = 2;

    const WARNING = 3;

    const INFORMATION = 4;

    public $uses = array(
        'Config',
        'SystemVar'
    );

    public $components = array(
        'Acl',
        'Session',
        'SessionAcl',
        'Auth',
        'Menus',
        'RequestHandler',
        'Structures',
        'PermissionManager',
        'Paginator',
        'Flash',
        'DebugKit.Toolbar'
    );

    public $helpers = array(
        'Session',
        'Csv',
        'Html',
        'Js',
        'Shell',
        'Structures',
        'Time',
        'Form'
    );
    
    // use AppController::getCalInfo to get those with translations
    private static $calInfoShort = array(
        1 => 'jan',
        'feb',
        'mar',
        'apr',
        'may',
        'jun',
        'jul',
        'aug',
        'sep',
        'oct',
        'nov',
        'dec'
    );

    private static $calInfoLong = array(
        1 => 'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    );

    public static $query;

    private static $calInfoShortTranslated = false;

    private static $calInfoLongTranslated = false;

    public static $highlightMissingTranslations = true;
    
    // Used as a set from the array keys
    public $allowedFilePrefixes = array();

    private function addUrl($type = "nonAjax")
    {
        if (! empty(Router::getPaths($this->here)->url)) {
            $_SESSION['url'][$type][] = "/" . Router::getPaths($this->here)->url;
        }
    }

    private function decompressData(&$data)
    {
        $data['compressedData'] = str_replace('||SingleQuote||', "'", $data['compressedData']);
        $compressedData = json_decode($data['compressedData'], true);

        if (!empty($compressedData[0]['name']) && $compressedData[0]['name'] == 'compressedData'){
            if (!empty($compressedData[0]['value']) && is_string($compressedData[0]['value'])){
                $compressedData = json_decode($compressedData[0]['value'], true);
            }
        }
        unset($data['compressedData']);
        $index= 0;
        foreach ($compressedData as &$datum){
            if (substr($datum['name'], strlen($datum['name'])-2, strlen($datum['name']))=='[]'){
                $datum['name'] = substr($datum['name'],0, strlen($datum['name'])-2) . "[$index]";
                $index++;
            }else{
                $index= 0;
            }
            $datum['name'] = str_replace(array('[', ']'), array("['", "']"), $datum['name']);
            eval('return $'.$datum['name'].' = $datum["value"];');
        }
    }

    private function decompressBatchSetData(&$data)
    {
        $batchSetData = $data['BatchSetCompressedData'];
        if (!empty(key($batchSetData)) && !empty(key(current($batchSetData))) && !empty(current(current($batchSetData)))){
            $modelKey = key($batchSetData);
            $fieldKey = key(current($batchSetData));
            $values = explode(",", current(current($batchSetData)));
            unset($data['BatchSetCompressedData']);
            foreach ($values as $value){
                eval('return $data["'.$modelKey.'"]["'.$fieldKey.'"][] = "'.$value.'";');
            }
        }
    }

    private function isCSVExportMode()
    {
        $csvExportMode = false;
        if (!empty($this->request->params['plugin']) && !empty($this->request->params['controller']) && !empty($this->request->params['action'])){
            if ($this->request->params['plugin'] == 'Datamart' && $this->request->params['controller'] == 'Browser' && $this->request->params['action'] == 'csv'){
                $csvExportMode = true;
            }elseif ($this->request->params['plugin'] == 'Datamart' && $this->request->params['controller'] == 'Csv' && $this->request->params['action'] == 'csv'){
                $csvExportMode = true;
            }
        }
        return $csvExportMode;
    }

    private function checkData()
    {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            return false;
        }

        if ($this->isCSVExportMode()){
            Configure::write('debug', 0);
            $this->Components->unload('DebugKit.Toolbar');
        }
        if (empty($this->request->data) && !$this->request->is('ajax')){
            if (isset($_SESSION['aliases'])){
                unset($_SESSION['aliases']);
            }
        }
        if (!$this->request->is('ajax')){
            if (isset($_SESSION['notification'])){
                if (!Configure::read('browser_notification')){
                    unset($_SESSION['notification']);
                }else{
                    $_SESSION['notification']['ready'] = "OK";
                }
            }
            $_SESSION['ATiMDebug']['time'] = round(microtime(true) * 10);
            $_SESSION['ajaxURLs'] = array();

            $_SESSION['hooks'] = array();
        }

        if (isset($this->request->data['compressedData'])){
            $this->decompressData($this->request->data);
        }

        if (isset($this->request->data['BatchSetCompressedData'])){
            $this->decompressBatchSetData($this->request->data);
        }

        if (Configure::read('installation_mode') != 0){
            ob_clean();
            echo "<h1>".__('The ATiM is on maintenance for now.')."</h1>";
            die();
        }

    }

    private function checkUrl()
    {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            return false;
        }
        if (empty($_SESSION['url'])) {
            $_SESSION['url'] = array(
                'nonAjax' => array(),
                'ajax' => array(),
                'all' => array()
            );
        }

        if (! $this->request->is('ajax')) {
            $this->addUrl('nonAjax');
        } else {
            $this->addUrl('ajax');
        }
        $this->addUrl('all');
    }

    private function getBackHistoryUrl($type = "nonAjax", $num = 1)
    {
        $url = "/Menus";
        if (isset($_SERVER["HTTP_REFERER"])) {
            $url = $_SERVER["HTTP_REFERER"];
        } else {
            $size = count($_SESSION['url'][$type]);
            if ($size <= $num) {
                $url = "/Menus";
            } else {
                $currentUrl = $_SESSION['url'][$type][$size - 1];
                $url = $_SESSION['url'][$type][$size - $num - 1];
                $index = $size - $num - 1;
                while ($url == $currentUrl && $index > 0) {
                    $index --;
                    $url = $_SESSION['url'][$type][$index];
                }
                if ($index == - 1) {
                    $url = "/Menus";
                }
                array_splice($_SESSION['url'][$type], $index + 1);
            }
        }
        return $url;
    }

    private function checkMultiLogin()
    {
        $multiUserLogin = Configure::read('multi_user_login');

        if (session_status() !== PHP_SESSION_ACTIVE) {
            return false;
        }

        if ($this->request->is('ajax')){
            return false;
        }

        if (!empty($multiUserLogin)){
            return false;
        }

        if ($this->params['action'] == 'logout' && $this->params['controller'] == 'Users'){
            return false;
        }

        $userCookieModel = AppModel::getInstance('', "UserCookie", false);
        $atimSessionID = (!empty($_COOKIE[Configure::read("Session.cookie")])) ? $_COOKIE[Configure::read("Session.cookie")] : null;

        if(empty($atimSessionID)){
            return false;
        }

        $userId = !empty($_SESSION['Auth']['User']['id']) ? $_SESSION['Auth']['User']['id'] : 0;

        if(empty($userId)){
            return false;
        }

        $userCookies = $userCookieModel->find('first', [
            'conditions' => [
                $userCookieModel->name.'.user_id' => $userId,
                $userCookieModel->name.'.session_id' => $atimSessionID,
            ]
        ]);
        if (empty($userCookies)){
            $userCookieModel->save(['UserCookie'=>[
                'user_id' => $userId,
                'session_id' => $atimSessionID,
                'deleted' => '0',

            ]]);
        }elseif(isset($userCookies['UserCookie']['deleted']) && $userCookies['UserCookie']['flag_active'] == '0'){
            $this->atimFlashError(__('there is another login with your username'), '/Users/logout');
        }

        $userCookieModel->updateAll(
            [
                'flag_active' => '0',
            ],[
                $userCookieModel->name.'.user_id' => $userId,
                $userCookieModel->name.'.flag_active' => '1',
                $userCookieModel->name.'.session_id <>' => $atimSessionID,
            ]
        );

    }

    private function initialization()
    {
        $this->checkUrl();
        $this->checkData();
        $this->checkMultiLogin();
    }

    /**
     * This function is executed before every action in the controller.
     * It's a
     * handy place to check for an active session or inspect user permissions.
     */
    public function beforeFilter()
    {
        $this->initialization();
        App::uses('Sanitize', 'Utility');
        AppController::$me = $this;
        if (Configure::read('debug') != 0) {
            Cache::clear(false, "structures");
            Cache::clear(false, "menus");
        }
        
        if ($this->Session->read('permission_timestamp') < $this->SystemVar->getVar('permission_timestamp')) {
            $this->resetPermissions();
        }
        if (Configure::read('Config.language') != $this->Session->read('Config.language')) {
            // set language
            // echo(Configure::read('Config.language'));
            $this->Session->write('Config.language', Configure::read('Config.language'));
        }
        
        $this->Auth->authorize = 'Actions';
        
        // Check password should be reset
        $lowerUrlHere = strtolower($this->request->here);
        if ($this->Session->read('Auth.User.force_password_reset') && strpos($lowerUrlHere, '/users/logout') === false) {
            if (strpos($lowerUrlHere, '/customize/passwords/index') === false) {
                if (! $this->request->is('ajax')) {
                    $this->redirect('/Customize/Passwords/index/');
                }
            }
        }
        
        // record URL in logs
        $logActivityData['UserLog']['user_id'] = $this->Session->read('Auth.User.id');
        $logActivityData['UserLog']['url'] = $this->request->here;
        $logActivityData['UserLog']['visited'] = now();
        $logActivityData['UserLog']['allowed'] = 1;
        
        if (isset($this->UserLog)) {
            $logActivityModel = & $this->UserLog;
        } else {
            App::uses('UserLog', 'Model');
            $logActivityModel = new UserLog();
        }

        if ($logActivityData['UserLog']['user_id']) {
            $logActivityModel->save($logActivityData);
        }


        // record URL in logs file
        
        $logPath = Configure::read('atim_user_log_output_path');
        $debug = Configure::read("debug");
        if (!empty($logPath)) {
            if (is_dir($logPath)){
                $userLogFileHandle = fopen($logPath . '/user_logs.txt', "a");
                if ($userLogFileHandle) {
                    $userLogStrg = '[' . $logActivityData['UserLog']['visited'] . '] ' . '{IP: ' . AppModel::getRemoteIPAddress() . ' || user_id: ' . (strlen($logActivityData['UserLog']['user_id']) ? $logActivityData['UserLog']['user_id'] : 'NULL') . '} ' . $logActivityData['UserLog']['url'] . ' (allowed:' . $logActivityData['UserLog']['allowed'] . ')';
                    fwrite($userLogFileHandle, "$userLogStrg\n");
                    fclose($userLogFileHandle);
                } else {
                    $logDirectory = $logPath;
                    $permission = substr(sprintf('%o', fileperms($logDirectory)), - 4);
                    if ($debug > 0) {
                        if ($permission != '0777') {
                            AppController::addWarningMsg(__('the permission of "log" directory is not correct.'));
                        } else {
                            AppController::addWarningMsg(__("unable to write user log data into 'user_logs.txt' file"));
                        }
                    }
                }
            }else{
                if ($debug > 0) {
                    AppController::addWarningMsg(__('the log directory doesActiveModelExist: %s', $logPath));
                }
            }
        }
        
        
        // menu grabbed for HEADER
        if ($this->request->is('ajax')) {
            Configure::write('debug', 0);
            $this->Components->unload('DebugKit.Toolbar');
        } else {
            $atimSubMenuForHeader = array();
            $menuModel = AppModel::getInstance("", "Menu", true);
            
            $mainMenuItems = $menuModel->find('all', array(
                'conditions' => array(
                    'Menu.parent_id' => 'MAIN_MENU_1'
                )
            ));
            foreach ($mainMenuItems as $item) {
                $atimSubMenuForHeader[$item['Menu']['id']] = $menuModel->find('all', array(
                    'conditions' => array(
                        'Menu.parent_id' => $item['Menu']['id'],
                        'Menu.is_root' => 1
                    ),
                    'order' => array(
                        'Menu.display_order'
                    )
                ));
            }
            
            $this->set('atimMenuForHeader', $this->Menus->get('/menus/tools'));
            $this->set('atimSubMenuForHeader', $atimSubMenuForHeader);
            
            // menu, passed to Layout where it would be rendered through a Helper
            $this->set('atimMenuVariables', array());
            $this->set('atimMenu', $this->Menus->get());
        }
        // get default STRUCTRES, used for forms, views, and validation
        $this->Structures->set();
        
        $this->checkIfDownloadFile();
        
        // Fixe for issue #3396: Msg "You are not authorized to access that location." is not translated
        $this->Auth->authError = __('You are not authorized to access that location.');
    }

    public function checkUserLogRecordNumber()
    {
        $logPath = Configure::read('atim_user_log_output_path');
        if (!empty($logPath)) {
            $userLogModel = AppModel::getInstance('', "UserLog", false);
            $currentYear = date('Y');
            $userLogsNumber = $userLogModel->find('count', array(
                'conditions' => array(
                    'YEAR(visited) < ' => $currentYear - 2
                )
            ));
            if ($userLogsNumber>0){
                $this->addWarningMsg(__('the user logs table overflow warning message'));
            }
        }
    }
    
    private function userLogsCleanupBy6Month()
    {
        // Check the old user_logs (the logs of the previous years)
        //ATTENTION: This function has a redirect if there is the user_logs to save so can not call this function in the middle of the code and should be at the end of the code.
        $memoryLimit = ini_get('memory_limit');
        $memoryLimitByte = convertFromKMG($memoryLimit);
        $totalMemoryBy4 = convertFromKMG(getTotalMemoryCapacity($error)) * 256 * 1024;
        if ($error) {
            self::forceMsgDisplayInPopup();
            self::addWarningMsg(__("the memory allocated to your query is low or undefined."));
        }

        $memoryLimitByte = max($memoryLimitByte, $totalMemoryBy4, 402653184);
        ini_set('memory_limit', (int)($memoryLimitByte/1024/1024).'M');

        $maxExecutionTime = ini_get('max_execution_time');
        set_time_limit(3600);
        $logPath = Configure::read('atim_user_log_output_path');
        $debug = Configure::read("debug");
        if (!empty($logPath)) {
            if (is_dir($logPath)) {

                $lockFile = $logPath . '/log_lock.lock';
                if (!file_exists($lockFile)) {
                    $lockFileVariable = fopen($lockFile, "a");
                    fclose($lockFileVariable);
                    if (file_exists($lockFile)) {
                        $userLogModel = AppModel::getInstance('', "UserLog", false);
                        $userLogs = $userLogModel->find('first', array(
                            'order' => array(
                                'UserLog.id'
                            )
                        ));
                        $firstVisited = (isset($userLogs['UserLog']['visited'])) ? $userLogs['UserLog']['visited'] : false;
                        $firstYear = ($firstVisited) ? date('Y', strtotime($firstVisited)) : false;
                        $currentYear = date('Y');

                        if ($currentYear != $firstYear) {
                            echo "\n<style> \n\tdiv#user-log-progress-bar-div>p:not(:last-child){display: none} \n\tdiv#user-log-progress-bar-div>p{margin: 0px; background-color: blue;} \n\tdiv#user-log-progress-bar-div{border-color: black;border-width: 1px;border-style: solid;} \n</style>";
                            $allWork = ($currentYear - $firstYear) * 12;
                            $pleaseWait = ___("please wait while cleaning up the tables");
                            echo "<h2 id = 'user-log-progress-bar-h2'>$pleaseWait</h2><div id = 'user-log-progress-bar-div'></div>";
                            $progress = 0;
                            for ($year = (int) $firstYear; $year < (int) ($currentYear); $year++) {
                                for ($sixMonth = 0; $sixMonth < 2; $sixMonth++) {
                                    echo
                                    "\n<script> \nvar progressPercent = " . (int) ($progress / $allWork * 100) . "\nvar para = document.createElement(\"p\"); \nvar node = document.createTextNode(progressPercent+' %'); \npara.appendChild(node); \nif (progressPercent == 0){ \n\tpara.style.backgroundColor = 'white'; \n}else{ \n\tpara.style.color = 'white'; \n\tpara.style.width = progressPercent + '%'; \n\tpara.style.backgroundColor = 'blue'; \n} \nvar element = document.getElementById(\"user-log-progress-bar-div\"); \nelement.appendChild(para); \n</script>";
                                    echo str_pad('', 32768);
                                    ob_flush();
                                    flush();
                                    $from = ($sixMonth == 0) ? '01' : '07';
                                    $to = ($sixMonth == 0) ? '07' : '13';
                                    $fileNameTmp = $logPath . '/user_logs_' . $year . '_' . $from . '.tmp';
                                    $fileName = $logPath . '/Do_not_delete_it_user_logs_' . $year . '_' . $from . '.csv';
                                    $userLogs = $userLogModel->find('count', array(
                                        'conditions' => array(
                                            'UserLog.visited >' => $year . '-' . $from,
                                            'UserLog.visited <=' => $year . '-' . $to,
                                        ),
                                        'order' => array(
                                            'UserLog.id'
                                        )
                                    ));
                                    if ($userLogs < 150000) {
                                        $progress += 6;
                                        $outPutContent = array();
                                        $userLogs = $userLogModel->find('all', array(
                                            'conditions' => array(
                                                'UserLog.visited >' => $year . '-' . $from,
                                                'UserLog.visited <=' => $year . '-' . $to,
                                            ),
                                            'order' => array(
                                                'UserLog.id'
                                            )
                                        ));
                                        if (!empty($userLogs)) {
                                            $outPutContent[] = implode(';', array_keys($userLogs[0]['UserLog']));
                                            foreach ($userLogs as &$value) {
                                                $outPutContent[] = implode(';', array_values($value['UserLog']));
                                                unset($value);
                                            }
                                            $outPutContent = implode("\n", $outPutContent);
                                            file_put_contents($fileNameTmp, $outPutContent, FILE_APPEND);
                                        }
                                    } else {
                                        for ($month = (int) $from; $month < (int) ($to); $month++) {
                                            $progress++;
                                            $fromMonthStr = str_pad($month . "", 2, "0", STR_PAD_LEFT);
                                            $toMonthStr = str_pad(($month + 1) . "", 2, "0", STR_PAD_LEFT);
                                            $outPutContent = array();
                                            $userLogs = $userLogModel->find('all', array(
                                                'conditions' => array(
                                                    'UserLog.visited >' => $year . '-' . $fromMonthStr,
                                                    'UserLog.visited <=' => $year . '-' . $toMonthStr,
                                                ),
                                                'order' => array(
                                                    'UserLog.id'
                                                )
                                            ));
                                            if (!empty($userLogs)) {
                                                $outPutContent[] = implode(';', array_keys($userLogs[0]['UserLog']));
                                                foreach ($userLogs as &$value) {
                                                    $outPutContent[] = implode(';', array_values($value['UserLog']));
                                                    unset($value);
                                                }
                                                $outPutContent = implode("\n", $outPutContent);
                                                file_put_contents($fileNameTmp, $outPutContent, FILE_APPEND);
                                            }
                                        }
                                    }

                                    if (file_exists($fileNameTmp) && filesize($fileNameTmp) > 0) {
                                        $y = ($to == "13") ? $year + 1 : $year;
                                        $to = ($to == "13") ? "01" : $to;
                                        $userLogModel->deleteAll(array(
                                            'UserLog.visited >' => $year . '-' . $from . '-01',
                                            'UserLog.visited <=' => $y . '-' . $to . '-01',
                                                ), false);
                                        rename($fileNameTmp, $fileName);
                                    } elseif (file_exists($fileNameTmp)) {
                                        unlink($fileNameTmp);
                                    }
                                }
                            }
                            echo
                            "<script>\n var para = document.createElement(\"p\");\n var node = document.createTextNode(\"100 %\");\n para.appendChild(node);\n var element = document.getElementById(\"user-log-progress-bar-div\");\n element.appendChild(para);\n </script>";

                            $userLogModel->query('OPTIMIZE TABLE user_logs');

                            echo "<script>document.getElementById('user-log-progress-bar-div').remove();</script>";
                            echo "<script>document.getElementById('user-log-progress-bar-h2').remove();</script>";
                            echo "<script>location.reload();</script>";
                        }

                        unlink($lockFile);
                    } else {
                        if (Configure . read('debug') > 0) {
                            $permission = substr(sprintf('%o', fileperms($logPath)), - 4);
                            if ($permission != '0777') {
                                AppController::addWarningMsg(__('the permission of "log" directory is not correct.'));
                            } else {
                                AppController::addWarningMsg(__("unable to create the backup file for users log"));
                            }
                        }
                    }
                }
            } else {
                if ($debug > 0) {
                    AppController::addWarningMsg(__('the log directory doesActiveModelExist: %s', $logPath));
                }
            }
        }
        set_time_limit($maxExecutionTime);
        ini_set('memory_limit', $memoryLimit);
    }

    private function checkIfDownloadFile()
    {
        if (isset($this->request->query['file']) && $this->Auth->isAuthorized()) {
            $plugin = $this->request->params['plugin'];
            $modelName = Inflector::camelize(Inflector::singularize($this->request->params['controller']));
            $fileName = $this->request->query['file'];
            $shortFileName = $this->request->query['filename'];
            if (! $this->Session->read('flag_show_confidential')) {
                preg_match('/(' . $modelName . ')\.(.+)\.([0-9]+)\.(.+)/', $fileName, $matches, PREG_OFFSET_CAPTURE);
                if (! empty($matches)) {
                    if ($matches[1][0] == $modelName) {
                        $model = AppModel::getInstance($plugin, $modelName, true);
                        $fields = $model->schema();
                        if (isset($fields[$matches[2][0]])) {
                            $this->atimFlashError(__('You are not authorized to access that location.'), '/Menus');
                        }
                    }
                }
            }
            $file = Configure::read('uploadDirectory') . DS . $fileName;
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($shortFileName) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                readfile($file);
            } else {
                $this->atimFlashError(__('file doesActiveModelExist'), '');
            }
        }
    }

    /**
     * AppController constructor.
     *
     * @param null $request
     * @param null $response
     */
    public function __construct($request = null, $response = null)
    {
        $className = get_class($this);
        $this->name = substr($className, 0, strlen(get_class($this)) - (strpos($className, 'ControllerCustom') === false ? 10 : 16));
        parent::__construct($request, $response);
    }

    /**
     *
     * @param string $hookExtension
     * @return bool|string
     */
    public function hook($hookExtension = '')
    {
        if ($hookExtension) {
            $hookExtension = '_' . $hookExtension;
        }

        $hookFile = APP . ($this->request->params['plugin'] ? 'Plugin' . DS . $this->request->params['plugin'] . DS : '') . 'Controller' . DS . 'Hook' . DS . $this->request->params['controller'] . '_' . $this->request->params['action'] . $hookExtension . '.php';
        $hook = $hookFile;
        if (! file_exists($hookFile)) {
            $hookFile = false;
        }
        $_SESSION['hooks'][] = array($hook, !empty($hookFile));

        return $hookFile;
    }

    /**
     *
     * @return CakeResponse|null
     */
    private function handleFileRequest()
    {
        $file = $this->request->query['file'];
        
        $redirectInvalidFile = function ($caseType) use(&$file) {
            CakeLog::error("User tried to download invalid file (" . $caseType . "): " . $file);
            if ($caseType === 3) {
                AppController::getInstance()->redirect("/Pages/err_file_not_auth?p[]=" . $file);
            } else {
                AppController::getInstance()->redirect("/Pages/err_file_not_found?p[]=" . $file);
            }
        };
        
        $index = - 1;
        foreach (range(0, 1) as $_) {
            $index = strpos($file, '.', $index + 1);
        }
        $prefix = substr($file, 0, $index);
        if ($prefix && array_key_exists($prefix, $this->allowedFilePrefixes)) {
            $dir = Configure::read('uploadDirectory');
            // NOTE: Cannot use flash for errors because file is still in the
            // url and that would cause an infinite loop
            if (strpos($file, '/') > - 1 || strpos($file, '\\') > - 1) {
                $redirectInvalidFile(1);
            }
            $fullFile = $dir . '/' . $file;
            if (! file_exists($fullFile)) {
                $redirectInvalidFile(2);
            }
            $index = strpos($file, '.', $index + 1) + 1;
            $this->response->file($fullFile, array(
                'name' => substr($file, $index)
            ));
            return $this->response;
        }
        $redirectInvalidFile(3);
    }

    /**
     * Called after controller action logic, but before the view is rendered.
     * This callback is not used often, but may be needed if you are calling
     * render() manually before the end of a given action.
     */
    public function beforeRender()
    {
        if (isset($this->request->query['file'])) {
            return $this->handleFileRequest();
        }
        // Fix an issue where cakephp 2.0 puts the first loaded model with the key model in the registry.
        // Causes issues on validation messages
        ClassRegistry::removeObject('model');
        
        if (isset($this->passedArgs['batchsetVar'])) {
            // batchset handling
            $data = null;
            if (isset($this->viewVars[$this->passedArgs['batchsetVar']])) {
                $data = $this->viewVars[$this->passedArgs['batchsetVar']];
            }
            if (empty($data)) {
                unset($this->passedArgs['batchsetVar']);
                $this->atimFlashWarning(__('there is no data to add to a temporary batchset'), 'javascript:history.back()');
                return false;
            }
            if (isset($this->passedArgs['batchsetCtrl'])) {
                $data = $data[$this->passedArgs['batchsetCtrl']];
            }
            $this->requestAction('/Datamart/BatchSets/add/0', array(
                '_data' => $data
            ));
        }
        
        if ($this->layout == 'ajax') {
            $_SESSION['query']['previous'][] = $this->getQueryLogs('default');
        }
    }

    public function afterFilter()
    {
        // global $startTime;
        // echo("Exec time (sec): ".(AppController::microtimeFloat() - $startTime));
        if (sizeof(AppController::$missingTranslations) > 0) {
            App::uses('MissingTranslation', 'Model');
            $mt = new MissingTranslation();
            foreach (AppController::$missingTranslations as $missingTranslation) {
                $mt->set(array(
                    "MissingTranslation" => array(
                        "id" => $missingTranslation
                    )
                ));
                $mt->save(); // ignore errors, kind of insert ingnore
            }
        }
    }

    /**
     * Simple function to replicate PHP 5 behaviour
     */
    public static function microtimeFloat()
    {
        list ($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }

    /**
     *
     * @param $word
     */
    public static function missingTranslation(&$word)
    {
        $source = isset(AppController::getStackTrace()[3])?AppController::getStackTrace()[3]:'Cake';
        $source = (strpos($source, ROOT.DS."lib".DS."Cake".DS) !==0)?'ATiM':'Cake';

        $addUntranslated = true;
        if ($source == 'ATiM'){
            $i18nModel = new Model(array(
                'table' => 'i18n',
                'name' => 0
            ));
            $translation = $i18nModel->find("first", array("conditions" => array('id' => $word)));
            if (!empty($translation)){
                $lang = Configure::read('Config.language') == 'eng' ? "en" : "fr";
                if (!empty($translation[0][$lang])){
                    $word = $translation[0][$lang];
                    $addUntranslated = false;
                }
            }
        }
        if ($addUntranslated && ! is_numeric($word) && strpos($word, "<span class='untranslated'>") === false) {
            AppController::$missingTranslations[] = $word;
            if (Configure::read('debug') == 2 && self::$highlightMissingTranslations) {
                $word = "<span class='untranslated'>" . $word . "</span>";
            }
        }
    }

    /**
     *
     * @param array|string $url
     * @param null $status
     * @param bool $exit
     * @return \Cake\Network\Response|null|void
     */
    public function redirect($url, $status = null, $exit = true)
    {
        $_SESSION['query']['previous'][] = $this->getQueryLogs('default');
        parent::redirect($url, $status, $exit);
    }

    /**
     *
     * @param $message
     * @param $url
     * @param int $type
     */
    public function atimFlash($message, $url, $type = self::CONFIRM)
    {
        if (empty($url)) {
            $url = "/Menus";
        }
        if (strpos(strtolower($url), 'javascript') !== false) {
            $url = $this->getBackHistoryUrl();
        }
        if (strpos(strtolower($url), 'javascript') === false) {
            if ($type == self::CONFIRM) {
                $_SESSION['ctrapp_core']['confirm_msg'] = $message;
                $title = ___('confirm');
                $icon = 'img/icons/small/confirm.png';
            } elseif ($type == self::INFORMATION) {
                $_SESSION['ctrapp_core']['info_msg'][] = $message;
                $title = ___('information');
                $icon = 'img/icons/small/information.png';
            } elseif ($type == self::WARNING) {
                $_SESSION['ctrapp_core']['warning_trace_msg'][] = $message;
                $title = ___('warning');
                $icon = 'img/icons/small/warning.png';
            } elseif ($type == self::ERROR) {
                $_SESSION['ctrapp_core']['error_msg'][] = $message;
                $title = ___('error');
                $icon = 'img/icons/small/delete.png';
            }
            $_SESSION['notification'] = array(
                'type' => $type,
                'message' => $message,
                'title' =>$title,
                'icon' =>$icon
            );

            $this->redirect($url);
        } elseif (false) {
            // TODO:: Check if can use javascript functions for blue screen message
            echo '<script type="text/javascript">', 'javascript:history.back();', 'window.location.reload();', '</script>';
        } else {
            $this->autoRender = false;
            $this->set('url', Router::url($url));
            $this->set('message', $message);
            $this->set('pageTitle', $message);
            $this->render(false, "flash");
        }
    }

    /**
     *
     * @param $message
     * @param $url
     */
    public function atimFlashError($message, $url)
    {
        $this->atimFlash($message, $url, self::ERROR);
    }

    /**
     *
     * @param $message
     * @param $url
     */
    public function atimFlashInfo($message, $url)
    {
        $this->atimFlash($message, $url, self::INFORMATION);
    }

    /**
     *
     * @param $message
     * @param $url
     */
    public function atimFlashConfirm($message, $url)
    {
        $this->atimFlash($message, $url, self::CONFIRM);
    }

    /**
     *
     * @param $message
     * @param $url
     */
    public function atimFlashWarning($message, $url)
    {
        $this->atimFlash($message, $url, self::WARNING);
    }

    /**
     *
     * @return null
     */
    public static function getInstance()
    {
        return AppController::$me;
    }

    public static function init()
    {
        Configure::write('Config.language', 'eng');
        Configure::write('Acl.classname', 'AtimAcl');
        Configure::write('Acl.database', 'default');
        
        // ATiM2 configuration variables from Datatable
        
        define('VALID_INTEGER', '/^[-+]?[\\s]?[0-9]+[\\s]?$/');
        define('VALID_INTEGER_POSITIVE', '/^[+]?[\\s]?[0-9]+[\\s]?$/');
        define('VALID_FLOAT', '/^[-+]?[\\s]?[0-9]{0,%s}[,\\.]?[0-9]+[\\s]?$/');
        define('VALID_FLOAT_POSITIVE', '/^[+]?[\\s]?[0-9]{0,%s}[,\\.]?[0-9]+[\\s]?$/');
        define('VALID_24TIME', '/^([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/');
        
        // ripped from validation.php date + time
        define('VALID_DATETIME_YMD', '%^(?:(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(-)(?:0?2\\1(?:29)))|(?:(?:(?:1[6-9]|2\\d)\\d{2})(-)(?:(?:(?:0?[13578]|1[02])\\2(?:31))|(?:(?:0?(1|[3-9])|1[0-2])\\2(29|30))|(?:(?:0?[1-9])|(?:1[0-2]))\\2(?:0?[1-9]|1\\d|2[0-8])))\s([0-1][0-9]|2[0-3])\:[0-5][0-9]\:[0-5][0-9]$%');
        
        // parse URI manually to get passed PARAMS
        global $startTime;
        $startTime = AppController::microtimeFloat();
        
        $requestUriParams = array();
        
        // Fix REQUEST_URI on IIS
        if (! isset($_SERVER['REQUEST_URI'])) {
            $_SERVER['REQUEST_URI'] = substr($_SERVER['PHP_SELF'], 1);
            if (isset($_SERVER['QUERY_STRING'])) {
                $_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
            }
        }
        $requestUri = $_SERVER['REQUEST_URI'];
        $requestUri = explode('/', $requestUri);
        $requestUri = array_filter($requestUri);
        
        foreach ($requestUri as $uri) {
            $explodedUri = explode(':', $uri);
            if (count($explodedUri) > 1) {
                $requestUriParams[$explodedUri[0]] = $explodedUri[1];
            }
        }
        
        // import APP code required...
        App::import('Model', 'Config');
        $configDataModel = new Config();
        
        // get CONFIG data from table and SET
        $configResults = false;
        
        App::uses('CakeSession', 'Model/Datasource');
        $userId = CakeSession::read('Auth.User.id');
        $loggedInUser = CakeSession::read('Auth.User.id');
        $loggedInGroup = CakeSession::read('Auth.User.group_id');
        $configResults = $configDataModel->getConfig(CakeSession::read('Auth.User.group_id'), CakeSession::read('Auth.User.id'));
        // parse result, set configs/defines
        if ($configResults) {
            
            Configure::write('Config.language', $configResults['Config']['config_language']);
            foreach ($configResults['Config'] as $configKey => $configData) {
                if (strpos($configKey, '_') !== false) {
                    
                    // break apart CONFIG key
                    $configKey = explode('_', $configKey);
                    $configFormat = array_shift($configKey);
                    $configKey = implode('_', $configKey);
                    
                    // if a DEFINE or CONFIG, set new setting for APP
                    if ($configFormat == 'define') {
                        
                        // override DATATABLE value with URI PARAM value
                        if ($configKey == 'PAGINATION_AMOUNT' && isset($requestUriParams['per'])) {
                            $configData = $requestUriParams['per'];
                        }
                        
                        define(strtoupper($configKey), $configData);
                    } elseif ($configFormat == 'config') {
                        Configure::write($configKey, $configData);
                    }
                }
            }
        }
        
        define('CONFIDENTIAL_MARKER', __('confidential data')); // Moved here to allow translation
        
        if (Configure::read('debug') == 0) {
            set_error_handler("myErrorHandler");
        }
    }

    /**
     *
     * @param boolean $short Wheter to return short or long month names
     * @return array associative array containing the translated months names so that key = month_number and value = month_name
     */
    public static function getCalInfo($short = true)
    {
        if ($short) {
            if (! AppController::$calInfoShortTranslated) {
                AppController::$calInfoShortTranslated = true;
                AppController::$calInfoShort = array_map(create_function('$a', 'return __($a);'), AppController::$calInfoShort);
            }
            return AppController::$calInfoShort;
        } else {
            if (! AppController::$calInfoLongTranslated) {
                AppController::$calInfoLongTranslated = true;
                AppController::$calInfoLong = array_map(create_function('$a', 'return __($a);'), AppController::$calInfoLong);
            }
            return AppController::$calInfoLong;
        }
    }

    /**
     *
     * @param int $year
     * @param mixed int | string $month
     * @param int $day
     * @param boolean $nbspSpaces True if white spaces must be printed as &nbsp;
     * @param boolean $shortMonths True if months names should be short (used if $month is an int)
     * @return string The formated datestring with user preferences
     */
    public static function getFormatedDateString($year, $month, $day, $nbspSpaces = true, $shortMonths = true)
    {
        $result = null;
        if (empty($year) && empty($month) && empty($day)) {
            $result = "";
        } else {
            $divider = $nbspSpaces ? "&nbsp;" : " ";
            if (is_numeric($month)) {
                $monthStr = AppController::getCalInfo($shortMonths);
                $month = $month > 0 && $month < 13 ? $monthStr[(int) $month] : "-";
            }
            if (DATE_FORMAT == 'MDY') {
                $result = $month . (empty($month) ? "" : $divider) . $day . (empty($day) ? "" : $divider) . $year;
            } elseif (DATE_FORMAT == 'YMD') {
                $result = $year . (empty($month) ? "" : $divider) . $month . (empty($day) ? "" : $divider) . $day;
            } else { // default of DATE_FORMAT=='DMY'
                $result = $day . (empty($day) ? "" : $divider) . $month . (empty($month) ? "" : $divider) . $year;
            }
        }
        return $result;
    }

    /**
     *
     * @param $hour
     * @param $minutes
     * @param bool $nbspSpaces
     * @return string
     */
    public static function getFormatedTimeString($hour, $minutes, $nbspSpaces = true)
    {
        if (TIME_FORMAT == 12) {
            $meridiem = $hour >= 12 ? "PM" : "AM";
            $hour %= 12;
            if ($hour == 0) {
                $hour = 12;
            }
            return $hour . (empty($minutes) ? '' : ":" . $minutes . ($nbspSpaces ? "&nbsp;" : " ")) . $meridiem;
        } elseif (empty($minutes)) {
            return $hour . __('hour_sign');
        } else {
            return $hour . ":" . $minutes;
        }
    }

    /**
     *
     * Enter description here ...
     *
     * @param String $datetimeString with format yyyy[-MM[-dd[ hh[:mm:ss]]]] (missing parts represent the accuracy
     * @param boolean $nbspSpaces True if white spaces must be printed as &nbsp;
     * @param boolean $shortMonths True if months names should be short (used if $month is an int)
     * @return string The formated datestring with user preferences
     */
    public static function getFormatedDatetimeString($datetimeString, $nbspSpaces = true, $shortMonths = true)
    {
        $month = null;
        $day = null;
        $hour = null;
        $minutes = null;
        if (strpos($datetimeString, ' ') === false) {
            $date = $datetimeString;
        } else {
            list ($date, $time) = explode(" ", $datetimeString);
            if (strpos($time, ":") === false) {
                $hour = $time;
            } else {
                list ($hour, $minutes) = explode(":", $time);
            }
        }
        
        $date = explode("-", $date);
        $year = $date[0];
        switch (count($date)) {
            case 3:
                $day = $date[2];
            case 2:
                $month = $date[1];
                break;
        }
        $formatedDate = self::getFormatedDateString($year, $month, $day, $nbspSpaces);
        return $hour === null ? $formatedDate : $formatedDate . ($nbspSpaces ? "&nbsp;" : " ") . self::getFormatedTimeString($hour, $minutes, $nbspSpaces);
    }

    /**
     * Return formatted date in SQL format from a date array returned by an application form.
     *
     * @param Array $datetimeArray gathering date data into following structure:
     *        array('month' => string, '
     *        'day' => string,
     *        'year' => string,
     *        'hour' => string,
     *        'min' => string)
     * @param Specify|string $dateType Specify
     *        the type of date ('normal', 'start', 'end')
     *        - normal => Will force function to build a date witout specific rules.
     *        - start => Will force function to build date as a 'start date' of date range defintion
     *        (ex1: when just year is specified, will return a value like year-01-01 00:00)
     *        (ex2: when array is empty, will return a value like -9999-99-99 00:00)
     *        - end => Will force function to build date as an 'end date' of date range defintion
     *        (ex1: when just year is specified, will return a value like year-12-31 23:59)
     *        (ex2: when array is empty, will return a value like 9999-99-99 23:59)
     * @return string The formated SQL date having following format yyyy-MM-dd hh:mn
     */
    public static function getFormatedDatetimeSQL($datetimeArray, $dateType = 'normal')
    {
        $formattedDate = '';
        switch ($dateType) {
            case 'normal':
                if ((! empty($datetimeArray['year'])) && (! empty($datetimeArray['month'])) && (! empty($datetimeArray['day']))) {
                    $formattedDate = $datetimeArray['year'] . '-' . $datetimeArray['month'] . '-' . $datetimeArray['day'];
                }
                if ((! empty($formattedDate)) && (! empty($datetimeArray['hour']))) {
                    $formattedDate .= ' ' . $datetimeArray['hour'] . ':' . (empty($datetimeArray['min']) ? '00' : $datetimeArray['min']);
                }
                break;
            
            case 'start':
                if (empty($datetimeArray['year'])) {
                    $formattedDate = '-9999-99-99 00:00';
                } else {
                    $formattedDate = $datetimeArray['year'];
                    if (empty($datetimeArray['month'])) {
                        $formattedDate .= '-01-01 00:00';
                    } else {
                        $formattedDate .= '-' . $datetimeArray['month'];
                        if (empty($datetimeArray['day'])) {
                            $formattedDate .= '-01 00:00';
                        } else {
                            $formattedDate .= '-' . $datetimeArray['day'];
                            if (empty($datetimeArray['hour'])) {
                                $formattedDate .= ' 00:00';
                            } else {
                                $formattedDate .= ' ' . $datetimeArray['hour'] . ':' . (empty($datetimeArray['min']) ? '00' : $datetimeArray['min']);
                            }
                        }
                    }
                }
                break;
            
            case 'end':
                if (empty($datetimeArray['year'])) {
                    $formattedDate = '9999-12-31 23:59';
                } else {
                    $formattedDate = $datetimeArray['year'];
                    if (empty($datetimeArray['month'])) {
                        $formattedDate .= '-12-31 23:59';
                    } else {
                        $formattedDate .= '-' . $datetimeArray['month'];
                        if (empty($datetimeArray['day'])) {
                            $formattedDate .= '-31 23:59';
                        } else {
                            $formattedDate .= '-' . $datetimeArray['day'];
                            if (empty($datetimeArray['hour'])) {
                                $formattedDate .= ' 23:59';
                            } else {
                                $formattedDate .= ' ' . $datetimeArray['hour'] . ':' . (empty($datetimeArray['min']) ? '59' : $datetimeArray['min']);
                            }
                        }
                    }
                }
                break;
            
            default:
        }
        
        return $formattedDate;
    }

    /**
     * Clones the first level of an array
     *
     * @param array $arr The array to clone
     * @return array
     */
    public static function cloneArray(array $arr)
    {
        $result = array();
        foreach ($arr as $k => $v) {
            if (is_array($v)) {
                $result[$k] = self::cloneArray($v);
            } else {
                $result[$k] = $v;
            }
        }
        return $result;
    }

    /**
     *
     * @param $msg
     * @param bool $withTrace
     */
    public static function addWarningMsg($msg, $withTrace = false)
    {
        if ($withTrace) {
            $_SESSION['ctrapp_core']['warning_trace_msg'][] = array(
                'msg' => $msg,
                'trace' => self::getStackTrace()
            );
        } else {
            if (isset($_SESSION['ctrapp_core']['warning_no_trace_msg'][$msg])) {
                $_SESSION['ctrapp_core']['warning_no_trace_msg'][$msg] ++;
            } else {
                $_SESSION['ctrapp_core']['warning_no_trace_msg'][$msg] = 1;
            }
        }
    }

    /**
     *
     * @param $msg
     */
    public static function addInfoMsg($msg)
    {
        if (isset($_SESSION['ctrapp_core']['info_msg'][$msg])) {
            $_SESSION['ctrapp_core']['info_msg'][$msg] ++;
        } else {
            $_SESSION['ctrapp_core']['info_msg'][$msg] = 1;
        }
    }
    
    /**
     *
     * @param $msg
     */
    public static function addErrorMsg($msg)
    {
        $_SESSION['ctrapp_core']['error_msg'][] = $msg;
    }

    /**
     */
    public static function forceMsgDisplayInPopup()
    {
        $_SESSION['ctrapp_core']['force_msg_display_in_popup'] = true;
    }

    /**
     *
     * @return array
     */
    public static function getStackTrace()
    {
        $bt = debug_backtrace();
        $result = array();
        foreach ($bt as $unit) {
            $result[] = (isset($unit['file']) ? $unit['file'] : '?') . ", " . $unit['function'] . " at line " . (isset($unit['line']) ? $unit['line'] : '?');
        }
        return $result;
    }

    /**
     * Builds the value definition array for an updateAll call
     *
     * @param array $data
     * @return array
     * @internal param $ array They data array to build the values with* array They data array to build the values with
     */
    public static function getUpdateAllValues(array $data)
    {
        $result = array();
        foreach ($data as $model => $fields) {
            foreach ($fields as $name => $value) {
                if (is_array($value)) {
                    if (strlen($value['year'])) {
                        $result[$model . "." . $name] = "'" . AppController::getFormatedDatetimeSQL($value) . "'";
                    }
                } elseif (strlen($value)) {
                    $result[$model . "." . $name] = "'" . $value . "'";
                }
            }
        }
        return $result;
    }

    /**
     * Encrypt a text
     *
     * @param String $string
     * @return String
     */
    public static function encrypt($string)
    {
        return Security::hash($string, null, true);
    }

    /**
     * cookie manipulation to counter cake problems.
     * see eventum #1032
     *
     * @param $skipExpirationCookie
     */
    public static function atimSetCookie($skipExpirationCookie)
    {
        if (! $skipExpirationCookie) {
            $sessionExpiration = time() + Configure::read("Session.timeout");
            setcookie('last_request', time(), $sessionExpiration, '/');
            setcookie('session_expiration', $sessionExpiration, $sessionExpiration, '/');
            if (isset($_COOKIE[Configure::read("Session.cookie")])) {
                setcookie(Configure::read("Session.cookie"), $_COOKIE[Configure::read("Session.cookie")], $sessionExpiration, "/");
            }
            $sessionId = (! empty($_SESSION['Auth']['User']['id'])) ? self::encrypt($_SESSION['Auth']['User']['id']) : self::encrypt("nul string");
            setcookie('sessionId', $sessionId, 0, "/");
        }
    }

    /**
     * Global function to initialize a batch action.
     * Redirect/flashes on error.
     *
     * @param AppModel $model The model to work on
     * @param string $dataModelName The model name used in $this->request->data
     * @param string $dataKey The data key name used in $this->request->data
     * @param string $controlKeyName The name of the control field used in the model table
     * @param AppModel $possibilitiesModel The model to fetch the possibilities from
     * @param string $possibilitiesParentKey The possibilities parent key to base the search on
     * @param $noPossibilitiesMsg
     * @return array array with the ids and the possibilities
     */
    public function batchInit($model, $dataModelName, $dataKey, $controlKeyName, $possibilitiesModel, $possibilitiesParentKey, $noPossibilitiesMsg)
    {
        if (empty($this->request->data)) {
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        } elseif (! is_array($this->request->data[$dataModelName][$dataKey]) && strpos($this->request->data[$dataModelName][$dataKey], ',')) {
            return array(
                'error' => "batch init - number of submitted records too big"
            );
        }
        // extract valid ids
        $ids = $model->find('all', array(
            'conditions' => array(
                $model->name . '.id' => $this->request->data[$dataModelName][$dataKey]
            ),
            'fields' => array(
                $model->name . '.id'
            ),
            'recursive' => - 1
        ));
        if (empty($ids)) {
            return array(
                'error' => "batch init no data"
            );
        }
        $model->sortForDisplay($ids, $this->request->data[$dataModelName][$dataKey]);
        $ids = self::defineArrayKey($ids, $model->name, 'id');
        $ids = array_keys($ids);
        
        $controls = $model->find('all', array(
            'conditions' => array(
                $model->name . '.id' => $ids
            ),
            'fields' => array(
                $model->name . '.' . $controlKeyName
            ),
            'group' => array(
                $model->name . '.' . $controlKeyName
            ),
            'recursive' => - 1
        ));
        if (count($controls) != 1) {
            return array(
                'error' => "you must select elements with a common type"
            );
        }
        
        $possibilities = $possibilitiesModel->find('all', array(
            'conditions' => array(
                $possibilitiesParentKey => $controls[0][$model->name][$controlKeyName],
                'flag_active' => '1'
            )
        ));
        
        if (empty($possibilities)) {
            return array(
                'error' => $noPossibilitiesMsg
            );
        }
        
        return array(
            'ids' => implode(',', $ids),
            'possibilities' => $possibilities,
            'control_id' => $controls[0][$model->name][$controlKeyName]
        );
    }

    /**
     * Replaces the array key (generally of a find) with an inner value
     *
     * @param array $inArray
     * @param string $model The model ($inArray[$model])
     * @param string $field The field (new key = $inArray[$model][$field])
     * @param bool $unique If true, the array block will be directly under the model.field, not in an array.
     * @return array
     */
    public static function defineArrayKey($inArray, $model, $field, $unique = false)
    {
        $outArray = array();
        if ($unique) {
            foreach ($inArray as $val) {
                $outArray[$val[$model][$field]] = $val;
            }
        } else {
            foreach ($inArray as $val) {
                if (isset($val[$model])) {
                    $outArray[$val[$model][$field]][] = $val;
                } else {
                    // the key cannot be foud
                    $outArray[- 1][] = $val;
                }
            }
        }
        return $outArray;
    }

    /**
     * Recursively removes entries returning true on empty($value).
     *
     * @param array $data
     * @internal param $ array &$data* array &$data
     */
    public static function removeEmptyValues(array &$data)
    {
        foreach ($data as $key => &$val) {
            if (is_array($val)) {
                self::removeEmptyValues($val);
            }
            if (empty($val)) {
                unset($data[$key]);
            }
        }
    }

    /**
     *
     * @return mixed
     */
    public static function getNewSearchId()
    {
        return AppController::getInstance()->Session->write('search_id', AppController::getInstance()->Session->read('search_id') + 1);
    }

    /**
     *
     * @param string $link The link to check
     * @return True if the user can access that page, false otherwise
     */
    public static function checkLinkPermission($link)
    {
        if (strpos($link, 'javascript:') === 0 || strpos($link, '#') === 0) {
            return true;
        }
        
        $parts = Router::parse($link);
        if (empty($parts)) {
            return false;
        }
        $acoAlias = 'Controller/' . ($parts['plugin'] ? Inflector::camelize($parts['plugin']) . '/' : '');
        $acoAlias .= ($parts['controller'] ? Inflector::camelize($parts['controller']) . '/' : '');
        $acoAlias .= ($parts['action'] ? $parts['action'] : '');
        $instance = AppController::getInstance();
        $acoAlias = $instance->PermissionManager->getSubstitutedUrl($acoAlias);
        return strpos($acoAlias, 'Controller/Users') !== false || strpos($acoAlias, 'Controller/Pages') !== false || $acoAlias == "Controller/Menus/index" || $instance->SessionAcl->check('Group::' . $instance->Session->read('Auth.User.group_id'), $acoAlias);
    }

    /**
     *
     * @param $inArray
     * @param $model
     * @param $field
     */
    public static function applyTranslation(&$inArray, $model, $field)
    {
        foreach ($inArray as &$part) {
            $part[$model][$field] = __($part[$model][$field]);
        }
    }

    /**
     * Handles automatic pagination of model records Adding
     * the necessary bind on the model to fetch detail level, if there is a unique ctrl id
     *
     * @param Model|string $object Model to paginate (e.g: model instance, or 'Model', or 'Model.InnerModel')
     * @param string|array $scope Conditions to use while paginating
     * @param array $whitelist List of allowed options for paging
     * @return array Model query results
     */
    public function paginate($object = null, $scope = array(), $whitelist = array())
    {
        $this->setControlerPaginatorSettings($object);
        $modelName = isset($object->baseModel) ? $object->baseModel : $object->name;
        if (isset($object->Behaviors->MasterDetail->__settings[$modelName])) {
            extract(self::convertArrayKeyFromSnakeToCamel($object->Behaviors->MasterDetail->__settings[$modelName]));
            if ($isMasterModel && isset($scope[$modelName . '.' . $controlForeign]) && preg_match('/^[0-9]+$/', $scope[$modelName . '.' . $controlForeign])) {
                self::buildDetailBinding($object, array(
                    $modelName . '.' . $controlForeign => $scope[$modelName . '.' . $controlForeign]
                ), $emptyStructureAlias);
            }
        }
        return parent::paginate($object, $scope, $whitelist);
    }

    /**
     * Finds and paginate search results.
     * Stores search in cache.
     * Handles detail level when there is a unique ctrl_id.
     * Defines/updates the result structure.
     * Sets 'result_are_unique_ctrl' as true if the results are based on a unique ctrl id,
     * false otherwise. (Non master/detail models will return false)
     *
     * @param int $searchId The search id used by the pagination
     * @param Object $model The model to search upon
     * @param string $structureAlias The structure alias to parse the search conditions on
     * @param string $url The base url to use in the pagination links (meaning without the search_id)
     * @param bool $ignoreDetail If true, even if the model is a master_detail ,the detail level won't be loaded
     * @param mixed $limit If false, will make a paginate call, if an int greater than 0, will make a find with the limit
     */
    public function searchHandler($searchId, $model, $structureAlias, $url, $ignoreDetail = false, $limit = false)
    {
        // setting structure
        $structure = $this->Structures->get('form', $structureAlias);
        $this->set('atimStructure', $structure);
        if (empty($searchId)) {
            $plugin = $this->request->params['plugin'];
            $controller = $this->request->params['controller'];
            $action = $this->request->params['action'];
            if (isset($_SESSION['post_data'][$plugin][$controller][$action])) {
                convertArrayToJavaScript($_SESSION['post_data'][$plugin][$controller][$action], 'jsPostData');
            }
            
            $this->Structures->set('empty', 'emptyStructure');
        } else {
            if ($this->request->data) {
                
                // newly submitted search, parse conditions and store in session
                $_SESSION['ctrapp_core']['search'][$searchId]['criteria'] = $this->Structures->parseSearchConditions($structure);
            } elseif (! isset($_SESSION['ctrapp_core']['search'][$searchId]['criteria'])) {
                self::addWarningMsg(__('you cannot resume a search that was made in a previous session'));
                $this->redirect('/Menus');
                exit();
            }
            
            // check if the current model is a master/detail one or a similar view
            if (! $ignoreDetail) {
                self::buildDetailBinding($model, $_SESSION['ctrapp_core']['search'][$searchId]['criteria'], $structureAlias);
                $this->Structures->set($structureAlias);
            }
            
            if ($limit) {
                $this->request->data = $model->find('all', array(
                    'conditions' => $_SESSION['ctrapp_core']['search'][$searchId]['criteria'],
                    'limit' => $limit
                ));
            } else {
                $this->setControlerPaginatorSettings($model);
                $this->request->data = $this->Paginator->paginate($model, $_SESSION['ctrapp_core']['search'][$searchId]['criteria']);
            }
            
            // if SEARCH form data, save number of RESULTS and URL (used by the form builder pagination links)
            if ($searchId == - 1) {
                // don't use the last search button if search id = -1
                unset($_SESSION['ctrapp_core']['search'][$searchId]);
            } else {
                $_SESSION['ctrapp_core']['search'][$searchId]['results'] = $this->request->params['paging'][$model->name]['count'];
                $_SESSION['ctrapp_core']['search'][$searchId]['url'] = $url;
            }
        }
        
        if ($this->request->is('ajax')) {
            Configure::write('debug', 0);
            $this->set('isAjax', true);
        }
    }

    /**
     * Set the Pagination settings based on user preferences and controller Pagination settings.
     *
     * @param Object $model The model to search upon
     */
    public function setControlerPaginatorSettings($model)
    {
        if (PAGINATION_AMOUNT) {
            $this->Paginator->settings = array_merge($this->Paginator->settings, array(
                'limit' => PAGINATION_AMOUNT
            ));
        }
        if ($model && isset($this->paginate[$model->name])) {
            $this->Paginator->settings = array_merge($this->Paginator->settings, $this->paginate[$model->name]);
        }
    }

    /**
     * Adds the necessary bind on the model to fetch detail level, if there is a unique ctrl id
     *
     * @param AppModel &$model
     * @param array $conditions Search conditions
     * @param string &$structureAlias
     */
    public static function buildDetailBinding(&$model, array $conditions, &$structureAlias)
    {
        $controller = AppController::getInstance();
        $masterClassName = isset($model->baseModel) ? $model->baseModel : $model->name;
        if (! isset($model->Behaviors->MasterDetail->__settings[$masterClassName])) {
            $controller->$masterClassName; // try to force lazyload
            if (! isset($model->Behaviors->MasterDetail->__settings[$masterClassName])) {
                if (Configure::read('debug') != 0) {
                    AppController::addWarningMsg("buildDetailBinding requires you to force instanciation of model " . $masterClassName);
                }
                return;
            }
        }
        if ($model->Behaviors->MasterDetail->__settings[$masterClassName]['is_master_model']) {
            $ctrlIds = null;
            $singleCtrlId = $model->getSingleControlIdCondition(array(
                'conditions' => $conditions
            ));
            $controlField = $model->Behaviors->MasterDetail->__settings[$masterClassName]['control_foreign'];
            if ($singleCtrlId === false) {
                // determine if the results contain only one control id
                $ctrlIds = $model->find('all', array(
                    'fields' => array(
                        $model->name . '.' . $controlField
                    ),
                    'conditions' => $conditions,
                    'group' => array(
                        $model->name . '.' . $controlField
                    ),
                    'limit' => 2
                ));
                if (count($ctrlIds) == 1) {
                    $singleCtrlId = current(current($ctrlIds[0]));
                }
            }
            if ($singleCtrlId !== false) {
                // only one ctrl, attach detail
                $hasOne = array();
                extract(self::convertArrayKeyFromSnakeToCamel($model->Behaviors->MasterDetail->__settings[$masterClassName]));
                $ctrlModel = isset($controller->$controlClass) ? $controller->$controlClass : AppModel::getInstance('', $controlClass, false);
                if (! $ctrlModel) {
                    if (Configure::read('debug') != 0) {
                        AppController::addWarningMsg('buildDetailBinding requires you to force instanciation of model ' . $controlClass);
                    }
                    return;
                }
                $ctrlData = $ctrlModel->findById($singleCtrlId);
                $ctrlData = current($ctrlData);
                // put a new instance of the detail model in the cache
                ClassRegistry::removeObject($detailClass); // flush the old detail from cache, we'll need to reinstance it
                assert(strlen($ctrlData['detail_tablename'])) or die("detail_tablename cannot be empty");
                new AppModel(array(
                    'table' => $ctrlData['detail_tablename'],
                    'name' => $detailClass,
                    'alias' => $detailClass
                ));
                
                // has one and win
                $hasOne[$detailClass] = array(
                    'className' => $detailClass,
                    'foreignKey' => $masterForeign
                );
                
                if ($masterClassName == 'SampleMaster') {
                    // join specimen/derivative details
                    if ($ctrlData['sample_category'] == 'specimen') {
                        $hasOne['SpecimenDetail'] = array(
                            'className' => 'SpecimenDetail',
                            'foreignKey' => 'sample_master_id'
                        );
                    } else {
                        // derivative
                        $hasOne['DerivativeDetail'] = array(
                            'className' => 'DerivativeDetail',
                            'foreignKey' => 'sample_master_id'
                        );
                    }
                }
                
                // persistent bind
                $model->bindModel(array(
                    'hasOne' => $hasOne,
                    'belongsTo' => array(
                        $controlClass => array(
                            'className' => $controlClass
                        )
                    )
                ), false);
                isset($model->{$detailClass}); // triggers model lazy loading (See cakephp Model class)
                                               
                // updating structure
                if (($pos = strpos($ctrlData['form_alias'], ',')) !== false) {
                    $structureAlias = $structureAlias . ',' . substr($ctrlData['form_alias'], $pos + 1);
                }
                
                ClassRegistry::removeObject($detailClass); // flush the new model to make sure the default one is loaded if needed
            } elseif (count($ctrlIds) > 0) {
                // more than one
                AppController::addInfoMsg(__("the results contain various data types, so the details are not displayed"));
            }
        }
    }

    /**
     * Builds menu options based on 1-display_order and 2-translation
     *
     * @param array $menuOptions An array containing arrays of the form array('order' => #, 'label' => '', 'link' => '')
     *        The label must be translated already.
     */
    public static function buildBottomMenuOptions(array &$menuOptions)
    {
        $tmp = array();
        foreach ($menuOptions as $menuOption) {
            $tmp[sprintf("%05d", $menuOption['order']) . '-' . $menuOption['label']] = $menuOption['link'];
        }
        ksort($tmp);
        $menuOptions = array();
        foreach ($tmp as $label => $link) {
            $menuOptions[preg_replace('/^[0-9]+-/', '', $label)] = $link;
        }
    }

    /**
     * Sets url_to_cancel based on $this->request->data['url_to_cancel']
     * If nothing exists, javascript:history.go(-1) is used.
     * If a similar entry exists, the value is decremented.
     * Otherwise, url_to_cancel is uses as such.
     * 
     * @deprecated : Use getCancelLink() to get cancel url and manage urlToCancel into your controller function. See ParticipantMessage.add() as an example.
     */
    public function setUrlToCancel()
    {
        if (isset($this->request->data['url_to_cancel'])) {
            $pattern = '/^javascript:history.go\((-?[0-9]*)\)$/';
            $matches = array();
            if (preg_match($pattern, $this->request->data['url_to_cancel'], $matches)) {
                $back = empty($matches[1]) ? - 2 : $matches[1] - 1;
                $this->request->data['url_to_cancel'] = 'javascript:history.go(' . $back . ')';
            }
        } else {
            $this->request->data['url_to_cancel'] = 'javascript:history.go(-1)';
        }
        
        $this->set('urlToCancel', $this->request->data['url_to_cancel']);
    }

    public function resetPermissions()
    {
        if ($this->Auth->user()) {
            $userModel = AppModel::getInstance('', 'User', true);
            $user = $userModel->findById($this->Session->read('Auth.User.id'));
            $this->Session->write('Auth.User.group_id', $user['User']['group_id']);
            $this->Session->write('flag_show_confidential', $user['Group']['flag_show_confidential']);
            $this->Session->write('permission_timestamp', time());
            $this->SessionAcl->flushCache();
        }
    }

    /**
     *
     * @param array $list
     * @param $lModel
     * @param $lKey
     * @param array $data
     * @param $dModel
     * @param $dKey
     * @return bool
     */
    public function setForRadiolist(array &$list, $lModel, $lKey, array $data, $dModel, $dKey)
    {
        foreach ($list as &$unit) {
            if ($data[$dModel][$dKey] == $unit[$lModel][$lKey]) {
                // we found the one that interests us
                $unit[$dModel] = $data[$dModel];
                return true;
            }
        }
        return false;
    }

    /**
     * Builds a cancel link based on the passed data.
     * Works for data send by batch sets and browsing.
     *
     * @param strint or null $data
     * @return null|string
     */
    public static function getCancelLink($data)
    {
        $result = null;
        if (isset($data['node']['id'])) {
            $result = '/Datamart/Browser/browse/' . $data['node']['id'];
        } elseif (isset($data['BatchSet']['id'])) {
            $result = '/Datamart/BatchSets/listall/' . $data['BatchSet']['id'];
        } elseif (isset($data['cancel_link'])) {
            $result = $data['cancel_link'];
        } elseif (isset($data['url_to_cancel'])) {
            $result = $data['url_to_cancel'];
        }
        return $result;
    }

    /**
     * Does multiple tasks related to having a version updated
     * -Permissions
     * -i18n version field
     * -language files
     * -cache
     * -Delete all browserIndex > Limit
     * -databrowser lft rght
     */
    public function newVersionSetup()
    {
        $updatePermission = 1;

        // new version installed!

        // ------------------------------------------------------------------------------------------------------------------------------------------

        // *** 1 *** regen permissions

        $this->PermissionManager->buildAcl();
        AppController::addWarningMsg(__('permissions have been regenerated'));

        // *** 1.5 *** Check the upload, temp and local directory permission

        $uploadDirectory = Configure::read('uploadDirectory');
        $permission = substr(sprintf('%o', fileperms($uploadDirectory)), - 4);
        if ($permission != '0777') {
            AppController::addWarningMsg(__('the permission of "upload" directory is not correct.'));
        }

        // *** 2 *** update the i18n string for version

        $storageControlModel = AppModel::getInstance('StorageLayout', 'StorageControl', true);
        $isTmaBlock = $storageControlModel->find('count', array(
            'condition' => array(
                'StorageControl.flag_active' => '1',
                'is_tma_block' => '1'
            )
        ));
        $this->Version->query("REPLACE INTO i18n (id,en,fr) (SELECT 'storage layout management - value generated by newVersionSetup function', en, fr FROM i18n WHERE id = '" . ($isTmaBlock ? 'storage layout & tma blocks management' : 'storage layout management') . "')");
        $this->Version->query("REPLACE INTO i18n (id,en,fr) (SELECT 'storage layout management description - value generated by newVersionSetup function', en, fr FROM i18n WHERE id = '" . ($isTmaBlock ? 'storage layout & tma blocks management description' : 'storage layout management description') . "')");
        $this->Version->query("REPLACE INTO i18n (id,en,fr) (SELECT 'storage (non tma block) - value generated by newVersionSetup function', en, fr FROM i18n WHERE id = '" . ($isTmaBlock ? 'storage (non tma block)' : 'storage') . "')");

        $i18nModel = new Model(array(
            'table' => 'i18n',
            'name' => 0
        ));
        $versionNumber = $this->Version->data['Version']['version_number'];
        $i18nModel->save(array(
            'id' => 'core_app_version',
            'en' => $versionNumber,
            'fr' => $versionNumber
        ));

        // *** 3 ***rebuild language files

        $filee = fopen("../../app/Locale/eng/LC_MESSAGES/default.po", "w+t") or die("Failed to open english file");
        $filef = fopen("../../app/Locale/fra/LC_MESSAGES/default.po", "w+t") or die("Failed to open french file");
        $i18n = $i18nModel->find('all');
        foreach ($i18n as &$i18nLine) {
            // Takes information returned by query and creates variable for each field
            $id = $i18nLine[0]['id'];
            $en = $i18nLine[0]['en'];
            $fr = $i18nLine[0]['fr'];
            if (strlen($en) > 1014) {
                $error = "msgid\t\"$id\"\nen\t\"$en\"\n";
                $en = substr($en, 0, 1014);
            }

            if (strlen($fr) > 1014) {
                if (strlen($error) > 2) {
                    $error = "$error\\nmsgstr\t\"$fr\"\n";
                } else {
                    $error = "msgid\t\"$id\"\nmsgstr=\"$fr\"\n";
                }
                $fr = substr($fr, 0, 1014);
            }
            $english = "msgid\t\"$id\"\nmsgstr\t\"$en\"\n";
            $french = "msgid\t\"$id\"\nmsgstr\t\"$fr\"\n";

            // Writes output to file
            fwrite($filee, $english);
            fwrite($filef, $french);
        }
        fclose($filee);
        fclose($filef);
        AppController::addWarningMsg(__('language files have been rebuilt'));

        // *** 4 *** rebuilts lft rght in datamart_browsing_result if needed + delete all temporary browsing index if > $tmpBrowsingLimit. Since v2.5.0.

        $browsingIndexModel = AppModel::getInstance('Datamart', 'BrowsingIndex', true);
        $browsingResultModel = AppModel::getInstance('Datamart', 'BrowsingResult', true);
        $rootNodeIdsToKeep = array();
        $userRootNodeCounter = 0;
        $lastUserId = null;
        $forceRebuildLeftRght = false;
        $tmpBrowsing = $browsingIndexModel->find('all', array(
            'conditions' => array(
                'BrowsingIndex.temporary' => true
            ),
            'order' => array(
                'BrowsingResult.user_id, BrowsingResult.created DESC'
            )
        ));
        foreach ($tmpBrowsing as $newBrowsingIndex) {
            if ($lastUserId != $newBrowsingIndex['BrowsingResult']['user_id'] || $userRootNodeCounter < $browsingIndexModel->tmpBrowsingLimit) {
                if ($lastUserId != $newBrowsingIndex['BrowsingResult']['user_id'])
                    $userRootNodeCounter = 0;
                $lastUserId = $newBrowsingIndex['BrowsingResult']['user_id'];
                $userRootNodeCounter ++;
                $rootNodeIdsToKeep[$newBrowsingIndex['BrowsingIndex']['root_node_id']] = $newBrowsingIndex['BrowsingIndex']['root_node_id'];
            } else {
                // Some browsing index will be deleted
                $forceRebuildLeftRght = true;
            }
        }
        $resultIdsToKeep = $rootNodeIdsToKeep;
        $newParentIds = $rootNodeIdsToKeep;
        $loopCounter = 0;
        while (! empty($newParentIds)) {
            // Just in case
            $loopCounter ++;
            if ($loopCounter > 100)
                $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            $newParentIds = $browsingResultModel->find('list', array(
                'conditions' => array(
                    "BrowsingResult.parent_id" => $newParentIds
                ),
                'fields' => array(
                    'BrowsingResult.id'
                )
            ));
            $resultIdsToKeep = array_merge($resultIdsToKeep, $newParentIds);
        }
        if (! empty($resultIdsToKeep)) {
            $browsingIndexModel->deleteAll("BrowsingIndex.root_node_id NOT IN (" . implode(',', $rootNodeIdsToKeep) . ")");
            $browsingResultModel->deleteAll("BrowsingResult.id NOT IN (" . implode(',', $resultIdsToKeep) . ")");
        }
        $result = $browsingResultModel->find('first', array(
            'conditions' => array(
                'NOT' => array(
                    'BrowsingResult.parent_id' => null
                ),
                'BrowsingResult.lft' => null
            )
        ));
        if ($result || $forceRebuildLeftRght) {
            self::addWarningMsg(__('rebuilt lft rght for datamart_browsing_results'));
            $browsingResultModel->recover('parent');
        }

        // *** 5 *** rebuild views

        $viewModels = array(
            AppModel::getInstance('InventoryManagement', 'ViewCollection'),
            AppModel::getInstance('InventoryManagement', 'ViewSample'),
            AppModel::getInstance('InventoryManagement', 'ViewAliquot'),
            AppModel::getInstance('StorageLayout', 'ViewStorageMaster'),
            AppModel::getInstance('InventoryManagement', 'ViewAliquotUse')
        );

        foreach ($viewModels as $viewModel) {
            $this->Version->query('DROP TABLE IF EXISTS ' . $viewModel->table);
            $this->Version->query('DROP VIEW IF EXISTS ' . $viewModel->table);
            if (isset($viewModel::$tableCreateQuery)) {
                // Must be done with multiple queries
                $this->Version->query($viewModel::$tableCreateQuery);
                $queries = explode("UNION ALL", $viewModel::$tableQuery);
                foreach ($queries as $query) {
                    $this->Version->query('INSERT INTO ' . $viewModel->table . '(' . str_replace('%%WHERE%%', '', $query) . ')');
                }
            } else {
                $this->Version->query('CREATE TABLE ' . $viewModel->table . '(' . str_replace('%%WHERE%%', '', $viewModel::$tableQuery) . ')');
                if ($viewModel->table == 'view_aliquots') {
                    $this->Version->query('ALTER TABLE `' . $viewModel->table . '` ADD INDEX `view_aliquot_barcode_index` (`barcode`)');
                    $this->Version->query('ALTER TABLE `' . $viewModel->table . '` ADD INDEX `view_aliquot_aliquot_label_index` (`aliquot_label`)');
                    $this->Version->query('ALTER TABLE `' . $viewModel->table . '` ADD INDEX `view_aliquot_acquisition_label_index` (`acquisition_label`)');
                }
                if ($viewModel->table == 'view_samples') {
                    $this->Version->query('ALTER TABLE `' . $viewModel->table . '` ADD INDEX `view_samples_sample_code_index` (`sample_code`)');
                }
                if ($viewModel->table == 'view_collections') {
                    $this->Version->query('ALTER TABLE `' . $viewModel->table . '` ADD INDEX `view_collections_acquisition_label_index` (`acquisition_label`)');
                }
            }
            $desc = $this->Version->query('DESC ' . $viewModel->table);
            $fields = array();
            $field = array_shift($desc);
            $pkey = $field['COLUMNS']['Field'];
            foreach ($desc as $field) {
                if ($field['COLUMNS']['Type'] != 'text') {
                    $fields[] = $field['COLUMNS']['Field'];
                }
            }
            $this->Version->query('ALTER TABLE ' . $viewModel->table . ' ADD PRIMARY KEY(' . $pkey . '), ADD KEY (' . implode('), ADD KEY (', $fields) . ')');
        }

        AppController::addWarningMsg(__('views have been rebuilt'));

        // *** 6 *** Current Volume clean up

        $viewAliquotModel = AppModel::getInstance("InventoryManagement", "ViewAliquot", false); // To fix bug on table created on the fly (http://stackoverflow.com/questions/8167038/cakephp-pagination-using-temporary-table)
        $tmpAliquotModelCacheSources = $viewAliquotModel->cacheSources;
        $viewAliquotModel->cacheSources = false;
        $viewAliquotModel->schema();
        $aliquotMasterModel = AppModel::getInstance("InventoryManagement", "AliquotMaster", true);
        $aliquotMasterModel->checkWritableFields = false;
        AppModel::acquireBatchViewsUpdateLock();
        // Current Volume
        $currentVolumesUpdated = array();
        // Search all aliquots having initial_volume but no current_volume
        $tmpSql = "SELECT am.id AS aliquot_master_id, am.barcode, am.aliquot_label, am.initial_volume, am.current_volume
			FROM aliquot_masters am INNER JOIN aliquot_controls ac ON ac.id = am.aliquot_control_id
			WHERE am.deleted != 1 AND ac.volume_unit IS NOT NULL AND am.initial_volume IS NOT NULL AND am.current_volume IS NULL;";
        $aliquotsToCleanUp = $aliquotMasterModel->query($tmpSql);
        foreach ($aliquotsToCleanUp as $newAliquot) {
            $aliquotMasterModel->data = array(); // *** To guaranty no merge will be done with previous AliquotMaster data ***
            $aliquotMasterModel->id = $newAliquot['am']['aliquot_master_id'];
            if (! $aliquotMasterModel->save(array(
                'AliquotMaster' => array(
                    'id' => $newAliquot['am']['aliquot_master_id'],
                    'current_volume' => $newAliquot['am']['initial_volume']
                )
            ), false))
                $this->redirect('/Pages/err_plugin_record_err?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            $currentVolumesUpdated[$newAliquot['am']['aliquot_master_id']] = $newAliquot['am']['barcode'];
        }
        // Search all aliquots having current_volume but no initial_volume
        $tmpSql = "SELECT am.id AS aliquot_master_id, am.barcode, am.aliquot_label, am.initial_volume, am.current_volume
			FROM aliquot_masters am INNER JOIN aliquot_controls ac ON ac.id = am.aliquot_control_id
			WHERE am.deleted != 1 AND ac.volume_unit IS NOT NULL AND am.initial_volume IS NULL AND am.current_volume IS NOT NULL;";
        $aliquotsToCleanUp = $aliquotMasterModel->query($tmpSql);
        foreach ($aliquotsToCleanUp as $newAliquot) {
            $aliquotMasterModel->data = array(); // *** To guaranty no merge will be done with previous AliquotMaster data ***
            $aliquotMasterModel->id = $newAliquot['am']['aliquot_master_id'];
            if (! $aliquotMasterModel->save(array(
                'AliquotMaster' => array(
                    'id' => $newAliquot['am']['aliquot_master_id'],
                    'current_volume' => ''
                )
            ), false))
                $this->redirect('/Pages/err_plugin_record_err?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            $currentVolumesUpdated[$newAliquot['am']['aliquot_master_id']] = $newAliquot['am']['barcode'];
        }
        // Search all aliquots having current_volume > 0 but a sum of used_volume (from view_aliquot_uses) > initial_volume
        $tmpSql = "SELECT am.id AS aliquot_master_id, am.barcode, am.aliquot_label, am.initial_volume, am.current_volume, us.sum_used_volumes FROM aliquot_masters am INNER JOIN aliquot_controls ac ON ac.id = am.aliquot_control_id INNER JOIN (SELECT aliquot_master_id, SUM(used_volume) AS sum_used_volumes FROM view_aliquot_uses WHERE used_volume IS NOT NULL GROUP BY aliquot_master_id) AS us ON us.aliquot_master_id = am.id WHERE am.deleted != 1 AND ac.volume_unit IS NOT NULL AND am.initial_volume < us.sum_used_volumes AND am.current_volume != 0;";
        $aliquotsToCleanUp = $aliquotMasterModel->query($tmpSql);
        foreach ($aliquotsToCleanUp as $newAliquot) {
            $aliquotMasterModel->data = array(); // *** To guaranty no merge will be done with previous AliquotMaster data ***
            $aliquotMasterModel->id = $newAliquot['am']['aliquot_master_id'];
            if (! $aliquotMasterModel->save(array(
                'AliquotMaster' => array(
                    'id' => $newAliquot['am']['aliquot_master_id'],
                    'current_volume' => '0'
                )
            ), false))
                $this->redirect('/Pages/err_plugin_record_err?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            $currentVolumesUpdated[$newAliquot['am']['aliquot_master_id']] = $newAliquot['am']['barcode'];
        }
        // Search all aliquots having current_volume != initial volume - used_volume (from view_aliquot_uses) > initial_volume
        $tmpSql = "SELECT am.id AS aliquot_master_id, am.barcode, am.aliquot_label, am.initial_volume, am.current_volume, us.sum_used_volumes FROM aliquot_masters am INNER JOIN aliquot_controls ac ON ac.id = am.aliquot_control_id INNER JOIN (SELECT aliquot_master_id, SUM(used_volume) AS sum_used_volumes FROM view_aliquot_uses WHERE used_volume IS NOT NULL GROUP BY aliquot_master_id) AS us ON us.aliquot_master_id = am.id WHERE am.deleted != 1 AND ac.volume_unit IS NOT NULL AND am.initial_volume >= us.sum_used_volumes AND am.current_volume != (am.initial_volume - us.sum_used_volumes);";
        $aliquotsToCleanUp = $aliquotMasterModel->query($tmpSql);
        foreach ($aliquotsToCleanUp as $newAliquot) {
            $aliquotMasterModel->data = array(); // *** To guaranty no merge will be done with previous AliquotMaster data ***
            $aliquotMasterModel->id = $newAliquot['am']['aliquot_master_id'];
            if (! $aliquotMasterModel->save(array(
                'AliquotMaster' => array(
                    'id' => $newAliquot['am']['aliquot_master_id'],
                    'current_volume' => ($newAliquot['am']['initial_volume'] - $newAliquot['us']['sum_used_volumes'])
                )
            ), false))
                $this->redirect('/Pages/err_plugin_record_err?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            $currentVolumesUpdated[$newAliquot['am']['aliquot_master_id']] = $newAliquot['am']['barcode'];
        }
        if ($currentVolumesUpdated) {
            AppController::addWarningMsg(__('aliquot current volume has been corrected for following aliquots : ') . (implode(', ', $currentVolumesUpdated)));
        }
        // -C-Used Volume
        $usedVolumeUpdated = array();
        // Search all aliquot internal use having used volume not null but no volume unit
        $tmpSql = "SELECT AliquotInternalUse.id AS aliquot_internal_use_id,
			AliquotMaster.id AS aliquot_master_id,
			AliquotMaster.barcode AS barcode,
			AliquotInternalUse.used_volume AS used_volume,
			AliquotControl.volume_unit
			FROM aliquot_internal_uses AS AliquotInternalUse
			JOIN aliquot_masters AS AliquotMaster ON AliquotMaster.id = AliquotInternalUse.aliquot_master_id
			JOIN aliquot_controls AS AliquotControl ON AliquotMaster.aliquot_control_id = AliquotControl.id
			WHERE AliquotInternalUse.deleted <> 1 AND AliquotControl.volume_unit IS NULL AND AliquotInternalUse.used_volume IS NOT NULL;";
        $aliquotsToCleanUp = $aliquotMasterModel->query($tmpSql);
        if ($aliquotsToCleanUp) {
            $aliquotInternalUseModel = AppModel::getInstance("InventoryManagement", "AliquotInternalUse", true);
            $aliquotInternalUseModel->checkWritableFields = false;
            foreach ($aliquotsToCleanUp as $newAliquot) {
                $aliquotInternalUseModel->data = array(); // *** To guaranty no merge will be done with previous AliquotMaster data ***
                $aliquotInternalUseModel->id = $newAliquot['AliquotInternalUse']['aliquot_internal_use_id'];
                if (! $aliquotInternalUseModel->save(array(
                    'AliquotInternalUse' => array(
                        'id' => $newAliquot['AliquotInternalUse']['aliquot_internal_use_id'],
                        'used_volume' => ''
                    )
                ), false))
                    $this->redirect('/Pages/err_plugin_record_err?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                $usedVolumeUpdated[$newAliquot['AliquotMaster']['aliquot_master_id']] = $newAliquot['AliquotMaster']['barcode'];
            }
        }
        // Search all aliquot used as source aliquot, used volume not null but no volume unit
        $tmpSql = "SELECT SourceAliquot.id AS source_aliquot_id,
			AliquotMaster.id AS aliquot_master_id,
			AliquotMaster.barcode AS barcode,
			SourceAliquot.used_volume AS used_volume,
			AliquotControl.volume_unit AS aliquot_volume_unit
			FROM source_aliquots AS SourceAliquot
			JOIN aliquot_masters AS AliquotMaster ON AliquotMaster.id = SourceAliquot.aliquot_master_id
			JOIN aliquot_controls AS AliquotControl ON AliquotMaster.aliquot_control_id = AliquotControl.id
			WHERE SourceAliquot.deleted <> 1 AND AliquotControl.volume_unit IS NULL AND SourceAliquot.used_volume IS NOT NULL;";
        $aliquotsToCleanUp = $aliquotMasterModel->query($tmpSql);
        if ($aliquotsToCleanUp) {
            $sourceAliquotModel = AppModel::getInstance("InventoryManagement", "SourceAliquot", true);
            $sourceAliquotModel->checkWritableFields = false;
            foreach ($aliquotsToCleanUp as $newAliquot) {
                $sourceAliquotModel->data = array(); // *** To guaranty no merge will be done with previous AliquotMaster data ***
                $sourceAliquotModel->id = $newAliquot['SourceAliquot']['source_aliquot_id'];
                if (! $sourceAliquotModel->save(array(
                    'SourceAliquot' => array(
                        'id' => $newAliquot['SourceAliquot']['source_aliquot_id'],
                        'used_volume' => ''
                    )
                ), false))
                    $this->redirect('/Pages/err_plugin_record_err?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                $usedVolumeUpdated[$newAliquot['AliquotMaster']['aliquot_master_id']] = $newAliquot['AliquotMaster']['barcode'];
            }
        }
        // Search all aliquot used as parent aliquot, used volume not null but no volume unit
        $tmpSql = "SELECT Realiquoting.id AS realiquoting_id,
			AliquotMaster.id AS aliquot_master_id,
			AliquotMaster.barcode AS barcode,
			Realiquoting.parent_used_volume AS used_volume,
			AliquotControl.volume_unit AS aliquot_volume_unit
			FROM realiquotings AS Realiquoting
			JOIN aliquot_masters AS AliquotMaster ON AliquotMaster.id = Realiquoting.parent_aliquot_master_id
			JOIN aliquot_controls AS AliquotControl ON AliquotMaster.aliquot_control_id = AliquotControl.id
			WHERE Realiquoting.deleted <> 1 AND AliquotControl.volume_unit IS NULL AND Realiquoting.parent_used_volume IS NOT NULL;";
        $aliquotsToCleanUp = $aliquotMasterModel->query($tmpSql);
        if ($aliquotsToCleanUp) {
            $realiquotingModel = AppModel::getInstance("InventoryManagement", "Realiquoting", true);
            $realiquotingModel->checkWritableFields = false;
            foreach ($aliquotsToCleanUp as $newAliquot) {
                $realiquotingModel->data = array(); // *** To guaranty no merge will be done with previous AliquotMaster data ***
                $realiquotingModel->id = $newAliquot['Realiquoting']['realiquoting_id'];
                if (! $realiquotingModel->save(array(
                    'Realiquoting' => array(
                        'id' => $newAliquot['Realiquoting']['realiquoting_id'],
                        'parent_used_volume' => ''
                    )
                ), false))
                    $this->redirect('/Pages/err_plugin_record_err?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                $usedVolumeUpdated[$newAliquot['AliquotMaster']['aliquot_master_id']] = $newAliquot['AliquotMaster']['barcode'];
            }
        }
        // Search all aliquot used for quality conbtrol, used volume not null but no volume unit
        $tmpSql = "SELECT QualityCtrl.id AS quality_control_id,
			AliquotMaster.id AS aliquot_master_id,
			AliquotMaster.barcode AS barcode,
			QualityCtrl.used_volume AS used_volume,
			AliquotControl.volume_unit AS aliquot_volume_unit
			FROM quality_ctrls AS QualityCtrl
			JOIN aliquot_masters AS AliquotMaster ON AliquotMaster.id = QualityCtrl.aliquot_master_id
			JOIN aliquot_controls AS AliquotControl ON AliquotMaster.aliquot_control_id = AliquotControl.id
			WHERE QualityCtrl.deleted <> 1 AND AliquotControl.volume_unit IS NULL AND QualityCtrl.used_volume IS NOT NULL;";
        $aliquotsToCleanUp = $aliquotMasterModel->query($tmpSql);
        if ($aliquotsToCleanUp) {
            $qualityCtrlModel = AppModel::getInstance("InventoryManagement", "QualityCtrl", true);
            $qualityCtrlModel->checkWritableFields = false;
            foreach ($aliquotsToCleanUp as $newAliquot) {
                $qualityCtrlModel->data = array(); // *** To guaranty no merge will be done with previous AliquotMaster data ***
                $qualityCtrlModel->id = $newAliquot['QualityCtrl']['quality_control_id'];
                if (! $qualityCtrlModel->save(array(
                    'QualityCtrl' => array(
                        'id' => $newAliquot['QualityCtrl']['quality_control_id'],
                        'used_volume' => ''
                    )
                ), false))
                    $this->redirect('/Pages/err_plugin_record_err?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                $usedVolumeUpdated[$newAliquot['AliquotMaster']['aliquot_master_id']] = $newAliquot['AliquotMaster']['barcode'];
            }
        }
        if ($usedVolumeUpdated) {
            $viewAliquotUseModel = AppModel::getInstance('InventoryManagement', 'ViewAliquotUse');
            foreach (explode("UNION ALL", $viewAliquotUseModel::$tableQuery) as $query) {
                $viewAliquotUseModel->query('REPLACE INTO ' . $viewAliquotUseModel->table . '(' . str_replace('%%WHERE%%', 'AND AliquotMaster.id IN (' . implode(',', array_keys($usedVolumeUpdated)) . ')', $query) . ')');
            }
            AppController::addWarningMsg(__('aliquot used volume has been removed for following aliquots : ') . (implode(', ', $usedVolumeUpdated)));
        }
        $viewAliquotModel->cacheSources = $tmpAliquotModelCacheSources;
        $viewAliquotModel->schema();

        // *** 7 *** clear cache

        Cache::clear(false);
        Cache::clear(false, 'structures');
        Cache::clear(false, 'menus');
        Cache::clear(false, 'browser');
        Cache::clear(false, 'models');
        Cache::clear(false, '_cake_core_');
        Cache::clear(false, '_cake_model_');
        AppController::addWarningMsg(__('cache has been cleared'));

        // *** 8 *** Clean up parent to sample control + aliquot control

        $studiedSampleControlId = array();
        $activeSampleControlIds = array();
        $this->ParentToDerivativeSampleControl = AppModel::getInstance("InventoryManagement", "ParentToDerivativeSampleControl", true);

        $conditions = array(
            'ParentToDerivativeSampleControl.parent_sample_control_id' => null,
            'ParentToDerivativeSampleControl.flag_active' => true
        );
        while ($activeParentSampleTypes = $this->ParentToDerivativeSampleControl->find('all', array(
            'conditions' => $conditions
        ))) {
            foreach ($activeParentSampleTypes as $newParentSampleType) {
                $activeSampleControlIds[] = $newParentSampleType['DerivativeControl']['id'];
                $studiedSampleControlId[] = $newParentSampleType['DerivativeControl']['id'];
            }
            $conditions = array(
                'ParentToDerivativeSampleControl.parent_sample_control_id' => $activeSampleControlIds,
                'ParentToDerivativeSampleControl.flag_active' => true,
                'not' => array(
                    'ParentToDerivativeSampleControl.derivative_sample_control_id' => $studiedSampleControlId
                )
            );
        }
        $this->Version->query('UPDATE parent_to_derivative_sample_controls SET flag_active = false WHERE parent_sample_control_id IS NOT NULL AND parent_sample_control_id NOT IN (' . implode(',', $activeSampleControlIds) . ')');
        $this->Version->query('UPDATE aliquot_controls SET flag_active = false WHERE sample_control_id NOT IN (' . implode(',', $activeSampleControlIds) . ')');

        // *** 9 *** Clean up structure_permissible_values_custom_controls counters values

        $structurePermissibleValuesCustomControl = AppModel::getInstance('', 'StructurePermissibleValuesCustomControl');
        $hasManyDetails = array(
            'hasMany' => array(
                'StructurePermissibleValuesCustom' => array(
                    'className' => 'StructurePermissibleValuesCustom',
                    'foreignKey' => 'control_id'
                )
            )
        );
        $structurePermissibleValuesCustomControl->bindModel($hasManyDetails);
        $allCusomListsControls = $structurePermissibleValuesCustomControl->find('all');
        foreach ($allCusomListsControls as $newCustomList) {
            $valuesUsedAsInputCounter = 0;
            $valuesCounter = 0;
            foreach ($newCustomList['StructurePermissibleValuesCustom'] as $newCustomValue) {
                if (! $newCustomValue['deleted']) {
                    $valuesCounter ++;
                    if ($newCustomValue['use_as_input'])
                        $valuesUsedAsInputCounter ++;
                }
            }
            $structurePermissibleValuesCustomControl->tryCatchQuery("UPDATE structure_permissible_values_custom_controls SET values_counter = $valuesCounter, values_used_as_input_counter = $valuesUsedAsInputCounter WHERE id = " . $newCustomList['StructurePermissibleValuesCustomControl']['id']);
        }

        // *** 10 *** rebuilts lft rght in storage_masters

        $storageMasterModel = AppModel::getInstance('StorageLayout', 'StorageMaster', true);
        $result = $storageMasterModel->find('first', array(
            'conditions' => array(
                'NOT' => array(
                    'StorageMaster.parent_id' => null
                ),
                'StorageMaster.lft' => null
            )
        ));
        if ($result) {
            self::addWarningMsg(__('rebuilt lft rght for storage_masters'));
            $storageMasterModel->recover('parent');
        }

        // *** 11 *** Disable unused treatment_extend_controls

        $this->Version->query("UPDATE treatment_extend_controls SET flag_active = 0 WHERE id NOT IN (select distinct treatment_extend_control_id from treatment_controls WHERE flag_active = 1 AND treatment_extend_control_id IS NOT NULL)");

        // *** 12 *** Check storage controls data

        $storageCtrlModel = AppModel::getInstance('Administrate', 'StorageCtrl', true);
        $storageCtrlModel->validatesAllStorageControls();

        // *** 13 *** Update users_form_for_admin structure based on ldap authentication core variable
        
        $ldap = Configure::read('if_use_ldap_authentication');
        $flag = '1';
        if (! empty($ldap)) {
            $flag = '0';    
        }
        $this->Version->query("update structure_formats SET `flag_edit`='$flag', `flag_add`='$flag' WHERE structure_id=(SELECT id FROM structures WHERE alias='users_form_for_admin') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='Administrate' AND `model`='User' AND `tablename`='users' AND `field`='force_password_reset')");
        
        // *** 14 *** Automatic ATiM Customisation
            
        if (Configure::read('letNewVersionSetupCustomisePartiallyATiM')) {
            
            AppController::addWarningMsg(__('letNewVersionSetupCustomisePartiallyATiM_active_message'));
            
            $automaticCustomisationMessages = array();
            $browsingControlModel = AppModel::getInstance('Datamart', 'BrowsingControl', true);

            // -------------------------------------------------------------------------------------------------------------
            // AUTOMATIC CUSTOMISATION: model - ClinicalAnnotation.MiscIdentifier
            // -------------------------------------------------------------------------------------------------------------
            // RULES:
            // When active MiscIdentifierControl (MiscIdentifierControl.flag_active = 1) exists:
            // - Activate menu '/ClinicalAnnotation/MiscIdentifiers/'
            // - Activate databrowser control for the MiscIdentifier to Participant link
            //
            // When no active MiscIdentifierControl exists:
            // - Inactivate menu '/ClinicalAnnotation/MiscIdentifiers/'
            // - Inactivate databrowser control for the MiscIdentifier to Participant link
            //
            // When active Study MiscIdentifierControl (MiscIdentifierControl.flag_link_to_study = 1) exists:
            // - Activate flag_search, flag_index of the study structure_fields attached to the
            // miscidentifiers_for_participant_search structure
            // - Activate flag_index of the study structure_fields attached to the miscidentifiers structure
            // - Activate databrowser control for the MiscIdentifier to StudySummary link
            //
            // When no active Study MiscIdentifierControl exists:
            // - Inactivate flag_search, flag_index of the study structure_fields attached to the
            // miscidentifiers_for_participant_search structure
            // - Inactivate flag_index of the study structure_fields attached to the miscidentifiers structure
            // - Inactivate databrowser control for the MiscIdentifier to StudySummary link
            // -------------------------------------------------------------------------------------------------------------
            // NOTES:
            // - The Add MiscIdentifier button is automaticaly activated or disabled by the view.
            // - The miscidentifiers_study structure is added to the view by controller to create or edit a Study
            // MiscIdentifier or not according to the MiscIdentifierControl.flag_active value of the created/edited
            // MiscIdentifier.
            // -------------------------------------------------------------------------------------------------------------
            $automaticCustomisationQueries = array();

            // MiscIdentifier
            $doesActiveModelExist = $browsingControlModel->query("SELECT count(*) AS res FROM misc_identifier_controls WHERE flag_active = '1'");
            $doesActiveModelExist = $doesActiveModelExist[0][0]['res'];
            $flagValue = $doesActiveModelExist ? '1' : '0';
            $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'Participant', 'MiscIdentifier', $automaticCustomisationMessages);
            $automaticCustomisationQueries[] = "UPDATE menus SET flag_active = $flagValue WHERE use_link LIKE '/ClinicalAnnotation/MiscIdentifiers/%'";
            $automaticCustomisationMessages['menu'][$flagValue][] = '/ClinicalAnnotation/MiscIdentifiers/';

            // MiscIdentifier Study Data
            $doesActiveModelExist = $browsingControlModel->query("SELECT count(*) AS res FROM misc_identifier_controls WHERE flag_active = '1' AND flag_link_to_study = '1'");
            $doesActiveModelExist = $doesActiveModelExist[0][0]['res'];
            $flagValue = $doesActiveModelExist ? '1' : '0';
            $selectStructureFieldSql = "SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'";
            $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                'flag_search',
                'flag_index'
            ), $flagValue, array(
                'miscidentifiers_for_participant_search'
            ), $selectStructureFieldSql);
            $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                'flag_index'
            ), $flagValue, array(
                'miscidentifiers'
            ), $selectStructureFieldSql);
            $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'MiscIdentifier', 'StudySummary', $automaticCustomisationMessages);
            $automaticCustomisationMessages['general'][] = __("structures miscidentifiers, miscidentifiers_for_participant_search and datamart_browsing_controls have been updated to " . ($doesActiveModelExist ? '' : 'not ') . "let people to create Study Participant Identifier and search on this field.");

            foreach ($automaticCustomisationQueries as $tmpSql) {
                $browsingControlModel->query($tmpSql);
            }

            // -------------------------------------------------------------------------------------------------------------
            // AUTOMATIC CUSTOMISATION: model - ClinicalAnnotation.ConsentMaster
            // -------------------------------------------------------------------------------------------------------------
            // RULES:
            // When active ConsentControl (ConsentControl.flag_active = 1) exists:
            // - Activate menu '/ClinicalAnnotation/ConsentMasters/'
            // - Activate databrowser control for the ConsentMaster to Participant link
            //
            // When no active ConsentControl exists:
            // - Inactivate menu '/ClinicalAnnotation/ConsentMasters/'
            // - Inactivate databrowser control for the ConsentMaster to Participant link
            // - Inactivate databrowser control for the ConsentMaster to Collection link
            //
            // When active Study ConsentControl (ConsentControl.flag_link_to_study = 1) exists:
            // - Activate flag_index of the study structure_fields attached to the consent_masters structure
            // - Activate databrowser control for the ConsentMaster to StudySummary link
            //
            // When no active Study ConsentControl exists:
            // - Inactivate flag_index of the study structure_fields attached to the consent_masters structure
            // - Inactivate databrowser control for the ConsentMaster to StudySummary link
            // -------------------------------------------------------------------------------------------------------------
            // NOTES:
            // - The structure 'consent_masters_study' should be added to the ConsentControl.detail_form_alias of a Study
            // ConsentControl type to let users to create, view and search on Study data for this type of consent.
            // -------------------------------------------------------------------------------------------------------------
            $automaticCustomisationQueries = array();

            // ConsentMaster
            $doesActiveModelExist = $browsingControlModel->query("SELECT count(*) AS res FROM consent_controls WHERE flag_active = '1'");
            $doesActiveModelExist = $doesActiveModelExist[0][0]['res'];
            $flagValue = $doesActiveModelExist ? '1' : '0';
            $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'Participant', 'ConsentMaster', $automaticCustomisationMessages);
            if (! $doesActiveModelExist) {
                // Inactivate databrowser control ConsentMaster to Collection
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'ConsentMaster', 'ViewCollection', $automaticCustomisationMessages);
            }
            $automaticCustomisationQueries[] = "UPDATE menus SET flag_active = $flagValue WHERE use_link LIKE '/ClinicalAnnotation/ConsentMasters/%'";
            $automaticCustomisationMessages['menu'][$flagValue][] = '/ClinicalAnnotation/ConsentMasters/';

            // ConsentMaster Study Data
            $doesActiveModelExist = $browsingControlModel->query("SELECT count(*) AS res FROM consent_controls WHERE flag_active = '1' AND flag_link_to_study = '1'");
            $doesActiveModelExist = $doesActiveModelExist[0][0]['res'];
            $flagValue = $doesActiveModelExist ? '1' : '0';
            $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                'flag_index'
            ), $flagValue, array(
                'consent_masters'
            ), "SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'");
            $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'ConsentMaster', 'StudySummary', $automaticCustomisationMessages);
            $automaticCustomisationMessages['general'][] = __("structure 'consent_masters' and 'datamart_browsing_controls' have been updated to " . ($doesActiveModelExist ? '' : 'not ') . "let people to create 'Study Consent' and search on this field.");

            foreach ($automaticCustomisationQueries as $tmpSql) {
                $browsingControlModel->query($tmpSql);
            }

            // -------------------------------------------------------------------------------------------------------------
            // AUTOMATIC CUSTOMISATION: model - ClinicalAnnotation.TreatmentMaster
            // -------------------------------------------------------------------------------------------------------------
            // RULES:
            // When active TreatmentControl (TreatmentControl.flag_active = 1) exists:
            // - Activate menu '/ClinicalAnnotation/TreatmentMasters/'
            // - Activate databrowser control for the TreatmentMaster to Participant link
            //
            // When no active TreatmentControl exists:
            // - Inactivate menu '/ClinicalAnnotation/TreatmentMasters/'
            // - Inactivate databrowser control for the TreatmentMaster to Participant link
            // - Inactivate databrowser control for the TreatmentMaster to CollectionMaster link
            // - Inactivate databrowser control for the TreatmentMaster to DiagnosisMaster link
            //
            // When active TreatmentExtendControl (TreatmentExtendControl.flag_active = 1) exists:
            // - Activate menu '/ClinicalAnnotation/TreatmentExtendMasters/'
            // - Activate databrowser control for the TreatmentExtendMaster to TreatmentMaster link
            //
            // When no active TreatmentControl exists:
            // - Inactivate menu '/ClinicalAnnotation/TreatmentExtendMasters/'
            // - Inactivate databrowser control for the TreatmentExtendMaster to TreatmentMaster link
            // -------------------------------------------------------------------------------------------------------------
            // NOTES:
            // - The display or not of treatment data in clinical collection views is managed by the trunk code.
            // - The display or not of diagnosis data in treatment views is managed by the trunk code.
            // - The display or not of linked collection in treatment detail view is managed by the trunk code.
            // -------------------------------------------------------------------------------------------------------------
            $automaticCustomisationQueries = array();

            // TreatmentMaster
            $doesActiveModelExist = $browsingControlModel->query("SELECT count(*) AS res FROM treatment_controls WHERE flag_active = '1'");
            $doesActiveModelExist = $doesActiveModelExist[0][0]['res'];
            $flagValue = $doesActiveModelExist ? '1' : '0';
            $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'Participant', 'TreatmentMaster', $automaticCustomisationMessages);
            if (! $doesActiveModelExist) {
                // Inactivate databrowser control TreatmentMaster to Collection
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'TreatmentMaster', 'ViewCollection', $automaticCustomisationMessages);
                // Inactivate databrowser control TreatmentMaster to Diagnosis
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'TreatmentMaster', 'DiagnosisMaster', $automaticCustomisationMessages);
            }
            $automaticCustomisationQueries[] = "UPDATE menus SET flag_active = $flagValue WHERE use_link LIKE '/ClinicalAnnotation/TreatmentMasters/%'";
            $automaticCustomisationMessages['menu'][$flagValue][] = '/ClinicalAnnotation/TreatmentMasters/';

            // TreatmentExtendMaster
            $doesActiveModelExist = $browsingControlModel->query("SELECT count(*) AS res FROM treatment_extend_controls WHERE flag_active = '1'");
            $doesActiveModelExist = $doesActiveModelExist[0][0]['res'];
            $flagValue = $doesActiveModelExist ? '1' : '0';
            $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'TreatmentMaster', 'TreatmentExtendMaster', $automaticCustomisationMessages);
            $automaticCustomisationQueries[] = "UPDATE menus SET flag_active = $flagValue WHERE use_link LIKE '/ClinicalAnnotation/TreatmentExtendMasters/%'";
            $automaticCustomisationMessages['menu'][$flagValue][] = '/ClinicalAnnotation/TreatmentExtendMasters/';

            foreach ($automaticCustomisationQueries as $tmpSql) {
                $browsingControlModel->query($tmpSql);
            }

            // -------------------------------------------------------------------------------------------------------------
            // AUTOMATIC CUSTOMISATION:
            // - model - ClinicalAnnotation.EventMaster
            // - menu - /ClinicalAnnotation/EventMasters/%
            // -------------------------------------------------------------------------------------------------------------
            // RULES:
            // When active EventControl (EventControl.flag_active = 1) exists:
            // - Activate menu '/ClinicalAnnotation/EventMasters/'
            // - Activate/Inactivate '/ClinicalAnnotation/EventMasters/' submenus according to active/inactive event_group
            // - Activate databrowser control for the EventMaster to Participant link
            //
            // When no active EventControl exists:
            // - Inactivate menu '/ClinicalAnnotation/EventMasters/'
            // - Inactivate databrowser control for the EventMaster to Participant link
            // - Inactivate databrowser control for the EventMaster to Collection link
            // - Inactivate databrowser control for the EventMaster to DiagnosisMaster link
            // -------------------------------------------------------------------------------------------------------------
            // NOTES:
            // - The display or not of event data in clinical collection views is managed by the trunk code.
            // - The display or not of diagnosis data in event views is managed by the trunk code.
            // - The display or not of linked collection in event detail view is managed by the trunk code.
            // -------------------------------------------------------------------------------------------------------------
            $automaticCustomisationQueries = array();

            // EventMaster
            $doesActiveModelExist = $browsingControlModel->query("SELECT count(*) AS res FROM event_controls WHERE flag_active = '1'");
            $doesActiveModelExist = $doesActiveModelExist[0][0]['res'];
            $flagValue = $doesActiveModelExist ? '1' : '0';
            $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'Participant', 'EventMaster', $automaticCustomisationMessages);
            if (! $doesActiveModelExist) {
                // Inactivate databrowser control EventMaster to Collection
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'EventMaster', 'ViewCollection', $automaticCustomisationMessages);
                // Inactivate databrowser control EventMaster to Diagnosis
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'EventMaster', 'DiagnosisMaster', $automaticCustomisationMessages);
            }
            $automaticCustomisationQueries[] = "UPDATE menus SET flag_active = $flagValue WHERE use_link LIKE '/ClinicalAnnotation/EventMasters/%'";
            $automaticCustomisationMessages['menu'][$flagValue][] = '/ClinicalAnnotation/EventMasters/';
            if ($doesActiveModelExist) {
                // Activate EventMaster menus but disable any unused sub menu
                $queryRes = $browsingControlModel->query("SELECT GROUP_CONCAT(DISTINCT event_group) AS res FROM event_controls WHERE flag_active = 1");
                $browsingControlModel->query("UPDATE menus SET flag_active = 1 WHERE use_link LIKE '" . '/ClinicalAnnotation/EventMasters/' . "%'");
                $activeEventGroups = explode(',', $queryRes[0][0]['res']);
                $activeUrls = array();
                $query = "SELECT id, parent_id, use_link, flag_active
                        FROM menus
                        WHERE parent_id = 'clin_CAN_4'
                        AND use_link LIKE '/ClinicalAnnotation/EventMasters/%'
                        AND id != 'clin_CAN_4' ORDER BY display_order";
                foreach ($browsingControlModel->query($query) as $newLink) {
                    if (preg_match('/^\/ClinicalAnnotation\/EventMasters\/listall\/([A-Za-z_]+)\//', $newLink['menus']['use_link'], $matches)) {
                        $menuEventGroup = $matches[1];
                        $flagValue = (in_array($menuEventGroup, $activeEventGroups)) ? '1' : '0';
                        $browsingControlModel->query("UPDATE menus SET flag_active = $flagValue WHERE use_link LIKE '/ClinicalAnnotation/EventMasters/listall/" . $menuEventGroup . "/%'");
                        $automaticCustomisationMessages['menu'][$flagValue][] = "/ClinicalAnnotation/EventMasters/listall/" . $menuEventGroup . '/';
                        $activeUrls[$flagValue][] = $newLink['menus']['use_link'];
                    } else {
                        $flagValue = $newLink['menus']['flag_active'];
                        $automaticCustomisationMessages['menu'][$flagValue][] = $newLink['menus']['use_link'];
                        $activeUrls[$flagValue][] = $newLink['menus']['use_link'];
                    }
                }
                if (isset($activeUrls[1])) {
                    // Set the default event group menu
                    $queryRes = $browsingControlModel->query("SELECT id, use_link, flag_active FROM menus WHERE id = 'clin_CAN_4' AND use_link LIKE '/ClinicalAnnotation/EventMasters/%';");
                    if ($queryRes) {
                        $rootUrl = $queryRes['0']['menus']['use_link'];
                        if (! in_array($rootUrl, $activeUrls[1])) {
                            // Update menus to an active event group
                            $firstActiveUrl = array_shift($activeUrls[1]);
                            $browsingControlModel->query("UPDATE menus SET use_link = '$firstActiveUrl', flag_active = '1' WHERE id = 'clin_CAN_4'");
                        }
                    }
                }
            }

            foreach ($automaticCustomisationQueries as $tmpSql) {
                $browsingControlModel->query($tmpSql);
            }

            // -------------------------------------------------------------------------------------------------------------
            // AUTOMATIC CUSTOMISATION: model - ClinicalAnnotation.DiagnosisMaster
            // -------------------------------------------------------------------------------------------------------------
            // RULES:
            // When active DiagnosisControl (DiagnosisControl.flag_active = 1) exists:
            // - Activate menu '/ClinicalAnnotation/DiagnosisMasters/'
            // - Activate databrowser control for the DiagnosisMaster to Participant link
            //
            // When no active DiagnosisControl exists:
            // - Inactivate menu '/ClinicalAnnotation/DiagnosisMasters/'
            // - Inactivate databrowser control for the DiagnosisMaster to Participant link
            // - Inactivate databrowser control for the DiagnosisMaster to ViewCollection link
            // - Inactivate databrowser control for the DiagnosisMaster to DiagnosisMaster link
            // - Inactivate databrowser control for the DiagnosisMaster to EventMaster link
            // - Inactivate databrowser control for the DiagnosisMaster to TreatmentMaster link
            //
            // When active progression DiagnosisControl (category NOT IN ('primary', 'nonneoplastic')) exists:
            // - Activate databrowser control for the DiagnosisMaster to DiagnosisMaster link
            //
            // When no active progression DiagnosisControl exists:
            // - Inactivate databrowser control for the DiagnosisMaster to DiagnosisMaster link
            // -------------------------------------------------------------------------------------------------------------
            // NOTES:
            // - The display or not of diagnosis data in clinical collection views is managed by the trunk code.
            // - The display or not of linked collection in diagnosis detail view is managed by the trunk code.
            // -------------------------------------------------------------------------------------------------------------
            $automaticCustomisationQueries = array();

            // DiagnosisMaster
            $doesActiveModelExist = $browsingControlModel->query("SELECT count(*) AS res FROM diagnosis_controls WHERE flag_active = '1'");
            $doesActiveModelExist = $doesActiveModelExist[0][0]['res'];
            $flagValue = $doesActiveModelExist ? '1' : '0';
            $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'Participant', 'DiagnosisMaster', $automaticCustomisationMessages);
            if (! $doesActiveModelExist) {
                // Inactivate databrowser control DiagnosisMaster to Collection
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'DiagnosisMaster', 'ViewCollection', $automaticCustomisationMessages);
                // Inactivate databrowser control DiagnosisMaster to Diagnosis
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'DiagnosisMaster', 'DiagnosisMaster', $automaticCustomisationMessages);
                // Inactivate databrowser control DiagnosisMaster to EventMaster
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'DiagnosisMaster', 'EventMaster', $automaticCustomisationMessages);
                // Inactivate databrowser control DiagnosisMaster to TreatmentMaster
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'DiagnosisMaster', 'TreatmentMaster', $automaticCustomisationMessages);
            }
            $automaticCustomisationQueries[] = "UPDATE menus SET flag_active = $flagValue WHERE use_link LIKE '/ClinicalAnnotation/DiagnosisMasters/%'";
            $automaticCustomisationMessages['menu'][$flagValue][] = '/ClinicalAnnotation/DiagnosisMasters/';

            // DiagnosisMaster to DiagnosisMaster
            $doesActiveModelExist = $browsingControlModel->query("SELECT count(*) AS res FROM diagnosis_controls WHERE flag_active = '1' AND category NOT IN ('primary', 'nonneoplastic')");
            $doesActiveModelExist = $doesActiveModelExist[0][0]['res'];
            $flagValue = $doesActiveModelExist ? '1' : '0';
            $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'DiagnosisMaster', 'DiagnosisMaster', $automaticCustomisationMessages);
            
            foreach ($automaticCustomisationQueries as $tmpSql) {
                $browsingControlModel->query($tmpSql);
            }

            // -------------------------------------------------------------------------------------------------------------
            // AUTOMATIC CUSTOMISATION:
            // - model - ClinicalAnnotation.ParticipantContact
            // - model - ClinicalAnnotation.ReproductiveHistory
            // - model - ClinicalAnnotation.FamilyHistory
            // - model - ClinicalAnnotation.ParticipantMessage
            // -------------------------------------------------------------------------------------------------------------
            // RULES:
            // When the databrowser control link {Model} to Participant is active:
            // - Activate menu '/ClinicalAnnotation/{Model}/'
            // When the databrowser control link {Model} to Participant is inactive:
            // - Disable menu '/ClinicalAnnotation/{Model}/'
            // -------------------------------------------------------------------------------------------------------------
            // NOTES:
            // - N/A
            // -------------------------------------------------------------------------------------------------------------
            $automaticCustomisationQueries = array();

            $dataToCheck = array(
                array(
                    'ClinicalAnnotation.Participant',
                    'ClinicalAnnotation.ParticipantContact',
                    array(
                        '/ClinicalAnnotation/ParticipantContacts/'
                    )
                ),
                array(
                    'ClinicalAnnotation.Participant',
                    'ClinicalAnnotation.ReproductiveHistory',
                    array(
                        '/ClinicalAnnotation/ReproductiveHistories/'
                    )
                ),
                array(
                    'ClinicalAnnotation.Participant',
                    'ClinicalAnnotation.FamilyHistory',
                    array(
                        '/ClinicalAnnotation/FamilyHistories/'
                    )
                ),
                array(
                    'ClinicalAnnotation.Participant',
                    'ClinicalAnnotation.ParticipantMessage',
                    array(
                        '/ClinicalAnnotation/ParticipantMessages/'
                    )
                )
            );
            foreach ($dataToCheck as $newDataToCheck) {
                list ($model1, $model2, $menuLinks) = $newDataToCheck;
                $model1 = explode('.', $model1);
                $plugin1 = $model1[0];
                $model1 = $model1[1];
                $model2 = explode('.', $model2);
                $plugin2 = $model2[0];
                $model2 = $model2[1];
                $query = "SELECT count(*) AS res
                    FROM datamart_browsing_controls
                    WHERE (flag_active_1_to_2 = 1 OR flag_active_2_to_1 = 1)
                    AND (
                        (id1 = (SELECT id FROM `datamart_structures` where model = '$model1' and plugin = '$plugin1') AND id2 =(SELECT id FROM `datamart_structures` where model = '$model2' and plugin = '$plugin2'))
                        OR (id1 = (SELECT id FROM `datamart_structures` where model = '$model2' and plugin = '$plugin2') AND id2 =(SELECT id FROM `datamart_structures` where model = '$model1' and plugin = '$plugin1'))
                    );";
                $queryRes = $browsingControlModel->query($query);
                $flagValue = $queryRes[0][0]['res'] ? '1' : '0';
                foreach ($menuLinks as $menuLink) {
                    $automaticCustomisationQueries[] = "UPDATE menus SET flag_active = $flagValue WHERE use_link LIKE '" . $menuLink . "%'";
                    $automaticCustomisationMessages['menu'][$flagValue][] = $menuLink;
                }
            }

            foreach ($automaticCustomisationQueries as $tmpSql) {
                $browsingControlModel->query($tmpSql);
            }

            // -------------------------------------------------------------------------------------------------------------
            // AUTOMATIC CUSTOMISATION: structure - clinicalcollectionlinks
            // -------------------------------------------------------------------------------------------------------------
            // RULES:
            // When the databrowser control link Participant to model {ConsentMaster, DiagnosisMaster, TreatmentMaster, EventMaster}
            // is not active:
            // - Inactivate flag_index of the {model} structure_fields attached to the clinicalcollectionlinks structure
            // -------------------------------------------------------------------------------------------------------------
            // NOTES:
            // - N/A
            // -------------------------------------------------------------------------------------------------------------
            $automaticCustomisationQueries = array();

            $cclModel = AppModel::getInstance('ClinicalAnnotation', 'ClinicalAnnotationAppModel', true);
            foreach ($cclModel->getLinkableObjectsListFromBrowsingControl('InventoryManagement', 'ViewCollection') as $item) {
                if (in_array($item['model'], array(
                    'ConsentMaster',
                    'DiagnosisMaster',
                    'TreatmentMaster',
                    'EventMaster'
                )) !== false && $item['plugin'] == 'ClinicalAnnotation' && $item['active'] == '0') {
                    $controlModel = str_replace("Master", "Control", $item['model']);
                    $flagValue = '0';
                    $selectStructureFieldSql = "SELECT id FROM structure_fields WHERE ((plugin = '" . $item['plugin'] . "' AND model = '" . $item['model'] . "') OR (plugin = '" . $item['plugin'] . "' AND model = '$controlModel'))";
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_index'
                    ), $flagValue, array(
                        'clinicalcollectionlinks'
                    ), $selectStructureFieldSql);
                    $automaticCustomisationMessages['general'][] = __("structure 'clinicalcollectionlinks' has been updated to not let people to view '%s' data.", str_replace("Master", "", $item['model']));
                }
            }
            
            foreach ($automaticCustomisationQueries as $tmpSql) {
                $browsingControlModel->query($tmpSql);
            }

            // -------------------------------------------------------------------------------------------------------------
            // AUTOMATIC CUSTOMISATION: structure - aliquot_masters
            // -------------------------------------------------------------------------------------------------------------
            // RULES:
            // Update structure_fields, structure_formats and structure_validations to apply core configurations:
            // - hideAliquotBarcodeFieldOfAliquotCreationForms
            // - useATiMAliquotBarcodeGeneratorForEmptyBarcode
            // -------------------------------------------------------------------------------------------------------------
            // NOTES:
            // - N/A
            // -------------------------------------------------------------------------------------------------------------
            $automaticCustomisationQueries = array();
            
            if (Configure::read("hideAliquotBarcodeFieldOfAliquotCreationForms")) {
                $automaticCustomisationQueries = array(
                    "UPDATE structure_formats SET `flag_add`='0', `flag_addgrid`='0', `flag_editgrid`='0'
                        WHERE structure_id=(SELECT id FROM structures WHERE alias='aliquot_masters')
                        AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='barcode' AND `structure_value_domain` IS NULL);",
                    "DELETE FROM structure_validations
                        WHERE rule = 'notBlank'
                        AND structure_field_id IN (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='barcode' AND `structure_value_domain` IS NULL)"
                );
                $automaticCustomisationMessages['general'][] = __("system has been configured to let atim to generate all aliquot barcodes");
            } elseif (Configure::read("useATiMAliquotBarcodeGeneratorForEmptyBarcode")) {
                $automaticCustomisationQueries = array(
                    "UPDATE structure_fields
                        SET setting = CONCAT(setting ,',placeholder=-- auto --')
                        WHERE model = 'AliquotMaster' AND field = 'barcode' AND `structure_value_domain` IS NULL AND setting IS NOT NULL AND setting NOT LIKE '';",
                    "UPDATE structure_fields
                        SET setting = 'placeholder=-- auto --'
                        WHERE model = 'AliquotMaster' AND field = 'barcode' AND `structure_value_domain` IS NULL AND (setting IS NULL OR setting LIKE '');",
                    "DELETE FROM structure_validations
                        WHERE rule = 'notBlank'
                        AND structure_field_id IN (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='barcode' AND `structure_value_domain` IS NULL)"
                );
                $automaticCustomisationMessages['general'][] = __("system has been configured to let ATim to generate the un-entered aliquot barcodes");
            }
            
            foreach ($automaticCustomisationQueries as $tmpSql) {
                $browsingControlModel->query($tmpSql);
            }
            
            // -------------------------------------------------------------------------------------------------------------
            // AUTOMATIC CUSTOMISATION:
            // - model - InventoryManagement.QualityCtrl
            // - model - InventoryManagement.SpecimenReviewMaster
            // -------------------------------------------------------------------------------------------------------------
            // RULES:
            // When the databrowser control link {Model} to Participant is active:
            // - Activate menu '/ClinicalAnnotation/{Model}/'
            // When the databrowser control link {Model} to Participant is inactive:
            // - Disable menu '/ClinicalAnnotation/{Model}/'
            // -------------------------------------------------------------------------------------------------------------
            // NOTES:
            // - N/A
            // -------------------------------------------------------------------------------------------------------------
            $automaticCustomisationQueries = array();

            $dataToCheck = array(
                array(
                    'InventoryManagement.QualityCtrl',
                    'InventoryManagement.ViewSample',
                    array(
                        '/InventoryManagement/QualityCtrls'
                    )
                ),
                array(
                    'InventoryManagement.SpecimenReviewMaster',
                    'InventoryManagement.ViewSample',
                    array(
                        '/InventoryManagement/SpecimenReviews/'
                    )
                )
            );
            foreach ($dataToCheck as $newDataToCheck) {
                list ($model1, $model2, $menuLinks) = $newDataToCheck;
                $model1 = explode('.', $model1);
                $plugin1 = $model1[0];
                $model1 = $model1[1];
                $model2 = explode('.', $model2);
                $plugin2 = $model2[0];
                $model2 = $model2[1];
                $query = "SELECT count(*) AS res
                    FROM datamart_browsing_controls
                    WHERE (flag_active_1_to_2 = 1 OR flag_active_2_to_1 = 1)
                    AND (
                        (id1 = (SELECT id FROM `datamart_structures` where model = '$model1' and plugin = '$plugin1') AND id2 =(SELECT id FROM `datamart_structures` where model = '$model2' and plugin = '$plugin2'))
                        OR (id1 = (SELECT id FROM `datamart_structures` where model = '$model2' and plugin = '$plugin2') AND id2 =(SELECT id FROM `datamart_structures` where model = '$model1' and plugin = '$plugin1'))
                    );";
                $queryRes = $browsingControlModel->query($query);
                $flagValue = $queryRes[0][0]['res'] ? '1' : '0';
                foreach ($menuLinks as $menuLink) {
                    $automaticCustomisationQueries[] = "UPDATE menus SET flag_active = $flagValue WHERE use_link LIKE '" . $menuLink . "%'";
                    $automaticCustomisationMessages['menu'][$flagValue][] = $menuLink;
                }
            }

            foreach ($automaticCustomisationQueries as $tmpSql) {
                $browsingControlModel->query($tmpSql);
            }

            // -------------------------------------------------------------------------------------------------------------
            // AUTOMATIC CUSTOMISATION:
            // - structure - orderlines
            // - structure - orderitems
            // - structure - orderitems_plus
            // - structure - shippeditems
            // -------------------------------------------------------------------------------------------------------------
            // RULES:
            // Apply core configuration order_item_type_config on structure_formats displaying or not the structure_fields
            // of models 'AliquotMaster', 'ViewAliquot' and 'TmaSlide'. See comment in code.
            // -------------------------------------------------------------------------------------------------------------
            // NOTES:
            // - N/A
            // -------------------------------------------------------------------------------------------------------------
            $automaticCustomisationQueries = array();

            // Check if is Aliquotmaster.aliquo_lable is a structure_fields displayed (use) in local installation and display/hide fields according to this setting.
            // Falg variable used in following cases.
            $tmpSql = "SELECT DISTINCT `flag_detail`
    			FROM structure_formats
    			WHERE structure_id=(SELECT id FROM structures WHERE alias='aliquot_masters')
    			AND structure_field_id IN (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='aliquot_label')";
            $flagDetailResult = $browsingControlModel->query($tmpSql);
            $aliquotLabelFlagDetail = '1';
            if ($flagDetailResult) {
                $aliquotLabelFlagDetail = empty($flagDetailResult[0]['structure_formats']['flag_detail']) ? '0' : '1';
            }

            $flagsArray = array(
                'shippeditems' => array(
                    'flag_addgrid',
                    'flag_addgrid_readonly',
                    'flag_edit',
                    'flag_edit_readonly',
                    'flag_editgrid',
                    'flag_editgrid_readonly',
                    'flag_index',
                    'flag_detail'
                )
            );
            $flagsArray['orderitems'] = $flagsArray['shippeditems'];

            switch (Configure::read('order_item_type_config')) {
                case '0':
                    // .....................................................................................
                    // Order tool not used - no item can be added to order
                    // Nothing to do
                    // .....................................................................................
                    break;
                case '1':
                    // .....................................................................................
                    // Both tma slide and aliquot can be attached to an order.
                    // Display structure_fields of all models.
                    // .....................................................................................
                    // 1 - 'shippeditems' structure
                    $structure = 'shippeditems';
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='Order' AND `model`='Generated' AND `field`='type'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], $aliquotLabelFlagDetail, array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='aliquot_label'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='InventoryManagement' AND `model` IN ('AliquotMaster', 'ViewAliquot') AND `field` NOT IN ('aliquot_label','barcode')");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='StorageLayout' AND `model`='TmaSlide' AND field != 'barcode'");
                    // 2 - 'orderitems' structure
                    $structure = 'orderitems';
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='Order' AND `model`='Generated' AND `field`='type'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_addgrid',
                        'flag_addgrid_readonly',
                        'flag_edit',
                        'flag_edit_readonly',
                        'flag_editgrid',
                        'flag_editgrid_readonly',
                        'flag_index',
                        'flag_detail'
                    ), $aliquotLabelFlagDetail, array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='aliquot_label'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_search'
                    ), '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model` IN ('AliquotMaster', 'TmaSlide') AND `field`='barcode'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_addgrid',
                        'flag_addgrid_readonly',
                        'flag_edit',
                        'flag_edit_readonly',
                        'flag_editgrid',
                        'flag_editgrid_readonly',
                        'flag_index',
                        'flag_detail'
                    ), '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model` IN ('AliquotMaster', 'ViewAliquot') AND `field` NOT IN ('aliquot_label','barcode')");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_edit',
                        'flag_edit_readonly',
                        'flag_editgrid',
                        'flag_editgrid_readonly',
                        'flag_index',
                        'flag_detail'
                    ), '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='StorageLayout' AND `model`='TmaSlide' AND field != 'barcode'");
                    // 3 - `orderlines` structure
                    $structure = 'orderlines';
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_search'
                    ), '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='OrderLine' AND `tablename`='order_lines' AND `field` IN ('is_tma_slide', 'sample_control_id')");
                    // 4 - `orderitems_plus` structure
                    $structure = 'orderitems_plus';
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_index'
                    ), '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field` IN ('sample_type', 'aliquot_type')");
                    break;

                case '2':
                    // .....................................................................................
                    // Only aliquot can be attached to an order.
                    // Display structure_fields of 'AliquotMaster' and 'ViewAliquot' models. Hide the other
                    // one.
                    // .....................................................................................
                    // 1 - 'shippeditems' structure
                    $structure = 'shippeditems';
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '0', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='Order' AND `model`='Generated' AND `field`='type'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], $aliquotLabelFlagDetail, array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='aliquot_label'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='InventoryManagement' AND `model` IN ('AliquotMaster', 'ViewAliquot') AND `field` NOT IN ('aliquot_label','barcode')");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '0', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='StorageLayout' AND `model`='TmaSlide'");
                    // 2 - 'orderitems' structure
                    $structure = 'orderitems';
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '0', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='Order' AND `model`='Generated' AND `field`='type'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_search'
                    ), '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='barcode'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_search'
                    ), '0', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='TmaSlide' AND `tablename`='tma_slides' AND `field`='barcode'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], $aliquotLabelFlagDetail, array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='aliquot_label'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model` IN ('AliquotMaster', 'ViewAliquot' ) AND `field` NOT IN ('aliquot_label','barcode')");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '0', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='StorageLayout' AND `model`='TmaSlide'");
                    // 3 - `orderlines` structure
                    $structure = 'orderlines';
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_search'
                    ), '0', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='OrderLine' AND `tablename`='order_lines' AND `field` IN ('is_tma_slide')");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_search'
                    ), '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='OrderLine' AND `tablename`='order_lines' AND `field` IN ('sample_control_id')");
                    // 4 - `orderitems_plus` structure
                    $structure = 'orderitems_plus';
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_index'
                    ), '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field` IN ('sample_type', 'aliquot_type')");
                    break;

                case '3': 
                    // .....................................................................................
                    // Only tma slide can be attached to an order.
                    // Display structure_fields of 'TmaSlide' model. Hide the other one.
                    // .....................................................................................
                    // 1 - 'shippeditems' structure
                    $structure = 'shippeditems';
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '0', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='Order' AND `model`='Generated' AND `field`='type'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '0', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='InventoryManagement' AND `model` IN ('AliquotMaster', 'ViewAliquot')");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='StorageLayout' AND `model`='TmaSlide' AND field != 'barcode'");
                    // 2 - 'orderitems' structure
                    $structure = 'orderitems';
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '0', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='Order' AND `model`='Generated' AND `field`='type'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_search'
                    ), '0', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='barcode'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_search'
                    ), '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='TmaSlide' AND `tablename`='tma_slides' AND `field`='barcode'");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '0', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model` IN ('AliquotMaster', 'ViewAliquot')");
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql($flagsArray[$structure], '1', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `plugin`='StorageLayout' AND `model`='TmaSlide' AND field != 'barcode'");
                    // 3 - `orderlines` structure
                    $structure = 'orderlines';
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_search'
                    ), '0', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='OrderLine' AND `tablename`='order_lines' AND `field` IN ('is_tma_slide', 'sample_control_id')");
                    // 4 - `orderitems_plus` structure
                    $structure = 'orderitems_plus';
                    $automaticCustomisationQueries[] = $this->getStructureFormatUpdateSql(array(
                        'flag_index'
                    ), '0', array(
                        $structure
                    ), "SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field` IN ('sample_type','aliquot_type')");
                    break;

                default:
            }
            $automaticCustomisationMessages['general'][] = __("structures 'shippeditems', 'orderitems', 'orderitems_returned' and 'orderlines' have been updated based on the core variable 'order_item_type_config'.");

            foreach ($automaticCustomisationQueries as $tmpSql) {
                $browsingControlModel->query($tmpSql);
            }
            
            // -------------------------------------------------------------------------------------------------------------
            // AUTOMATIC CUSTOMISATION:
            // - model - Order.Order
            // - model - Order.OrderLine
            // - model - Order.OrderItem
            // - model - Order.Shipment
            // -------------------------------------------------------------------------------------------------------------
            // RULES:
            // Inactivate/activate databrowser control for the models link according to core variable:
            // - order_item_type_config
            // - order_item_to_order_objetcs_link_setting.
            // See comments in code.
            // -------------------------------------------------------------------------------------------------------------
            // NOTES:
            // - N/A.
            // -------------------------------------------------------------------------------------------------------------

            $automaticCustomisationQueries = array();

            if (! Configure::read('order_item_type_config')) {
                // .....................................................................................
                // Order tool not used - no item can be added to order.
                // No active link
                // .....................................................................................
                $flagValue = '0';
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'Order', 'StudySummary', $automaticCustomisationMessages);
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'Order', 'OrderItem', $automaticCustomisationMessages);
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'Order', 'OrderLine', $automaticCustomisationMessages);
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'OrderLine', 'StudySummary', $automaticCustomisationMessages);
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'OrderLine', 'OrderItem', $automaticCustomisationMessages);
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'OrderItem', 'ViewAliquot', $automaticCustomisationMessages);
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'OrderItem', 'TmaSlide', $automaticCustomisationMessages);
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'Order', 'Shipment', $automaticCustomisationMessages);
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'OrderItem', 'Shipment', $automaticCustomisationMessages);
            } else {
                // .....................................................................................
                // Both tma slide and aliquot can be attached to an order.
                // Active links:
                // - Order to OderItem
                // - Order to Shipment
                // - OderItem to Shipment
                // When order_item_type_config different than 3 then activate link OrderItem to ViewAliquot.
                // When order_item_type_config different than 2 then activate link OrderItem to TmaSlide.
                // .....................................................................................
                $flagValue = '1';
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'Order', 'OrderItem', $automaticCustomisationMessages);
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'Order', 'Shipment', $automaticCustomisationMessages);
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'OrderItem', 'Shipment', $automaticCustomisationMessages);
                // Order Line
                $flagValue = (Configure::read('order_item_to_order_objetcs_link_setting') == '3') ? '0' : '1';
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'Order', 'OrderLine', $automaticCustomisationMessages);
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'OrderItem', 'OrderLine', $automaticCustomisationMessages);
                if (Configure::read('order_item_to_order_objetcs_link_setting') == '3') {
                    // Order Line not used
                    $flagValue = '0';
                    $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'OrderLine', 'StudySummary', $automaticCustomisationMessages);
                }
                // Order Item to Aliquot or TMA
                $flagValue = (Configure::read('order_item_type_config') != '3') ? '1' : '0';
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'OrderItem', 'ViewAliquot', $automaticCustomisationMessages);

                $flagValue = (Configure::read('order_item_type_config') != '2') ? '1' : '0';
                $automaticCustomisationQueries[] = $this->getDatamartBrowsingControlUpdateSql($flagValue, 'OrderItem', 'TmaSlide', $automaticCustomisationMessages);
            }

            foreach ($automaticCustomisationQueries as $tmpSql) {
                $browsingControlModel->query($tmpSql);
            }
            
            // -------------------------------------------------------------------------------------------------------------
            // AUTOMATIC CUSTOMISATION: 
            // - structure - aliquot_masters
            // - structure - aliquot_master_edit_in_batchs
            // - structure - view_aliquot_joined_to_sample_and_collection
            // - structure - aliquotinternaluses
            // - structure - orders
            // - structure - orders_short
            // - structure - orderlines
            // - structure -tma_slides
            // - structure - tma_slide_uses
            // - structure - viewaliquotuses
            // -------------------------------------------------------------------------------------------------------------
            // RULES:
            // Display/hide study fields in structures used to display and manage data of a model considering property of the link between the model and the study model into the databrowser
            // (the flag_active_1_to_2 and flag_active_1_to_2 values). Rules applied to following links:
            // - Aliquot to study
            // - Aliquot Use to study
            // - TMA Slide to study
            // - TMA Slide Use to study
            // - Order Line to study
            // - Order to study
            // -------------------------------------------------------------------------------------------------------------
            // NOTES:
            // - The links to consent or misc identifier are managed by code above.
            // -------------------------------------------------------------------------------------------------------------
            $linkStudyConfig = array(
                "ViewAliquot" => array(
                    array(
                        'aliquot_masters',
                        'aliquot_master_edit_in_batchs',
                        'view_aliquot_joined_to_sample_and_collection'
                    ),
                    // aliquot_masters
                    $this->getStructureFormatUpdateSql(array(
                        'flag_search',
                        'flag_batchedit',
                        'flag_index',
                        'flag_detail'
                    ), '%%flag_value%%', array('aliquot_masters'), "SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL"),
                    $this->getStructureFormatUpdateSql(array(
                        'flag_add',
                        'flag_edit',
                        'flag_addgrid',
                        'flag_editgrid',
                        'flag_batchedit'
                    ), '%%flag_value%%', array('aliquot_masters'), "SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='autocomplete_aliquot_master_study_summary_id' AND `structure_value_domain`  IS NULL"),
                    // aliquot_master_edit_in_batchs
                    $this->getStructureFormatUpdateSql(array(
                        'flag_batchedit'
                    ), '%%flag_value%%', array('aliquot_master_edit_in_batchs'), "SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL"),
                    $this->getStructureFormatUpdateSql(array(
                        'flag_edit',
                        'flag_batchedit'
                    ), '%%flag_value%%', array('aliquot_master_edit_in_batchs'), "SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='autocomplete_aliquot_master_study_summary_id' AND `structure_value_domain`  IS NULL"),
                    $this->getStructureFormatUpdateSql(array(
                        'flag_edit',
                        'flag_batchedit'
                    ), '%%flag_value%%', array('aliquot_master_edit_in_batchs'), "SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_study_summary_id' AND `structure_value_domain`  IS NULL"),
                    // view_aliquot_joined_to_sample_and_collection
                    $this->getStructureFormatUpdateSql(array(
                        'flag_search',
                        'flag_index'
                    ), '%%flag_value%%', array('view_aliquot_joined_to_sample_and_collection'), "SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='study_summary_title' AND `structure_value_domain`  IS NULL")
                ),
                "ViewAliquotUse" => array(
                    array(
                        'aliquotinternaluses'
                    ),
                    // aliquotinternaluses
                    $this->getStructureFormatUpdateSql(array(
                        'flag_search',
                        'flag_index',
                        'flag_detail',
                        'flag_summary'
                    ), '%%flag_value%%', array('aliquotinternaluses'), "SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL"),
                    $this->getStructureFormatUpdateSql(array(
                        'flag_add',
                        'flag_edit',
                        'flag_addgrid'
                    ), '%%flag_value%%', array('aliquotinternaluses'), "SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='autocomplete_aliquot_internal_use_study_summary_id' AND `structure_value_domain`  IS NULL")
                ),
                "Order" => array(
                    array(
                        'orders',
                        'orders_short'
                    ),
                    // orders
                    $this->getStructureFormatUpdateSql(array(
                        'flag_search',
                        'flag_index',
                        'flag_detail',
                        'flag_summary'
                    ), '%%flag_value%%', array('orders'), "SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL"),
                    $this->getStructureFormatUpdateSql(array(
                        'flag_add',
                        'flag_edit'
                    ), '%%flag_value%%', array('orders'), "SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='autocomplete_order_study_summary_id' AND `structure_value_domain`  IS NULL"),
                    // orders_short
                    $this->getStructureFormatUpdateSql(array(
                        'flag_index'
                    ), '%%flag_value%%', array('orders_short'), "SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL")
                ),
                "OrderLine" => array(
                    array(
                        'orderlines'
                    ),
                    // orderlines
                    $this->getStructureFormatUpdateSql(array(
                        'flag_search',
                        'flag_index',
                        'flag_detail',
                        'flag_summary'
                    ), '%%flag_value%%', array('orderlines'), "SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL"),
                    $this->getStructureFormatUpdateSql(array(
                        'flag_add',
                        'flag_edit'
                    ), '%%flag_value%%', array('orderlines'), "SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='autocomplete_order_line_study_summary_id' AND `structure_value_domain`  IS NULL")
                ),
                "TmaSlide" => array(
                    array(
                        'tma_slides'
                    ),
                    // tma_slides
                    $this->getStructureFormatUpdateSql(array(
                        'flag_search',
                        'flag_index',
                        'flag_detail'
                    ), '%%flag_value%%', array('tma_slides'), "SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL"),
                    $this->getStructureFormatUpdateSql(array(
                        'flag_add',
                        'flag_edit',
                        'flag_addgrid',
                        'flag_editgrid',
                        'flag_editgrid_readonly'
                    ), '%%flag_value%%', array('tma_slides'), "SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='autocomplete_tma_slide_study_summary_id' AND `structure_value_domain`  IS NULL")
                ),
                "TmaSlideUse" => array(
                    array(
                        'tma_slide_uses'
                    ),
                    // tma_slide_uses
                    $this->getStructureFormatUpdateSql(array(
                        'flag_search',
                        'flag_index',
                        'flag_detail'
                    ), '%%flag_value%%', array('tma_slide_uses'), "SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL"),
                    $this->getStructureFormatUpdateSql(array(
                        'flag_add',
                        'flag_edit',
                        'flag_addgrid',
                        'flag_editgrid', 'flag_editgrid_readonly'
                    ), '%%flag_value%%', array('tma_slide_uses'), "SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='autocomplete_tma_slide_use_study_summary_id' AND `structure_value_domain`  IS NULL")
                ),
                "Order','OrderLine','ViewAliquotUse" => array(
                    array(
                        'viewaliquotuses'
                    ),
                    // viewaliquotuses
                    $this->getStructureFormatUpdateSql(array(
                        'flag_search',
                        'flag_index'
                    ), '%%flag_value%%', array('viewaliquotuses'), "SELECT id FROM structure_fields WHERE `model`='ViewAliquotUse' AND `tablename`='view_aliquot_uses' AND `field`='study_summary_title' AND `structure_value_domain`  IS NULL"),
                    $this->getStructureFormatUpdateSql(array(
                        'flag_override_label'
                    ), '%%flag_value%%', array('viewaliquotuses'), "SELECT id FROM structure_fields WHERE `model`='ViewAliquotUse' AND `tablename`='view_aliquot_uses' AND `field`='aliquot_volume_unit' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit') AND `flag_confidential`='0'")
                )
            );
            $fieldUpdateSummary = array(
                '0' => array(),
                '1' => array()
            );
            foreach ($linkStudyConfig as $model1 => $queriesToRun) {
                $tmpSql = "SELECT 'Found'
                FROM datamart_browsing_controls dbc
                INNER JOIN datamart_structures ds1 ON dbc.id1 = ds1.id
                INNER JOIN datamart_structures ds2 ON dbc.id2 = ds2.id
                WHERE ds2.model = 'StudySummary'
                AND ds1.model IN ('$model1')
                AND (dbc.flag_active_1_to_2 = 1 OR dbc.flag_active_1_to_2 = 1);";
                $flagValue = empty($browsingControlModel->query($tmpSql)) ? '0' : '1';
                $tablesNames = array_shift($queriesToRun);
                $fieldUpdateSummary[$flagValue] = array_merge($fieldUpdateSummary[$flagValue], $tablesNames);
                foreach ($queriesToRun as $tmpSql) {
                    $tmpSql = str_replace('%%flag_value%%', $flagValue, $tmpSql);
                    $browsingControlModel->query($tmpSql);
                }
            }
            if ($fieldUpdateSummary[1]) {
                $automaticCustomisationMessages['general'][] = __('set up structures to display study fields in following structures [%s] based on datamart configuration.', implode(', ', $fieldUpdateSummary[1]));
            }
            if ($fieldUpdateSummary[0]) {
                $automaticCustomisationMessages['general'][] = __('set up structures to hide study fields in following structures [%s] based on datamart configuration.', implode(', ', $fieldUpdateSummary[0]));
            }
            
            // -------------------------------------------------------------------------------------------------------------
            // AUTOMATIC CUSTOMISATION: datamart_structure_functions
            // -------------------------------------------------------------------------------------------------------------
            // RULES:
            // Update datamart_structure_functions based on different criteria
            // -------------------------------------------------------------------------------------------------------------
            // NOTES:
            // - N/A
            // -------------------------------------------------------------------------------------------------------------
            $automaticCustomisationQueries = array();
            
            switch (Configure::read('order_item_type_config')) {
                case '0':
                    // .....................................................................................
                    // Order tool not used - no item can be added to order
                    // .....................................................................................
                    $automaticCustomisationQueries[] = "UPDATE datamart_structure_functions fct, datamart_structures str SET fct.flag_active = '0' WHERE fct.datamart_structure_id = str.id AND str.model = 'TmaSlide' AND label = 'add to order';";
                    $automaticCustomisationQueries[] = "UPDATE datamart_structure_functions fct, datamart_structures str SET fct.flag_active = '0' WHERE fct.datamart_structure_id = str.id AND str.model = 'ViewAliquot' AND label = 'add to order';";
                    break;
                case '1':
                    // .....................................................................................
                    // Both tma slide and aliquot can be attached to an order.
                    // .....................................................................................
                    $automaticCustomisationQueries[] = "UPDATE datamart_structure_functions fct, datamart_structures str SET fct.flag_active = '1' WHERE fct.datamart_structure_id = str.id AND str.model = 'TmaSlide' AND label = 'add to order';";
                    $automaticCustomisationQueries[] = "UPDATE datamart_structure_functions fct, datamart_structures str SET fct.flag_active = '1' WHERE fct.datamart_structure_id = str.id AND str.model = 'ViewAliquot' AND label = 'add to order';";
                    break;
                case '2':
                    // .....................................................................................
                    // Only aliquot can be attached to an order.
                    // .....................................................................................
                    $automaticCustomisationQueries[] = "UPDATE datamart_structure_functions fct, datamart_structures str SET fct.flag_active = '0' WHERE fct.datamart_structure_id = str.id AND str.model = 'TmaSlide' AND label = 'add to order';";
                    $automaticCustomisationQueries[] = "UPDATE datamart_structure_functions fct, datamart_structures str SET fct.flag_active = '1' WHERE fct.datamart_structure_id = str.id AND str.model = 'ViewAliquot' AND label = 'add to order';";
                    break;
                case '3':
                    // .....................................................................................
                    // Only tma slide can be attached to an order.
                    // .....................................................................................
                    $automaticCustomisationQueries[] = "UPDATE datamart_structure_functions fct, datamart_structures str SET fct.flag_active = '1' WHERE fct.datamart_structure_id = str.id AND str.model = 'TmaSlide' AND label = 'add to order';";
                    $automaticCustomisationQueries[] = "UPDATE datamart_structure_functions fct, datamart_structures str SET fct.flag_active = '0' WHERE fct.datamart_structure_id = str.id AND str.model = 'ViewAliquot' AND label = 'add to order';";
                    break;
                default:
            }
            $automaticCustomisationMessages['general'][] = __("launch of 'add to order' actions have been updated based on the core variable 'order_item_type_config'.");
            
            foreach ($automaticCustomisationQueries as $tmpSql) {
                $browsingControlModel->query($tmpSql);
            }
            
            // -------------------------------------------------------------------------------------------------------------
            // AUTOMATIC CUSTOMISATION: Message Display
            // -------------------------------------------------------------------------------------------------------------
            
            if (isset($automaticCustomisationMessages['general'])) {
                AppController::addWarningMsg(__("following automatic customisations have been executed : %s", '<br><br># ' . implode('<br><br># ', $automaticCustomisationMessages['general'])));
            }
            if (isset($automaticCustomisationMessages['datamart_browsing_controls-activated'])) {
                AppController::addWarningMsg(__("datamart_browsing_controls has been updated to let people to search on following models : %s", '<br># ' . implode('.<br># ', $automaticCustomisationMessages['datamart_browsing_controls-activated']) . '.'));
            }
            if (isset($automaticCustomisationMessages['datamart_browsing_controls-disabled'])) {
                AppController::addWarningMsg(__("datamart_browsing_controls has been updated to not let people to search on following models : %s", '<br># ' . implode('.<br># ', $automaticCustomisationMessages['datamart_browsing_controls-disabled']) . '.'));
            }
            if (isset($automaticCustomisationMessages['menu'][0])) {
                AppController::addWarningMsg(__("following menus have been inactivated : %s.", '<br># ' . implode('.<br># ', $automaticCustomisationMessages['menu'][0])));
            }
            if (isset($automaticCustomisationMessages['menu'][1])) {
                AppController::addWarningMsg(__("following menus have been activated : %s.", '<br># ' . implode('.<br># ', $automaticCustomisationMessages['menu'][1])));
            }
            
        } else {
            AppController::addWarningMsg(__('letNewVersionSetupCustomisePartiallyATiM_not_active_message'));
        }
        
        // *** 15 *** Check date structure_field not attached to a tablename 
        // See issue #194: Date of detail model record with no table_name definition generate a warning [https://gitlab.com/ctrnet/atim/-/issues/194]
        
        $browsingControlModel = AppModel::getInstance('Datamart', 'BrowsingControl', true);
        $query = "SELECT structure_alias, structure_field_id, model, field, type
            FROM view_structure_formats_simplified View
            WHERE (tablename IS NULL OR tablename LIKE '')
            AND ( flag_add = '1' OR flag_edit = '1' OR flag_addgrid = '1' OR flag_editgrid = '1' OR flag_index = '1' OR flag_detail = '1' )
            AND type IN ('date', 'datetime')
            AND model NOT IN ('Generated', 'custom', '0', 'ViewCollection', 'ViewSample', 'ViewAliquot', 'ViewAliquotUse')
            ORDER BY structure_alias, model, field;";
        $msgDateField = array();
        $resDateFields = $browsingControlModel->query($query);
        foreach($resDateFields as $newDateField) {
            $msgDateField[] = __("%s field [%s] in form structure '%s.'", $newDateField['View']['type'], ($newDateField['View']['model'] .'-'. $newDateField['View']['field']), $newDateField['View']['structure_alias']);  
        }
        if ($msgDateField) {
            AppController::addWarningMsg(__("date accuracy feature can not be used on following field(s) because field tablename is probably not set. check and set field tablename for: <br>- %s", implode(',<br>- ', $msgDateField)));
        }
        
        // ------------------------------------------------------------------------------------------------------------------------------------------
        
        // update the permissions_regenerated flag and redirect
        $this->Version->data = array(
            'Version' => array(
                'permissions_regenerated' => $updatePermission
            )
        );
        $this->Version->checkWritableFields = false;
        
        
        if ($this->Version->save()) {
            // Check the old user_logs (the logs of the previous years)
            //ATTENTION: This function has a redirect if there is the user_logs to save so can not call this function in the middle of the code and should be at the end of the code.
            $this->userLogsCleanupBy6Month();
            $this->redirect('/Users/login');
        }
    }

    private function getDatamartBrowsingControlUpdateSql($flagValue, $model1, $model2, &$automaticCustomisationMessages = null)
    {
        if (! is_null($automaticCustomisationMessages)) {
            $automaticCustomisationMessages["datamart_browsing_controls-" . (($flagValue == '1') ? 'activated' : 'disabled')][] = ("$model1 & $model2");
        }
        return "UPDATE datamart_browsing_controls
                SET flag_active_1_to_2 = '$flagValue', flag_active_2_to_1 = '$flagValue'
                WHERE (id1 = (SELECT id FROM datamart_structures WHERE model = '$model1')
                AND id2 = (SELECT id FROM datamart_structures WHERE model = '$model2'))
                OR  (id2 = (SELECT id FROM datamart_structures WHERE model = '$model1')
                AND id1 = (SELECT id FROM datamart_structures WHERE model = '$model2'));";
    }

    private function getStructureFormatUpdateSql($flags, $flagValue, $structureAlias, $selectStructureFieldSql)
    {
        $flagSql = array();
        foreach ($flags as $flag) {
            $flagSql[] = "$flag = '$flagValue'";
        }
        $flagSql = implode(', ', $flagSql);
        $query = "";
        $structureAlias = "'" . implode("', '", $structureAlias) . "'";
        return "UPDATE structure_formats
                SET $flagSql
                WHERE structure_id IN (SELECT id FROM structures WHERE alias IN ($structureAlias))
                AND structure_field_id IN ($selectStructureFieldSql);";
    }
    
    /**
     *
     * @param $config
     */
    public function configureCsv($config)
    {
        $this->csvConfig = $config;
        $this->Session->write('Config.language', $config['config_language']);
    }

    /**
     *
     * @param $connection
     * @param array $options
     * @return string
     */
    public function getQueryLogs($connection, $options = array())
    {
        $db = ConnectionManager::getDataSource($connection);
        $log = $db->getLog();
        if ($log['count'] == 0) {
            $out = "";
        } else {
            $out = 'Total Time: ' . $log['time'] . '<br>Total Queries: ' . $log['count'] . '<br>';
            $out .= '<table class="debug-table">' . '<thead>' . '<tr>' . '<th>Query</th>' . '<th>Affected</th>' . '<th>Num. rows</th>' . '<th>Took (ms)</th>' . 
            // '<th>Actions</th>'.
            '</tr>';
            $class = 'odd';
            foreach ($log['log'] as $i => $value) {
                $out .= '<tr class="' . $class . '">' . '<td>' . $value['query'] . '</td>' . '<td>' . $value['affected'] . '</td>' . '<td>' . $value['numRows'] . '</td>' . '<td>' . $value['took'] . '</td>' . '</tr>';
                $class = ($class == 'even' ? 'odd' : 'even');
            }
            $out .= "</thead>" . "</table>";
        }
        return $out;
    }

    /**
     *
     * @param null $array
     * @return array
     */
    public static function convertArrayKeyFromSnakeToCamel($array = null)
    {
        $answer = array();
        if ($array) {
            foreach ($array as $key => $value) {
                $answer[Inflector::variable($key)] = $value;
            }
        }
        return $answer;
    }
}

AppController::init();

/**
 * Returns the date in a classic format (useful for SQL)
 *
 * @throws Exception
 */
function now()
{
    return date("Y-m-d H:i:s");
}

/**
 *
 * @param $errno
 * @param $errstr
 * @param $errfile
 * @param $errline
 * @param null $context
 * @return bool
 */
function myErrorHandler($errno, $errstr, $errfile, $errline, $context = null)
{
    if (class_exists("AppController")) {
        $controller = AppController::getInstance();
        if ($errno == E_USER_WARNING && strpos($errstr, "SQL Error:") !== false && $controller->name != 'Pages') {
            $traceMsg = "<table><tr><th>File</th><th>Line</th><th>Function</th></tr>";
            try {
                throw new Exception("");
            } catch (Exception $e) {
                $traceArr = $e->getTrace();
                foreach ($traceArr as $traceLine) {
                    if (is_array($traceLine)) {
                        $traceMsg .= "<tr><td>" . (isset($traceLine['file']) ? $traceLine['file'] : "") . "</td><td>" . (isset($traceLine['line']) ? $traceLine['line'] : "") . "</td><td>" . $traceLine['function'] . "</td></tr>";
                    } else {
                        $traceMsg .= "<tr><td></td><td></td><td></td></tr>";
                    }
                }
            }
            $traceMsg .= "</table>";
            $controller->redirect('/Pages/err_query?err_msg=' . urlencode($errstr . $traceMsg));
        }
    }
    return false;
}