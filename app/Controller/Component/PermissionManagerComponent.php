<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */

/**
 * Class PermissionManagerComponent
 */
class PermissionManagerComponent extends Component
{

    public $controller;

    public $log = array();

    /*
     * Specify the default permissions here
     * If there are no permissions in the DB these will be inserted.
     * NOTE: Don't allow the acos tables to be emptied or this will fail.
     */
    public $defaults = array(
        'controllers' => array(
            'allow' => array(
                'Group::1',
                'Group::2',
                'Group::3'
            ),
            'deny' => array()
        ),
        'controllers/Administrate/Permissions' => array(
            'allow' => array(),
            'deny' => array(
                'Group::2',
                'Group::3'
            )
        )
    );

    public $hiddenPermissionsAndSubstitutions = array(
        'Controller/Administrate/AdminUsers/listall' => 'Controller/Administrate/AdminUsers/detail',
        'Controller/Administrate/AdminUsers/search' => 'Controller/Administrate/AdminUsers/detail',
        'Controller/Administrate/Announcements/index' => 'Controller/Administrate/Announcements/detail',
        'Controller/Administrate/Banks/index' => 'Controller/Administrate/Banks/detail',
        'Controller/Administrate/Dropdowns/configure' => 'Controller/Administrate/Dropdowns/add',
        'Controller/Administrate/Dropdowns/subIndex' => 'Controller/Administrate/Dropdowns/index',
        'Controller/Administrate/Dropdowns/view' => 'Controller/Administrate/Dropdowns/index',
        'Controller/Administrate/Groups/index' => 'Controller/Administrate/Groups/detail',
        'Controller/Administrate/Menus/' => null, // Not used? Controller to delete?
        'Controller/Administrate/PasswordsAdmin/' => null, // To hide PasswordsAdmin in permissions tree
        'Controller/Administrate/PasswordsAdmin/index' => 'Controller/Administrate/AdminUsers/add',
        'Controller/Administrate/Permissions/tree' => 'Controller/Administrate/Permissions/index',
        'Controller/Administrate/Permissions/updatePermission' => 'Controller/Administrate/Permissions/update',
        'Controller/Administrate/Permissions/loadPreset' => 'Controller/Administrate/Permissions/update',
        'Controller/Administrate/Permissions/regenerate' => 'Controller/Administrate/Permissions/update',
        'Controller/Administrate/Permissions/savePreset' => 'Controller/Administrate/Permissions/update',
        'Controller/Administrate/Permissions/addPermissionStateToThreadedData' => 'Controller/Administrate/Permissions/update',
        'Controller/Administrate/Permissions/deletePreset' => 'Controller/Administrate/Permissions/update',
        'Controller/Administrate/PreferencesAdmin/' => null,
        'Controller/Administrate/PreferencesAdmin/index' => 'Controller/Administrate/AdminUsers/detail',
        'Controller/Administrate/PreferencesAdmin/edit' => 'Controller/Administrate/AdminUsers/edit',
        'Controller/Administrate/StorageControls/changeActiveStatus' => 'Controller/Administrate/StorageControls/edit',
        'Controller/Administrate/StorageControls/seeStorageLayout' => 'Controller/Administrate/StorageControls/listAll',
        'Controller/Administrate/StructureFormats/' => null, // Not used? Controller to delete?
        'Controller/Administrate/Structures/' => null, // Not used? Controller to delete?
        'Controller/Administrate/UserLogs/' => null, // To hide UserLogs
        'Controller/Administrate/UserLogs/index' => 'Controller/Administrate/AdminUsers/detail',
        'Controller/Administrate/Versions/test' => null, // Not used? Controller to delete?
        'Controller/Administrate/Versions/latencyTest' => null, // Not used? Controller to delete?

        'Controller/App//' => null, // Not part of the permissions set by users. Don't display.

        'Controller/ClinicalAnnotation/ClinicalCollectionLinks/listall' => 'Controller/ClinicalAnnotation/ClinicalCollectionLinks/detail',
        'Controller/ClinicalAnnotation/ConsentMasters/listall' => 'Controller/ClinicalAnnotation/ConsentMasters/detail',
        'Controller/ClinicalAnnotation/DiagnosisMasters/listall' => 'Controller/ClinicalAnnotation/DiagnosisMasters/detail',
        'Controller/ClinicalAnnotation/DiagnosisMasters/setDiagnosisMenu' => 'Controller/ClinicalAnnotation/DiagnosisMasters/detail',
        'Controller/ClinicalAnnotation/EventMasters/listall' => 'Controller/ClinicalAnnotation/EventMasters/detail',
        'Controller/ClinicalAnnotation/FamilyHistories/listall' => 'Controller/ClinicalAnnotation/FamilyHistories/detail',
        'Controller/ClinicalAnnotation/MiscIdentifiers/reuse' => 'Controller/ClinicalAnnotation/MiscIdentifiers/add',
        'Controller/ClinicalAnnotation/MiscIdentifiers/search' => 'Controller/ClinicalAnnotation/MiscIdentifiers/listall',
        'Controller/ClinicalAnnotation/ParticipantContacts/listall' => 'Controller/ClinicalAnnotation/ParticipantContacts/detail',
        'Controller/ClinicalAnnotation/ParticipantMessages/listall' => 'Controller/ClinicalAnnotation/ParticipantMessages/detail',
        'Controller/ClinicalAnnotation/ParticipantMessages/search' => 'Controller/ClinicalAnnotation/ParticipantMessages/detail',
        'Controller/ClinicalAnnotation/Participants/batchEdit' => null, // Not used? Make no sens to use it. Function to delete?
        'Controller/ClinicalAnnotation/Participants/chronology' => 'Controller/ClinicalAnnotation/Participants/profile',
        'Controller/ClinicalAnnotation/Participants/search' => 'Controller/ClinicalAnnotation/Participants/profile',
        'Controller/ClinicalAnnotation/ReproductiveHistories/listall' => 'Controller/ClinicalAnnotation/ReproductiveHistories/detail',
        'Controller/ClinicalAnnotation/TreatmentExtendMasters/' => null, // To hide TreatmentExtendMasters in permissions tree
        'Controller/ClinicalAnnotation/TreatmentExtendMasters/add' => 'Controller/ClinicalAnnotation/TreatmentMasters/edit',
        'Controller/ClinicalAnnotation/TreatmentExtendMasters/edit' => 'Controller/ClinicalAnnotation/TreatmentMasters/edit',
        'Controller/ClinicalAnnotation/TreatmentExtendMasters/delete' => 'Controller/ClinicalAnnotation/TreatmentMasters/edit',
        'Controller/ClinicalAnnotation/TreatmentExtendMasters/importDrugFromChemoProtocol' => 'Controller/ClinicalAnnotation/TreatmentMasters/edit',
        'Controller/ClinicalAnnotation/TreatmentMasters/listall' => 'Controller/ClinicalAnnotation/TreatmentMasters/detail',

        'Controller/CodingIcd/CodingIcd10s/search' => 'Controller/CodingIcd/CodingIcd10s/tool',
        'Controller/CodingIcd/CodingIcd10s/autocomplete' => 'Controller/CodingIcd/CodingIcd10s/tool',
        'Controller/CodingIcd/CodingIcd10s/getIcd10Type' => 'Controller/CodingIcd/CodingIcd10s/tool',
        'Controller/CodingIcd/CodingIcdo3s/search' => 'Controller/CodingIcd/CodingIcdo3s/tool',
        'Controller/CodingIcd/CodingIcdo3s/autocomplete' => 'Controller/CodingIcd/CodingIcdo3s/tool',
        'Controller/CodingIcd/CodingIcdo3s/getIcdo3Type' => 'Controller/CodingIcd/CodingIcdo3s/tool',

        'Controller/Customize/UserAnnouncements/index' => 'Controller/Customize/UserAnnouncements/detail',

        'Controller/Datamart/BatchSets/listall' => 'Controller/Datamart/BatchSets/index',
        'Controller/Datamart/BatchSets/deleteInBatch' => 'Controller/Datamart/BatchSets/delete',
        'Controller/Datamart/BatchSets/remove' => 'Controller/Datamart/BatchSets/edit',
        'Controller/Datamart/BatchSets/save' => 'Controller/Datamart/BatchSets/edit',
        'Controller/Datamart/BatchSets/unlock' => 'Controller/Datamart/BatchSets/edit',

        'Controller/Datamart/Browser/getIdsAndParentChild' => 'Controller/Datamart/Browser/index',
        'Controller/Datamart/Browser/browse' => 'Controller/Datamart/Browser/edit',
        'Controller/Datamart/Browser/csv' => 'Controller/Datamart/Csv/csv',
        'Controller/Datamart/Browser/batchToDatabrowser' => 'Controller/Datamart/Browser/edit',
        'Controller/Datamart/Browser/save' => 'Controller/Datamart/Browser/edit',
        'Controller/Datamart/Browser/unusedParent' => 'Controller/Datamart/Browser/edit',
        'Controller/Datamart/Browser/applyBrowsingSteps' => 'Controller/Datamart/Browser/edit',
        'Controller/Datamart/Browser/getDataMartDiagram' => 'Controller/Datamart/Browser/index',
        'Controller/Datamart/BrowsingSteps/' => 'Controller/Datamart/Browser/index',
        'Controller/Datamart/BrowsingSteps/listall' => 'Controller/Datamart/Browser/index',
        'Controller/Datamart/BrowsingSteps/save' => 'Controller/Datamart/Browser/edit',
        'Controller/Datamart/BrowsingSteps/edit' => 'Controller/Datamart/Browser/edit',
        'Controller/Datamart/BrowsingSteps/delete' => 'Controller/Datamart/Browser/delete',

        'Controller/Datamart/Reports/compareToBatchSetOrNode' => null, // Tested in controller. User has to have access to 'Controller/Datamart/BatchSets/listall &' => 'Controller/Datamart/Browser/index'
        'Controller/Datamart/Reports/manageReport' => 'Controller/Datamart/Reports/index',

        'Controller/DebugKit//' => null, // Not part of the permissions set by users. Don't display.

        'Controller/Drug/Drugs/autocompleteDrug' => 'Controller/Drug/Drugs/detail',
        'Controller/Drug/Drugs/search' => 'Controller/Drug/Drugs/detail',

        'Controller/InventoryManagement/AliquotMasters/search' => 'Controller/InventoryManagement/AliquotMasters/detail',
        'Controller/InventoryManagement/AliquotMasters/addInit' => 'Controller/InventoryManagement/AliquotMasters/add',
        'Controller/InventoryManagement/AliquotMasters/removeAliquotFromStorage' => 'Controller/InventoryManagement/AliquotMasters/edit',
        'Controller/InventoryManagement/AliquotMasters/editAliquotInternalUse' => 'Controller/InventoryManagement/AliquotMasters/addAliquotInternalUse',
        'Controller/InventoryManagement/AliquotMasters/deleteAliquotInternalUse' => 'Controller/InventoryManagement/AliquotMasters/addAliquotInternalUse',
        'Controller/InventoryManagement/AliquotMasters/addInternalUseToManyAliquots' => 'Controller/InventoryManagement/AliquotMasters/addAliquotInternalUse',
        'Controller/InventoryManagement/AliquotMasters/addSourceAliquots' => 'Controller/InventoryManagement/SampleMasters/add',
        'Controller/InventoryManagement/AliquotMasters/editSourceAliquot' => 'Controller/InventoryManagement/SampleMasters/edit',
        'Controller/InventoryManagement/AliquotMasters/deleteSourceAliquot' => 'Controller/InventoryManagement/SampleMasters/delete',
        'Controller/InventoryManagement/AliquotMasters/realiquotInit' => 'Controller/InventoryManagement/AliquotMasters/add',
        'Controller/InventoryManagement/AliquotMasters/realiquotInit2' => 'Controller/InventoryManagement/AliquotMasters/add',
        'Controller/InventoryManagement/AliquotMasters/realiquot' => 'Controller/InventoryManagement/AliquotMasters/add',
        'Controller/InventoryManagement/AliquotMasters/defineRealiquotedChildren' => 'Controller/InventoryManagement/AliquotMasters/add',
        'Controller/InventoryManagement/AliquotMasters/listAllRealiquotedParents' => 'Controller/InventoryManagement/AliquotMasters/detail',
        'Controller/InventoryManagement/AliquotMasters/editRealiquoting' => 'Controller/InventoryManagement/AliquotMasters/edit',
        'Controller/InventoryManagement/AliquotMasters/deleteRealiquotingData' => 'Controller/InventoryManagement/AliquotMasters/delete',
        'Controller/InventoryManagement/AliquotMasters/autocompleteBarcode' => 'Controller/InventoryManagement/AliquotMasters/detail',
        'Controller/InventoryManagement/AliquotMasters/contentTreeView' => 'Controller/InventoryManagement/AliquotMasters/detail',
        'Controller/InventoryManagement/AliquotMasters/editInBatch' => 'Controller/InventoryManagement/AliquotMasters/edit',
        'Controller/InventoryManagement/AliquotMasters/listallUses' => 'Controller/InventoryManagement/AliquotMasters/detailAliquotInternalUse',
        'Controller/InventoryManagement/AliquotMasters/storageHistory' => 'Controller/InventoryManagement/AliquotMasters/detail',
        'Controller/InventoryManagement/AliquotMasters/printBarcodes' => 'Controller/InventoryManagement/AliquotMasters/detail',
        
        'Controller/InventoryManagement/Collections/search' => 'Controller/InventoryManagement/Collections/detail',
        'Controller/InventoryManagement/Collections/template' => 'Controller/InventoryManagement/SampleMasters/add',
        'Controller/InventoryManagement/Collections/templateInit' => 'Controller/InventoryManagement/SampleMasters/add',
        
        'Controller/InventoryManagement/QualityCtrls/addInit' => 'Controller/InventoryManagement/QualityCtrls/add',
        'Controller/InventoryManagement/QualityCtrls/listAll' => 'Controller/InventoryManagement/QualityCtrls/detail',
        
        'Controller/InventoryManagement/SampleMasters/contentTreeView' => 'Controller/InventoryManagement/SampleMasters/detail',
        'Controller/InventoryManagement/SampleMasters/listAllSampleAliquots' => 'Controller/InventoryManagement/AliquotMasters/detail',
        'Controller/InventoryManagement/SampleMasters/listAllDerivatives' => 'Controller/InventoryManagement/SampleMasters/detail',
        'Controller/InventoryManagement/SampleMasters/batchDerivativeInit' => 'Controller/InventoryManagement/SampleMasters/add',
        'Controller/InventoryManagement/SampleMasters/batchDerivativeInit2' => 'Controller/InventoryManagement/SampleMasters/add',
        'Controller/InventoryManagement/SampleMasters/batchDerivative' => 'Controller/InventoryManagement/SampleMasters/add',
        'Controller/InventoryManagement/SampleMasters/search' => 'Controller/InventoryManagement/SampleMasters/detail',
        
        'Controller/InventoryManagement/SpecimenReviews/listAll' => 'Controller/InventoryManagement/SpecimenReviews/detail',

        'Controller/LabBook/LabBookMasters/search' => 'Controller/LabBook/LabBookMasters/detail',
        'Controller/LabBook/LabBookMasters/editSynchOptions' => 'Controller/LabBook/LabBookMasters/edit',
        'Controller/LabBook/LabBookMasters/autocomplete' => 'Controller/LabBook/LabBookMasters/detail',

        'Controller/Material/Materials/index' => 'Controller/Material/Materials/detail',
        'Controller/Material/Materials/search' => 'Controller/Material/Materials/detail',

        'Controller/Order/OrderItems/' => null, // To hide OrderItems in permissions tree
        'Controller/Order/OrderItems/search' => 'Controller/Order/Orders/detail',
        'Controller/Order/OrderItems/listall' => 'Controller/Order/Orders/detail',
        'Controller/Order/OrderItems/listAllOrderItemsLinkedToOneObject' => 'Controller/Order/Orders/detail',
        'Controller/Order/OrderItems/add' => 'Controller/Order/Orders/edit',
        'Controller/Order/OrderItems/addAliquotsInBatch' => 'Controller/Order/Orders/edit',
        'Controller/Order/OrderItems/addOrderItemsInBatch' => 'Controller/Order/Orders/edit',
        'Controller/Order/OrderItems/edit' => 'Controller/Order/Orders/edit',
        'Controller/Order/OrderItems/editInBatch' => 'Controller/Order/Orders/edit',
        'Controller/Order/OrderItems/delete' => 'Controller/Order/Orders/edit',
        'Controller/Order/OrderItems/defineOrderItemsReturned' => 'Controller/Order/Orders/edit',
        'Controller/Order/OrderItems/removeFlagReturned' => 'Controller/Order/Orders/edit',
        'Controller/Order/OrderLines/' => 'Controller/Order/Orders', // To hide OrderLines in permissions tree
        'Controller/Order/OrderLines/listall' => 'Controller/Order/Orders/detail',
        'Controller/Order/OrderLines/add' => 'Controller/Order/Orders/edit',
        'Controller/Order/OrderLines/edit' => 'Controller/Order/Orders/edit',
        'Controller/Order/OrderLines/detail' => 'Controller/Order/Orders/detail',
        'Controller/Order/OrderLines/delete' => 'Controller/Order/Orders/edit',
        'Controller/Order/Orders/search' => 'Controller/Order/Orders/detail',
        'Controller/Order/Shipments/' => null, // To hide Shipments in permissions tree
        'Controller/Order/Shipments/search' => 'Controller/Order/Orders/detail',
        'Controller/Order/Shipments/listall' => 'Controller/Order/Orders/detail',
        'Controller/Order/Shipments/add' => 'Controller/Order/Orders/edit',
        'Controller/Order/Shipments/edit' => 'Controller/Order/Orders/edit',
        'Controller/Order/Shipments/detail' => 'Controller/Order/Orders/detail',
        'Controller/Order/Shipments/delete' => 'Controller/Order/Orders/edit',
        'Controller/Order/Shipments/addToShipment' => 'Controller/Order/Orders/edit',
        'Controller/Order/Shipments/formatDataForShippedItemsSelection' => 'Controller/Order/Orders/edit',
        'Controller/Order/Shipments/deleteFromShipment' => 'Controller/Order/Orders/edit',
        'Controller/Order/Shipments/manageContact' => 'Controller/Order/Orders/edit',
        'Controller/Order/Shipments/saveContact' => 'Controller/Order/Orders/edit',
        'Controller/Order/Shipments/deleteContact' => 'Controller/Order/Orders/edit',

        'Controller/Protocol/ProtocolExtendMasters/' => null, // To hide ProtocolExtendMasters in permissions tree
        'Controller/Protocol/ProtocolExtendMasters/listall' => 'Controller/Protocol/ProtocolMasters/detail',
        'Controller/Protocol/ProtocolExtendMasters/detail' => 'Controller/Protocol/ProtocolMasters/detail',
        'Controller/Protocol/ProtocolExtendMasters/add' => 'Controller/Protocol/ProtocolMasters/edit',
        'Controller/Protocol/ProtocolExtendMasters/edit' => 'Controller/Protocol/ProtocolMasters/edit',
        'Controller/Protocol/ProtocolExtendMasters/delete' => 'Controller/Protocol/ProtocolMasters/edit',
        'Controller/Protocol/ProtocolMasters/search' => 'Controller/Protocol/ProtocolMasters/detail',

        'Controller/RtbForm//' => null, // Not used?

        'Controller/Sop/SopExtends/' => null, // To hide SopExtends in permissions tree
        'Controller/Sop/SopExtends/listall' => 'Controller/Sop/SopMasters/detail',
        'Controller/Sop/SopExtends/detail' => 'Controller/Sop/SopMasters/detail',
        'Controller/Sop/SopExtends/add' => 'Controller/Sop/SopMasters/edit',
        'Controller/Sop/SopExtends/edit' => 'Controller/Sop/SopMasters/edit',
        'Controller/Sop/SopExtends/delete' => 'Controller/Sop/SopMasters/edit',
        'Controller/Sop/SopMasters/listall' => 'Controller/Sop/SopMasters/detail',

        'Controller/StorageLayout/StorageCoordinates/' => null, // To hide StorageCoordinates in permissions tree
        'Controller/StorageLayout/StorageCoordinates/listAll' => 'Controller/StorageLayout/StorageMasters/detail',
        'Controller/StorageLayout/StorageCoordinates/add' => 'Controller/StorageLayout/StorageMasters/edit',
        'Controller/StorageLayout/StorageCoordinates/delete' => 'Controller/StorageLayout/StorageMasters/edit',
        'Controller/StorageLayout/StorageMasters/search' => 'Controller/StorageLayout/StorageMasters/detail',
        'Controller/StorageLayout/StorageMasters/contentTreeView' => 'Controller/StorageLayout/StorageMasters/detail',
        'Controller/StorageLayout/StorageMasters/storageLayout' => 'Controller/StorageLayout/StorageMasters/edit',
        'Controller/StorageLayout/StorageMasters/autocompleteLabel' => 'Controller/StorageLayout/StorageMasters/detail',
        'Controller/StorageLayout/StorageMasters/contentListView' => 'Controller/StorageLayout/StorageMasters/detail',
        'Controller/StorageLayout/StorageMasters/getAliquotDetail' => 'Controller/InventoryManagement/AliquotMasters/detail',
        'Controller/StorageLayout/StorageMasters/getCsvFile' => 'Controller/Datamart/Csv/csv',

        'Controller/StorageLayout/TmaSlideUses/' => null, // To hide TmaSlideUses in permissions tree
        'Controller/StorageLayout/TmaSlideUses/add' => 'Controller/StorageLayout/TmaSlides/edit',
        'Controller/StorageLayout/TmaSlideUses/listAll' => 'Controller/StorageLayout/TmaSlides/detail',
        'Controller/StorageLayout/TmaSlideUses/edit' => 'Controller/StorageLayout/TmaSlides/edit',
        'Controller/StorageLayout/TmaSlideUses/editInBatch' => 'Controller/StorageLayout/TmaSlides/edit',
        'Controller/StorageLayout/TmaSlideUses/delete' => 'Controller/StorageLayout/TmaSlides/edit',
        'Controller/StorageLayout/TmaSlides/listAll' => 'Controller/StorageLayout/TmaSlides/detail',
        'Controller/StorageLayout/TmaSlides/editInBatch' => 'Controller/StorageLayout/TmaSlides/edit',
        'Controller/StorageLayout/TmaSlides/autocompleteBarcode' => 'Controller/StorageLayout/TmaSlides/detail',
        'Controller/StorageLayout/TmaSlides/autocompleteTmaSlideImmunochemistry' => 'Controller/StorageLayout/TmaSlides/add',

        'Controller/Study/StudyContacts/' => null, // Not used?
        'Controller/Study/StudyEthicsBoards/' => null, // Not used?
        'Controller/Study/StudyFundings/' => null, // To hide SopExtends in permissions tree
        'Controller/Study/StudyFundings/add' => 'Controller/Study/StudySummaries/edit',
        'Controller/Study/StudyFundings/listall' => 'Controller/Study/StudySummaries/detail',
        'Controller/Study/StudyFundings/detail' => 'Controller/Study/StudySummaries/detail',
        'Controller/Study/StudyFundings/edit' => 'Controller/Study/StudySummaries/edit',
        'Controller/Study/StudyFundings/delete' => 'Controller/Study/StudySummaries/edit',
        'Controller/Study/StudyInvestigators/' => null, // To hide SopExtends in permissions tree
        'Controller/Study/StudyInvestigators/add' => 'Controller/Study/StudySummaries/edit',
        'Controller/Study/StudyInvestigators/listall' => 'Controller/Study/StudySummaries/detail',
        'Controller/Study/StudyInvestigators/detail' => 'Controller/Study/StudySummaries/detail',
        'Controller/Study/StudyInvestigators/edit' => 'Controller/Study/StudySummaries/edit',
        'Controller/Study/StudyInvestigators/delete' => 'Controller/Study/StudySummaries/edit',
        'Controller/Study/StudyRelated/' => null, // Not used?
        'Controller/Study/StudyResults/' => null, // Not used?
        'Controller/Study/StudyReviews/' => null, // Not used?
        'Controller/Study/StudySummaries/search' => 'Controller/Study/StudySummaries/detail',
        'Controller/Study/StudySummaries/listAllLinkedRecords' => 'Controller/Study/StudySummaries/detail',
        'Controller/Study/StudySummaries/autocompleteStudy' => 'Controller/Study/StudySummaries/detail',

        'Controller/Tools/CollectionProtocol/index' => 'Controller/Tools/CollectionProtocol/detail',
        'Controller/Tools/Template/listProtocolsAndTemplates' => 'Controller/Tools/Template/index',
        'Controller/Tools/Template/editProperties' => 'Controller/Tools/Template/edit',
        'Controller/Tools/Template/defaultValue' => 'Controller/Tools/Template/edit',
        'Controller/Tools/Template/formatedDefaultValue' => 'Controller/Tools/Template/index',

        'Controller/test_app//' => null // Not used?
    );

    /**
     *
     * @param Controller $controller
     */
    public function initialize(Controller $controller)
    {
        $this->log = array();
        $this->controller = $controller;

        // If there are no Aco entries, build the entire list.
        if (! $this->controller->Acl->Aco->find('count', array(
            'fields' => 'Aco.id'
        ))) {
            $this->controller->Acl->Aco->deleteAll(array()); // reset auto increment
            $this->buildAcl();
        }

        // If there are no permissions in the DB, set up the defaults
        if (! $this->controller->Acl->Aco->Permission->find('count', array(
            'fields' => 'Permission.id'
        ))) {
            $this->controller->Acl->Aco->deleteAll(array()); // reset auto increment
            $this->initDB();
        }
    }

    /**
     * initiate PERMISSIONS
     */
    public function initDB()
    {
        $group = & $this->controller->User->Group;
        $user = & $this->controller->User;

        foreach ($this->defaults as $alias => $perms) {

            if (isset($perms['allow']) && count($perms['allow'])) {
                foreach ($perms['allow'] as $userAlias) {
                    list ($type, $id) = explode('::', $userAlias);

                    switch ($type) {
                        case 'Group':
                            $group->id = $id;
                            $this->controller->Acl->allow($group, $alias);
                            break;
                        case 'User':
                            $user->id = $id;
                            $this->controller->Acl->allow($user, $alias);
                            break;
                    }
                }
            }
            if (isset($perms['deny']) && count($perms['deny'])) {
                foreach ($perms['deny'] as $userAlias) {
                    list ($type, $id) = explode('::', $userAlias);

                    switch ($type) {
                        case 'Group':
                            $group->id = $id;
                            $this->controller->Acl->deny($group, $alias);
                            break;
                        case 'User':
                            $user->id = $id;
                            $this->controller->Acl->deny($user, $alias);
                            break;
                    }
                }
            }
        }
    }

    /**
     *
     * @param $plugin
     * @param $ctrlName
     * @return array|bool|mixed
     */
    public function getControllerMethods($plugin, $ctrlName)
    {
        if (! $plugin || $plugin == 'App') {
            $filePath = APP . 'Controller' . DS . $ctrlName . '.php';
        } else {
            $filePath = APP . 'Plugin' . DS . $plugin . DS . 'Controller' . DS . $ctrlName . 'Controller.php';
        }

        if (! file_exists($filePath)) {
            return false;
        }

        $matches = array();
        preg_match_all('/function\s+(\w+)\s*\(/', file_get_contents($filePath), $matches);

        $methods = $matches[1];

        if (! $plugin || $plugin == 'App') {
            $filePath = APP . 'Controller' . DS . 'Custom' . DS . $ctrlName . 'Controller.php';
        } else {
            $filePath = APP . 'Plugin' . DS . $plugin . DS . 'Controller' . DS . 'Custom' . DS . $ctrlName . 'Controller.php';
        }

        if (file_exists($filePath)) {

            $matches = array();
            preg_match_all('/function\s+(\w+)\s*\(/', file_get_contents($filePath), $matches);

            foreach ($matches[1] as $match) {
                if (! in_array($match, $methods)) {
                    $methods[] = $match;
                }
            }
        }
        return $methods;
    }

    /**
     * Rebuild the Acl based on the current controllers in the application
     *
     * @return void
     */
    public function buildAcl()
    {
        $aco = & $this->controller->Acl->Aco;

        $controllers = App::objects('controller');

        $appIndex = array_search('App', $controllers);
        if ($appIndex !== false) {
            unset($controllers[$appIndex]);
        }
        foreach ($controllers as $i => $name) {
            if ($name !== 'App')
                $controllers[$i] = 'App.' . $name;
        }
        // call FUNCTION to get APP.PLUGIN.CONTROLLER list, and append to APP.CONTROLLER list
        $plugins = $this->getPluginControllerNames();
        $controllers = array_merge($controllers, $plugins);
        asort($controllers);

        $plugins = array();
        foreach ($controllers as $ctrlName) {
            $plugin = preg_match('/^.+\..*$/', $ctrlName) ? preg_replace('/^(.+)\..*$/', '\1', $ctrlName) : 'App';
            $ctrlName = preg_replace('/^.+\./', '', $ctrlName);

            if (! isset($plugins[$plugin])) {
                $plugins[$plugin] = array();
            }
            $plugins[$plugin][] = $ctrlName;
        }

        $this->log = array();
        $root = $aco->node('Controller');
        if (! $root) {
            $aco->create(array(
                'parent_id' => null,
                'model' => null,
                'alias' => 'Controller',
                'flag_use_permission' => '1'
            ));
            $root = $aco->save();
            $root['Aco']['id'] = $aco->id;
            $this->log[] = 'Created Aco node for Controller';
        } else {
            $root = $root[0];
        }

        $baseMethods = get_class_methods('AppController');
        $baseMethods[] = 'buildAcl';

        // look at each controller in app/controllers
        $pluginNodeIds = array();
        foreach ($plugins as $plugin => $controllers) {
            // find / make controller node
            $url = 'Controller/' . $plugin;
            $pluginNode = $aco->node($url);
            $displayPermissionsInTreeView = $this->displayPermissionsInTreeView($url);
            if (! $pluginNode) {
                $aco->create(array(
                    'parent_id' => $root['Aco']['id'],
                    'model' => null,
                    'alias' => $plugin,
                    'flag_use_permission' => $displayPermissionsInTreeView
                ));
                $pluginNode = $aco->save();
                $pluginNode['Aco']['id'] = $aco->id;
                $this->log[] = 'Created Aco node for ' . $plugin;
            } else {
                $pluginNode = $pluginNode[0];
                if ($pluginNode['Aco']['flag_use_permission'] != $displayPermissionsInTreeView) {
                    $aco->data = null;
                    $aco->id = $pluginNode['Aco']['id'];
                    $aco->save(array(
                        'Aco' => array(
                            'flag_use_permission' => $displayPermissionsInTreeView
                        )
                    ));
                }
            }
            $pluginNodeIds[] = $pluginNode['Aco']['id'];

            $controllerNodeIds = array();

            foreach ($controllers as $ctrlName) {
                $methods = $this->getControllerMethods($plugin, $ctrlName);
                if ($methods === false) {
                    $this->log[] = $plugin . '.' . $ctrlName . ' could not be located.' . print_r($methods, 1);
                    continue;
                }
                $url = 'Controller/' . $plugin . '/' . $ctrlName;
                $controllerNode = $aco->node($url);
                $displayPermissionsInTreeView = $this->displayPermissionsInTreeView($url);
                if (! $controllerNode) {
                    $aco->create(array(
                        'parent_id' => $pluginNode['Aco']['id'],
                        'model' => null,
                        'alias' => $ctrlName,
                        'flag_use_permission' => $displayPermissionsInTreeView
                    ));
                    $controllerNode = $aco->save();
                    $controllerNode['Aco']['id'] = $aco->id;
                    $this->log[] = 'Created Aco node for ' . $plugin . '.' . $ctrlName;
                } else {
                    $controllerNode = $controllerNode[0];
                    if ($controllerNode['Aco']['flag_use_permission'] != $displayPermissionsInTreeView) {
                        $aco->data = null;
                        $aco->id = $controllerNode['Aco']['id'];
                        $aco->save(array(
                            'Aco' => array(
                                'flag_use_permission' => $displayPermissionsInTreeView
                            )
                        ));
                    }
                }
                $controllerNodeIds[] = $controllerNode['Aco']['id'];

                $methodNodeIds = array();

                // clean the methods. to remove those in Controller and private actions.
                foreach ($methods as $k => $method) {
                    if (strpos($method, '_', 0) === 0) {
                        unset($methods[$k]);
                        continue;
                    }
                    if (in_array($method, $baseMethods)) {
                        unset($methods[$k]);
                        continue;
                    }
                    $url = 'Controller/' . $plugin . '/' . $ctrlName . '/' . $method;
                    $methodNode = $aco->node($url);
                    $displayPermissionsInTreeView = $this->displayPermissionsInTreeView($url);
                    if (! $methodNode) {
                        $aco->create(array(
                            'parent_id' => $controllerNode['Aco']['id'],
                            'model' => null,
                            'alias' => $method,
                            'plugin' => $plugin,
                            'flag_use_permission' => $displayPermissionsInTreeView
                        ));
                        $aco->id = null;
                        $methodNode = $aco->save();
                        $this->log[] = 'Created Aco node for ' . $plugin . '.' . $ctrlName . '.' . $method;
                        $methodNodeIds[] = $aco->id;
                    } else {
                        $methodNodeIds[] = $methodNode[0]['Aco']['id'];
                        if ($methodNode[0]['Aco']['flag_use_permission'] != $displayPermissionsInTreeView) {
                            $aco->data = null;
                            $aco->id = $methodNode[0]['Aco']['id'];
                            $aco->save(array(
                                'Aco' => array(
                                    'flag_use_permission' => $displayPermissionsInTreeView
                                )
                            ));
                        }
                    }
                }

                $this->removeMissingNodes('controllers/' . $plugin . '/' . $ctrlName, $methodNodeIds);
            }

            $this->removeMissingNodes('controllers/' . $plugin, $controllerNodeIds);
        }

        $this->removeMissingNodes('controllers', $pluginNodeIds);
    }

    /**
     *
     * @param $path
     * @param array $knownIds
     * @return bool
     */
    public function removeMissingNodes($path, $knownIds = array())
    {
        $aco = & $this->controller->Acl->Aco;

        $parentNode = $aco->node($path);
        if (! $parentNode)
            return false;

        $conditions = 'Aco.parent_id = "' . $parentNode[0]['Aco']['id'] . '"';
        if (count($knownIds))
            $conditions .= ' AND Aco.id NOT IN("' . join('","', $knownIds) . '")';

        $result = $aco->find('all', array(
            'conditions' => $conditions
        ));

        if (count($result)) {
            foreach ($result as $toRemove) {
                $alias = $toRemove['Aco']['alias'];
                $this->log[] = 'Removed Aco node ' . $path . '/' . $alias . ' (' . $toRemove['Aco']['id'] . ')';
                $aco->delete($toRemove['Aco']['id']);
            }
            return true;
        }

        return false;
    }

    /**
     * Get the names of the plugin controllers .
     *
     *
     * This function will get an array of the plugin controller names, and
     * also makes sure the controllers are available for us to get the
     * method names by doing an App::import for each plugin controller.
     *
     * @return array of plugin names.
     *        
     */
    public function getPluginControllerNames()
    {
        App::uses('Folder', 'Utility');
        $folder = new Folder();
        // Change directory to the plugins
        $folder->cd(APP . 'Plugin');
        // Get a list of the files that have a file name that ends
        // with controller.php
        $files = $folder->findRecursive('.*Controller\.php');
        // Get the list of plugins
        $plugins = App::objects('Plugin');

        // Loop through the controllers we found int the plugins directory
        foreach ($files as $f => $fileName) {
            // Get the base file name
            $pluginName = preg_replace('!^(.*' . preg_quote(DS) . ')(.+)(' . preg_quote(DS) . 'Controller)' . preg_quote(DS) . '(.*)Controller\.php!', '$2', $fileName);

            // Get the base file name
            $file = basename($fileName);

            // Get the controller name
            $file = Inflector::camelize(substr($file, 0, strlen($file) - strlen('Controller.php')));
            if (! preg_match('/^.*App$/', $file) && strpos(DS . 'plugins' . DS, $pluginName) === false) {
                $files[$f] = $pluginName . '.' . $file;
            } else {
                unset($files[$f]);
            }
        }
        return $files;
    }

    /**
     *
     * @param string $url
     * @return boolean
     */
    private function displayPermissionsInTreeView($url)
    {
        $hiddenPermissionsAndSubstitutions = $this->hiddenPermissionsAndSubstitutions;
        $urlToTest = $url . str_pad('', (3 - (mb_substr_count($url, '/'))), "/");
        return (array_key_exists($urlToTest, $hiddenPermissionsAndSubstitutions) ? '0' : '1');
    }

    /**
     *
     * @param string $url
     * @return string
     */
    public function getSubstitutedUrl($url)
    {
        return (array_key_exists($url, $this->hiddenPermissionsAndSubstitutions) && ! empty($this->hiddenPermissionsAndSubstitutions[$url])) ? $this->hiddenPermissionsAndSubstitutions[$url] : $url;
    }
}