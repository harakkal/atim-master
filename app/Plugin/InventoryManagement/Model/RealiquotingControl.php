<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */

/**
 * Class RealiquotingControl 
 * 
 * @deprecated Table realiquoting_controls should not be used any more. The table is not deleted in v2.7.3 to allow
 * the support of labbook for a short period of time until we replace lab book by a new feature. 
 */
class RealiquotingControl extends InventoryManagementAppModel
{

    public $belongsTo = array(
        'ParentAliquotControl' => array(
            'className' => 'InventoryManagement.AliquotControl',
            'foreignKey' => 'parent_aliquot_control_id'
        ),
        'ChildAliquotControl' => array(
            'className' => 'InventoryManagement.AliquotControl',
            'foreignKey' => 'child_aliquot_control_id'
        )
    );

    /**
     * Get all aliquot types (aliquot_control_id) linked to a sample type that could be created
     * from a specific aliquot type.
     *
     * @param $sampleControlId
     * @param $parentAliquotControlId
     * 
     * @deprecated Table realiquoting_controls should not be used any more. The table is not deleted in v2.7.3 to allow
     * the support of labbook for a short period of time until we replace lab book by a new feature. Function will return
     * all active AliquotControls,aliquot_control_id  that are linked to the sample type (sample_control_id).
     * 
     * @return array All active AliquotControls,aliquot_control_id  that are linked to the sample type (sample_control_id).
     */
    public function getAllowedChildrenCtrlId($sampleControlId, $parentAliquotControlId)
    {
        $ac = AppModel::getInstance("InventoryManagement", "AliquotControl", true);
        $allowedChildrenAliquotControlIds = $ac->find('list', array(
            'conditions' => array(
                "AliquotControl.sample_control_id" => $sampleControlId,
                "AliquotControl.flag_active" => 1
            ),
            'fields' => 'AliquotControl.id',
            'recursive' => - 1
        ));
        return $allowedChildrenAliquotControlIds;
    }

    /**
     * Return control id of the labbooks that could be linked to realiquoting process based on selected sample_control_id 
     * and aliquot_control_ids.
     *
     * @param $parentSampleCtrlId
     * @param $parentAliquotCtrlId
     * @param $childAliquotCtrlId
     * 
     * @deprecated Table realiquoting_controls should not be used any more. The table is not deleted in v2.7.3 to allow
     * the support of labbook for a short period of time until we replace lab book by a new feature. 
     * 
     * @return mixed
     */
    public function getLabBookCtrlId($parentSampleCtrlId, $parentAliquotCtrlId, $childAliquotCtrlId)
    {
        $criteria = array(
            'ParentAliquotControl.sample_control_id' => $parentSampleCtrlId,
            'ParentAliquotControl.id' => $parentAliquotCtrlId,
            'ParentAliquotControl.flag_active' => '1',
            'RealiquotingControl.flag_active' => '1',
            'ChildAliquotControl.sample_control_id' => $parentSampleCtrlId,
            'ChildAliquotControl.id' => $childAliquotCtrlId,
            'ChildAliquotControl.flag_active' => '1'
        );
        $realiquotingControlData = $this->find('first', array(
            'conditions' => $criteria
        ));
        return $realiquotingControlData['RealiquotingControl']['lab_book_control_id'];
    }
}