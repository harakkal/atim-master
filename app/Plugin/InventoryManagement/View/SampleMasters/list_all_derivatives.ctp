<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */
if (isset($allDerivativeSampleControlsData)) {

    // Manage main page display

    $hookLink = $this->Structures->hook('main');

    $counter = 0;
    $nbOfSampleControls = sizeof($allDerivativeSampleControlsData);
    foreach ($allDerivativeSampleControlsData as $sampleControlData) {
        $counter ++;
        $finalAtimStructure = array();
        $finalOptions = array(
            'type' => 'detail',
            'links' => array(),
            'data' => array(),
            'settings' => array(
                'language_heading' => __($sampleControlData['SampleControl']['sample_type']),
                'actions' => ($counter == $nbOfSampleControls) ? true : false
            ),
            'extras' => $this->Structures->ajaxIndex('InventoryManagement/SampleMasters/listAllDerivatives/' . $atimMenuVariables['Collection.id'] . '/' . $atimMenuVariables['SampleMaster.initial_specimen_sample_id'] . '/' . $sampleControlData['SampleControl']['id'])
        );

        // CUSTOM CODE
        if ($hookLink) {
            require ($hookLink);
        }

        // BUILD FORM
        $this->Structures->build($finalAtimStructure, $finalOptions);
    }
} else {

    // Manage specific derivatives type display

    $structureLinks = array();
    $structureLinks['index']['detail'] = '/InventoryManagement/SampleMasters/detail/%%Collection.id%%/%%SampleMaster.id%%';
    $structureLinks['index']['edit'] = '/InventoryManagement/SampleMasters/edit/%%Collection.id%%/%%SampleMaster.id%%';
    $structureLinks['index']['delete'] = '/InventoryManagement/SampleMasters/delete/%%Collection.id%%/%%SampleMaster.id%%';

    $finalAtimStructure = $atimStructure;
    $finalOptions = array(
        'type' => 'index',
        'links' => $structureLinks,
        'settings' => array(
            'actions' => false
        )
    );

    // CUSTOM CODE
    $hookLink = $this->Structures->hook('specific_derivative_type');
    if ($hookLink) {
        require ($hookLink);
    }

    // BUILD FORM
    $this->Structures->build($finalAtimStructure, $finalOptions);
}