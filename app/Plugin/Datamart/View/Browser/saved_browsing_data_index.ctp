<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */
$structureLinks = array(
    "index" => $links,
    "bottom" => array(
        "new browsing/search" => array(
            'link' => "/Datamart/Browser/browse/",
            'icon' => 'add'
        )
    )
);

$settings = array(
    'form_bottom' => false,
    'actions' => false
);
$this->Structures->build($atimStructure, array(
    'data' => $data,
    'type' => 'index',
    'links' => $structureLinks,
    'settings' => $settings
));
