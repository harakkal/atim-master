<?php
 /**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */
$structureLinks = array(
    "index" => array(
        'display browsing tree' => [
            'link' => "/Datamart/Browser/browse/%%BrowsingIndex.root_node_id%%",
            'icon' => 'detail'
        ],
        'edit accessibility' => [
            'link' => "/Datamart/Browser/edit/%%BrowsingIndex.id%%",
            'icon' => 'edit'
            ],
        'save browsing' => array(
            'link' => "/Datamart/Browser/save/%%BrowsingIndex.id%%",
            'icon' => 'disk'
        ),
        'delete browsing' => [
            'link' => "/Datamart/Browser/delete/%%BrowsingIndex.id%%",
            'icon' => 'delete'
        ]
    ),
    "bottom" => array(
        "new browsing/search" => array(
            'link' => "/Datamart/Browser/browse/",
            'icon' => 'add'
        )
    )
);

$settings = array(
    'header' => array(
        'title' => __('temporary browsing'),
        'description' => __('unsaved browsing trees that are automatically deleted when there are more than %d', $tmpBrowsingLimit)
    ),
    'form_bottom' => false,
    'actions' => false,
    'pagination' => false
);

$this->Structures->build($atimStructure, array(
    'data' => $tmpBrowsing,
    'type' => 'index',
    'links' => $structureLinks,
    'settings' => $settings
));

$settings = array(
    'header' => array(
        'title' => __('saved browsing'),
        'description' => __('saved browsing trees')
    ),
    'form_bottom' => false,
    'actions' => false,
);

$this->Structures->build([], array(
    'type' => 'detail',
    'settings' => $settings,
    'extras' => $this->Structures->ajaxIndex('Datamart/Browser/savedBrowsingDataIndex/'),
));

$settings = array(
    'header' => array(
        'title' => __('shared browsing'),
        'description' => __('shared browsing trees')
    ),
    'form_top' => false
);

$this->Structures->build([], array(
    'type' => 'detail',
    'settings' => $settings,
    'links' => $structureLinks,
    'extras' => $this->Structures->ajaxIndex('Datamart/Browser/savedBrowsingDataIndex/others'),
));