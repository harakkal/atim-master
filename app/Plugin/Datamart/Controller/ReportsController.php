<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */

/**
 * Class ReportsController
 */
class ReportsController extends DatamartAppController
{

    public $component = array(
        'Structures'
    );

    public $uses = array(
        "Datamart.Report",
        "Datamart.DatamartStructure",
        "Datamart.BrowsingResult",
        "Datamart.BatchSet",
        "Structure"
    );

    public $paginate = array(
        'Report' => array(
            'order' => 'Report.name ASC'
        )
    );

    // -------------------------------------------------------------------------------------------------------------------
    // SELECT APP . 'View' . DS . 'Elements' . DS vs BATCHSET OR NODE DISTRIBUTION (trunk report)
    // -------------------------------------------------------------------------------------------------------------------
    /**
     *
     * @param $typeOfObjectToCompare
     * @param $batchSetOrNodeIdToCompare
     * @param bool $csvCreation
     * @param null $previousCurrentNodeId
     */
    public function compareToBatchSetOrNode($typeOfObjectToCompare, $batchSetOrNodeIdToCompare, $csvCreation = false, $previousCurrentNodeId = null)
    {
        if ((! AppController::checkLinkPermission('/Datamart/BatchSets/listall')) || (! AppController::checkLinkPermission('/Datamart/Browser/index'))) {
            $this->atimFlashError(__('you need privileges to access this page'), 'javascript:history.back()');
        }

        // Get data of object to compare
        $comparedObjectDatamartStructureId = null;
        $comparedObjectElementIds = array();
        $selectedObjectTitle = null;
        switch ($typeOfObjectToCompare) {
            case 'batchset':
                // Get batch set data and check permissions on selected batch set
                $selectedBatchset = $this->BatchSet->getOrRedirect($batchSetOrNodeIdToCompare);
                if (! $this->BatchSet->isUserAuthorizedToRw($selectedBatchset, true))
                    return;
                if (! AppController::checkLinkPermission($selectedBatchset['DatamartStructure']['index_link'])) {
                    $this->atimFlashError(__("You are not authorized to access that location."), 'javascript:history.back()');
                    return;
                }
                $comparedObjectDatamartStructureId = $selectedBatchset['DatamartStructure']['id'];
                foreach ($selectedBatchset['BatchId'] as $tmp)
                    $comparedObjectElementIds[] = $tmp['lookup_id'];
                $selectedObjectTitle = 'batchset';
                break;
            case 'node':
                $selectedDatabrowserNode = $this->BrowsingResult->findById($batchSetOrNodeIdToCompare);
                $comparedObjectDatamartStructureId = $selectedDatabrowserNode['DatamartStructure']['id'];
                $comparedObjectElementIds = explode(',', $selectedDatabrowserNode['BrowsingResult']['id_csv']);
                $selectedObjectTitle = 'databrowser node';
                break;
            default:
                $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }

        // Get selected elements of either previous batchset or report or databrowser node
        $selectedElementsDatamartStructureData = null;
        $previouslyDisplayedObjectTitle = null;
        $tmpNodeOfSelectedElements = null;
        if ($previousCurrentNodeId) {
            // User just launched process to compare 2 nodes
            if (! empty($this->request->data))
                $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            // Launched process from databrowser node on selected elements
            $tmpNodeOfSelectedElements = $this->BrowsingResult->findById($previousCurrentNodeId);
            $selectedElementsDatamartStructureData = array(
                'DatamartStructure' => $tmpNodeOfSelectedElements['DatamartStructure']
            );
            $previouslyDisplayedObjectTitle = 'databrowser node';
        } elseif (empty($this->request->data)) {
            // Sort on displayed data based on selected field
            if (! array_key_exists('sort', $this->passedArgs))
                $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            $selectedElementsDatamartStructureData = $this->DatamartStructure->findById($_SESSION['compareToBatchSetOrNode']['datamart_structure_id']);
            $previouslyDisplayedObjectTitle = $_SESSION['compareToBatchSetOrNode']['previously_displayed_object_title'];
        } elseif (array_key_exists('Config', $this->request->data) && $csvCreation) {
            // Export data in csv
            $config = array_merge($this->request->data['Config'], (array_key_exists(0, $this->request->data) ? $this->request->data[0] : array()));
            unset($this->request->data[0]);
            unset($this->request->data['Config']);
            $this->configureCsv($config);
            $selectedElementsDatamartStructureData = $this->DatamartStructure->findById($_SESSION['compareToBatchSetOrNode']['datamart_structure_id']);
            $previouslyDisplayedObjectTitle = $_SESSION['compareToBatchSetOrNode']['previously_displayed_object_title'];
        } elseif (array_key_exists('Report', $this->request->data)) {
            // Launched process from report on selected elements
            $selectedElementsDatamartStructureData = $this->DatamartStructure->findById($this->request->data['Report']['datamart_structure_id']);
            $previouslyDisplayedObjectTitle = 'report';
        } elseif (array_key_exists('node', $this->request->data)) {
            // Launched process from databrowser node on selected elements
            $tmpNodeOfSelectedElements = $this->BrowsingResult->findById($this->request->data['node']['id']);
            $selectedElementsDatamartStructureData = array(
                'DatamartStructure' => $tmpNodeOfSelectedElements['DatamartStructure']
            );
            $previouslyDisplayedObjectTitle = 'databrowser node';
        } elseif (array_key_exists('BatchSet', $this->request->data)) {
            // Launched process from previous batchset on selected elements
            $tmpBatchsetOfSelectedElements = $this->BatchSet->getOrRedirect($this->request->data['BatchSet']['id']);
            if (! $this->BatchSet->isUserAuthorizedToRw($tmpBatchsetOfSelectedElements, true))
                return;
            $selectedElementsDatamartStructureData = array(
                'DatamartStructure' => $tmpBatchsetOfSelectedElements['DatamartStructure']
            );
            $previouslyDisplayedObjectTitle = 'batchset';
        }

        // Get shared datamart structure
        if (! $selectedElementsDatamartStructureData || ($selectedElementsDatamartStructureData['DatamartStructure']['id'] != $comparedObjectDatamartStructureId))
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        $datamartStructure = $selectedElementsDatamartStructureData['DatamartStructure']; // Same Datamart Structure
        $this->set('$datamartStructureId', $datamartStructure['id']);

        // Get selected elements ids
        $model = null;
        $lookupKeyName = null;
        $modelInstance = null;
        $controlForeignKey = null;
        $studiedElementIdsToExport = null;
        if ($datamartStructure['control_master_model']) {
            if (isset($this->request->data[$datamartStructure['model']])) {
                $modelInstance = AppModel::getInstance($datamartStructure['plugin'], $datamartStructure['model'], true);
                $model = $datamartStructure['model'];
                $lookupKeyName = $modelInstance->primaryKey;
            } else {
                $modelInstance = AppModel::getInstance($datamartStructure['plugin'], $datamartStructure['control_master_model'], true);
                $model = $datamartStructure['control_master_model'];
                $lookupKeyName = $modelInstance->primaryKey;
            }
            $controlForeignKey = $modelInstance->getControlForeign();
        } else {
            $model = $datamartStructure['model'];
            $modelInstance = AppModel::getInstance($datamartStructure['plugin'], $datamartStructure['model'], true);
            $lookupKeyName = $modelInstance->primaryKey;
        }
        if ($csvCreation) {
            // Export data to csv
            $studiedElementIdsToExport = $this->request->data[$model][$lookupKeyName];
            $this->request->data[$model][$lookupKeyName] = explode(",", $_SESSION['compareToBatchSetOrNode']['selected_elements_ids']);
            // Nothing to do, selected elements are already submitted by form and recorded into $this->request->data[ $model ][ $lookupKeyName ]
        } elseif ($tmpNodeOfSelectedElements && ($previousCurrentNodeId || $this->request->data[$model][$lookupKeyName] == 'all')) {
            // Launched from node with elements > display limit or launched to compare 2 nodes: get all ids of node
            $this->request->data[$model][$lookupKeyName] = explode(",", $tmpNodeOfSelectedElements['BrowsingResult']['id_csv']);
        } elseif (empty($this->request->data)) {
            // Sort data
            $this->request->data[$model][$lookupKeyName] = explode(",", $_SESSION['compareToBatchSetOrNode']['selected_elements_ids']);
        }
        $selectedElementsIds = array_filter($this->request->data[$model][$lookupKeyName]);

        // Get diff results
        $allStudiedElements = $modelInstance->find('all', array(
            'conditions' => array(
                "$model.$lookupKeyName" => array_merge($comparedObjectElementIds, $selectedElementsIds)
            )
        ));
        $elementsIdsJustInSelectedObject = array_diff($comparedObjectElementIds, $selectedElementsIds);
        $elementsIdsJustInPreviouslyDispObject = array_diff($selectedElementsIds, $comparedObjectElementIds);
        $sortedAllStudiedElements = array(
            '1' => array(),
            '2' => array(),
            '3' => array()
        );
        $controlMasterIds = array();
        foreach ($allStudiedElements as $newStudiedElement) {
            if ($csvCreation && ! in_array($newStudiedElement[$model][$lookupKeyName], $studiedElementIdsToExport))
                continue;
            if ($controlForeignKey) {
                $controlMasterId = $newStudiedElement[$model][$controlForeignKey];
                $controlMasterIds[$controlMasterId] = $controlMasterId;
            }
            if (in_array($newStudiedElement[$model][$lookupKeyName], $elementsIdsJustInSelectedObject)) {
                $newStudiedElement['Generated']['batchset_and_node_elements_distribution_description'] = str_replace('%s_2', $selectedObjectTitle, __('data of selected %s_2 only (2)'));
                $sortedAllStudiedElements[3][] = $newStudiedElement;
            } elseif (in_array($newStudiedElement[$model][$lookupKeyName], $elementsIdsJustInPreviouslyDispObject)) {
                $newStudiedElement['Generated']['batchset_and_node_elements_distribution_description'] = str_replace('%s_1', $previouslyDisplayedObjectTitle, __("data of previously displayed %s_1 only (1)"));
                $sortedAllStudiedElements[2][] = $newStudiedElement;
            } else {
                $newStudiedElement['Generated']['batchset_and_node_elements_distribution_description'] = str_replace(array(
                    '%s_1',
                    '%s_2'
                ), array(
                    $previouslyDisplayedObjectTitle,
                    $selectedObjectTitle
                ), __('data both in previously displayed %s_1 and selected %s_2 (1 & 2)'));
                $sortedAllStudiedElements[1][] = $newStudiedElement;
            }
        }
        $diffResultsData = array_merge($sortedAllStudiedElements[1], $sortedAllStudiedElements[2], $sortedAllStudiedElements[3]);
        $this->set('diffResultsData', AppModel::sortWithUrl($diffResultsData, $this->passedArgs));

        // Manage structure for display
        $structureAlias = null;
        if ($controlMasterIds && (sizeof($controlMasterIds) == 1)) {
            $controlMasterId = array_shift($controlMasterIds);
            AppModel::getInstance("Datamart", "Browser", true);
            $alternateInfo = Browser::getAlternateStructureInfo($datamartStructure['plugin'], $modelInstance->getControlName(), $controlMasterId);
            $structureAlias = $alternateInfo['form_alias'];
        } else {
            $this->Structure = AppModel::getInstance("", "Structure", true);
            $atimStructureData = $this->Structure->find('first', array(
                'conditions' => array(
                    'Structure.id' => $datamartStructure['structure_id']
                ),
                'recursive' => - 1
            ));
            $structureAlias = $atimStructureData['structure']['Structure']['alias'];
        }
        $this->set('atimStructureForResults', $this->Structures->get('form', 'batchset_and_node_elements_distribution,' . $structureAlias));
        $this->set('datamartStructureId', $datamartStructure['id']);
        $this->set('typeOfObjectToCompare', $typeOfObjectToCompare);
        $this->set('batchSetOrNodeIdToCompare', $batchSetOrNodeIdToCompare);
        $this->set('csvCreation', $csvCreation);
        $this->set('header1', str_replace('%s_1', $previouslyDisplayedObjectTitle, __('data of previously displayed %s_1 (1)')));
        $this->set('header2', str_replace('%s_2', $selectedObjectTitle, __('data of selected %s_2 (2)')));

        if ($csvCreation) {
            // CSV cretion
            Configure::write('debug', 0);
            $this->layout = false;
        } else {
            // Results display
            // - Manage drop down action
            $this->set('datamartStructureModelName', $model);
            $this->set('datamartStructureKeyName', $lookupKeyName);
            if ($datamartStructure['index_link'])
                $this->set('datamartStructureLinks', $datamartStructure['index_link']);
            $datamartStructureActions = $this->DatamartStructure->getDropdownOptions($datamartStructure['plugin'], $datamartStructure['model'], $modelInstance->primaryKey, null, $datamartStructure['model'], $modelInstance->primaryKey);
            foreach ($datamartStructureActions as $key => $newAction) {
                if ($newAction['value'] && strpos($newAction['value'], 'Datamart/Csv/csv'))
                    unset($datamartStructureActions[$key]);
            }
            if (AppController::checkLinkPermission('/Datamart/Csv/csv')) {
                $sortArgs = array_key_exists('sort', $this->passedArgs) ? 'sort:' . $this->passedArgs['sort'] . '/direction:' . $this->passedArgs['direction'] : '';
                $csvAction = "javascript:setCsvPopup('Datamart/Reports/compareToBatchSetOrNode/$typeOfObjectToCompare/$batchSetOrNodeIdToCompare/1/$sortArgs');";
                $datamartStructureActions[] = array(
                    'label' => __('export as CSV file (comma-separated values)'),
                    'value' => sprintf($csvAction, 0)
                );
            }
            $datamartStructureActions[] = array(
                'label' => __("initiate browsing"),
                'value' => "Datamart/Browser/batchToDatabrowser/" . $datamartStructure['model'] . "/report/"
            );
            $this->set('datamartStructureActions', $datamartStructureActions);
            // - Add session data
            $_SESSION['compareToBatchSetOrNode'] = array(
                'datamart_structure_id' => $datamartStructure['id'],
                'previously_displayed_object_title' => $previouslyDisplayedObjectTitle,
                'selected_elements_ids' => implode(",", $selectedElementsIds)
            );
        }
        if ($this->layout == false) {
            $_SESSION['query']['previous'][] = $this->getQueryLogs('default');
        }
    }

    // -------------------------------------------------------------------------------------------------------------------
    // CUSTOM REPORTS DISPLAY AND MANAGEMENT
    // -------------------------------------------------------------------------------------------------------------------
    public function index()
    {
        $_SESSION['report'] = array(); // clear SEARCH criteria

        $this->request->data = $this->paginate($this->Report, array(
            'Report.flag_active' => '1',
            'Report.limit_access_from_datamart_structure_function' => '0'
        ));

        // Translate data
        foreach ($this->request->data as $key => $data) {
            $this->request->data[$key]['Report']['name'] = __($this->request->data[$key]['Report']['name']);
            $this->request->data[$key]['Report']['description'] = __($this->request->data[$key]['Report']['description']);
        }

        $this->Structures->set("reports");
    }

    /**
     *
     * @param $reportId
     * @param bool $csvCreation
     */
    public function manageReport($reportId, $csvCreation = false)
    {
        $error = false;
        $totalMemory = getTotalMemoryCapacity($error);
        if ($error) {
            AppController::forceMsgDisplayInPopup();
            AppController::addWarningMsg(__("the memory allocated to your query is low or undefined."));
        }
        ini_set("memory_limit", $totalMemory / 4 . "M");

        $reportId = ! empty($reportId) ? $reportId : "-1";
        $plugin = "Datamart";
        $controller = "Reports";
        $action = "manageReport";
        if (! empty($this->request->data)) {
            $_SESSION['post_data'][$plugin][$controller][$action][$reportId] = $this->request->data;
        } else {
            if (isset($_SESSION['post_data'][$plugin][$controller][$action][$reportId])) {
                convertArrayToJavaScript($_SESSION['post_data'][$plugin][$controller][$action][$reportId], 'jsPostData');
            }
        }

        // Get report data
        $report = $this->Report->find('first', array(
            'conditions' => array(
                'Report.id' => $reportId,
                'Report.flag_active' => '1'
            )
        ));
        if (empty($report) || empty($report['Report']['function']) || empty($report['Report']['form_alias_for_results']) || empty($report['Report']['form_type_for_results']) || ($report['Report']['form_type_for_results'] != 'index' && ! empty($report['Report']['associated_datamart_structure_id']))) {
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }

        // Set menu variables
        $this->set('atimMenuVariables', array(
            'Report.id' => $reportId
        ));
        $this->set('atimMenu', $this->Menus->get('/Datamart/Reports/manageReport/%%Report.id%%/'));

        if ($report['Report']['limit_access_from_datamart_structure_function'] && empty($this->request->data) && (! $csvCreation) && ! array_key_exists('sort', $this->passedArgs)) {
            $this->atimFlashError(__('the selected report can only be launched from a batchset or a databrowser node'), "/Datamart/Reports/index");
        } elseif (empty($this->request->data) && (! empty($report['Report']['form_alias_for_search'])) && (! $csvCreation) && ! array_key_exists('sort', $this->passedArgs)) {

            // ** SEARCH FROM DISPLAY **

            $this->Structures->set($report['Report']['form_alias_for_search'], 'search_form_structure');
            $_SESSION['report'][$reportId]['search_criteria'] = array(); // clear SEARCH criteria
            $_SESSION['report'][$reportId]['sort_criteria'] = array(); // clear SEARCH criteria
        } else {

            // ** RESULTS/ACTIONS MANAGEMENT **

            $linkedDatamartStructure = null;
            $linkedModel = null;
            if ($report['Report']['form_type_for_results'] == 'index' && $report['Report']['associated_datamart_structure_id']) {
                // Load linked structure and model if required
                $linkedDatamartStructure = $this->DatamartStructure->find('first', array(
                    'conditions' => array(
                        "DatamartStructure.id" => $report['Report']['associated_datamart_structure_id']
                    )
                ));
                if (empty($linkedDatamartStructure))
                    $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                $linkedModel = AppModel::getInstance($linkedDatamartStructure['DatamartStructure']['plugin'], $linkedDatamartStructure['DatamartStructure']['model'], true);
                $this->set('linkedDatamartStructureId', $report['Report']['associated_datamart_structure_id']);
            }

            // Set criteria to build report/csv
            $criteriaToBuildReport = null;
            $criteriaToSortReport = array();
            if ($csvCreation) {
                if (! AppController::checkLinkPermission('/Datamart/Csv/csv')) {
                    $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                }
                if (array_key_exists('Config', $this->request->data)) {
                    $config = array_merge($this->request->data['Config'], (array_key_exists(0, $this->request->data) ? $this->request->data[0] : array()));
                    unset($this->request->data[0]);
                    unset($this->request->data['Config']);
                    $this->configureCsv($config);
                }
                // Get criteria from session data for csv
                $criteriaToBuildReport = $_SESSION['report'][$reportId]['search_criteria'];
                $criteriaToSortReport = isset($_SESSION['report'][$reportId]['sort_criteria']) ? $_SESSION['report'][$reportId]['sort_criteria'] : array();
                if ($linkedModel && isset($this->request->data[$linkedDatamartStructure['DatamartStructure']['model']][$linkedModel->primaryKey])) {
                    // Take care about selected items (the number of records did not reach the limit of items that could be displayed)
                    $ids = array_filter($this->request->data[$linkedDatamartStructure['DatamartStructure']['model']][$linkedModel->primaryKey]);
                    if (! empty($ids)) {
                        $criteriaToBuildReport['SelectedItemsForCsv'][$linkedDatamartStructure['DatamartStructure']['model']][$linkedModel->primaryKey] = $ids;
                    }
                }
            } elseif (array_key_exists('sort', $this->passedArgs)) {
                // Data sort: Get criteria from session data
                $criteriaToBuildReport = $_SESSION['report'][$reportId]['search_criteria'];
                $criteriaToSortReport = array(
                    'sort' => $this->passedArgs['sort'],
                    'direction' => $this->passedArgs['direction']
                );
                $_SESSION['report'][$reportId]['sort_criteria'] = $criteriaToSortReport;
            } else {
                // Get criteria from search form
                $criteriaToBuildReport = empty($this->request->data) ? array() : $this->request->data;
                // Manage data from csv file
                foreach ($criteriaToBuildReport as $model => $fieldsParameters) {
                    if (! ($model == 'exact_search' && ! is_array($fieldsParameters))) {
                        foreach ($fieldsParameters as $field => $parameters) {
                            if (preg_match('/^(.+)_with_file_upload$/', $field, $matches)) {
                                $matchedFieldName = $matches[1];
                                if (! isset($criteriaToBuildReport[$model][$matchedFieldName]))
                                    $criteriaToBuildReport[$model][$matchedFieldName] = array();
                                if (strlen($parameters['tmp_name'])) {
                                    if (! preg_match('/((\.txt)|(\.csv))$/', $parameters['name'])) {
                                        $this->redirect('/Pages/err_submitted_file_extension', null, true);
                                    } else {
                                        $handle = fopen($parameters['tmp_name'], "r");
                                        if ($handle) {
                                            while (($csvData = fgetcsv($handle, 1000, CSV_SEPARATOR, '"')) !== false) {
                                                $criteriaToBuildReport[$model][$matchedFieldName][] = $csvData[0];
                                            }
                                            fclose($handle);
                                        } else {
                                            $this->redirect('/Pages/err_opening_submitted_file', null, true);
                                        }
                                    }
                                }
                                unset($criteriaToBuildReport[$model][$field]);
                            }
                        }
                    }
                }

                // Manage data when launched from databrowser node having a nbr of elements > databrowser_and_report_results_display_limit
                if (array_key_exists('node', $criteriaToBuildReport)) {
                    $browsingResult = $this->BrowsingResult->find('first', array(
                        'conditions' => array(
                            'BrowsingResult.id' => $criteriaToBuildReport['node']['id']
                        )
                    ));
                    $datamartStructure = $browsingResult['DatamartStructure'];
                    if (empty($browsingResult) || empty($datamartStructure)) {
                        $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                    }
                    // Get model and key name
                    $model = null;
                    $lookupKeyName = null;
                    if ($datamartStructure['control_master_model']) {
                        if (isset($criteriaToBuildReport[$datamartStructure['model']])) {
                            $modelInstance = AppModel::getInstance($datamartStructure['plugin'], $datamartStructure['model'], true);
                            $model = $datamartStructure['model'];
                            $lookupKeyName = $modelInstance->primaryKey;
                        } else {
                            $modelInstance = AppModel::getInstance($datamartStructure['plugin'], $datamartStructure['control_master_model'], true);
                            $model = $datamartStructure['control_master_model'];
                            $lookupKeyName = $modelInstance->primaryKey;
                        }
                    } else {
                        $model = $datamartStructure['model'];
                        $modelInstance = AppModel::getInstance($datamartStructure['plugin'], $datamartStructure['model'], true);
                        $lookupKeyName = $modelInstance->primaryKey;
                    }
                    if ($criteriaToBuildReport[$model][$lookupKeyName] == 'all')
                        $criteriaToBuildReport[$model][$lookupKeyName] = explode(",", $browsingResult['BrowsingResult']['id_csv']);
                }
                // Load search criteria in session
                $_SESSION['report'][$reportId]['search_criteria'] = $criteriaToBuildReport;
            }

            // Get and manage results
            $dataReturnedByFct = call_user_func_array(array(
                $this,
                $report['Report']['function']
            ), array(
                $criteriaToBuildReport
            ));
            if (empty($dataReturnedByFct) || (! array_key_exists('header', $dataReturnedByFct)) || (! array_key_exists('data', $dataReturnedByFct)) || (! array_key_exists('columns_names', $dataReturnedByFct)) || (! array_key_exists('error_msg', $dataReturnedByFct))) {
                // Wrong array keys returned by custom function
                $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            } elseif (! empty($dataReturnedByFct['error_msg'])) {
                // Error detected by custom function -> Display custom error message with empty form
                $this->request->data = array();
                $this->Structures->set('empty', 'result_form_structure');
                $this->set('resultFormType', 'index');
                $this->set('displayNewSearch', (empty($report['Report']['form_alias_for_search']) || $report['Report']['limit_access_from_datamart_structure_function']) ? false : true);
                $this->set('csvCreation', false);
                $this->Report->validationErrors[][] = $dataReturnedByFct['error_msg'];
            } elseif (sizeof($dataReturnedByFct['data']) > Configure::read('databrowser_and_report_results_display_limit') && ! $csvCreation) {
                // Too many results
                $this->request->data = array();
                $this->Structures->set('empty', 'result_form_structure');
                $this->set('resultFormType', 'index');
                $this->set('displayNewSearch', (empty($report['Report']['form_alias_for_search']) || $report['Report']['limit_access_from_datamart_structure_function']) ? false : true);
                $this->set('csvCreation', false);
                $this->Report->validationErrors[][] = __('the report contains too many results - please redefine search criteria') . ' [' . sizeof($dataReturnedByFct['data']) . ' ' . __('lines') . ']';
            } else {
                // Set data for display/csv
                $this->request->data = AppModel::sortWithUrl($dataReturnedByFct['data'], $criteriaToSortReport);
                $this->Structures->set($report['Report']['form_alias_for_results'], 'result_form_structure');
                $this->set('resultFormStructureAccuracy', array_key_exists('structure_accuracy', $dataReturnedByFct) ? $dataReturnedByFct['structure_accuracy'] : array());
                $this->set('resultFormType', $report['Report']['form_type_for_results']);
                $this->set('resultHeader', $dataReturnedByFct['header']);
                $this->set('resultColumnsNames', $dataReturnedByFct['columns_names']);
                $this->set('displayNewSearch', (empty($report['Report']['form_alias_for_search']) || $report['Report']['limit_access_from_datamart_structure_function']) ? false : true);
                $this->set('csvCreation', $csvCreation);
                $this->set('chartsData', (isset($dataReturnedByFct['charts']) ? $dataReturnedByFct['charts'] : null));
                $this->Structures->set('empty', 'empty_structure');

                if ($csvCreation) {
                    Configure::write('debug', 0);
                    $this->layout = false;
                } elseif ($linkedDatamartStructure) {
                    // Code to be able to launch actions from report linked to structure and model
                    $this->set('linkedDatamartStructureModelName', $linkedDatamartStructure['DatamartStructure']['model']);
                    $this->set('linkedDatamartStructureKeyName', $linkedModel->primaryKey);
                    if ($linkedDatamartStructure['DatamartStructure']['index_link']) {
                        $this->set('linkedDatamartStructureLinks', $linkedDatamartStructure['DatamartStructure']['index_link']);
                    }
                    $linkedDatamartStructureActions = $this->DatamartStructure->getDropdownOptions($linkedDatamartStructure['DatamartStructure']['plugin'], $linkedDatamartStructure['DatamartStructure']['model'], $linkedModel->primaryKey, null, null, null, null, false);
                    if (AppController::checkLinkPermission('/Datamart/Csv/csv')) {
                        $csvAction = "javascript:setCsvPopup('Datamart/Reports/manageReport/$reportId/1/');";
                        $linkedDatamartStructureActions[] = array(
                            'value' => '0',
                            'label' => __('export as CSV file (comma-separated values)'),
                            'value' => sprintf($csvAction, 0)
                        );
                    }
                    $linkedDatamartStructureActions[] = array(
                        'label' => __("initiate browsing"),
                        'value' => "Datamart/Browser/batchToDatabrowser/" . $linkedDatamartStructure['DatamartStructure']['model'] . "/report/"
                    );
                    $this->set('linkedDatamartStructureActions', $linkedDatamartStructureActions);
                }
                if ($this->layout == false) {
                    $_SESSION['query']['previous'][] = $this->getQueryLogs('default');
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------------------
    // FUNCTIONS ADDED TO THE CONTROLLER AS CUSTOM REPORT EXAMPLES
    // -------------------------------------------------------------------------------------------------------------------
    /**
     *
     * @param $parameters
     * @return array
     */
    public function bankActiviySummary($parameters)
    {
        if (! AppController::checkLinkPermission('/ClinicalAnnotation/Participants/profile')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }
        if (! AppController::checkLinkPermission('/InventoryManagement/Collections/detail')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }

        // 1- Build Header
        $startDateForDisplay = AppController::getFormatedDateString($parameters[0]['report_date_range_start']['year'], $parameters[0]['report_date_range_start']['month'], $parameters[0]['report_date_range_start']['day']);
        $endDateForDisplay = AppController::getFormatedDateString($parameters[0]['report_date_range_end']['year'], $parameters[0]['report_date_range_end']['month'], $parameters[0]['report_date_range_end']['day']);
        $header = array(
            'title' => __('from') . ' ' . (empty($parameters[0]['report_date_range_start']['year']) ? '?' : $startDateForDisplay) . ' ' . __('to') . ' ' . (empty($parameters[0]['report_date_range_end']['year']) ? '?' : $endDateForDisplay),
            'description' => 'n/a'
        );

        // 2- Search data
        $startDateForSql = AppController::getFormatedDatetimeSQL($parameters[0]['report_date_range_start'], 'start');
        $endDateForSql = AppController::getFormatedDatetimeSQL($parameters[0]['report_date_range_end'], 'end');

        $searchOnDateRange = true;
        if ((strpos($startDateForSql, '-9999') === 0) && (strpos($endDateForSql, '9999') === 0))
            $searchOnDateRange = false;

        // Get new participant
        if (! isset($this->Participant)) {
            $this->Participant = AppModel::getInstance("ClinicalAnnotation", "Participant", true);
        }
        $conditions = $searchOnDateRange ? array(
            "Participant.created >= '$startDateForSql'",
            "Participant.created <= '$endDateForSql'"
        ) : array();
        $data['0']['new_participants_nbr'] = $this->Participant->find('count', (array(
            'conditions' => $conditions
        )));

        // Get new consents obtained
        if (! isset($this->ConsentMaster)) {
            $this->ConsentMaster = AppModel::getInstance("ClinicalAnnotation", "ConsentMaster", true);
        }
        $conditions = $searchOnDateRange ? array(
            "ConsentMaster.consent_signed_date >= '$startDateForSql'",
            "ConsentMaster.consent_signed_date <= '$endDateForSql'"
        ) : array();
        $allConsent = $this->ConsentMaster->find('count', (array(
            'conditions' => $conditions
        )));
        $conditions['ConsentMaster.consent_status'] = 'obtained';
        $allObtainedConsent = $this->ConsentMaster->find('count', (array(
            'conditions' => $conditions
        )));
        $data['0']['obtained_consents_nbr'] = "$allObtainedConsent/$allConsent";

        // Get new collections
        $conditions = $searchOnDateRange ? "col.collection_datetime >= '$startDateForSql' AND col.collection_datetime <= '$endDateForSql'" : 'TRUE';
        $newCollectionsNbr = $this->Report->tryCatchQuery("SELECT COUNT(*) FROM (
				SELECT DISTINCT col.participant_id 
				FROM sample_masters AS sm 
				INNER JOIN collections AS col ON col.id = sm.collection_id 
				WHERE col.participant_id IS NOT NULL 
				AND col.participant_id != '0'
				AND ($conditions)
				AND col.deleted != '1'
				AND sm.deleted != '1'
			) AS res;");
        $data['0']['new_collections_nbr'] = $newCollectionsNbr[0][0]['COUNT(*)'];

        $arrayToReturn = array(
            'header' => $header,
            'data' => $data,
            'columns_names' => null,
            'error_msg' => null
        );

        return $arrayToReturn;
    }

    /**
     *
     * @param $parameters
     * @return array
     */
    public function sampleAndDerivativeCreationSummary($parameters)
    {
        if (! AppController::checkLinkPermission('/InventoryManagement/SampleMasters/detail')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }

        // 1- Build Header
        $startDateForDisplay = AppController::getFormatedDateString($parameters[0]['report_datetime_range_start']['year'], $parameters[0]['report_datetime_range_start']['month'], $parameters[0]['report_datetime_range_start']['day']);
        $endDateForDisplay = AppController::getFormatedDateString($parameters[0]['report_datetime_range_end']['year'], $parameters[0]['report_datetime_range_end']['month'], $parameters[0]['report_datetime_range_end']['day']);

        $header = array(
            'title' => __('from') . ' ' . (empty($parameters[0]['report_datetime_range_start']['year']) ? '?' : $startDateForDisplay) . ' ' . __('to') . ' ' . (empty($parameters[0]['report_datetime_range_end']['year']) ? '?' : $endDateForDisplay),
            'description' => 'n/a'
        );

        $bankIds = array();
        if (isset($parameters[0]['bank_id'])) {
            foreach ($parameters[0]['bank_id'] as $bankId)
                if (! empty($bankId))
                    $bankIds[] = $bankId;
            if (! empty($bankIds)) {
                $bank = AppModel::getInstance("Administrate", "Bank", true);
                $bankList = $bank->find('all', array(
                    'conditions' => array(
                        'id' => $bankIds
                    )
                ));
                $bankNames = array();
                foreach ($bankList as $newBank)
                    $bankNames[] = $newBank['Bank']['name'];
                $header['description'] = __('bank') . ': ' . implode(',', $bankNames);
            }
        }

        // 2- Search data

        $bankConditions = empty($bankIds) ? 'TRUE' : 'col.bank_id IN (' . implode(',', $bankIds) . ')';

        $startDateForSql = AppController::getFormatedDatetimeSQL($parameters[0]['report_datetime_range_start'], 'start');
        $endDateForSql = AppController::getFormatedDatetimeSQL($parameters[0]['report_datetime_range_end'], 'end');

        $searchOnDateRange = true;
        if ((strpos($startDateForSql, '-9999') === 0) && (strpos($endDateForSql, '9999') === 0))
            $searchOnDateRange = false;

        $resFinal = array();
        $tmpResFinal = array();

        // Work on specimen

        $conditions = $searchOnDateRange ? "col.collection_datetime >= '$startDateForSql' AND col.collection_datetime <= '$endDateForSql'" : 'TRUE';
        $resSamples = $this->Report->tryCatchQuery("SELECT COUNT(*), sc.sample_type
			FROM sample_masters AS sm
			INNER JOIN sample_controls AS sc ON sm.sample_control_id=sc.id
			INNER JOIN collections AS col ON col.id = sm.collection_id
			WHERE col.participant_id IS NOT NULL
			AND col.participant_id != '0'
			AND sc.sample_category = 'specimen'
			AND ($conditions)
			AND ($bankConditions)
			AND sm.deleted != '1'
			GROUP BY sample_type;");
        $resParticipants = $this->Report->tryCatchQuery("SELECT COUNT(*), res.sample_type FROM (
				SELECT DISTINCT col.participant_id, sc.sample_type
				FROM sample_masters AS sm
				INNER JOIN sample_controls AS sc ON sm.sample_control_id=sc.id
				INNER JOIN collections AS col ON col.id = sm.collection_id
				WHERE col.participant_id IS NOT NULL
				AND col.participant_id != '0'
				AND sc.sample_category = 'specimen'
				AND ($conditions)
				AND ($bankConditions)
				AND sm.deleted != '1'
			) AS res GROUP BY res.sample_type;");

        foreach ($resSamples as $data) {
            $tmpResFinal['specimen-' . $data['sc']['sample_type']] = array(
                'SampleControl' => array(
                    'sample_category' => 'specimen',
                    'sample_type' => $data['sc']['sample_type']
                ),
                '0' => array(
                    'created_samples_nbr' => $data[0]['COUNT(*)'],
                    'matching_participant_number' => null
                )
            );
        }
        foreach ($resParticipants as $data) {
            $tmpResFinal['specimen-' . $data['res']['sample_type']]['0']['matching_participant_number'] = $data[0]['COUNT(*)'];
        }

        // Work on derivative

        $conditions = $searchOnDateRange ? "der.creation_datetime >= '$startDateForSql' AND der.creation_datetime <= '$endDateForSql'" : 'TRUE';
        $resSamples = $this->Report->tryCatchQuery("SELECT COUNT(*), sc.sample_type
				FROM sample_masters AS sm
				INNER JOIN sample_controls AS sc ON sm.sample_control_id=sc.id
				INNER JOIN collections AS col ON col.id = sm.collection_id
				INNER JOIN derivative_details AS der ON der.sample_master_id = sm.id
				WHERE col.participant_id IS NOT NULL
				AND col.participant_id != '0'
				AND sc.sample_category = 'derivative'
				AND ($conditions)
				AND ($bankConditions)
				AND sm.deleted != '1'
				GROUP BY sample_type;");
        $resParticipants = $this->Report->tryCatchQuery("SELECT COUNT(*), res.sample_type FROM (
					SELECT DISTINCT col.participant_id, sc.sample_type
					FROM sample_masters AS sm
					INNER JOIN sample_controls AS sc ON sm.sample_control_id=sc.id
					INNER JOIN collections AS col ON col.id = sm.collection_id
					INNER JOIN derivative_details AS der ON der.sample_master_id = sm.id
					WHERE col.participant_id IS NOT NULL
					AND col.participant_id != '0'
					AND sc.sample_category = 'derivative'
					AND ($conditions)
					AND ($bankConditions)
					AND sm.deleted != '1'
			) AS res GROUP BY res.sample_type;");

        foreach ($resSamples as $data) {
            $tmpResFinal['derivative-' . $data['sc']['sample_type']] = array(
                'SampleControl' => array(
                    'sample_category' => 'derivative',
                    'sample_type' => $data['sc']['sample_type']
                ),
                '0' => array(
                    'created_samples_nbr' => $data[0]['COUNT(*)'],
                    'matching_participant_number' => null
                )
            );
        }
        foreach ($resParticipants as $data) {
            $tmpResFinal['derivative-' . $data['res']['sample_type']]['0']['matching_participant_number'] = $data[0]['COUNT(*)'];
        }

        // Format data for report
        foreach ($tmpResFinal as $newSampleTypeData) {
            $resFinal[] = $newSampleTypeData;
        }

        $arrayToReturn = array(
            'header' => $header,
            'data' => $resFinal,
            'columns_names' => null,
            'error_msg' => null
        );

        return $arrayToReturn;
    }

    /**
     *
     * @param $parameters
     * @return array
     */
    public function bankActiviySummaryPerPeriod($parameters)
    {
        if (! AppController::checkLinkPermission('/ClinicalAnnotation/Participants/profile')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }
        if (! AppController::checkLinkPermission('/InventoryManagement/Collections/detail')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }

        if (empty($parameters[0]['report_date_range_period'])) {
            return array(
                'error_msg' => 'no period has been defined',
                'header' => null,
                'data' => null,
                'columns_names' => null
            );
        }
        $monthPeriod = ($parameters[0]['report_date_range_period'] == 'month') ? true : false;

        // 1- Build Header
        $startDateForDisplay = AppController::getFormatedDateString($parameters[0]['report_date_range_start']['year'], $parameters[0]['report_date_range_start']['month'], $parameters[0]['report_date_range_start']['day']);
        $endDateForDisplay = AppController::getFormatedDateString($parameters[0]['report_date_range_end']['year'], $parameters[0]['report_date_range_end']['month'], $parameters[0]['report_date_range_end']['day']);
        $header = array(
            'title' => __('from') . ' ' . (empty($parameters[0]['report_date_range_start']['year']) ? '?' : $startDateForDisplay) . ' ' . __('to') . ' ' . (empty($parameters[0]['report_date_range_end']['year']) ? '?' : $endDateForDisplay),
            'description' => 'n/a'
        );

        // 2- Search data
        $startDateForSql = AppController::getFormatedDatetimeSQL($parameters[0]['report_date_range_start'], 'start');
        $endDateForSql = AppController::getFormatedDatetimeSQL($parameters[0]['report_date_range_end'], 'end');

        $searchOnDateRange = true;
        if ((strpos($startDateForSql, '-9999') === 0) && (strpos($endDateForSql, '9999') === 0))
            $searchOnDateRange = false;

        $arrFormatMonthToString = AppController::getCalInfo(false);

        $tmpRes = array(
            array(
                'new_participants_nbr' => array(),
                'obtained_consents_nbr' => array(),
                'new_collections_nbr' => array()
            )
        );

        $dateKeyList = array();

        // Get new participant
        $conditions = $searchOnDateRange ? "Participant.created >= '$startDateForSql' AND Participant.created <= '$endDateForSql'" : 'TRUE';
        $participantRes = $this->Report->tryCatchQuery("SELECT COUNT(*), YEAR(Participant.created) AS created_year" . ($monthPeriod ? ", MONTH(Participant.created) AS created_month" : "") . " FROM participants AS Participant 
			WHERE ($conditions) AND Participant.deleted != 1 
			GROUP BY created_year" . ($monthPeriod ? ", created_month" : "") . ";");
        foreach ($participantRes as $newData) {
            $dateKey = '';
            $dateValue = __('unknown');
            if (! empty($newData['0']['created_year'])) {
                if ($monthPeriod) {
                    $dateKey = $newData['0']['created_year'] . "-" . ((strlen($newData['0']['created_month']) == 1) ? "0" : "") . $newData['0']['created_month'];
                    $dateValue = $arrFormatMonthToString[$newData['0']['created_month']] . ' ' . $newData['0']['created_year'];
                } else {
                    $dateKey = $newData['0']['created_year'];
                    $dateValue = $newData['0']['created_year'];
                }
            }

            $dateKeyList[$dateKey] = $dateValue;
            $tmpRes['0']['new_participants_nbr'][$dateValue] = $newData['0']['COUNT(*)'];
        }

        // Get new consents obtained
        $conditions = $searchOnDateRange ? "ConsentMaster.consent_signed_date >= '$startDateForSql' AND ConsentMaster.consent_signed_date <= '$endDateForSql'" : 'TRUE';
        $consentRes = $this->Report->tryCatchQuery("SELECT COUNT(*), YEAR(ConsentMaster.consent_signed_date) AS signed_year" . ($monthPeriod ? ", MONTH(ConsentMaster.consent_signed_date) AS signed_month" : "") . " FROM consent_masters AS ConsentMaster
			WHERE ($conditions) AND ConsentMaster.deleted != 1 
			GROUP BY signed_year" . ($monthPeriod ? ", signed_month" : "") . ";");
        foreach ($consentRes as $newData) {
            $dateKey = '';
            $dateValue = __('unknown');
            if (! empty($newData['0']['signed_year'])) {
                if ($monthPeriod) {
                    $dateKey = $newData['0']['signed_year'] . "-" . ((strlen($newData['0']['signed_month']) == 1) ? "0" : "") . $newData['0']['signed_month'];
                    $dateValue = $arrFormatMonthToString[$newData['0']['signed_month']] . ' ' . $newData['0']['signed_year'];
                } else {
                    $dateKey = $newData['0']['signed_year'];
                    $dateValue = $newData['0']['signed_year'];
                }
            }

            $dateKeyList[$dateKey] = $dateValue;
            $tmpRes['0']['obtained_consents_nbr'][$dateValue] = $newData['0']['COUNT(*)'];
        }

        // Get new collections
        $conditions = $searchOnDateRange ? "col.collection_datetime >= '$startDateForSql' AND col.collection_datetime <= '$endDateForSql'" : 'TRUE';
        $collectionRes = $this->Report->tryCatchQuery("SELECT COUNT(*), res.collection_year" . ($monthPeriod ? ", res.collection_month" : "") . " FROM (
				SELECT DISTINCT col.participant_id, YEAR(col.collection_datetime) AS collection_year" . ($monthPeriod ? ", MONTH(col.collection_datetime) AS collection_month" : "") . " FROM sample_masters AS sm 
				INNER JOIN collections AS col ON col.id = sm.collection_id 
				WHERE col.participant_id IS NOT NULL 
				AND col.participant_id != '0'
				AND ($conditions)
				AND col.deleted != '1'
				AND sm.deleted != '1'
			) AS res
			GROUP BY res.collection_year" . ($monthPeriod ? ", res.collection_month" : "") . ";");
        foreach ($collectionRes as $newData) {
            $dateKey = '';
            $dateValue = __('unknown');
            if (! empty($newData['res']['collection_year'])) {
                if ($monthPeriod) {
                    $dateKey = $newData['res']['collection_year'] . "-" . ((strlen($newData['res']['collection_month']) == 1) ? "0" : "") . $newData['res']['collection_month'];
                    $dateValue = $arrFormatMonthToString[$newData['res']['collection_month']] . ' ' . $newData['res']['collection_year'];
                } else {
                    $dateKey = $newData['res']['collection_year'];
                    $dateValue = $newData['res']['collection_year'];
                }
            }

            $dateKeyList[$dateKey] = $dateValue;
            $tmpRes['0']['new_collections_nbr'][$dateValue] = $newData['0']['COUNT(*)'];
        }

        ksort($dateKeyList);
        $errorMsg = null;
        if (sizeof($dateKeyList) > 20) {
            $errorMsg = 'number of report columns will be too big, please redefine parameters';
        }

        $arrayToReturn = array(
            'header' => $header,
            'data' => $tmpRes,
            'columns_names' => array_values($dateKeyList),
            'error_msg' => $errorMsg
        );

        // Build Graphics

        $pieCharts = array(
            'new_participants_nbr' => array(
                'type' => 'pieChart',
                'title' => __('participants'),
                'data' => array()
            ),
            'obtained_consents_nbr' => array(
                'type' => 'pieChart',
                'title' => __('consents'),
                'data' => array()
            ),
            'new_collections_nbr' => array(
                'type' => 'pieChart',
                'title' => __('collections'),
                'data' => array()
            )
        );
        $tmpMultiBarChartData = array(
            'new_participants_nbr' => array(),
            'obtained_consents_nbr' => array(),
            'new_collections_nbr' => array()
        );
        $tmpLineBarChartData = array(
            'new_participants_nbr' => array(),
            'obtained_consents_nbr' => array(),
            'new_collections_nbr' => array()
        );

        foreach (array_keys($tmpRes[0]) as $newDataKey) {
            $keyCounter = 0;
            foreach ($dateKeyList as $unusedData => $newDatePeriod) {
                // pieCharts
                $pieCharts[$newDataKey]['data'][] = array(
                    $newDatePeriod,
                    isset($tmpRes[0][$newDataKey][$newDatePeriod]) ? (int) $tmpRes[0][$newDataKey][$newDatePeriod] : 0
                );
                // tmpMultiBarChartData
                $tmpMultiBarChartData[$newDataKey][] = array(
                    $newDatePeriod,
                    isset($tmpRes[0][$newDataKey][$newDatePeriod]) ? (int) $tmpRes[0][$newDataKey][$newDatePeriod] : 0
                );
                // tmpLineBarChartData
                $previousValue = 0;
                if ($keyCounter != 0) {
                    list ($tmpUnusedValue, $previousValue) = $tmpLineBarChartData[$newDataKey][($keyCounter - 1)];
                }
                $keyCounter ++;
                $tmpLineBarChartData[$newDataKey][] = array(
                    $newDatePeriod,
                    (isset($tmpRes[0][$newDataKey][$newDatePeriod]) ? (int) $tmpRes[0][$newDataKey][$newDatePeriod] : 0) + (int) $previousValue
                );
            }
        }

        $multiBarChart = array(
            'type' => 'multiBarChart',
            'title' => __('summary'),
            'xAxis' => array(
                'ticks' => array(),
                'axisLabel' => __('date')
            ),
            'yAxis' => array(
                'axisLabel' => __('number of data')
            ),
            'data' => array(
                array(
                    'key' => __('participants'),
                    'values' => $tmpMultiBarChartData['new_participants_nbr']
                ),
                array(
                    'key' => __('consents'),
                    'values' => $tmpMultiBarChartData['obtained_consents_nbr']
                ),
                array(
                    'key' => __('collections'),
                    'values' => $tmpMultiBarChartData['new_collections_nbr']
                )
            )
        );

        $lineBarChart = array(
            'type' => 'lineChart',
            'title' => __('summary'),
            'xAxis' => array(
                'ticks' => array(),
                'axisLabel' => __('date')
            ),
            'yAxis' => array(
                'axisLabel' => __('number of data')
            ),
            'data' => array(
                array(
                    'key' => __('participants'),
                    'values' => $tmpLineBarChartData['new_participants_nbr']
                ),
                array(
                    'key' => __('consents'),
                    'values' => $tmpLineBarChartData['obtained_consents_nbr']
                ),
                array(
                    'key' => __('collections'),
                    'values' => $tmpLineBarChartData['new_collections_nbr']
                )
            )
        );

        $arrayToReturn['charts'] = array(
            'data' => array(
                $pieCharts['new_participants_nbr'],
                $pieCharts['obtained_consents_nbr'],
                $pieCharts['new_collections_nbr'],
                $multiBarChart
            ),
            'setting' => array(
                'top' => false,
                'popup' => false
            )
        );
        if (! $monthPeriod) {
            foreach ($lineBarChart['data'] as $keyA => $dataLevelA) {
                foreach ($dataLevelA['values'] as $keyb => $dataLevelb) {
                    if (! preg_match('/^[0-9]+$/', $dataLevelb[0])) {
                        unset($lineBarChart[$keyA]['values'][$keyb]);
                    }
                }
            }
            $arrayToReturn['charts']['data'][] = $lineBarChart;
        }

        return $arrayToReturn;
    }

    /**
     *
     * @param $parameters
     * @return array
     */
    public function ctrnetAtimDashboard($parameters)
    {
        // Check permissions
        if (! AppController::checkLinkPermission('/ClinicalAnnotation/Participants/profile')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }
        if (! AppController::checkLinkPermission('/InventoryManagement/Collections/detail')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }

        // Get array containing the translated months names so that key = month_number and value = month_name
        $monthsStr = AppController::getCalInfo(true);

        // Load models to use
        $participantModel = AppModel::getInstance("ClinicalAnnotation", "Participant", true);
        $viewAliquotModel = AppModel::getInstance("InventoryManagement", "ViewAliquot", true);

        // Build array to return
        $arrayToReturn = array(
            'header' => array(
                'title' => __('atim dashboard - graphics'),
                'description' => __('atim dashboard decription')
            ),
            'data' => array(),
            'columns_names' => array(),
            'charts' => array(
                'data' => array(),
                'setting' => array(
                    'top' => false,
                    'popup' => false
                )
            ),
            'error_msg' => ''
        );

        // Build dashboard table
        // ----------------------------------------------------------------------------------------------------

        $currentDateMinus6d = date('Y-m-d', strtotime("-6 days"));
        $currentDateMinus23d = date('Y-m-d', strtotime("-23 days"));
        $currentDateMinus1y = date('Y-m-d', strtotime("-1 years"));
        $last12Months = date('Y-m', strtotime("-11 months")) . "-01";

        // Participant creation
        $query = "
              SELECT count(*) AS `count`
              FROM participants
              WHERE deleted <> 1
              AND %%cond%%";
        $statisticsTableData = array(
            'atim_dashboard_created_participants_week' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "created >= '$currentDateMinus6d'", $query))[0][0]['count'],
            'atim_dashboard_created_participants_month' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "created >= '$currentDateMinus23d'", $query))[0][0]['count'],
            'atim_dashboard_created_participants_year' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "created >= '$currentDateMinus1y'", $query))[0][0]['count'],
            'atim_dashboard_created_participants_total' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "TRUE", $query))[0][0]['count']
        );

        // Participant data updated (including clinical data)
        $query = "
              SELECT count(*) AS `count`
              FROM participants
              WHERE deleted <> 1
              AND created <> last_modification
              AND %%cond%%";
        $statisticsTableData = array_merge($statisticsTableData, array(
            'atim_dashboard_updated_participants_week' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "last_modification >= '$currentDateMinus6d'", $query))[0][0]['count'],
            'atim_dashboard_updated_participants_month' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "last_modification >= '$currentDateMinus23d'", $query))[0][0]['count'],
            'atim_dashboard_updated_participants_year' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "last_modification >= '$currentDateMinus1y'", $query))[0][0]['count'],
            'atim_dashboard_updated_participants_total' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "TRUE", $query))[0][0]['count']
        ));

        // Participant with consent signed
        $query = "
              SELECT count(DISTINCT(participant_id)) AS `count`
              FROM consent_masters
              WHERE consent_status = 'obtained'
              AND deleted <> 1
              AND %%cond%%";
        $statisticsTableData = array_merge($statisticsTableData, array(
            'atim_dashboard_consented_participants_week' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "consent_signed_date >= '$currentDateMinus6d'", $query))[0][0]['count'],
            'atim_dashboard_consented_participants_month' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "consent_signed_date >= '$currentDateMinus23d'", $query))[0][0]['count'],
            'atim_dashboard_consented_participants_year' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "consent_signed_date >= '$currentDateMinus1y'", $query))[0][0]['count'],
            'atim_dashboard_consented_participants_total' => $this->Report->tryCatchQuery(str_replace('%%cond%%', 'TRUE', $query))[0][0]['count']
        ));

        // Participant with collection
        $query = "
              SELECT count(DISTINCT(participant_id)) AS `count`
              FROM collections
              INNER JOIN sample_masters ON collections.id = sample_masters.collection_id AND sample_masters.deleted <> 1
              WHERE participant_id IS NOT NULL
              AND collections.deleted <> 1
              AND %%cond%%";
        $statisticsTableData = array_merge($statisticsTableData, array(
            'atim_dashboard_participants_with_collection_week' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "collection_datetime >= '$currentDateMinus6d'", $query))[0][0]['count'],
            'atim_dashboard_participants_with_collection_month' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "collection_datetime >= '$currentDateMinus23d'", $query))[0][0]['count'],
            'atim_dashboard_participants_with_collection_year' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "collection_datetime >= '$currentDateMinus1y'", $query))[0][0]['count'],
            'atim_dashboard_participants_with_collection_total' => $this->Report->tryCatchQuery(str_replace('%%cond%%', 'TRUE', $query))[0][0]['count']
        ));

        // Created aliquots
        $query = "
              SELECT count(DISTINCT(id)) AS `count`
              FROM aliquot_masters
              WHERE aliquot_masters.deleted <> 1
              AND %%cond%%";
        $statisticsTableData = array_merge($statisticsTableData, array(
            'atim_dashboard_aliquots_in_stock' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "in_stock != 'no'", $query))[0][0]['count'],
            'atim_dashboard_aliquots_total' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "(TRUE)", $query))[0][0]['count'],
            'atim_dashboard_created_aliquots_week' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "created >= '$currentDateMinus6d'", $query))[0][0]['count'],
            'atim_dashboard_created_aliquots_month' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "created >= '$currentDateMinus23d'", $query))[0][0]['count'],
            'atim_dashboard_created_aliquots_year' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "created >= '$currentDateMinus1y'", $query))[0][0]['count']
        ));

        // Shipped aliquots
        $query = "
              SELECT count(DISTINCT(order_items.id)) AS `count`
              FROM order_items
              INNER JOIN shipments ON shipments.id = order_items.shipment_id AND shipments.deleted <> 1
              WHERE order_items.deleted <> 1
              AND %%cond%%";
        $statisticsTableData = array_merge($statisticsTableData, array(
            'atim_dashboard_shipped_aliquots_week' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "shipments.datetime_shipped >= '$currentDateMinus6d'", $query))[0][0]['count'],
            'atim_dashboard_shipped_aliquots_month' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "shipments.datetime_shipped >= '$currentDateMinus23d'", $query))[0][0]['count'],
            'atim_dashboard_shipped_aliquots_year' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "shipments.datetime_shipped >= '$currentDateMinus1y'", $query))[0][0]['count'],
            'atim_dashboard_shipped_aliquots_total' => $this->Report->tryCatchQuery(str_replace('%%cond%%', 'TRUE', $query))[0][0]['count']
        ));

        // Aliquots locally used
        $query = "
              SELECT count(DISTINCT(aliquot_internal_uses.aliquot_master_id)) AS `count`
              FROM aliquot_internal_uses
              WHERE aliquot_internal_uses.deleted <> 1
              AND type = 'internal use'
              AND %%cond%%";
        $statisticsTableData = array_merge($statisticsTableData, array(
            'atim_dashboard_internally_used_aliquots_week' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "use_datetime >= '$currentDateMinus6d'", $query))[0][0]['count'],
            'atim_dashboard_internally_used_aliquots_month' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "use_datetime >= '$currentDateMinus23d'", $query))[0][0]['count'],
            'atim_dashboard_internally_used_aliquots_year' => $this->Report->tryCatchQuery(str_replace('%%cond%%', "use_datetime >= '$currentDateMinus1y'", $query))[0][0]['count'],
            'atim_dashboard_internally_used_aliquots_total' => $this->Report->tryCatchQuery(str_replace('%%cond%%', 'TRUE', $query))[0][0]['count']
        ));

        // Active studies
        $query = "
              SELECT count(DISTINCT(id)) AS `count`
              FROM study_summaries
              WHERE study_summaries.deleted <> 1
              AND %%cond%%";
        $statisticsTableData = array_merge($statisticsTableData, array(
            'atim_dashboard_created_studies' => $this->Report->tryCatchQuery(str_replace('%%cond%%', 'TRUE', $query))[0][0]['count']
        ));
        $query = "
            SELECT count(DISTINCT(study_summary_id)) AS `count`
                FROM (
                    SELECT DISTINCT aliquot_internal_uses.study_summary_id
                    FROM aliquot_internal_uses
                    WHERE aliquot_internal_uses.deleted <> 1
                    AND type = 'internal use'
                    AND use_datetime IS NOT NULL
                    AND use_datetime >= '%%date_cond%%'
                    UNION ALL
                    SELECT DISTINCT IFNULL(order_lines.study_summary_id, default_study_summary_id) AS study_summary_id
                    FROM order_items
                    INNER JOIN shipments ON shipments.id = order_items.shipment_id AND shipments.deleted <> 1 AND datetime_shipped IS NOT NULL
                    INNER JOIN orders ON orders.id = order_items.order_id AND orders.deleted <> 1 AND orders.default_study_summary_id IS NOT NULL 
                    LEFT JOIN order_lines ON order_lines.id = order_items.order_line_id AND order_lines.deleted <> 1 AND order_lines.study_summary_id IS NOT NULL
                    WHERE order_items.deleted <> 1
                    AND datetime_shipped IS NOT NULL
                    AND datetime_shipped >= '%%date_cond%%'
                ) AS Res
                WHERE Res.study_summary_id IS NOT NULL;";
        $statisticsTableData = array_merge($statisticsTableData, array(
            'atim_dashboard_active_studies_week' => $this->Report->tryCatchQuery(str_replace('%%date_cond%%', $currentDateMinus6d, $query))[0][0]['count'],
            'atim_dashboard_active_studies_month' => $this->Report->tryCatchQuery(str_replace('%%date_cond%%', $currentDateMinus23d, $query))[0][0]['count'],
            'atim_dashboard_active_studies_year' => $this->Report->tryCatchQuery(str_replace('%%date_cond%%', $currentDateMinus1y, $query))[0][0]['count']
        ));

        // Statistic on dates
        $statisticsTableData = array_merge($statisticsTableData, array(
            'atim_dashboard_last_created_participant' => $this->Report->tryCatchQuery("SELECT IFNULL(MAX(created), '0') AS `count` FROM participants WHERE deleted <> 1")[0][0]['count'],
            'atim_dashboard_last_consented_participant' => $this->Report->tryCatchQuery("SELECT IFNULL(MAX(consent_signed_date), '0') AS `count` FROM consent_masters WHERE deleted <> 1 AND consent_status = 'obtained'")[0][0]['count'],
            'atim_dashboard_last_collection' => $this->Report->tryCatchQuery("SELECT IFNULL(MAX(collection_datetime), '0') AS `count` FROM collections WHERE deleted <> 1")[0][0]['count']
        ));

        $arrayToReturn['data'][0] = $statisticsTableData;

        // Build dashboard graphics
        // ----------------------------------------------------------------------------------------------------

        // ### multiBarChart ####
        $query = "SELECT MIN(min_date) AS min_year FROM (
                SELECT YEAR(MIN(Participant.created)) AS min_date
                FROM participants AS Participant
                WHERE Participant.deleted != 1
                UNION ALL
                SELECT YEAR(MIN(consent_signed_date)) AS min_date
                FROM consent_masters
                WHERE consent_status = 'obtained'
                AND deleted <> 1
                AND consent_signed_date IS NOT NULL
            ) AS res;";
        $arrayYears = array();
        for ($newYear = $this->Report->tryCatchQuery($query)[0][0]['min_year']; $newYear <= date('Y'); $newYear ++) {
            $arrayYears[$newYear] = 0;
        }
        $arrayMonths = array();
        for ($minusMonth = 11; $minusMonth >= 0; $minusMonth --) {
            $arrayMonths[date('Y-m', strtotime("-$minusMonth months"))] = 0;
        }
        $queriesInfo = array(
            array(
                __('recruitments of participants') . " (" . __('all periods') . ")",
                'years',
                __('created participants'),
                "SELECT COUNT(*) AS `count`, YEAR(Participant.created) AS date_year
                FROM participants AS Participant
                WHERE Participant.deleted != 1
                AND Participant.created IS NOT NULL
    			GROUP BY date_year;"
            ),
            array(
                __('recruitments of participants') . " (" . __('all periods') . ")",
                'years',
                __('participants who consented'),
                "SELECT count(DISTINCT(participant_id)) AS `count`, YEAR(consent_signed_date) AS date_year
                FROM consent_masters
                WHERE (consent_status = 'obtained' OR (consent_status = 'withdrawn' AND status_date IS NOT NULL && consent_signed_date != status_date))
                AND deleted <> 1
                AND consent_signed_date IS NOT NULL
                GROUP BY date_year;"
            ),
            array(
                __('recruitments of participants') . " (" . __('all periods') . ")",
                'years',
                __('participants who withdrawn'),
                "SELECT count(DISTINCT(participant_id)) AS `count`, YEAR(status_date) AS date_year
                FROM consent_masters
                WHERE consent_status = 'withdrawn'
                AND deleted <> 1
                AND consent_signed_date IS NOT NULL AND status_date IS NOT NULL && consent_signed_date != status_date
                GROUP BY date_year;"
            ),
            array(
                __('recruitments of participants') . " (" . __('last 12 months') . ")",
                '12 months',
                __('created participants'),
                "SELECT COUNT(*) AS `count`, YEAR(Participant.created) AS date_year, MONTH(Participant.created) AS date_month
                FROM participants AS Participant
                WHERE Participant.deleted != 1
                AND Participant.created IS NOT NULL
                AND Participant.created >= '$last12Months'
    			GROUP BY date_year, date_month;"
            ),
            array(
                __('recruitments of participants') . " (" . __('last 12 months') . ")",
                '12 months',
                __('participants who consented'),
                "SELECT count(DISTINCT(participant_id)) AS `count`, YEAR(consent_signed_date) AS date_year, MONTH(consent_signed_date) AS date_month
                FROM consent_masters
                WHERE (consent_status = 'obtained' OR (consent_status = 'withdrawn' AND status_date IS NOT NULL && consent_signed_date != status_date))
                AND deleted <> 1
                AND consent_signed_date IS NOT NULL
                AND consent_signed_date >= '$last12Months'
                GROUP BY date_year, date_month;"
            ),
            array(
                __('recruitments of participants') . " (" . __('last 12 months') . ")",
                '12 months',
                __('participants who withdrawn'),
                "SELECT count(DISTINCT(participant_id)) AS `count`, YEAR(status_date) AS date_year, MONTH(status_date) AS date_month
                FROM consent_masters
                WHERE consent_status = 'withdrawn'
                AND deleted <> 1
                AND consent_signed_date IS NOT NULL AND status_date IS NOT NULL && consent_signed_date != status_date
                AND status_date >= '$last12Months'
                GROUP BY date_year, date_month;"
            )
        );
        $query = "SELECT DISTINCT sample_controls.id, sample_type
            FROM  sample_controls
            INNER JOIN parent_to_derivative_sample_controls ON parent_sample_control_id IS NULL AND derivative_sample_control_id = sample_controls.id AND parent_to_derivative_sample_controls.flag_active = 1
            INNER JOIN sample_masters ON sample_controls.id = sample_masters.sample_control_id AND sample_masters.deleted <> 1";
        foreach ($this->Report->tryCatchQuery($query) as $newSpecimenType) {
            $queriesInfo[] = array(
                __('participants with collected specimens') . " (" . __('all periods') . ")",
                'years',
                __($newSpecimenType['sample_controls']['sample_type']),
                "SELECT COUNT(DISTINCT Participant.id) AS `count`, YEAR(Collection.collection_datetime) AS date_year
                    FROM participants AS Participant
                    INNER JOIN collections Collection ON Collection.participant_id = Participant.id AND Collection.deleted <> 1
                    INNER JOIN sample_masters SampleMaster ON SampleMaster.collection_id = Collection.id AND SampleMaster.deleted <> 1 AND SampleMaster.sample_control_id = " . $newSpecimenType['sample_controls']['id'] . "
                    WHERE Participant.deleted != 1
                    AND Collection.collection_datetime IS NOT NULL
        			GROUP BY date_year;"
            );
        }
        $queriesInfo[] = array(
            __('processed studies'),
            'years',
            __('with aliquots locally used'),
            "SELECT count(DISTINCT(aliquot_internal_uses.study_summary_id)) AS `count`, YEAR(use_datetime) AS date_year
                FROM aliquot_internal_uses
                WHERE aliquot_internal_uses.deleted <> 1
                AND type = 'internal use'
                AND use_datetime IS NOT NULL
                AND aliquot_internal_uses.study_summary_id IS NOT NULL
                GROUP BY date_year;"
        );
        $queriesInfo[] = array(
            __('processed studies'),
            'years',
            __('with shipped aliquots'),
            "SELECT count(DISTINCT(Res.study_summary_id)) AS `count`, date_year
                FROM (
                    SELECT DISTINCT IFNULL(order_lines.study_summary_id, default_study_summary_id) AS study_summary_id, YEAR(datetime_shipped) AS date_year
                    FROM order_items
                    INNER JOIN shipments ON shipments.id = order_items.shipment_id AND shipments.deleted <> 1 AND datetime_shipped IS NOT NULL
                    INNER JOIN orders ON orders.id = order_items.order_id AND orders.deleted <> 1 AND orders.default_study_summary_id IS NOT NULL 
                    LEFT JOIN order_lines ON order_lines.id = order_items.order_line_id AND order_lines.deleted <> 1 AND order_lines.study_summary_id IS NOT NULL
                    WHERE order_items.deleted <> 1
                    AND datetime_shipped IS NOT NULL
                ) AS Res
                WHERE Res.study_summary_id IS NOT NULL
                GROUP BY Res.date_year;"
        );
        $queriesInfo[] = array(
            __('processed studies'),
            'years',
            __('total'),
            "SELECT count(DISTINCT(study_summary_id)) AS `count`, date_year
                FROM (
                    SELECT DISTINCT aliquot_internal_uses.study_summary_id, YEAR(use_datetime) AS date_year
                    FROM aliquot_internal_uses
                    WHERE aliquot_internal_uses.deleted <> 1
                    AND type = 'internal use'
                    AND use_datetime IS NOT NULL
                    UNION ALL
                    SELECT DISTINCT IFNULL(order_lines.study_summary_id, default_study_summary_id) AS study_summary_id, YEAR(datetime_shipped) AS date_year
                    FROM order_items
                    INNER JOIN shipments ON shipments.id = order_items.shipment_id AND shipments.deleted <> 1 AND datetime_shipped IS NOT NULL
                    INNER JOIN orders ON orders.id = order_items.order_id AND orders.deleted <> 1 AND orders.default_study_summary_id IS NOT NULL 
                    LEFT JOIN order_lines ON order_lines.id = order_items.order_line_id AND order_lines.deleted <> 1 AND order_lines.study_summary_id IS NOT NULL
                    WHERE order_items.deleted <> 1
                    AND datetime_shipped IS NOT NULL
                ) AS Res
                WHERE Res.study_summary_id IS NOT NULL
                GROUP BY Res.date_year;"
        );
        $processedDataToBuildChart = array();
        // Get counts from queries
        foreach ($queriesInfo as $newQueryInfo) {
            list ($chartTitle, $graphPeriod, $lineTitle, $query) = $newQueryInfo;
            if (! isset($processedDataToBuildChart[$chartTitle]))
                $processedDataToBuildChart[$chartTitle] = array();
            if (! isset($processedDataToBuildChart[$chartTitle][$lineTitle]))
                $processedDataToBuildChart[$chartTitle][$lineTitle] = ($graphPeriod == 'years' ? $arrayYears : $arrayMonths);
            foreach ($this->Report->tryCatchQuery($query) as $newQueryResult) {
                if (array_key_exists('Res', $newQueryResult)) {
                    $newQueryResult[0] = array_merge($newQueryResult['Res'], $newQueryResult[0]);
                }
                $newQueryResult = $newQueryResult[0];
                $dateMonth = array_key_exists('date_month', $newQueryResult) ? '-' . (strlen($newQueryResult['date_month']) == 1 ? '0' . $newQueryResult['date_month'] : $newQueryResult['date_month']) : '';
                $processedDataToBuildChart[$chartTitle][$lineTitle][$newQueryResult['date_year'] . $dateMonth] = $newQueryResult['count'];
            }
        }
        // Change to cumulative count
        foreach ($processedDataToBuildChart as $chartTitle => $chartLinesData) {
            foreach ($chartLinesData as $lineTitle => $chartLineData) {
                $totalCount = 0;
                foreach ($chartLineData as $dateTime => $count) {
                    $totalCount += $count;
                    // $processedDataToBuildChart[$chartTitle][$lineTitle][$dateTime] = $totalCount;
                }
                if (! $totalCount) {
                    $processedDataToBuildChart[$chartTitle][$lineTitle] = array();
                }
            }
        }
        // Build chart
        foreach ($processedDataToBuildChart as $chartTitle => $chartLinesData) {
            $newChart = array(
                'type' => 'multiBarChart', // 'lineChart',
                'title' => $chartTitle,
                'xAxis' => array(
                    'ticks' => array(),
                    'axisLabel' => __('date')
                ),
                'yAxis' => array(
                    'axisLabel' => __('number of participants')
                ),
                'data' => array()
            );
            foreach ($chartLinesData as $lineTitle => $chartLineData) {
                $values = array();
                foreach ($chartLineData as $dateTime => $count) {
                    // Display month as string - Does not work with lineChart on 2019-08-30 (should be fiexed soon)
                    if (preg_match('/^([0-9]{4})\-((0([0-9]{1}))|([0-9]{2}))$/', $dateTime, $matches)) {
                        $dateTime = $matches[1] . ' ' . $monthsStr[($matches[4] ? $matches[4] : $matches[5])];
                    }
                    $values[] = array(
                        $dateTime,
                        (int) $count
                    );
                }
                $newChart['data'][] = array(
                    'key' => $lineTitle,
                    'values' => $values
                );
            }
            $arrayToReturn['charts']['data'][] = $newChart;
        }

        // ### pieChart ####

        // Aliquots Counter Graphics
        $queriesInfo = array(
            array(
                'aliquots in stock counter',
                "SELECT COUNT(*) AS `count`, sample_type, aliquot_type, BlockDetail.block_type
                FROM view_aliquots ViewAliquot
                LEFT JOIN ad_blocks BlockDetail ON BlockDetail.aliquot_master_id = ViewAliquot.aliquot_master_id
                WHERE in_stock IN ('yes - not available', 'yes - available')
                GROUP BY sample_type, aliquot_type, BlockDetail.block_type"
            ),
            array(
                'all aliquots (in stock and not) counter',
                "SELECT COUNT(*) AS `count`, sample_type, aliquot_type, BlockDetail.block_type
                FROM view_aliquots ViewAliquot
                LEFT JOIN ad_blocks BlockDetail ON BlockDetail.aliquot_master_id = ViewAliquot.aliquot_master_id
                WHERE in_stock IN ('yes - not available', 'yes - available', 'no')
                GROUP BY sample_type, aliquot_type, BlockDetail.block_type"
            )
        );
        foreach ($queriesInfo as $newQueryInfo) {
            list ($newChartTitle, $query) = $newQueryInfo;
            $newChart = array(
                'type' => 'pieChart',
                'title' => __($newChartTitle),
                'data' => array()
            );
            $finalCount = 0;
            foreach ($this->Report->tryCatchQuery($query) as $newAliquotType) {
                if ($newAliquotType['ViewAliquot']['sample_type'] == 'tissue' && in_array($newAliquotType['ViewAliquot']['aliquot_type'], array(
                    'core',
                    'block'
                ))) {
                    $key = __($newAliquotType['ViewAliquot']['sample_type']) . ' - ' . __($newAliquotType['ViewAliquot']['aliquot_type']) . ($newAliquotType['BlockDetail']['block_type'] ? ' - ' . __($newAliquotType['BlockDetail']['block_type']) : '');
                } elseif ($newAliquotType['ViewAliquot']['sample_type'] == 'tissue') {
                    $key = __($newAliquotType['ViewAliquot']['sample_type']) . ' - ' . __('other');
                } else {
                    $key = __($newAliquotType['ViewAliquot']['sample_type']);
                }
                $finalCount += $newAliquotType['0']['count'];
                if (isset($newChart['data'][$key])) {
                    $newChart['data'][$key][1] += (int) $newAliquotType['0']['count'];
                } else {
                    $newChart['data'][$key] = array(
                        $key,
                        (int) $newAliquotType['0']['count']
                    );
                }
            }
            $newChart['data'] = array_values($newChart['data']);
            usort($newChart['data'], function ($a, $b) {
                return $a[1] - $b[1];
            });
            $finalCount = number_format($finalCount, 0, ', ', ' ');
            $newChart['title'] .= ' : ' . $finalCount . ' alq.';
            $arrayToReturn['charts']['data'][] = $newChart;
        }

        // Return data
        // ----------------------------------------------------------------------------------------------------

        return $arrayToReturn;
    }

    /**
     *
     * @param $parameters
     * @return array
     */
    public function ctrnetCatalogueSubmissionFile($parameters)
    {
        if (! AppController::checkLinkPermission('/InventoryManagement/Collections/detail')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }

        // 1- Build Header
        $header = array(
            'title' => __('report_ctrnet_catalogue_name'),
            'description' => 'n/a'
        );

        $bankIds = array();
        if (isset($parameters[0]['bank_id'])) {
            foreach ($parameters[0]['bank_id'] as $bankId)
                if (! empty($bankId))
                    $bankIds[] = $bankId;
            if (! empty($bankIds)) {
                $bank = AppModel::getInstance("Administrate", "Bank", true);
                $bankList = $bank->find('all', array(
                    'conditions' => array(
                        'id' => $bankIds
                    )
                ));
                $bankNames = array();
                foreach ($bankList as $newBank)
                    $bankNames[] = $newBank['Bank']['name'];
                $header['title'] .= ' (' . __('bank') . ': ' . implode(',', $bankNames) . ')';
            }
        }

        // 2- Search data

        $bankConditions = empty($bankIds) ? 'TRUE' : 'col.bank_id IN (' . implode(',', $bankIds) . ')';
        $aliquotTypeConfitions = $parameters[0]['include_core_and_slide'][0] ? 'TRUE' : "ac.aliquot_type NOT IN ('core','slide')";
        $whatmanPaperConfitions = $parameters[0]['include_whatman_paper'][0] ? 'TRUE' : "ac.aliquot_type NOT IN ('whatman paper')";
        $detailOtherCount = $parameters[0]['detail_other_count'][0] ? true : false;

        $data = array();

        // **all**

        $tmpData = array(
            'sample_type' => __('total'),
            'cases_nbr' => '',
            'aliquots_nbr' => '',
            'notes' => ''
        );

        $sql = "
			SELECT count(*) AS nbr FROM (
				SELECT DISTINCT %%id%%
				FROM collections AS col
				INNER JOIN sample_masters AS sm ON col.id = sm.collection_id AND sm.deleted != '1'
				INNER JOIN aliquot_masters AS am ON am.sample_master_id = sm.id AND am.deleted != '1'
				INNER JOIN aliquot_controls AS ac ON ac.id = am.aliquot_control_id
				WHERE col.deleted != '1' 
				AND ($bankConditions)
				AND ($aliquotTypeConfitions) 
				AND am.in_stock IN ('yes - available ','yes - not available')
			) AS res;";
        $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'col.participant_id', $sql));
        $tmpData['cases_nbr'] = $queryResults[0][0]['nbr'];

        $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'am.id', $sql));
        $tmpData['aliquots_nbr'] = $queryResults[0][0]['nbr'];

        $data[] = $tmpData;

        // **FFPE**

        $tmpData = array(
            'sample_type' => __('FFPE'),
            'cases_nbr' => '',
            'aliquots_nbr' => '',
            'notes' => __('tissue') . ' ' . __('block') . ' (' . __('paraffin') . ')'
        );

        $sql = "	
			SELECT count(*) AS nbr FROM (
				SELECT DISTINCT  %%id%%
				FROM collections AS col
				INNER JOIN sample_masters AS sm ON col.id = sm.collection_id AND sm.deleted != '1'
				INNER JOIN sample_controls AS sc ON sc.id = sm.sample_control_id
				INNER JOIN sd_spe_tissues AS tiss ON tiss.sample_master_id = sm.id
				INNER JOIN aliquot_masters AS am ON am.sample_master_id = sm.id AND am.deleted != '1'
				INNER JOIN aliquot_controls AS ac ON ac.id = am.aliquot_control_id
				INNER JOIN ad_blocks AS blk ON blk.aliquot_master_id = am.id
				WHERE col.deleted != '1' AND ($bankConditions)
				AND am.in_stock IN ('yes - available ','yes - not available')
				AND sc.sample_type IN ('tissue')
				AND ac.aliquot_type = 'block'
				AND blk.block_type = 'paraffin'
			) AS res;";
        $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'col.participant_id', $sql));
        $tmpData['cases_nbr'] = $queryResults[0][0]['nbr'];

        $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'am.id', $sql));
        $tmpData['aliquots_nbr'] = $queryResults[0][0]['nbr'];

        $data[] = $tmpData;

        // **frozen tissue**

        $tmpData = array(
            'sample_type' => __('frozen tissue'),
            'cases_nbr' => '',
            'aliquots_nbr' => '',
            'notes' => ''
        );

        $sql = "
			SELECT count(*) AS nbr FROM (
				SELECT DISTINCT  %%id%%
				FROM collections AS col
				INNER JOIN sample_masters AS sm ON col.id = sm.collection_id AND sm.deleted != '1'
				INNER JOIN sample_controls AS sc ON sc.id = sm.sample_control_id
				INNER JOIN sd_spe_tissues AS tiss ON tiss.sample_master_id = sm.id
				INNER JOIN aliquot_masters AS am ON am.sample_master_id = sm.id AND am.deleted != '1'
				INNER JOIN aliquot_controls AS ac ON ac.id = am.aliquot_control_id
				WHERE col.deleted != '1' AND ($bankConditions)
				AND am.in_stock IN ('yes - available ','yes - not available')
				AND sc.sample_type IN ('tissue')
				AND ($aliquotTypeConfitions) 
				AND am.id NOT IN (SELECT aliquot_master_id FROM ad_blocks WHERE block_type = 'paraffin')
			) AS res;";
        $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'col.participant_id', $sql));
        $tmpData['cases_nbr'] = $queryResults[0][0]['nbr'];

        $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'am.id', $sql));
        $tmpData['aliquots_nbr'] = $queryResults[0][0]['nbr'];

        $sql = "
			SELECT DISTINCT sc.sample_type,ac.aliquot_type,blk.block_type
			FROM collections AS col
			INNER JOIN sample_masters AS sm ON col.id = sm.collection_id AND sm.deleted != '1'
			INNER JOIN sample_controls AS sc ON sc.id = sm.sample_control_id
			INNER JOIN sd_spe_tissues AS tiss ON tiss.sample_master_id = sm.id
			INNER JOIN aliquot_masters AS am ON am.sample_master_id = sm.id AND am.deleted != '1'
			INNER JOIN aliquot_controls AS ac ON ac.id = am.aliquot_control_id
			LEFT JOIN ad_blocks AS blk ON blk.aliquot_master_id = am.id
			WHERE col.deleted != '1' AND ($bankConditions)
			AND am.in_stock IN ('yes - available ','yes - not available')
			AND sc.sample_type IN ('tissue')
				AND ($aliquotTypeConfitions) 
			AND am.id NOT IN (SELECT aliquot_master_id FROM ad_blocks WHERE block_type = 'paraffin');";
        $queryResults = $this->Report->tryCatchQuery($sql);
        foreach ($queryResults as $newType)
            $tmpData['notes'] .= (empty($tmpData['notes']) ? '' : ' & ') . __($newType['sc']['sample_type']) . ' ' . __($newType['ac']['aliquot_type']) . (empty($newType['blk']['block_type']) ? '' : ' (' . __($newType['blk']['block_type']) . ')');

        $data[] = $tmpData;

        // **blood**
        // **pbmc**
        // **buffy coat**
        // **blood cell**
        // **plasma**
        // **serum**
        // **rna**
        // **dna**
        // **cell culture**

        $sampleTypes = "'blood', 'pbmc', 'buffy coat',  'blood cell', 'plasma', 'serum', 'rna', 'dna', 'cell culture'";

        $tmpData = array();
        $sql = "
			SELECT count(*) AS nbr,sample_type, aliquot_type FROM (
				SELECT DISTINCT  %%id%%, sc.sample_type, ac.aliquot_type
				FROM collections AS col
				INNER JOIN sample_masters AS sm ON col.id = sm.collection_id AND sm.deleted != '1'
				INNER JOIN sample_controls AS sc ON sc.id = sm.sample_control_id
				INNER JOIN aliquot_masters AS am ON am.sample_master_id = sm.id AND am.deleted != '1'
				INNER JOIN aliquot_controls AS ac ON ac.id = am.aliquot_control_id
				WHERE col.deleted != '1' AND ($bankConditions)
				AND am.in_stock IN ('yes - available ','yes - not available')
				AND sc.sample_type IN ($sampleTypes)
				AND ($whatmanPaperConfitions)	
			) AS res GROUP BY sample_type, aliquot_type;";
        $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'col.participant_id', $sql));
        foreach ($queryResults as $newRes) {
            $sampleType = $newRes['res']['sample_type'];
            $aliquotType = $newRes['res']['aliquot_type'];
            $tmpData[$sampleType . $aliquotType] = array(
                'sample_type' => __($sampleType) . ' ' . __($aliquotType),
                'cases_nbr' => $newRes[0]['nbr'],
                'aliquots_nbr' => '',
                'notes' => ''
            );
        }
        $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'am.id', $sql));
        foreach ($queryResults as $newRes) {
            $sampleType = $newRes['res']['sample_type'];
            $aliquotType = $newRes['res']['aliquot_type'];
            $tmpData[$sampleType . $aliquotType]['aliquots_nbr'] = $newRes[0]['nbr'];
            $data[] = $tmpData[$sampleType . $aliquotType];
        }

        // **Urine**

        $tmpData = array(
            'sample_type' => __('urine'),
            'cases_nbr' => '',
            'aliquots_nbr' => '',
            'notes' => ''
        );
        $sql = "
			SELECT count(*) AS nbr FROM (
				SELECT DISTINCT  %%id%%
				FROM collections AS col
				INNER JOIN sample_masters AS sm ON col.id = sm.collection_id AND sm.deleted != '1'
				INNER JOIN sample_controls AS sc ON sc.id = sm.sample_control_id
				INNER JOIN aliquot_masters AS am ON am.sample_master_id = sm.id AND am.deleted != '1'
				INNER JOIN aliquot_controls AS ac ON ac.id = am.aliquot_control_id
				WHERE col.deleted != '1' AND ($bankConditions)
				AND am.in_stock IN ('yes - available ','yes - not available')
				AND sc.sample_type LIKE '%urine%'
			) AS res;";
        $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'col.participant_id', $sql));
        $tmpData['cases_nbr'] = $queryResults[0][0]['nbr'];

        $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'am.id', $sql));
        $tmpData['aliquots_nbr'] = $queryResults[0][0]['nbr'];

        $sql = "
			SELECT DISTINCT sc.sample_type,ac.aliquot_type
			FROM collections AS col
			INNER JOIN sample_masters AS sm ON col.id = sm.collection_id AND sm.deleted != '1'
			INNER JOIN sample_controls AS sc ON sc.id = sm.sample_control_id
			INNER JOIN aliquot_masters AS am ON am.sample_master_id = sm.id AND am.deleted != '1'
			INNER JOIN aliquot_controls AS ac ON ac.id = am.aliquot_control_id
			WHERE col.deleted != '1' AND ($bankConditions)
			AND am.in_stock IN ('yes - available ','yes - not available')
			AND sc.sample_type LIKE '%urine%'";
        $queryResults = $this->Report->tryCatchQuery($sql);
        foreach ($queryResults as $newType)
            $tmpData['notes'] .= (empty($tmpData['notes']) ? '' : ' & ') . __($newType['sc']['sample_type']) . ' ' . __($newType['ac']['aliquot_type']);

        $data[] = $tmpData;

        // **Ascite**

        $tmpData = array(
            'sample_type' => __('ascite'),
            'cases_nbr' => '',
            'aliquots_nbr' => '',
            'notes' => ''
        );
        $sql = "
			SELECT count(*) AS nbr FROM (
				SELECT DISTINCT  %%id%%
				FROM collections AS col
				INNER JOIN sample_masters AS sm ON col.id = sm.collection_id AND sm.deleted != '1'
				INNER JOIN sample_controls AS sc ON sc.id = sm.sample_control_id
				INNER JOIN aliquot_masters AS am ON am.sample_master_id = sm.id AND am.deleted != '1'
				INNER JOIN aliquot_controls AS ac ON ac.id = am.aliquot_control_id
				WHERE col.deleted != '1' AND ($bankConditions)
				AND am.in_stock IN ('yes - available ','yes - not available')
				AND sc.sample_type LIKE '%ascite%'
			) AS res;";
        $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'col.participant_id', $sql));
        $tmpData['cases_nbr'] = $queryResults[0][0]['nbr'];

        $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'am.id', $sql));
        $tmpData['aliquots_nbr'] = $queryResults[0][0]['nbr'];

        $sql = "
			SELECT DISTINCT sc.sample_type,ac.aliquot_type
			FROM collections AS col
			INNER JOIN sample_masters AS sm ON col.id = sm.collection_id AND sm.deleted != '1'
			INNER JOIN sample_controls AS sc ON sc.id = sm.sample_control_id
			INNER JOIN aliquot_masters AS am ON am.sample_master_id = sm.id AND am.deleted != '1'
			INNER JOIN aliquot_controls AS ac ON ac.id = am.aliquot_control_id
			WHERE col.deleted != '1' AND ($bankConditions)
			AND am.in_stock IN ('yes - available ','yes - not available')
			AND sc.sample_type LIKE '%ascite%'";
        $queryResults = $this->Report->tryCatchQuery($sql);
        foreach ($queryResults as $newType)
            $tmpData['notes'] .= (empty($tmpData['notes']) ? '' : ' & ') . __($newType['sc']['sample_type']) . ' ' . __($newType['ac']['aliquot_type']);

        $data[] = $tmpData;

        // **other**

        $otherConditions = "sc.sample_type NOT LIKE '%ascite%' AND sc.sample_type NOT LIKE '%urine%' AND sc.sample_type NOT IN ('tissue', $sampleTypes)";

        if ($detailOtherCount) {

            $tmpData = array();
            $sql = "
				SELECT count(*) AS nbr,sample_type, aliquot_type FROM (
					SELECT DISTINCT  %%id%%, sc.sample_type, ac.aliquot_type
					FROM collections AS col
					INNER JOIN sample_masters AS sm ON col.id = sm.collection_id AND sm.deleted != '1'
					INNER JOIN sample_controls AS sc ON sc.id = sm.sample_control_id
					INNER JOIN aliquot_masters AS am ON am.sample_master_id = sm.id AND am.deleted != '1'
					INNER JOIN aliquot_controls AS ac ON ac.id = am.aliquot_control_id
					WHERE col.deleted != '1' AND ($bankConditions)
					AND am.in_stock IN ('yes - available ','yes - not available')
					AND ($otherConditions)
				) AS res GROUP BY sample_type, aliquot_type;";
            $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'col.participant_id', $sql));
            foreach ($queryResults as $newRes) {
                $sampleType = $newRes['res']['sample_type'];
                $aliquotType = $newRes['res']['aliquot_type'];
                $tmpData[$sampleType . $aliquotType] = array(
                    'sample_type' => __($sampleType) . ' ' . __($aliquotType),
                    'cases_nbr' => $newRes[0]['nbr'],
                    'aliquots_nbr' => '',
                    'notes' => ''
                );
            }
            $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'am.id', $sql));
            foreach ($queryResults as $newRes) {
                $sampleType = $newRes['res']['sample_type'];
                $aliquotType = $newRes['res']['aliquot_type'];
                $tmpData[$sampleType . $aliquotType]['aliquots_nbr'] = $newRes[0]['nbr'];
                $data[] = $tmpData[$sampleType . $aliquotType];
            }
        } else {

            $tmpData = array(
                'sample_type' => __('other'),
                'cases_nbr' => '',
                'aliquots_nbr' => '',
                'notes' => ''
            );
            $sql = "
				SELECT count(*) AS nbr FROM (
					SELECT DISTINCT  %%id%%
					FROM collections AS col
					INNER JOIN sample_masters AS sm ON col.id = sm.collection_id AND sm.deleted != '1'
					INNER JOIN sample_controls AS sc ON sc.id = sm.sample_control_id
					INNER JOIN aliquot_masters AS am ON am.sample_master_id = sm.id AND am.deleted != '1'
					INNER JOIN aliquot_controls AS ac ON ac.id = am.aliquot_control_id
					WHERE col.deleted != '1' AND ($bankConditions)
					AND am.in_stock IN ('yes - available ','yes - not available')
					AND ($otherConditions)
				) AS res;";
            $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'col.participant_id', $sql));
            $tmpData['cases_nbr'] = $queryResults[0][0]['nbr'];

            $queryResults = $this->Report->tryCatchQuery(str_replace('%%id%%', 'am.id', $sql));
            $tmpData['aliquots_nbr'] = $queryResults[0][0]['nbr'];

            $sql = "
				SELECT DISTINCT sc.sample_type,ac.aliquot_type
				FROM collections AS col
				INNER JOIN sample_masters AS sm ON col.id = sm.collection_id AND sm.deleted != '1'
				INNER JOIN sample_controls AS sc ON sc.id = sm.sample_control_id
				INNER JOIN aliquot_masters AS am ON am.sample_master_id = sm.id AND am.deleted != '1'
				INNER JOIN aliquot_controls AS ac ON ac.id = am.aliquot_control_id
				WHERE col.deleted != '1' AND ($bankConditions)
				AND am.in_stock IN ('yes - available ','yes - not available')
				AND ($otherConditions)";
            $queryResults = $this->Report->tryCatchQuery($sql);
            foreach ($queryResults as $newType)
                $tmpData['notes'] .= (empty($tmpData['notes']) ? '' : ' & ') . __($newType['sc']['sample_type']) . ' ' . __($newType['ac']['aliquot_type']);

            $data[] = $tmpData;
        }

        // Format data form display

        $finalData = array();
        foreach ($data as $newRow) {
            if ($newRow['cases_nbr']) {
                $finalData[][0] = $newRow;
            }
        }

        $arrayToReturn = array(
            'header' => $header,
            'data' => $finalData,
            'columns_names' => null,
            'error_msg' => null
        );

        return $arrayToReturn;
    }

    /**
     *
     * @param $parameters
     * @return array
     */
    public function participantIdentifiersSummary($parameters)
    {
        if (! AppController::checkLinkPermission('/ClinicalAnnotation/Participants/profile')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }
        if (! AppController::checkLinkPermission('/ClinicalAnnotation/MiscIdentifiers/listall')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }

        $header = null;
        $conditions = array();

        if (isset($parameters['SelectedItemsForCsv']['Participant']['id']))
            $parameters['Participant']['id'] = $parameters['SelectedItemsForCsv']['Participant']['id'];
        if (isset($parameters['Participant']['id'])) {
            // From databrowser
            $participantIds = array_filter($parameters['Participant']['id']);
            if ($participantIds)
                $conditions['Participant.id'] = $participantIds;
        } elseif (isset($parameters['Participant']['participant_identifier_start'])) {
            $participantIdentifierStart = (! empty($parameters['Participant']['participant_identifier_start'])) ? $parameters['Participant']['participant_identifier_start'] : null;
            $participantIdentifierEnd = (! empty($parameters['Participant']['participant_identifier_end'])) ? $parameters['Participant']['participant_identifier_end'] : null;
            if ($participantIdentifierStart)
                $conditions[] = "Participant.participant_identifier >= '$participantIdentifierStart'";
            if ($participantIdentifierEnd)
                $conditions[] = "Participant.participant_identifier <= '$participantIdentifierEnd'";
        } elseif (isset($parameters['Participant']['participant_identifier'])) {
            $participantIdentifiers = array_filter($parameters['Participant']['participant_identifier']);
            if ($participantIdentifiers)
                $conditions['Participant.participant_identifier'] = $participantIdentifiers;
        } else {
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        $conditions[] = "MiscIdentifierControl.flag_link_to_study <> 1";
        if (! $this->Session->read('flag_show_confidential')) {
            $conditions[] = "MiscIdentifierControl.flag_confidential <> 1";
        }

        $miscIdentifierModel = AppModel::getInstance("ClinicalAnnotation", "MiscIdentifier", true);
        $miscIdentifierControlModel = AppModel::getInstance("ClinicalAnnotation", "MiscIdentifierControl", true);

        // *** NOTE: It's user choice to display report in csv whatever the number of records ***
        // $tmpResCount = $miscIdentifierModel->find('count', array('conditions' => $conditions, 'order' => array('MiscIdentifier.participant_id ASC')));
        // if($tmpResCount > Configure::read('databrowser_and_report_results_display_limit')) {
        // return array(
        // 'header' => null,
        // 'data' => null,
        // 'columns_names' => null,
        // 'error_msg' => 'the report contains too many results - please redefine search criteria');
        // }

        //Build template array
        $allCtrls = $miscIdentifierControlModel->find('all', array(
            'conditions' => array(
                "MiscIdentifierControl.flag_link_to_study <> 1"
            )
        ));
        $participantIdentifiersDataTemplate = array(
            // Fields to be consistent with code version 2.7.1
            'BR_Nbr' => null,
            'PR_Nbr' => null,
            'hospital_number' => null
        );
        foreach ($allCtrls as $newCtrl) {
            $key = preg_replace('/([\ \-]+)/', '_', $newCtrl['MiscIdentifierControl']['misc_identifier_name']);
            $participantIdentifiersDataTemplate[$key] = ($newCtrl['MiscIdentifierControl']['flag_confidential'] && ! $this->Session->read('flag_show_confidential')) ? CONFIDENTIAL_MARKER : null;
        }

        // Get and format data
        $miscIdentifiers = $miscIdentifierModel->find('all', array(
            'conditions' => $conditions,
            'order' => array(
                'MiscIdentifier.participant_id ASC'
            )
        ));
        $data = array();
        foreach ($miscIdentifiers as $newIdent) {
            $participantId = $newIdent['Participant']['id'];
            if (! isset($data[$participantId])) {
                $data[$participantId] = array(
                    'Participant' => array(
                        'id' => $newIdent['Participant']['id'],
                        'participant_identifier' => $newIdent['Participant']['participant_identifier'],
                        'first_name' => $newIdent['Participant']['first_name'],
                        'last_name' => $newIdent['Participant']['last_name']
                    ),
                    '0' => $participantIdentifiersDataTemplate
                );
            }
            $data[$participantId]['0'][str_replace(array(
                ' ',
                '-'
            ), array(
                '_',
                '_'
            ), preg_replace('/([\ \-]+)/', '_', $newIdent['MiscIdentifierControl']['misc_identifier_name']))] = $newIdent['MiscIdentifier']['identifier_value'];
        }

        AppController::addWarningMsg(__('all searches are considered as exact searches'));

        return array(
            'header' => $header,
            'data' => $data,
            'columns_names' => null,
            'error_msg' => null
        );
    }

    /**
     *
     * @param $parameters
     * @return array
     */
    public function participantIdentifiersWithCollectionDateSummary($parameters)
    {
        if (! AppController::checkLinkPermission('/ClinicalAnnotation/Participants/profile')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }
        if (! AppController::checkLinkPermission('/ClinicalAnnotation/MiscIdentifiers/listall')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }
        if (! AppController::checkLinkPermission('/InventoryManagement/Collections/detail')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }

        $header = null;
        $conditions = array();
        $participantIdentifiersWithCollectionDate['data'] = array();

        $participantIdentifiers = $this->participantIdentifiersSummary($parameters);

        $inventoryManagementCollectionModel = AppModel::getInstance("InventoryManagement", "Collection", true);

        $startDateForSql = AppController::getFormatedDatetimeSQL($parameters['Collection']['collection_datetime_start'], 'start');
        $endDateForSql = AppController::getFormatedDatetimeSQL($parameters['Collection']['collection_datetime_end'], 'end');

        foreach ($participantIdentifiers['data'] as $key => $participantIdentifier) {
            $conditions = array(
                'Collection.participant_id' => $participantIdentifiers['data'][$key]['Participant']['id'],
                'Collection.collection_datetime BETWEEN ? AND ?' => array(
                    $startDateForSql,
                    $endDateForSql
                )
            );

            $collectionForParticipantId = $inventoryManagementCollectionModel->find('all', array(
                'conditions' => $conditions
            ));

            $infos = '';
            foreach ($collectionForParticipantId as $keyCollection => $collection) {
                $dateCollection = $collection['Collection']['collection_datetime'];
                $dateCollectionAccuracy = $collection['Collection']['collection_datetime_accuracy'];

                $participantIdentifiersWithCollectionDate['data'][$key . '-' . $keyCollection] = array(
                    'Participant' => array(
                        'id' => $participantIdentifiers['data'][$key]['Participant']['id'],
                        'participant_identifier' => $participantIdentifiers['data'][$key]['Participant']['participant_identifier'],
                        'first_name' => $participantIdentifiers['data'][$key]['Participant']['first_name'],
                        'last_name' => $participantIdentifiers['data'][$key]['Participant']['last_name']
                    ),
                    'Collection' => array(
                        'collection_datetime' => $dateCollection,
                        'collection_datetime_accuracy' => $dateCollectionAccuracy
                    ),
                    '0' => array(
                        'BR_Nbr' => $participantIdentifiers['data'][$key][0]['BR_Nbr'],
                        'PR_Nbr' => $participantIdentifiers['data'][$key][0]['PR_Nbr'],
                        'hospital_number' => $participantIdentifiers['data'][$key][0]['hospital_number']
                    )
                );
            }
        }

        AppController::addWarningMsg(__('all searches are considered as exact searches'));

        return array(
            'header' => $participantIdentifiers['header'],
            'data' => $participantIdentifiersWithCollectionDate['data'],
            'columns_names' => null,
            'error_msg' => null
        );
    }

    /**
     *
     * @param $parameters
     * @return array
     */
    public function getAllDerivatives($parameters)
    {
        if (! AppController::checkLinkPermission('/InventoryManagement/SampleMasters/detail')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }

        $header = null;
        $conditions = array();
        // Get Parameters
        if (isset($parameters['SampleMaster']['sample_code'])) {
            // From databrowser
            $selectionLabels = array_filter($parameters['SampleMaster']['sample_code']);
            if ($selectionLabels)
                $conditions['SampleMaster.sample_code'] = $selectionLabels;
        } elseif (isset($parameters['ViewSample']['sample_master_id'])) {
            // From databrowser
            $sampleMasterIds = array_filter($parameters['ViewSample']['sample_master_id']);
            if ($sampleMasterIds)
                $conditions['SampleMaster.id'] = $sampleMasterIds;
        } else {
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        // Load Model
        $viewSampleModel = AppModel::getInstance("InventoryManagement", "ViewSample", true);
        $sampleMasterModel = AppModel::getInstance("InventoryManagement", "SampleMaster", true);
        // Build Res
        $sampleMasterModel->unbindModel(array(
            'belongsTo' => array(
                'Collection'
            ),
            'hasOne' => array(
                'SpecimenDetail',
                'DerivativeDetail'
            ),
            'hasMany' => array(
                'AliquotMaster'
            )
        ));
        $tmpResCount = $sampleMasterModel->find('count', array(
            'conditions' => $conditions,
            'fields' => array(
                'SampleMaster.*',
                'SampleControl.*'
            ),
            'order' => array(
                'SampleMaster.sample_code ASC'
            ),
            'recursive' => 0
        ));
        // *** NOTE: Has to control the number of record because the next report code lines can be really time and memory consuming ***
        if ($tmpResCount > Configure::read('databrowser_and_report_results_display_limit')) {
            return array(
                'header' => null,
                'data' => null,
                'columns_names' => null,
                'error_msg' => __('the report contains too many results - please redefine search criteria') . " [> $tmpResCount " . __('lines') . ']'
            );
        }
        $studiedSamples = $sampleMasterModel->find('all', array(
            'conditions' => $conditions,
            'fields' => array(
                'SampleMaster.*',
                'SampleControl.*'
            ),
            'order' => array(
                'SampleMaster.sample_code ASC'
            ),
            'recursive' => 0
        ));
        $res = array();
        foreach ($studiedSamples as $newStudiedSample) {
            $allDerivativesSamples = $this->getChildrenSamples($viewSampleModel, array(
                $newStudiedSample['SampleMaster']['id']
            ));
            if ($allDerivativesSamples) {
                foreach ($allDerivativesSamples as $newDerivativeSample) {
                    if (array_key_exists('SelectedItemsForCsv', $parameters) && ! in_array($newDerivativeSample['ViewSample']['sample_master_id'], $parameters['SelectedItemsForCsv']['ViewSample']['sample_master_id']))
                        continue;
                    $res[] = array_merge($newStudiedSample, $newDerivativeSample);
                }
            }
        }
        return array(
            'header' => $header,
            'data' => $res,
            'columns_names' => null,
            'error_msg' => null
        );
    }

    /**
     *
     * @param $viewSampleModel
     * @param array $parentSampleIds
     * @return array
     */
    public function getChildrenSamples($viewSampleModel, $parentSampleIds = array())
    {
        if (! AppController::checkLinkPermission('/InventoryManagement/SampleMasters/detail')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }

        if (! empty($parentSampleIds)) {
            // $viewSampleModel->unbindModel(array('hasMany' => array('AliquotMaster')));
            $childrenSamples = $viewSampleModel->find('all', array(
                'conditions' => array(
                    'ViewSample.parent_id' => $parentSampleIds
                ),
                'fields' => array(
                    'ViewSample.*, DerivativeDetail.*'
                ),
                'order' => array(
                    'ViewSample.sample_code ASC'
                ),
                'recursive' => 0
            ));
            $childrenSampleIds = array();
            foreach ($childrenSamples as $tmpSample)
                $childrenSampleIds[] = $tmpSample['ViewSample']['sample_master_id'];
            $subChildrenSamples = $this->getChildrenSamples($viewSampleModel, $childrenSampleIds);
            return array_merge($childrenSamples, $subChildrenSamples);
        }
        return array();
    }

    /**
     *
     * @param $parameters
     * @return array
     */
    public function getAllSpecimens($parameters)
    {
        if (! AppController::checkLinkPermission('/InventoryManagement/SampleMasters/detail')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }

        $header = null;
        $conditions = array(
            "SampleMaster.id != SampleMaster.initial_specimen_sample_id"
        );
        // Get Parameters
        if (isset($parameters['SampleMaster']['sample_code'])) {
            // From databrowser
            $selectionLabels = array_filter($parameters['SampleMaster']['sample_code']);
            if ($selectionLabels)
                $conditions['SampleMaster.sample_code'] = $selectionLabels;
        } elseif (isset($parameters['ViewSample']['sample_master_id'])) {
            // From databrowser
            $sampleMasterIds = array_filter($parameters['ViewSample']['sample_master_id']);
            if ($sampleMasterIds)
                $conditions['SampleMaster.id'] = $sampleMasterIds;
        } else {
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        // Load Model
        $viewSampleModel = AppModel::getInstance("InventoryManagement", "ViewSample", true);
        $sampleMasterModel = AppModel::getInstance("InventoryManagement", "SampleMaster", true);
        // Build Res
        $sampleMasterModel->unbindModel(array(
            'belongsTo' => array(
                'Collection'
            ),
            'hasOne' => array(
                'SpecimenDetail',
                'DerivativeDetail'
            ),
            'hasMany' => array(
                'AliquotMaster'
            )
        ));
        $tmpResCount = $sampleMasterModel->find('count', array(
            'conditions' => $conditions,
            'fields' => array(
                'SampleMaster.*',
                'SampleControl.*'
            ),
            'order' => array(
                'SampleMaster.sample_code ASC'
            ),
            'recursive' => 0
        ));
        // *** NOTE: Has to control the number of record because the next report code lines can be really time and memory consuming ***
        if ($tmpResCount > Configure::read('databrowser_and_report_results_display_limit')) {
            return array(
                'header' => null,
                'data' => null,
                'columns_names' => null,
                'error_msg' => __('the report contains too many results - please redefine search criteria') . " [> $tmpResCount " . __('lines') . ']'
            );
        }
        $studiedSamples = $sampleMasterModel->find('all', array(
            'conditions' => $conditions,
            'fields' => array(
                'SampleMaster.*',
                'SampleControl.*'
            ),
            'order' => array(
                'SampleMaster.sample_code ASC'
            ),
            'recursive' => 0
        ));
        $res = array();
        $tmpInitialSpecimens = array();
        foreach ($studiedSamples as $newStudiedSample) {
            $initialSpecimen = isset($tmpInitialSpecimens[$newStudiedSample['SampleMaster']['initial_specimen_sample_id']]) ? $tmpInitialSpecimens[$newStudiedSample['SampleMaster']['initial_specimen_sample_id']] : $viewSampleModel->find('first', array(
                'conditions' => array(
                    'ViewSample.sample_master_id' => $newStudiedSample['SampleMaster']['initial_specimen_sample_id']
                ),
                'fields' => array(
                    'ViewSample.*, SpecimenDetail.*'
                ),
                'order' => array(
                    'ViewSample.sample_code ASC'
                ),
                'recursive' => 0
            ));
            $tmpInitialSpecimens[$newStudiedSample['SampleMaster']['initial_specimen_sample_id']] = $initialSpecimen;
            if ($initialSpecimen) {
                if (! (array_key_exists('SelectedItemsForCsv', $parameters) && ! in_array($initialSpecimen['ViewSample']['sample_master_id'], $parameters['SelectedItemsForCsv']['ViewSample']['sample_master_id'])))
                    $res[] = array_merge($newStudiedSample, $initialSpecimen);
            }
        }
        return array(
            'header' => $header,
            'data' => $res,
            'columns_names' => null,
            'error_msg' => null
        );
    }

    /**
     *
     * @param $parameters
     * @return array
     */
    public function getAllChildrenStorage($parameters)
    {
        if (! AppController::checkLinkPermission('/StorageLayout/StorageMasters/detail')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }

        $header = null;
        $conditions = array();
        // Get Parameters
        if (isset($parameters['StorageMaster']['selection_label'])) {
            // From databrowser
            $selectionLabels = array_filter($parameters['StorageMaster']['selection_label']);
            if ($selectionLabels)
                $conditions['StorageMaster.selection_label'] = $selectionLabels;
        } elseif (isset($parameters['ViewStorageMaster']['id'])) {
            // From databrowser
            $storageMasterIds = array_filter($parameters['ViewStorageMaster']['id']);
            if ($storageMasterIds)
                $conditions['StorageMaster.id'] = $storageMasterIds;
        } elseif (isset($parameters['NonTmaBlockStorage']['id'])) {
            // From databrowser
            $storageMasterIds = array_filter($parameters['NonTmaBlockStorage']['id']);
            if ($storageMasterIds)
                $conditions['StorageMaster.id'] = $storageMasterIds;
        } else {
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        // Load Model
        $storageMasterModel = AppModel::getInstance("StorageLayout", "StorageMaster", true);
        $storageControlModel = AppModel::getInstance("StorageLayout", "StorageControl", true);
        // Build Res
        $tmpResCount = $storageMasterModel->find('count', array(
            'conditions' => $conditions,
            'fields' => array(
                'StorageMaster.*'
            ),
            'order' => array(
                'StorageMaster.selection_label ASC'
            ),
            'recursive' => - 1
        ));
        // *** NOTE: Has to control the number of record because the next report code lines can be really time and memory consuming ***
        if ($tmpResCount > Configure::read('databrowser_and_report_results_display_limit')) {
            return array(
                'header' => null,
                'data' => null,
                'columns_names' => null,
                'error_msg' => __('the report contains too many results - please redefine search criteria') . " [> $tmpResCount " . __('lines') . ']'
            );
        }
        $tmaStorageContolIds = $storageControlModel->getTmaBlockStorageTypePermissibleValues();
        $tmaStorageContolIds = array_keys($tmaStorageContolIds);
        $studiedStorages = $storageMasterModel->find('all', array(
            'conditions' => $conditions,
            'fields' => array(
                'StorageMaster.*'
            ),
            'order' => array(
                'StorageMaster.selection_label ASC'
            ),
            'recursive' => - 1
        ));
        $res = array();
        foreach ($studiedStorages as $newStudiedStorage) {
            $childrenStorageMasters = $storageMasterModel->children($newStudiedStorage['StorageMaster']['id'], false, array(
                'StorageMaster.*'
            ));
            if ($childrenStorageMasters) {
                foreach ($childrenStorageMasters as $newChild) {
                    if (! in_array($newChild['StorageMaster']['storage_control_id'], $tmaStorageContolIds)) {
                        if (array_key_exists('SelectedItemsForCsv', $parameters) && ! in_array($newChild['StorageMaster']['id'], $parameters['SelectedItemsForCsv']['NonTmaBlockStorage']['id']))
                            continue;
                        $res[] = array_merge($newStudiedStorage, array(
                            'ViewStorageMaster' => $newChild['StorageMaster'],
                            'NonTmaBlockStorage' => $newChild['StorageMaster']
                        ));
                    }
                }
            }
        }
        return array(
            'header' => $header,
            'data' => $res,
            'columns_names' => null,
            'error_msg' => null
        );
    }

    /**
     *
     * @param $parameters
     * @return array
     */
    public function getAllRelatedDiagnosis($parameters)
    {
        if (! AppController::checkLinkPermission('/ClinicalAnnotation/DiagnosisMasters/listall')) {
            $this->atimFlashError(__('you need privileges to access this page'), Router::url(null, true));
        }

        $header = null;
        $conditions = array();
        // Get Parameters
        if (isset($parameters['DiagnosisMaster']['id'])) {
            // From databrowser
            $diagnosisMasterIds = array_filter($parameters['DiagnosisMaster']['id']);
            if ($diagnosisMasterIds)
                $conditions['DiagnosisMaster.id'] = $diagnosisMasterIds;
        } elseif (isset($parameters['Participant']['participant_identifier'])) {
            // From databrowser
            $participantIdentifiers = array_filter($parameters['Participant']['participant_identifier']);
            if ($participantIdentifiers)
                $conditions['Participant.participant_identifier'] = $participantIdentifiers;
        } elseif (isset($parameters['Participant']['id'])) {
            // From databrowser
            $participantIds = array_filter($parameters['Participant']['id']);
            if ($participantIds)
                $conditions['DiagnosisMaster.participant_id'] = $participantIds;
        } else {
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        // Load Model
        $diagnosisMasterModel = AppModel::getInstance("ClinicalAnnotation", "DiagnosisMaster", true);
        // Build Res
        $diagnosisMasterModel->bindModel(array(
            'belongsTo' => array(
                'Participant' => array(
                    'className' => 'ClinicalAnnotation.Participant',
                    'foreignKey' => 'participant_id'
                )
            )
        ), false);
        $diagnosisMasterModel->unbindModel(array(
            'hasMany' => array(
                'Collection'
            )
        ), false);
        $tmpResCount = $diagnosisMasterModel->find('count', array(
            'conditions' => $conditions,
            'fields' => array(
                'DISTINCT primary_id'
            ),
            'recursive' => 0
        ));
        // *** NOTE: Has to control the number of record because the next report code lines can be really time and memory consuming ***
        if ($tmpResCount > Configure::read('databrowser_and_report_results_display_limit')) {
            return array(
                'header' => null,
                'data' => null,
                'columns_names' => null,
                'error_msg' => __('the report contains too many results - please redefine search criteria') . " [> $tmpResCount " . __('lines') . ']'
            );
        }
        $tmpPrimaryIds = $diagnosisMasterModel->find('all', array(
            'conditions' => $conditions,
            'fields' => array(
                'DISTINCT primary_id'
            ),
            'recursive' => 0
        ));
        $primaryIds = array();
        foreach ($tmpPrimaryIds as $newPrimaryId)
            $primaryIds[] = $newPrimaryId['DiagnosisMaster']['primary_id'];
        $conditions2 = array(
            'DiagnosisMaster.primary_id' => $primaryIds
        );
        if (isset($parameters['SelectedItemsForCsv']['DiagnosisMaster']['id']))
            $conditions2['DiagnosisMaster.id'] = $parameters['SelectedItemsForCsv']['DiagnosisMaster']['id'];
        $res = $diagnosisMasterModel->find('all', array(
            'conditions' => $conditions2,
            'fields' => array(
                'Participant.*',
                'DiagnosisMaster.*',
                'DiagnosisControl.*'
            ),
            'order' => array(
                'Participant.participant_identifier ASC',
                'DiagnosisMaster.primary_id ASC',
                'DiagnosisMaster.dx_date ASC'
            ),
            'recursive' => 0
        ));
        return array(
            'header' => $header,
            'data' => $res,
            'columns_names' => null,
            'error_msg' => null
        );
    }

    /**
     *
     * @param $parameters
     * @return array
     */
    public function countNumberOfElementsPerParticipants($parameters)
    {
        if (! AppController::checkLinkPermission('/ClinicalAnnotation/Participants/profile')) {
            $this->atimFlashError(__('you need privileges to access this page'), 'javascript:history.back()');
        }

        $header = null;
        $conditions = array();

        // Get studied model

        $modelsList = array(
            'MiscIdentifier' => array(
                'id',
                array(
                    'misc_identifiers'
                ),
                'misc identifiers'
            ),
            'ConsentMaster' => array(
                'id',
                array(
                    'consent_masters'
                ),
                'consents'
            ),
            'DiagnosisMaster' => array(
                'id',
                array(
                    'diagnosis_masters'
                ),
                'diagnosis'
            ),
            'TreatmentMaster' => array(
                'id',
                array(
                    'treatment_masters'
                ),
                'treatments'
            ),
            'EventMaster' => array(
                'id',
                array(
                    'event_masters'
                ),
                'events'
            ),
            'ReproductiveHistory' => array(
                'id',
                array(
                    'consent_masters'
                ),
                'reproductive histories'
            ),
            'FamilyHistory' => array(
                'id',
                array(
                    'family_histories'
                ),
                'family histories'
            ),
            'ParticipantMessage' => array(
                'id',
                array(
                    'participant_messages'
                ),
                'messages'
            ),
            'ParticipantContact' => array(
                'id',
                array(
                    'participant_contacts'
                ),
                'contacts'
            ),
            'ViewCollection' => array(
                'collection_id',
                array(
                    'collections'
                ),
                'collections'
            ),
            'TreatmentExtendMaster' => array(
                'id',
                array(
                    'treatment_masters'
                ),
                'xxxx'
            ),

            'ViewAliquot' => array(
                'aliquot_master_id',
                array(
                    'aliquot_masters',
                    'collections'
                ),
                'aliquots'
            ),
            'ViewSample' => array(
                'sample_master_id',
                array(
                    'sample_masters',
                    'collections'
                ),
                'samples'
            ),
            'QualityCtrl' => array(
                'id',
                array(
                    'quality_ctrls',
                    'sample_masters',
                    'collections'
                ),
                'quality controls'
            ),
            'SpecimenReviewMaster' => array(
                'id',
                array(
                    'specimen_review_masters',
                    'sample_masters',
                    'collections'
                ),
                'specimen review'
            ),
            'ViewAliquotUse' => array(
                'id',
                array(
                    'view_aliquot_uses',
                    'aliquot_masters',
                    'collections'
                ),
                'aliquot uses and events'
            ),
            'AliquotReviewMaster' => array(
                'id',
                array(
                    'aliquot_review_masters',
                    'aliquot_masters',
                    'sample_masters',
                    'collections'
                ),
                'aliquot review'
            )
        );
        $modelName = null;
        foreach (array_keys($modelsList) as $tmModelName) {
            if (isset($parameters[$tmModelName])) {
                $modelName = $tmModelName;
                break;
            }
        }

        // Get data

        if ($modelName) {
            list ($modelIdKey, $orderedLinkedTableNames, $headerDetail) = $modelsList[$modelName];
            $ids = array_filter($parameters[$modelName][$modelIdKey]);
            $ids = empty($ids) ? '-1' : implode(',', $ids);
            $joins = array();
            $deleteConstraints = array();
            $modelLevels = 0;
            $foreignKeyToPreviousModel = null;
            foreach (array_reverse($orderedLinkedTableNames) as $newTableName) {
                $modelLevels ++;
                if ($modelLevels == 1) {
                    $joins[] = "INNER JOIN $newTableName ModelLevel1 ON ModelLevel1.participant_id = Participant.id";
                    $foreignKeyToPreviousModel = preg_replace('/s$/', '_id', $newTableName);
                } else {
                    $joins[] = "INNER JOIN $newTableName ModelLevel$modelLevels ON ModelLevel$modelLevels." . $foreignKeyToPreviousModel . " = ModelLevel" . ($modelLevels - 1) . ".id";
                    $foreignKeyToPreviousModel = preg_replace('/s$/', '_id', $newTableName);
                }
                if ($newTableName != 'view_aliquot_uses')
                    $deleteConstraints[] = "ModelLevel$modelLevels.deleted <> 1";
            }
            $query = "SELECT count(*) AS nbr_of_elements, Participant.*
				FROM participants AS Participant " . implode(' ', $joins) . " WHERE Participant.deleted <> 1 AND " . implode(' AND ', $deleteConstraints) . " AND ModelLevel$modelLevels.id IN ($ids)
				GROUP BY Participant.id;";
            // Set header
            $header = str_replace('%s', __($headerDetail), __('number of %s per participant'));
        } else {
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }

        $participantModel = AppModel::getInstance("ClinicalAnnotation", "Participant", true);
        $data = $participantModel->tryCatchQuery($query);
        // *** NOTE: It's user choice to display report in csv whatever the number of records ***
        // if(sizeof($data) > Configure::read('databrowser_and_report_results_display_limit')) {
        // return array(
        // 'header' => null,
        // 'data' => null,
        // 'columns_names' => null,
        // 'error_msg' => 'the report contains too many results - please redefine search criteria');
        // }
        foreach ($data as &$newRow)
            $newRow['Generated'] = $newRow['0'];
        return array(
            'header' => $header,
            'data' => $data,
            'columns_names' => null,
            'error_msg' => null
        );
    }

    /**
     *
     * @param $parameters
     * @return array
     */
    public function atimDemoReportParticipantClinicalData($parameters)
    {
        if (! AppController::checkLinkPermission('/ClinicalAnnotation/Participants/profile')) {
            $this->atimFlashError(__('you need privileges to access this page') . 'xxx', 'javascript:history.back()');
        }

        $displayExactSearchWarning = false;
        $conditions = array();
        if (isset($parameters['Participant']['id']) && ! empty($parameters['Participant']['id'])) {
            // From databrowser
            $participantIds = array_filter($parameters['Participant']['id']);
            if ($participantIds)
                $conditions[] = "Participant.id IN ('" . implode("','", $participantIds) . "')";
        } elseif (isset($parameters['Participant']['participant_identifier_start'])) {
            $participantIdentifierStart = (! empty($parameters['Participant']['participant_identifier_start'])) ? $parameters['Participant']['participant_identifier_start'] : null;
            $participantIdentifierEnd = (! empty($parameters['Participant']['participant_identifier_end'])) ? $parameters['Participant']['participant_identifier_end'] : null;
            if ($participantIdentifierStart) {
                $conditions[] = "Participant.participant_identifier >= '$participantIdentifierStart'";
            } elseif ($participantIdentifierEnd) {
                $conditions[] = "Participant.participant_identifier <= '$participantIdentifierEnd'";
            }
        } elseif (isset($parameters['Participant']['participant_identifier'])) {
            $displayExactSearchWarning = true;
            $participantIdentifiers = array_filter($parameters['Participant']['participant_identifier']);
            if ($participantIdentifiers)
                $conditions[] = "Participant.participant_identifier IN ('" . implode("','", $participantIdentifiers) . "')";
        } else {
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }

        $participantModel = AppModel::getInstance("ClinicalAnnotation", "Participant", true);

        // Get Controls Data

        $query = "SELECT id,event_type, detail_tablename FROM event_controls WHERE flag_active = 1;";
        $eventControls = array();
        foreach ($participantModel->query($query) as $res) {
            $eventControls[$res['event_controls']['event_type']] = array(
                'id' => $res['event_controls']['id'],
                'detail_tablename' => $res['event_controls']['detail_tablename']
            );
        }
        $query = "SELECT id,tx_method, detail_tablename FROM treatment_controls WHERE flag_active = 1;";
        $txControls = array();
        foreach ($participantModel->query($query) as $res) {
            $txControls[$res['treatment_controls']['tx_method']] = array(
                'id' => $res['treatment_controls']['id'],
                'detail_tablename' => $res['treatment_controls']['detail_tablename']
            );
        }
        $query = "SELECT id, category, controls_type, detail_tablename FROM diagnosis_controls WHERE flag_active = 1;";
        $dxControls = array();
        foreach ($participantModel->query($query) as $res) {
            $dxControls[$res['diagnosis_controls']['category'] . '-' . $res['diagnosis_controls']['controls_type']] = array(
                'id' => $res['diagnosis_controls']['id'],
                'detail_tablename' => $res['diagnosis_controls']['detail_tablename']
            );
        }

        // Get Participant

        $participantsData = $participantModel->find('all', array(
            'conditions' => $conditions
        ));
        if (! $participantsData) {
            return array(
                'header' => '',
                'data' => array(),
                'columns_names' => null,
                'error_msg' => null
            );
        }
        $participantidToData = array();
        foreach ($participantsData as $newParticipant) {
            $participantidToData[$newParticipant['Participant']['id']] = array_merge($newParticipant, array(
                'DiagnosisControl' => array(
                    'controls_type' => null,
                    'category' => null
                ),
                'DiagnosisMaster' => array(
                    'id' => null,
                    'dx_date' => null,
                    'dx_date_accuracy' => null,
                    'icd10_code' => null,
                    'icd_0_3_topography_category' => null,
                    'clinical_tstage' => null,
                    'clinical_nstage' => null,
                    'clinical_mstage' => null,
                    'clinical_stage_summary' => null,
                    'path_tstage' => null,
                    'path_nstage' => null,
                    'path_mstage' => null,
                    'path_stage_summary' => null
                ),
                'TreatmentMaster' => array(
                    'target_site_icdo' => null,
                    'start_date' => null,
                    'start_date_accuracy' => null
                ),
                'TreatmentControl' => array(
                    'tx_method' => null,
                    'disease_site' => null
                ),
                'TreatmentDetail' => array(
                    'path_num' => null
                ),
                'Generated' => array(
                    'chemo_pre_surgery_start_date' => null,
                    'chemo_post_surgery_start_date' => null
                )
            ));
        }
        $participantIds = $participantModel->find('list', array(
            'fields' => array(
                'Participant.id'
            ),
            'conditions' => $conditions
        ));

        // Get first primary diagnosis + first surgery + pre-post chemo

        $diagnosisModel = AppModel::getInstance("ClinicalAnnotation", "DiagnosisMaster", true);

        $treatmentModel = AppModel::getInstance("ClinicalAnnotation", "TreatmentMaster", true);
        $txJoin = array(
            'table' => 'txd_surgeries',
            'alias' => 'TreatmentDetail',
            'type' => 'INNER',
            'conditions' => array(
                'TreatmentDetail.treatment_master_id = TreatmentMaster.id'
            )
        );

        $participantIdToFirstDiagnosis = array();
        $conditions = array(
            'DiagnosisMaster.participant_id' => $participantIds,
            'DiagnosisMaster.diagnosis_control_id' => array(
                $dxControls['primary-breast']['id'],
                $dxControls['primary-blood']['id'],
                $dxControls['primary-other tissue']['id']
            )
        );
        $diagnosisData = $diagnosisModel->find('all', array(
            'conditions' => $conditions,
            'order' => array(
                'DiagnosisMaster.participant_id, DiagnosisMaster.dx_date ASC'
            )
        ));
        foreach ($diagnosisData as $newDiagnosis) {
            if (! $participantidToData[$newDiagnosis['DiagnosisMaster']['participant_id']]['DiagnosisMaster']['id']) {
                $participantidToData[$newDiagnosis['DiagnosisMaster']['participant_id']]['DiagnosisMaster'] = $newDiagnosis['DiagnosisMaster'];
                $participantidToData[$newDiagnosis['DiagnosisMaster']['participant_id']]['DiagnosisControl'] = $newDiagnosis['DiagnosisControl'];

                // Get first treatment (demo does not take care about accuracy)
                if ($newDiagnosis['DiagnosisMaster']['dx_date']) {
                    $conditions = array(
                        'TreatmentMaster.participant_id' => $newDiagnosis['DiagnosisMaster']['participant_id'],
                        'TreatmentMaster.start_date IS NOT NULL',
                        'TreatmentMaster.start_date >= ' => $newDiagnosis['DiagnosisMaster']['dx_date']
                    );
                    $participantSurgery = $treatmentModel->find('first', array(
                        'conditions' => $conditions,
                        'joins' => array(
                            $txJoin
                        ),
                        'order' => array(
                            'TreatmentMaster.start_date ASC'
                        )
                    ));
                    if ($participantSurgery) {
                        $participantidToData[$newDiagnosis['DiagnosisMaster']['participant_id']] = array_merge($participantidToData[$newDiagnosis['DiagnosisMaster']['participant_id']], $participantSurgery);
                        // Chemo Pre-Post surgery (demo does not take care about accuracy)
                        $conditions = array(
                            'TreatmentMaster.participant_id' => $newDiagnosis['DiagnosisMaster']['participant_id'],
                            'TreatmentMaster.start_date IS NOT NULL',
                            'TreatmentMaster.start_date <= ' => $participantSurgery['TreatmentMaster']['start_date'],
                            'TreatmentMaster.treatment_control_id' => $txControls['chemotherapy']['id']
                        );
                        $participantChemo = $treatmentModel->find('first', array(
                            'conditions' => $conditions,
                            'order' => array(
                                'TreatmentMaster.start_date DESC'
                            )
                        ));
                        if ($participantChemo) {
                            $participantidToData[$newDiagnosis['DiagnosisMaster']['participant_id']]['Generated']['chemo_pre_surgery_start_date'] = $participantChemo['TreatmentMaster']['start_date'];
                            $participantidToData[$newDiagnosis['DiagnosisMaster']['participant_id']]['Generated']['chemo_pre_surgery_start_date_accuracy'] = $participantChemo['TreatmentMaster']['start_date_accuracy'];
                        }
                        $conditions = array(
                            'TreatmentMaster.participant_id' => $newDiagnosis['DiagnosisMaster']['participant_id'],
                            'TreatmentMaster.start_date IS NOT NULL',
                            'TreatmentMaster.start_date >= ' => $participantSurgery['TreatmentMaster']['start_date'],
                            'TreatmentMaster.treatment_control_id' => $txControls['chemotherapy']['id']
                        );
                        $participantChemo = $treatmentModel->find('first', array(
                            'conditions' => $conditions,
                            'order' => array(
                                'TreatmentMaster.start_date ASC'
                            )
                        ));
                        if ($participantChemo) {
                            $participantidToData[$newDiagnosis['DiagnosisMaster']['participant_id']]['Generated']['chemo_post_surgery_start_date'] = $participantChemo['TreatmentMaster']['start_date'];
                            $participantidToData[$newDiagnosis['DiagnosisMaster']['participant_id']]['Generated']['chemo_post_surgery_start_date_accuracy'] = $participantChemo['TreatmentMaster']['start_date_accuracy'];
                        }
                    }
                }
            }
        }

        if ($displayExactSearchWarning)
            AppController::addWarningMsg(__('all searches are considered as exact searches'));

        return array(
            'header' => '',
            'data' => $participantidToData,
            'columns_names' => null,
            'error_msg' => null,
            'structure_accuracy' => array(
                'Generated' => array(
                    'chemo_pre_surgery_start_date' => 'chemo_pre_surgery_start_date_accuracy',
                    'chemo_post_surgery_start_date' => 'chemo_post_surgery_start_date_accuracy'
                )
            )
        );
    }

    /**
     * @param $parameters
     * @return array
     */
    private function getUnclassifiedStorageList($parameters)
    {
        if (! AppController::checkLinkPermission('/StorageLayout/StorageMasters/detail')) {
            $this->atimFlashError(__('you need privileges to access this page'), "javascript::back()");
        }

        $storageMasterModel = AppModel::getInstance("StorageLayout", "ViewStorageMaster", true);
        $datamartReportModel = AppModel::getInstance("Datamart", "Report", true);
        $aliquotMasterModel = AppModel::getInstance('InventoryManagement', 'AliquotMaster', true);
        $tmaSlideModel = AppModel::getInstance('StorageLayout', 'TmaSlide', true);

        $reportData = $datamartReportModel->findByFunction(__FUNCTION__);
        $aliasSearch = $reportData['Report']['form_alias_for_search'];
        $conditions = array(
            'conditions' => $this->Structures->parseSearchConditions($aliasSearch)
        );

        $result = $storageMasterModel->find('all', $conditions);

        foreach ($result as $key => &$field) {
            $field['NonTmaBlockStorage']['id'] = $field['ViewStorageMaster']['id'];
            $sum = 0;
            $field['FunctionManagement']['unclassified'] = $sum;
            $storageMasterId = $field['ViewStorageMaster']['id'];
            $storageMasterX = $field['StorageControl']['coord_x_size'];
            $storageMasterY = $field['StorageControl']['coord_y_size'];

            if (! empty($storageMasterX) || ! empty($storageMasterY)) {
                $aliquotMasterConditions = array();
                $aliquotMasterConditions['AliquotMaster.storage_master_id'] = $storageMasterId;
                if (! empty($storageMasterX)) {
                    $aliquotMasterConditions['OR']['AliquotMaster.storage_coord_x'] = null;
                    $aliquotMasterConditions['OR']["AliquotMaster.storage_coord_x like"] = '';
                }
                if (! empty($storageMasterY)) {
                    $aliquotMasterConditions['OR']['AliquotMaster.storage_coord_y'] = null;
                    $aliquotMasterConditions['OR']["AliquotMaster.storage_coord_y like"] = '';
                }
                $aliquotMasterC = $aliquotMasterModel->find('all', array(
                    'conditions' => $aliquotMasterConditions,
                    'recursive' => - 1
                ));
                $sum += count($aliquotMasterC);
                $aliquotMasterC = Set::extract("{n}.AliquotMaster", $aliquotMasterC);

                $tmaSlideConditions = array();
                $tmaSlideConditions['TmaSlide.storage_master_id'] = $storageMasterId;
                if (! empty($storageMasterX)) {
                    $tmaSlideConditions['OR']['TmaSlide.storage_coord_x'] = null;
                    $tmaSlideConditions['OR']["TmaSlide.storage_coord_x like"] = '';
                }
                if (! empty($storageMasterY)) {
                    $tmaSlideConditions['OR']['TmaSlide.storage_coord_y'] = null;
                    $tmaSlideConditions['OR']["TmaSlide.storage_coord_y like"] = '';
                }
                $tmaSlideC = $tmaSlideModel->find('all', array(
                    'conditions' => $tmaSlideConditions,
                    'recursive' => - 1
                ));
                $sum += count($tmaSlideC);
                $tmaSlideC = Set::extract("{n}.TmaSlide", $tmaSlideC);

                $storageMasterConditions = array();
                $storageMasterConditions['ViewStorageMaster.parent_id'] = $storageMasterId;
                if (! empty($storageMasterX)) {
                    $storageMasterConditions['OR']['ViewStorageMaster.parent_storage_coord_x'] = null;
                    $storageMasterConditions['OR']["ViewStorageMaster.parent_storage_coord_x like"] = '';
                }
                if (! empty($storageMasterY)) {
                    $storageMasterConditions['OR']['ViewStorageMaster.parent_storage_coord_y'] = null;
                    $storageMasterConditions['OR']["ViewStorageMaster.parent_storage_coord_y like"] = '';
                }
                $storageMasterC = $storageMasterModel->find('all', array(
                    'conditions' => $storageMasterConditions
                ));
                $sum += count($storageMasterC);
                $storageMasterC = Set::extract("{n}.ViewStorageMaster", $storageMasterC);

                $field['FunctionManagement']['unclassified'] = $sum;
            }
            if (empty($sum)) {
                unset($result[$key]);
            }
        }

        return array(
            'header' => null,
            'data' => $result,
            'columns_names' => null,
            'error_msg' => null
        );
    }
}