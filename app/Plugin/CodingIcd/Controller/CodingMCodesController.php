<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */

/**
 * Class CodingMCodesController
 */
class CodingMCodesController extends CodingIcdAppController
{

    public $uses = array(
        "CodingIcd.CodingMCode"
    );

    public $icdDescriptionTableFields = array(
        'description'
    );

    /*
     * Forms Helper appends a "tool" link to the "add" and "edit" form types
     * Clicking that link reveals a DIV tag with this Action/View that should have functionality to affect the indicated form field.
     */
    /**
     *
     * @param $mCodeValueSetName
     */
    public function tool($mCodeValueSetName)
    {
        parent::tool($mCodeValueSetName);
        $this->set("mCodeValueSetName", $mCodeValueSetName);
    }

    /**
     *
     * @param string $mCodeValueSetName
     * @param bool $isTool
     */
    public function search($mCodeValueSetName, $isTool = true)
    {
        $this->CodingMCode->setValueSetName($mCodeValueSetName);
        parent::globalSearch($isTool, $this->CodingMCode);
        $this->set("mCodeValueSetName", $mCodeValueSetName);
    }

    /**
     *
     * @param string $mCodeValueSetName
     */
    public function autocomplete($mCodeValueSetName)
    {
        $this->CodingMCode->setValueSetName($mCodeValueSetName);
        parent::globalAutocomplete($this->CodingMCode);
    }
}