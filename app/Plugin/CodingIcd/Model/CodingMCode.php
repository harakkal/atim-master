<?php

/**
 * Class CodingMCode
 */
class CodingMCode extends CodingIcdAppModel
{

    // ---------------------------------------------------------------------------------------------------------------
    // Coding System: mCode
    // From: mCode Data Dictionnary (mCODE™: Minimal Common Oncology Data Elements)
    // Version: 0.9.1
    // Notes: https://mcodeinitiative.org/
    // ---------------------------------------------------------------------------------------------------------------
    protected static $singleton = null;

    public $name = 'CodingMCode';

    public $useTable = 'coding_mcode_codes';

    public $icdDescriptionTableFields = array(
        'search_format' => array(
            'description',
            'code_system'
        ),
        'detail_format' => array(
            'description',
            'code_system'
        )
    );

    public $validate = array();

    protected static $valueSetName = null;

    /**
     * CodingMCode constructor.
     */
    public function __construct()
    {
        parent::__construct();
        self::$singleton = $this;
    }

    public static function setValueSetName($mCodeValueSetName)
    {
        self::$valueSetName = $mCodeValueSetName;
    }

    /**
     *
     * @param $id
     * @param $mCodeValueSetName
     * @return bool
     */
    public static function validateId($id)
    {
        return self::$singleton->globalValidateId($id);
    }

    /**
     *
     * @return CodingMCode|null
     */
    public static function getSingleton()
    {
        return self::$singleton;
    }

    /**
     *
     * @param array $queryData
     * @return array
     */
    public function beforeFind($queryData)
    {
        if (self::$valueSetName) {
            $queryData['conditions'] = array(
                'AND' => array(
                    $queryData['conditions'],
                    array(
                        "CodingMCode.value_set_name = '" . self::$valueSetName . "'"
                    )
                )
            );
        }
        return $queryData;
    }

    /**
     *
     * @param $mCodeValueSetName Value
     *            Set Name
     * @return array mCode Value Set Details
     */
    public function getValueSetDetails($mCodeValueSetName)
    {
        $mCodeValueSetName = is_array($mCodeValueSetName) ? array_shift($mCodeValueSetName) : $mCodeValueSetName;
        $lang = Configure::read('Config.language') == "eng" ? "en" : "fr";
        $conditions = array(
            "CodingMCode.value_set_name = '$mCodeValueSetName'"
        );
        $valueSetDetails = array();
        foreach ($this->find('all', array('conditions' => $conditions)) as $newCodeSystem) {
            $valueSetDetails[$newCodeSystem['CodingMCode']['id']] = $newCodeSystem['CodingMCode'][$lang . '_description'] . ' - ' . $newCodeSystem['CodingMCode'][$lang . '_code_system'];
        }
        return $valueSetDetails;
    }
}