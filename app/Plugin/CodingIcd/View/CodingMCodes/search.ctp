<?php
 /**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */
if (isset($overflow)) {
    ?>
<ul class="error">
	<li><?php echo(__("the query returned too many results").". ".__("try refining the search parameters")); ?>.</li>
</ul>
<?php
}
$header = array(
    'title' => __('mcode %s code picker', $mCodeValueSetName),
    'description' => __('select a mcode %s code', $mCodeValueSetName)
);
$indexLinkPrefix = "/CodingIcd/CodingMCodes/search/";
$links = array(
    'index' => array(
        'detail' => $indexLinkPrefix . '%%CodingIcd.id%%'
    ),
    'bottom' => array(
        'back' => '/CodingIcd/CodingMCodes/tool/' . $mCodeValueSetName
    )
);
$this->Structures->build($atimStructure, array(
    'type' => 'index',
    'settings' => array(
        'pagination' => false,
        'header' => $header
    ),
    'links' => $links
));
?>
<script type="text/javascript">
$(function(){
	$("#default_popup a.detail").click(function(){
		val = $(this).attr("href");
		indexLinkPrefix = "<?php echo($indexLinkPrefix); ?>";
		val = val.substr(val.indexOf(indexLinkPrefix) + indexLinkPrefix.length);
		$(toolTarget).val(val);
		$("#default_popup").popup('close');
		return false;
	});
	$("#default_popup a.cancel").click(function(){
		$.get($(this).attr("href"), null, function(data){
            data = normaliseData(data);
			$("#default_popup").html("<div class='wrapper'><div class='frame'>" + data + "</div></div>").popup();
			initMultiChoiceSelect("#default_popup");
		});
		return false;
	});
	$("#default_popup div.search-result-div").hide();
});
</script>