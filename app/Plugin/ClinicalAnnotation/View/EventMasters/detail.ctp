<?php
 /**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */
$structureLinks = array(
    'index' => array(
        'detail' => '/ClinicalAnnotation/DiagnosisMasters/detail/%%DiagnosisMaster.participant_id%%/%%DiagnosisMaster.id%%'
    ),
    'bottom' => array(
        'edit' => '/ClinicalAnnotation/EventMasters/edit/' . $atimMenuVariables['Participant.id'] . '/' . $atimMenuVariables['EventMaster.id'],
        'delete' => '/ClinicalAnnotation/EventMasters/delete/' . $atimMenuVariables['Participant.id'] . '/' . $atimMenuVariables['EventMaster.id']
    )
);


// Define subform with action bar
$flagUseForCcl = $this->data['EventControl']['flag_use_for_ccl'];
$subFormWithActionButton = 'event';
if ($flagUseForCcl && !$isAjax && $cclsList['EventMaster']['active']) {
    $subFormWithActionButton = 'ccl';
} elseif (empty($notAccessToDiagnosis) && !$isAjax) {
    $subFormWithActionButton = 'diagnosis';
}

// 1- EVENT DATA

$structureSettings = array(
    'actions' => ($subFormWithActionButton == 'event'),
    'form_bottom' => ($subFormWithActionButton == 'event')
);

$finalAtimStructure = $atimStructure;
$finalOptions = array(
    'links' => $structureLinks,
    'settings' => $structureSettings
);

$hookLink = $this->Structures->hook();
if ($hookLink) {
    require ($hookLink);
}

$this->Structures->build($finalAtimStructure, $finalOptions);

if (! $isAjax) {
    
    // 2- DIAGNOSTICS
    
    $structureSettings = array(
        'form_inputs' => false,
        'pagination' => false,
        'actions' => ($subFormWithActionButton == 'diagnosis'),
        'form_bottom' => true,
        'header' => __('related diagnosis'),
        'form_top' => false
    );
    
    $finalOptions = array(
        'data' => $diagnosisData,
        'type' => 'index',
        'settings' => $structureSettings,
        'links' => $structureLinks
    );
    $finalAtimStructure = $diagnosisStructure;
    
    if (! AppController::checkLinkPermission('/ClinicalAnnotation/DiagnosisMasters/listall')) {
        $finalOptions['type'] = 'detail';
        $finalAtimStructure = array();
        $finalOptions['extras'] = '<div>' . __('You are not authorized to access that location.') . '</div>';
    }
    
    $displayNextSubForm = empty($notAccessToDiagnosis);
    
    $hookLink = $this->Structures->hook('dx_list');
    if ($hookLink) {
        require ($hookLink);
    }
    
    if ($displayNextSubForm)
        $this->Structures->build($finalAtimStructure, $finalOptions);
    
    $finalAtimStructure = array();
    $finalOptions['type'] = 'detail';
    $finalOptions['settings']['header'] = __('links to collections');
    $finalOptions['settings']['actions'] = true;
    $finalOptions['extras'] = $this->Structures->ajaxIndex('ClinicalAnnotation/ClinicalCollectionLinks/listall/' . $atimMenuVariables['Participant.id'] . '/noActions:/filterModel:EventMaster/filterId:' . $atimMenuVariables['EventMaster.id']);
    
    $displayNextSubForm = ($subFormWithActionButton == 'ccl');
    
    $hookLink = $this->Structures->hook('ccl');
    if ($hookLink) {
        require ($hookLink);
    }
    
    if ($displayNextSubForm)
        $this->Structures->build(array(), $finalOptions);
}