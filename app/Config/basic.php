<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */

/**
 * Class AtimDebug
 */
class AtimDebug
{
    private static $timeout = 1000;
    private static $counter = 0;

    private static $end = false;
    private static $saveInFile = true;
    private static $fileNameOutput = "logs.txt";
    private static $screen = true;
    private static $menu = true;
    private static $conditions = true;
    private static $from = 1;
    private static $count = 65536;
    private static $function = false;
    private static $file = false;
    private static $class = false;
    private static $force = false;
    private static $clear = false;
    private static $data = array();
    private static $options = array();
    private static $key = "";
    private static $time = "";
    private static $template = "";
    private static $hasFunction = "";
    private static $hasClass = "";
    private static $hasFile = "";
    private static $lineSeperator = "\n\t\t------------------------\n";


    /**
     *
     * @param int $counter after $counter time that this function run, it send $message to API
     * @param array $message the message that will be send to API after $counter time of execution of this function
     */
    public static function stop($counter = 1, $message = array('message'))
    {
        if (! is_array($message)) {
            $message = array(
                $message
            );
        }
        if (self::$counter == $counter) {
            d($message);
            die("stop");
        } else {
            self::$counter ++;
        }
    }


    /**
     * @return array
     */
    private static function getStackTrace()
    {
        $bt = debug_backtrace();
        $result = array();
        extract(self::$options);
        self::$hasFunction = empty($function)? true : false;
        self::$hasFile = empty($file)? true : false;
        self::$hasClass = empty($class)? true : false;

        foreach ($bt as $key => $unit) {
            unset($unit['args']);
            unset($unit['object']);
            $u['file'] = isset($unit['file'])? $unit['file'] : '?';
            $u['line'] = isset($unit['line'])? $unit['line'] : '?';
            $u['function'] = isset($unit['function'])? $unit['function'] : '?';
            $u['class'] = isset($unit['class'])? $unit['class'] : '?';

            $files = explode(DS, $u['file']);

            if (!self::$hasFile){
                foreach ($files as $f) {
                    if (strtolower($f) == strtolower($file)) {
                        self::$hasFile = true;
                        break;
                    }
                }
            }

            if (!self::$hasFunction ) {
                if (strtolower($function) == strtolower($u['function'])){
                    self::$hasFunction = true;
                }
            }

            if (!self::$hasClass ) {
                if (strtolower($class) == strtolower($u['class'])){
                    self::$hasClass = true;
                }
            }

            if (!self::$hasFunction || !self::$hasFile || !self::$hasClass){
                self::$hasFunction = empty($function)? true : false;
                self::$hasClass = empty($class)? true : false;
                self::$hasFile = empty($file)? true : false;
            }

            foreach ($files as $f) {
                if ($f == "DebugKit") {
                    self::$hasFile = false;
                    self::$hasFunction = false;
                    self::$hasClass = false;
                }
            }

            if ($u['class'] != __CLASS__){
                //$result[] = $u['file'] . $u['line'] . $u['function'] . $u['class']."\n";
            }else{
                $result[0] = $u['file'] . $u['line'] . $u['function'] . $u['class']."\n";
            }
        }
        return $result;
    }

    /**
     * @return string
     */
    private static function getHashKey()
    {
        return md5(json_encode(self::getStackTrace()));
    }

    /**
     * @param null $key
     */
    private static function cleanup($key = null)
    {
        extract(self::$options);
        $key = ($clear) ? 'all' : $key;

        if (isset($_SESSION['ATiMDebug']) && is_array($_SESSION['ATiMDebug'])){
            if (strtolower($key) == 'all'){
                $_SESSION['ATiMDebug'] = array(
                    'time' => self::$time
                );
            }else{
                foreach ($_SESSION['ATiMDebug'] as $key => &$debug){
                    if (is_numeric($key)){
                        if ((self::$time - $key > self::$timeout * 10) || (self::$time != $key && empty($debug))){
                            unset($_SESSION['ATiMDebug'][$key]);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $options
     */
    private static function initialize($options)
    {
//        self::$options = array_change_key_case_recursive($options);

        self::$options = $options + array(
            'end' => self::$end,
            'saveInFile' => self::$saveInFile,
            'fileNameOutput' => self::$fileNameOutput,
            'screen' => self::$screen,
            'menu' => self::$menu,
            'conditions' => self::$conditions,
            'from' => self::$from,
            'count' => self::$count,
            'function' => self::$function,
            'file' => self::$file,
            'class' => self::$class,
            'force' =>self::$force,
            'clear' => self::$clear
        );

        self::$key = self::getHashKey();

        self::$time = isset($_SESSION['ATiMDebug']['time']) ? $_SESSION['ATiMDebug']['time'] : round(microtime(true) * 10);
        if (!isset($_SESSION['ATiMDebug'][self::$time])) {
            $_SESSION['ATiMDebug'][self::$time] = array(
            );
        }

        App::uses('Debugger', 'Utility');

        if (!isset(self::$data[self::$key])){
            self::$data[self::$key] = array(
                'time' => date('j-m-Y H:i:s'),
                'counter' => 0,
                'runCounter' => 0,
                'message' => "",
            );
        }

    }

    /**
     * @return bool
     */
    private static function CanContinue()
    {
        $canContinue = true;

        extract(self::$options);
        if (!self::$hasFunction || !self::$hasFile || !self::$hasClass){
            $canContinue = false;
        }

        if ((! Configure::read('debug'))) {
            if ($force){
                if (!isset($_SESSION['Auth']['User']['group_id']) || $_SESSION['Auth']['User']['group_id'] != 1){
                    self::$options['screen'] = false;
                    self::$options['menu'] = false;
                }
            }else{
                self::$options['screen'] = false;
                self::$options['menu'] = false;
            }
        }

        if(!$conditions){
            $canContinue = false;
        }


        if ($canContinue && self::$data[self::$key]['counter'] < $from - 1){
            self::$data[self::$key]['counter'] ++;
            $canContinue = false;
        }

        if (self::$data[self::$key]['runCounter'] > $count - 1){
            $canContinue = false;
        }

        return $canContinue;
    }

    /**
     * ### options
     * - end(false): End the execution
     * - saveInFile(true): Determine if save in a file or not
     * - fileNameOutput(logs.txt): The output file name
     * - screen(true): Show the result on the screen
     * - menu(true): Show the result on debug-kit D menu
     * - conditions(true): Show if the condition is true
     * - from(1): Show the message from which counter
     * - count(65536): The maximum of time to show the message
     * - function: Show the output if the function exists in the call sequence
     * - class: Show the output if the class exists in the call sequence
     * - file: Show the output if the file exists in the call sequence
     * - force: Force to show in file even in the debug mode and the usr belong admin group
     * - clear: Erase the Session
     * @param string $message The message will be as an output to file, screen or debugKit d menu
     * @param array $options The different debug options
     * @return null
     */
    public static function d($message = "", $options = array())
    {
        self::initialize($options);

        self::cleanup();
        if (!self::CanContinue()){
            return;
        }
        extract(self::$options);

        App::uses('Debugger', 'Utility');

        $trace = Debugger::trace(array(
            'start' => 1,
            'depth' => 2,
            'format' => 'array'
        ));
        $fileName = str_replace(array(
            CAKE_CORE_INCLUDE_PATH,
            ROOT
        ), '', $trace[0]['file']);

        $line = $trace[0]['line'];
        $html = <<<HTML
<div class="cake-debug-output">
<div class="minus-button"><a href="javascript:void(0)" class="debug-button">-</a></div>
%s
<pre class="cake-debug">
%s
</pre>
</div>
HTML;
        $text = <<<TEXT
%s
########## DEBUG ##########
%s
###########################

TEXT;
        $template = $html;
        self::$data[self::$key]["runCounter"]++;
        $time = date('j-m-Y H:i:s', round(self::$time / 10));
        $counter = self::$data[self::$key]["runCounter"];
        if (PHP_SAPI === 'cli') {
            $template = $text;
            $lineInfo = sprintf('%s- %s%s (line %s)', $counter, $fileName, $line, $time);
        }
        $var = print_r($message, true);
        $var = h($var);
        self::$data[self::$key]["message"] = $var;
        $lineInfo = sprintf('<span><strong>%s- %s</strong> (line <strong>%s</strong>)</span><span style="color: green"> %s</span>', $counter, $fileName, $line, $time);
        if ($screen) {
            printf($template, $lineInfo, $var);
        }

        $_SESSION['ATiMDebug']['template'] = $template;
        if ($menu) {
            $_SESSION['ATiMDebug'][self::$time][] = array(
                'message' => $var,
                'lineInfo' => $lineInfo,
            );
        }

        if ($saveInFile){
            $var = print_r($message, true);
            $print = self::$lineSeperator . self::$data[self::$key]["runCounter"]. "- " . $fileName . "(line $line) ". $time . "\n" .$var;
            $fileNameOutput = LOGS . $fileNameOutput;
            file_put_contents($fileNameOutput, $print, FILE_APPEND);
        }

        if ($end) {
            die();
        }

    }
}

/**
 * @param $arr
 * @return array
 */
function array_change_key_case_recursive($arr)
{
    return array_map(function($item){
        if(is_array($item))
            $item = array_change_key_case_recursive($item);
        return $item;
    },array_change_key_case($arr));
}

/**
 *
 * @param $message1
 * @param string $message2
 */
function debug_die($message1, $message2 = "")
{
    debug($message1);
    if (is_array($message2)) {
        $message2 = json_encode($message2);
    }
    die($message2);
}

// override_function('__', '$singular, $args', 'return newTranslate($singular, $args);');
/**
 *
 * @param $singular
 * @param null $args
 * @return mixed|string
 */
function newTranslate($singular, $args = null)
{
    if (is_numeric($singular)) {
        return '123456789';
    }
    return __($singular, $args);
}

/**
 *
 * @param array|int $arr
 * @return bool
 */
function isAssoc(array $arr)
{
    if (array() === $arr) {
        return false;
    }
    return array_keys($arr) !== range(0, count($arr) - 1);
}

/**
 *
 * @param $data
 * @param int $level
 * @param int $formattage
 * @return string
 */
function convertJSONtoArray($data, $level = 3, $formattage = 0)
{
    $s = "";
    if ($formattage == 0) {
        $newLine = "";
        $tab = "";
    } else {
        $newLine = "\n";
        $tab = "\t";
    }
    if (isAssoc($data)) {
        foreach ($data as $key1 => $value1) {
            if (is_array($value1)) {
                $s .= str_repeat($tab, $level) . "'" . $key1 . "' => [" . $newLine;
                $s .= convertJSONtoArray($value1, $level + 1);
                $s .= str_repeat($tab, $level) . "]," . $newLine;
            } else {
                if (($value1 == 'true') || ($value1 == 'false') || (is_numeric($value1))) {
                    $s .= str_repeat($tab, $level) . "'" . $key1 . "' => '" . $value1 . "'," . $newLine;
                } else {
                    $s .= str_repeat($tab, $level) . "'" . $key1 . "' => '" . $value1 . "'," . $newLine;
                }
            }
        }
        $s = rtrim($s, ",");
    } else {
        foreach ($data as $key1 => $value1) {
            if (is_array($value1)) {
                $s .= str_repeat($tab, $level) . "[" . $newLine;
                $s .= convertJSONtoArray($value1, $level + 1);
                $s .= str_repeat($tab, $level) . "]," . $newLine;
            } else {
                if (($value1 == 'true') || ($value1 == 'false') || (is_numeric($value1))) {
                    $s .= str_repeat($tab, $level) . "'" . $value1 . "'," . $newLine;
                } else {
                    $s .= str_repeat($tab, $level) . "'" . $value1 . "'," . $newLine;
                }
            }
        }
        $s = rtrim($s, ",");
    }
    return $s;
}

/**
 *
 * @param $data
 * @param int $level
 * @param int $formattage
 * @return string
 */
function json_encode_js($data, $level = 3, $formattage = 0)
{
    $s = "";
    if ($formattage == 0) {
        $newLine = "";
        $tab = "";
    } else {
        $newLine = "\n";
        $tab = "\t";
    }
    if (isAssoc($data)) {
        foreach ($data as $key1 => $value1) {
            if (is_array($value1)) {
                $s .= str_repeat($tab, $level) . "'" . $key1 . "': [" . $newLine;
                $s .= json_encode_js($value1, $level + 1);
                $s .= str_repeat($tab, $level) . "]," . $newLine;
            } else {
                if (($value1 == 'true') || ($value1 == 'false') || (is_numeric($value1))) {
                    $s .= str_repeat($tab, $level) . "'" . $key1 . "' : '" . $value1 . "'," . $newLine;
                } else {
                    $s .= str_repeat($tab, $level) . "'" . $key1 . "' : '" . $value1 . "'," . $newLine;
                }
            }
        }
        $s = rtrim($s, ",");
    } else {
        foreach ($data as $value1) {
            if (is_array($value1)) {
                $s .= str_repeat($tab, $level) . "[" . $newLine;
                $s .= json_encode_js($value1, $level + 1);
                $s .= str_repeat($tab, $level) . "]," . $newLine;
            } else {

                if (($value1 == 'true') || ($value1 == 'false') || (is_numeric($value1))) {
                    $s .= str_repeat($tab, $level) . "'" . $value1 . "'," . $newLine;
                } else {
                    $s .= str_repeat($tab, $level) . "'" . $value1 . "'," . $newLine;
                }
            }
        }
        $s = rtrim($s, ",");
    }
    return $s;
}

/**
 *
 * @param $str
 * @return mixed
 */
function stringCorrection($str)
{
    if (empty($str)) {
        return $str;
    } elseif (is_string($str)) {
        return str_replace("~~~~", "\\", str_replace("\\\\", "~~~~", $str));
    } elseif (is_array($str)) {
        foreach ($str as &$value) {
            $value = stringCorrection($value);
        }
        return $str;
    } else {
        return $str;
    }
}

/**
 *
 * @param mixed $var The variable that will be printed.
 * @param array $options The options of the function
 * @return void
 */
function d($var, $options = array())
{
    $options += array(
        "screen" => true,
        "log" => true,
        "die" => false
    );
    extract($options);

    if (! Configure::read('debug')) {
        return;
    }
    App::uses('Debugger', 'Utility');

    $trace = Debugger::trace(array(
        'start' => 1,
        'depth' => 2,
        'format' => 'array'
    ));
    $file = str_replace(array(
        CAKE_CORE_INCLUDE_PATH,
        ROOT
    ), '', $trace[0]['file']);
    $line = $trace[0]['line'];
    $html = <<<HTML
<div class="cake-debug-output">
<div class="minus-button"><a href="javascript:void(0)" class="debug-button">-</a></div>
%s
<pre class="cake-debug">
%s
</pre>
</div>
HTML;
    $text = <<<TEXT
%s
########## DEBUG ##########
%s
###########################

TEXT;
    $template = $html;
    if (PHP_SAPI === 'cli') {
        $template = $text;
        $lineInfo = sprintf('%s%s (line %s)', $file, $line, date('j-m-Y H:i:s'));
    }
    $var = print_r($var, true);
    $var = h($var);
    $lineInfo = sprintf('<span><strong>%s</strong> (line <strong>%s</strong>)</span><span style="color: green"> %s</span>', $file, $line, date('j-m-Y H:i:s'));
    if ($screen) {
        printf($template, $lineInfo, $var);
    }
    if ($log) {
        $l = empty($_SESSION['debug']['dl']) ? 0 : count($_SESSION['debug']['dl']);
        $_SESSION['debug']['dl'][$l][0] = $template;
        $_SESSION['debug']['dl'][$l][1] = $lineInfo;
        $_SESSION['debug']['dl'][$l][2] = $var;
    }
    if ($die) {
        die();
    }
}

/**
 *
 * @param int $number
 */
function dc($number = 0)
{
    if (! Configure::read('debug')) {
        return;
    }
    if ($number == 0) {
        unset($_SESSION['debug']);
    } else {
        array_splice($_SESSION['debug']['dl'], 0, $number);
    }
}

/**
 *
 * @param array $phpArray
 * @param $jsArray
 */
function convertArrayToJavaScript($phpArray, $jsArray)
{
    if (is_string($jsArray) && is_array($phpArray) && ! empty($phpArray) && ! is_array_empty($phpArray)) {
        $_SESSION['js_post_data'] = "\r\n" . 'var ' . $jsArray . "=" . json_encode($phpArray) . "\r\n";
    }
}

/**
 *
 * @param array $InputVariable
 * @return bool
 */
function is_array_empty($InputVariable)
{
    $result = true;
    if (is_array($InputVariable) && count($InputVariable) > 0) {
        foreach ($InputVariable as $Value) {
            $result = $result && is_array_empty($Value);
            if (! $result) {
                return false;
            }
        }
    } else {
        $result = empty($InputVariable) && $InputVariable != 0 && $InputVariable != '0';
    }

    return $result;
}

/**
 *
 * @param $data
 * @return array
 */
function removeEmptySubArray($data)
{
    if (is_array($data)) {
        $data = is_integer(key($data)) ? array_values(array_filter($data, 'removeEmptyStringArray')) : array_filter($data, 'removeEmptyStringArray');

        foreach ($data as &$v) {
            $v = removeEmptySubArray($v);
        }
        $data = is_integer(key($data)) ? array_values(array_filter($data, 'removeEmptyStringArray')) : array_filter($data, 'removeEmptyStringArray');
    }
    return $data;
}

/**
 *
 * @param $value
 * @return bool
 */
function removeEmptyStringArray($value)
{
    return ($value != "" && $value != array());
}

/**
 * @param bool $error
 * @return float
 */
function getTotalMemoryCapacity(&$error = false)
{
    $os = substr(PHP_OS, 0, 3);
    $defaultValue = 4294967296;
    if ($os == "WIN") {
        $totalMemory = array();
        exec('wmic memorychip get capacity', $totalMemory);
        if (isset($totalMemory[1])) {
            if (is_numeric($totalMemory[1])) {
                return round($totalMemory[1] / 1024 / 1024);
            } else {
                $totalMemory[1] = preg_replace("/[^0-9.]/", "", $totalMemory[1]);
                if (is_numeric($totalMemory[1])) {
                    return round($totalMemory[1] / 1024 / 1024);
                } else {
                    $error = true;
                    return round($defaultValue / 1024 / 1024);
                }
            }
        } elseif (isset($totalMemory[0])) {
            $totalMemory[0] = preg_replace("/[^0-9.]/", "", $totalMemory[0]);
            if (is_numeric($totalMemory[0])) {
                return round($totalMemory[0] / 1024 / 1024);
            } else {
                $error = true;
                return round($defaultValue / 1024 / 1024);
            }
        }
    } elseif ($os == "Lin") {
        $fh = fopen('/proc/meminfo', 'r');
        $mem = 0;
        while ($line = fgets($fh)) {
            $pieces = array();
            if (preg_match('/^MemTotal:\s+(\d+)\skB$/', $line, $pieces)) {
                $mem = $pieces[1];
                break;
            }
        }
        fclose($fh);
        return round($mem / 1024);
    }
}

/**
 * @param $data
 * @return float|int
 */
function convertFromKMG($data)
{
    if (strtoupper(substr($data, - 1)) == 'K') {
        $number = floatval(substr($data, 0, - 1)) * 1024;
    } elseif (strtoupper(substr($data, - 1)) == 'M') {
        $number = floatval(substr($data, 0, - 1)) * 1024 * 1024;
    } elseif (strtoupper(substr($data, - 1)) == 'G') {
        $number = floatval(substr($data, 0, - 1)) * 1024 * 1024 * 1024;
    } elseif (is_numeric($data)) {
        $number = floatval($data);
    }
    return $number;
}

/**
 * @param $word
 * @return null|string|string[]
 */
function ___($word)
{
    $string = __($word);
    $pattern = '/(.*)(\<span class\=[\"|\']{1}untranslated[\"|\']{1}\>)(.+)(\<\/span\>)(.*)/i';
    $replacement = '$3';
    $response = preg_replace($pattern, $replacement, $string);

    return $response;
}

function isEmpty($InputVariable)
{
    $result = true;

    if (is_array($InputVariable) && count($InputVariable) > 0){
        foreach ($InputVariable as $Value){
            $result = $result && isEmpty($Value);
        }
    }else{
        $result = empty($InputVariable);
    }

    return $result;
}