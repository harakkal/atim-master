-- ----------------------------------------------------------------------------------------------------------------------------------
-- ATiM Database Upgrade Script
-- Version: 2.7.2
--
-- For more information:
--    ./app/scripts/v2.7.0/ReadMe.txt
--    http://www.ctrnet.ca/mediawiki/index.php/Main_Page
-- ----------------------------------------------------------------------------------------------------------------------------------

SET @user_id = (SELECT id FROM users WHERE username = 'system');
SET @user_id = (SELECT IFNULL(@user_id, 1));

-- ----------------------------------------------------------------------------------------------------------------------------------
--	The warning for Memory allocation
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	(
'the memory allocated to your query is low or undefined.', 
'The memory allocated to your query is low or undefined from your system data. Please contact your system administrator to optimize your tool.',
'La mémoire allouée à vos requêtes est basse ou non définie à partir de vos données systèmes. Veuillez contacter votre administrateur du système pour optimiser votre outil.'
);

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Add "." and "," to i18n for avoiding add untranslation in select in debug mode
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	
    ('.', ".", "."),
    (',', ",", ",");

-- ----------------------------------------------------------------------------------------------------------------------------------
--	PHP memory_limit error message
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT IGNORE INTO
i18n (id,en,fr)
VALUES
    ('warning_PHP the minimum recommended memory should be considered (384M)', 
    "The minimum recommended memory should be considered (384M). Please contact the administrator to configure your application.", 
    "La mémoire minimum recommandée doit être considérée (384M). Veuillez contacter l'administrateur pour configurer votre application.");

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Check if the diagnosisMaster is related to the correct participant
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	(
'the diagnosis is not related to the participant', 
'The diagnosis is not related to the participant.',
'Le diagnostic n\'est pas lié au participant.'
);

-- ----------------------------------------------------------------------------------------------------------------------------------
--  In CCL, check if the annotation can be add
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	(
'the annotation #%s is not for clinical collection link', 
'The annotation #%s is not for Clinical Collection Link.',
'L\'annotation #%s ne concerne pas le lien de collecte clinique.'
);

-- ----------------------------------------------------------------------------------------------------------------------------------
--  In CCL, check if the annotation can be add 
-- issue #3704: Avoid accepting the treatments which are not event_controls.flag_use_for_ccl = 1
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	(
'the treatment #%s is not for clinical collection link', 
'The treatment #%s is not for Clinical Collection Link.',
'L\'traitement #%s ne concerne pas le lien de collecte clinique.'
);

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Add link for value domain help message
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	
    ('for customising the <b>%s</b> list click <b>%s</b>', 'For customising the "<b>%s</b>" list click <b>%s</b>', 'Pour personnaliser la "<b>%s</b>" liste, cliquez <b>%s</b>'),
    ('here', 'here', 'ici');

-- ----------------------------------------------------------------------------------------------------------------------------------
--	List validation error message
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	
    ('the value is not part of the list [%s]', 
    'The value is not part of the list [%s], if the value is part of a custom list, you can enable it temporary.', 
    "La valeur ne fait pas partie de la liste [%s], si la valeur fait partie d'une listes de valeurs personnalisée, vous pouvez l'activer temporairement.");

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #3609 : Change the pop-up layout of Manage contacts
--  Contacts information in order
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	
    ('contacts information', 'Contacts information.', 'Informations des contacts.');

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #3676 : Race to ethnicity
-- ----------------------------------------------------------------------------------------------------------------------------------

DELETE FROM i18n
WHERE id in ('race', 'help_race');

INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	
    ('help_race', "The participant's self declared ethnic origination.", "L'origine ethnique, telle que déclarée par le participant lui-même."),
    ('race', 'Ethnicity', 'Éthnique');

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #3644 : Missing password_format_error_msg_[1234] messages
-- ----------------------------------------------------------------------------------------------------------------------------------

DELETE FROM i18n
WHERE id in ('passwords minimal length', 'password_format_error_msg_1', 'password_format_error_msg_2', 'password_format_error_msg_3');

INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	
    ('passwords minimal length', 'Passwords must have a minimal length of 8 characters.', 'Les mots de passe doivent avoir une longueur minimale de 8 caractères.'),
    ('password_format_error_msg_1', 'Passwords must have a minimum length of 8 characters and contain lowercase letters.', 'Les mots de passe doivent avoir une longueur minimale de 8 caractères et être composés de lettres minuscules.'),
    ('password_format_error_msg_2', 'Passwords must have a minimum length of 8 characters and contain lowercase letters and numbers.', 'Les mots de passe doivent avoir une longueur minimale de 8 caractères et être composés de lettres majuscules, de lettres minuscules.'),
    ('password_format_error_msg_3', 'Passwords must have a minimum length of 8 characters and contain uppercase letters, lowercase letters and numbers.', 'Les mots de passe doivent avoir une longueur minimale de 8 caractères et être composés de lettres majuscules, de lettres minuscules et de chiffres.'),
    ('password_format_error_msg_4', 'Passwords must have a minimum length of 8 characters and contain uppercase letters, lowercase letters, numbers and special characters.', 'Les mots de passe doivent avoir une longueur minimale de 8 caractères et être composés de lettres majuscules, de lettres minuscules, de chiffres et de caractères spéciaux.');

-- ----------------------------------------------------------------------------------------------------------------------------------
--  Issue#3615 : Delete old records in user_logs table (or other table if exists)
--	Save old recoreds of user_logs into a file
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	
("the log directory does not exist: %s", "The log directory does not exist: %s.", "Le répertoire du journal n'existe pas: %s."),
("unable to create the backup file for users log", "Unable to create the backup file for users log.", "Impossible de créer le fichier de sauvegarde pour le journal des utilisateurs.");

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue#3585: Change the Active icon according to the status
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	
('activate', 'Activate', 'Activer'),
('deactivate', 'Deactivate', 'Désactiver');

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue#3689: newVersionSetup(): Configure ATiM when a misc identifier control is defined as 'linkable' to study
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO
i18n (id,en,fr)
VALUES 	
("structures miscidentifiers, miscidentifiers_for_participant_search and datamart_browsing_controls have been updated to let people to create Study Participant Identifier and search on this field.",
"The structures 'miscidentifiers', 'miscidentifiers_for_participant_search' and the 'flags' values of the table 'datamart_browsing_controls' have been updated based on the values of the field 'flag_link_to_study' of the table 'misc_identifier_controls' to let people  :<br>- to create 'Participant Study Identifier',<br>- to search on 'Study' field of a 'Participant Identifier' form,<br>- and to search on 'Participant Identifier' from 'Study' into the 'Data Browser'.",
"Les structures 'miscidentifiers', 'miscidentifiers_for_participant_search' et les valeurs des 'flags' de la table 'datamart_browsing_controls' ont été mises à jour selon les valeurs du champ 'flag_link_to_study'  de la table 'misc_identifier_controls' pour permettre aux utilisateurs :<br>- de créer un 'Identifiant d'étude du participant',<br>- d'effectuer une recherche sur le champ 'Étude' du formulaire 'Identifiant de participant',<br>- et de chercher des 'Identifiants de participant' à partir des 'Études' dans le 'Navigateur de données'.");
INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	
("structures miscidentifiers, miscidentifiers_for_participant_search and datamart_browsing_controls have been updated to not let people to create Study Participant Identifier and search on this field.",
"The structures 'miscidentifiers', 'miscidentifiers_for_participant_search' and the 'flags' values of the table 'datamart_browsing_controls' have been updated based on the values of the field 'flag_link_to_study' of the table 'misc_identifier_controls' to not let people  :<br>- to create 'Participant Study Identifier', <br>- to search on 'Study' field of a 'Participant Identifier' form,<br>- and to search on 'Participant Identifier' from 'Study' into the 'Data Browser'.",
"Les structures 'miscidentifiers', 'miscidentifiers_for_participant_search' et les valeurs des 'flags' de la table 'datamart_browsing_controls' ont été mises à jour selon les valeurs du champ 'flag_link_to_study'  de la table 'misc_identifier_controls' pour ne pas permettre aux utilisateurs :<br>- de créer un 'Identifiant d'étude du participant',<br>- d'effectuer une recherche sur le champ 'Étude' du formulaire 'Identifiant de participant',<br>- et de chercher des 'Identifiants de participant' à partir des 'Études' dans le 'Navigateur de données'.");

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Study Consent
--     - Add control to the  consent 'Study' field to make it mandatory.
--     - Add consent 'Study' field to consent masters.
--     - Add flag to consent controls to define fields that could be linked to study
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO structure_validations(structure_field_id, rule, language_message) VALUES
((SELECT id FROM structure_fields WHERE `field`='autocomplete_consent_study_summary_id'), 'notBlank', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES 
((SELECT id FROM structures WHERE alias='consent_masters'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '2', '', '0', '1', '', '1', '-', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
UPDATE structure_formats SET `flag_index`='0' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='consent_masters_study') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue#3690: newVersionSetup(): Configure ATiM when a consent control is defined as 'linkable' to study
-- ----------------------------------------------------------------------------------------------------------------------------------

ALTER TABLE consent_controls ADD COLUMN flag_link_to_study tinyint(1) DEFAULT '0';

INSERT IGNORE INTO
i18n (id,en,fr)
VALUES 	
("structure 'consent_masters' and 'datamart_browsing_controls' have been updated to let people to create 'Study Consent' and search on this field.",
"The structure 'consent_masters' and the 'flags' values of the table 'datamart_browsing_controls' have been updated based on the values of the field 'flag_link_to_study' of the table 'consent_controls' to let people  :<br>- to create 'Consent Study',<br>- to search on 'Study' field of a 'Consent' form,<br>- and to search pn 'Consent' from 'Study' into the 'Data Browser'.",
"La structure 'consent_masters' et les valeurs des 'flags' de la table 'datamart_browsing_controls' ont été mises à jour selon les valeurs du champ 'flag_link_to_study'  de la table 'consent_controls' pour permettre aux utilisateurs :<br>- de créer un 'Consentement d'étude',<br>- d'effectuer une recherche sur le champ 'Étude' du formulaire 'Consentement',<br>- et de chercher des 'Consentements' à partir des 'Études' dans le 'Navigateur de données'.");
INSERT IGNORE INTO
i18n (id,en,fr)
VALUES 	
("structure 'consent_masters' and 'datamart_browsing_controls' have been updated to not let people to create 'Study Consent' and search on this field.",
"The structure 'consent_masters' and the 'flags' values of the table 'datamart_browsing_controls' have been updated based on the values of the field 'flag_link_to_study' of the table 'consent_controls' to not let people  :<br>- to create 'Consent Study',<br>- to search on 'Study' field of a 'Consent' form,<br>- and to search on 'Consent' from 'Study' into the 'Data Browser'.",
"La structure 'consent_masters' et les valeurs des 'flags' de la table 'datamart_browsing_controls' ont été mises à jour selon les valeurs du champ 'flag_link_to_study'  de la table 'consent_controls' pour ne pas permettre aux utilisateurs :<br>- de créer un 'Consentement d'étude',<br>- d'effectuer une recherche sur le champ 'Étude' du formulaire 'Consentement',<br>- et de chercher des 'Consentements' à partir des 'Études' dans le 'Navigateur de données'.");

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #3693: LabBook features do not work
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE menus SET use_summary = 'LabBook.LabBookMaster::summary' WHERE use_summary = 'Labbook.LabBookMaster::summary';

UPDATE lab_book_controls SET detail_form_alias = 'lbd_dna_extractions' WHERE detail_form_alias = 'bd_dna_extractions';
UPDATE lab_book_controls SET detail_form_alias = 'lbd_slide_creations' WHERE detail_form_alias = 'bd_slide_creations';

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'SampleMaster', 'sample_masters', 'sample_control_id', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='sample_type_from_id') , '0', '', '', '', 'sample', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='lab_book_realiquotings_summary'), 
(SELECT id FROM structure_fields WHERE `model`='SampleMaster' AND `tablename`='sample_masters' AND `field`='sample_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='sample_type_from_id')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='sample' AND `language_tag`=''), 
'0', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='lab_book_realiquotings_summary') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='SampleControl' AND `tablename`='sample_controls' AND `field`='sample_type' AND `language_label`='sample type' AND `language_tag`='' AND `type`='select' AND `setting`='' AND `default`='' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='sample_type') AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'AliquotMaster', 'aliquot_masters', 'aliquot_control_id', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type_from_id') , '0', '', '', '', 'parent aliquot', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='lab_book_realiquotings_summary'), 
(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='aliquot_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type_from_id')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='parent aliquot' AND `language_tag`=''), 
'0', '7', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='lab_book_realiquotings_summary') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='aliquot_type' AND `language_label`='aliquot type' AND `language_tag`='' AND `type`='select' AND `setting`='' AND `default`='' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type') AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'AliquotMasterChildren', 'aliquot_masters', 'aliquot_control_id', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type_from_id') , '0', '', '', '', 'aliquot', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='lab_book_realiquotings_summary'), 
(SELECT id FROM structure_fields WHERE `model`='AliquotMasterChildren' AND `tablename`='aliquot_masters' AND `field`='aliquot_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type_from_id')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='aliquot' AND `language_tag`=''), 
'0', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='lab_book_realiquotings_summary') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='AliquotMasterChildren' AND `tablename`='aliquot_masters' AND `field`='aliquot_type' AND `language_label`='aliquot type' AND `language_tag`='' AND `type`='select' AND `setting`='' AND `default`='' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type') AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='lab_book_derivatives_summary'), 
(SELECT id FROM structure_fields WHERE `model`='SampleMaster' AND `tablename`='sample_masters' AND `field`='sample_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='sample_type_from_id')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='sample' AND `language_tag`=''), 
'0', '7', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='lab_book_derivatives_summary') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='SampleMaster' AND `tablename`='sample_controls' AND `field`='sample_type' AND `language_label`='sample type' AND `language_tag`='' AND `type`='select' AND `setting`='' AND `default`='' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='sample_type') AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'DerivativeDetail', 'derivative_details', 'sample_master_id', 'hidden',  NULL , '0', '', '', '', '', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='lab_book_derivatives_summary'), 
(SELECT id FROM structure_fields WHERE `model`='DerivativeDetail' AND `tablename`='derivative_details' AND `field`='sample_master_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'0', '0', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='lab_book_derivatives_summary') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='DerivativeDetail' AND `tablename`='derivative_details' AND `field`='id' AND `language_label`='' AND `language_tag`='' AND `type`='hidden' AND `setting`='' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');
DELETE FROM structure_validations WHERE structure_field_id IN (SELECT id FROM structure_fields WHERE (`public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='DerivativeDetail' AND `tablename`='derivative_details' AND `field`='id' AND `language_label`='' AND `language_tag`='' AND `type`='hidden' AND `setting`='' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1'));
DELETE FROM structure_fields WHERE (`public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='DerivativeDetail' AND `tablename`='derivative_details' AND `field`='id' AND `language_label`='' AND `language_tag`='' AND `type`='hidden' AND `setting`='' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue#3700: 	Name of permission_presets is unique
-- ----------------------------------------------------------------------------------------------------------------------------------

alter table `permissions_presets` drop index `name`;

UPDATE structure_formats SET `flag_add`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='permission_save_preset') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='overwrite' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

INSERT INTO `structure_validations` (`structure_field_id`, `rule`, `on_action`, `language_message`)
VALUES ((SELECT id FROM structure_fields WHERE `model`='PermissionsPreset' AND `tablename`='' AND `field`='name' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 'isUnique', '', NULL);

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue#3694: 	DataBrowser search problem, when search criteria loded by big CSV
-- ----------------------------------------------------------------------------------------------------------------------------------

ALTER TABLE `datamart_browsing_results` CHANGE `serialized_search_params` `serialized_search_params` longtext COLLATE 'latin1_swedish_ci' NULL AFTER `browsing_type`;

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue#3702: LabBook 'dna extraction' : no data for [SampleControl.sample_type]
-- ----------------------------------------------------------------------------------------------------------------------------------

DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='lab_book_derivatives_summary') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='SampleControl' AND `tablename`='sample_controls' AND `field`='sample_type' AND `language_label`='sample type' AND `language_tag`='' AND `type`='select' AND `setting`='' AND `default`='' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='sample_type') AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue#6307: Hide the unnecessary Plugins, actions and controllers in permission list
-- ----------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `acos`
ADD `flag_use_permission` tinyint(1) NOT NULL DEFAULT '1' AFTER `rght`;

-- Hiding 3 Plugins
update acos set flag_use_permission = 0 where alias = 'App' and parent_id = '1';
update acos set flag_use_permission = 0 where alias = 'DebugKit' and parent_id = '1';
update acos set flag_use_permission = 0 where alias = 'test_app' and parent_id = '1';

-- Hiding an action
UPDATE acos acos_1 
INNER JOIN acos acos_2 ON acos_1.parent_id = acos_2.id 
INNER JOIN acos acos_3 ON acos_2.parent_id = acos_3.id 
SET acos_1.flag_use_permission = 0
WHERE 
acos_1.alias = 'Csv' AND 
acos_2.alias = 'Browser' AND 
acos_3.alias = 'Datamart' AND 
acos_3.parent_id = '1' ;

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue#3638: Unable to use field 'Reception Date in Pathology' in tempalte init
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO i18n (id, en, fr)
VALUES
("check the detail tablename of the field [%s] is not missing or could be assigned to multi-tables",
"Check the tablename of the field [%s] is not missing or could be assigned to multi-tables!",
"Vérifier que le nom de la table du champ [%s] n'est pas manquant ou peut être attribué à plusieurs tables");

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue#3715 : List the storage with unclassified items"
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO `datamart_reports` (`name`, `description`, `form_alias_for_search`, `form_alias_for_results`, `form_type_for_results`, `function`, `flag_active`, `associated_datamart_structure_id`, `limit_access_from_datamart_structrue_function`)
VALUES 
('unclassified storage list', 'unclassified storage item description', 'view_storage_masters', 'view_storage_masters_unclassified_list', 2, 'getUnclassifiedStorageList', '1', (SELECT id FROM `datamart_structures` WHERE `plugin` = 'StorageLayout' AND `model` = 'NonTmaBlockStorage' ), '0');

INSERT IGNORE INTO i18n (id, en, fr)
VALUES
("unclassified storage list", "Unclassified storage list.", "Liste de stockage non classifiée."),
("unclassifided item report", "Unclassified item report.", "Rapport d'élément non classifié."),
("unclassified storage item description", "List all the storage contains the unclassified items.", "Lister tout le stockage contient les éléments non classés.");

INSERT INTO structures(`alias`) VALUES ('view_storage_masters_unclassified_list');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('core', 'FunctionManagement', '', 'unclassified', 'integer',  NULL , '0', '', '', '', 'unclassified', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='view_storage_masters_unclassified_list'), 
(SELECT id FROM structure_fields WHERE `model`='ViewStorageMaster' AND `tablename`='view_storage_masters' AND `field`='storage_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='storage_types_from_control_id')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='storage type' AND `language_tag`=''), 
'0', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='view_storage_masters_unclassified_list'), 
(SELECT id FROM structure_fields WHERE `model`='ViewStorageMaster' AND `tablename`='view_storage_masters' AND `field`='short_label' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=6' AND `default`='' AND `language_help`='stor_short_label_defintion' AND `language_label`='storage short label' AND `language_tag`=''), 
'0', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='view_storage_masters_unclassified_list'), 
(SELECT id FROM structure_fields WHERE `model`='ViewStorageMaster' AND `tablename`='view_storage_masters' AND `field`='selection_label' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20,url=/storagelayout/storage_masters/autoComplete/' AND `default`='' AND `language_help`='stor_selection_label_defintion' AND `language_label`='storage selection label' AND `language_tag`=''), 
'0', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='view_storage_masters_unclassified_list'), 
(SELECT id FROM structure_fields WHERE `model`='ViewStorageMaster' AND `tablename`='view_storage_masters' AND `field`='temperature' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='storage temperature' AND `language_tag`=''), 
'0', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='view_storage_masters_unclassified_list'), 
(SELECT id FROM structure_fields WHERE `model`='ViewStorageMaster' AND `tablename`='view_storage_masters' AND `field`='temp_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='temperature_unit_code')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'0', '21', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='view_storage_masters_unclassified_list'), 
(SELECT id FROM structure_fields WHERE `model`='ViewStorageMaster' AND `tablename`='view_storage_masters' AND `field`='empty_spaces' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='empty spaces' AND `language_tag`=''), 
'0', '24', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='view_storage_masters_unclassified_list'), 
(SELECT id FROM structure_fields WHERE `model`='ViewStorageMaster' AND `tablename`='view_storage_masters' AND `field`='code' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='storage_code_help' AND `language_label`='storage code' AND `language_tag`=''), 
'1', '100', 'system data', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='view_storage_masters_unclassified_list'), 
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='unclassified' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='unclassified' AND `language_tag`=''), 
'0', '25', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');



-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue# 3705: Derivative creation in batch - Unable to save : The value is not part of the list [parent sample].
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'Generated', '', 'parent_sample', 'input',  NULL , '0', '', '', '', 'parent sample', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='derivatives'), 
(SELECT id FROM structure_fields WHERE `model`='Generated' AND `tablename`='' AND `field`='parent_sample' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='parent sample' AND `language_tag`=''), 
'0', '349', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0');

DELETE FROM structure_formats 
WHERE structure_id=(SELECT id FROM structures WHERE alias='derivatives') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='SampleMaster' AND `tablename`='sample_masters' AND `field`='parent_id' AND `language_label`='parent sample' AND `language_tag`='' AND `type`='select' AND `setting`='' AND `default`='' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='sample_master_parent_id') AND `language_help`='inv_sample_parent_id_defintion' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');
DELETE FROM structure_validations WHERE structure_field_id IN (SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='SampleMaster' AND `tablename`='sample_masters' AND `field`='parent_id' AND `language_label`='parent sample' AND `language_tag`='' AND `type`='select' AND `setting`='' AND `default`='' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='sample_master_parent_id') AND `language_help`='inv_sample_parent_id_defintion' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');
DELETE FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='SampleMaster' AND `tablename`='sample_masters' AND `field`='parent_id' AND `language_label`='parent sample' AND `language_tag`='' AND `type`='select' AND `setting`='' AND `default`='' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='sample_master_parent_id') AND `language_help`='inv_sample_parent_id_defintion' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1';

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue# 3652: Change Aliquot In Stock Detail list from fix to custom
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE structure_value_domains SET source = 'StructurePermissibleValuesCustom::getCustomDropdown(\'Aliquot In Stock Details\')' WHERE domain_name = 'aliquot_in_stock_detail';
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('Aliquot In Stock Details', 1, 30, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Aliquot In Stock Details');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
(SELECT structure_permissible_values.value, i18n.en, i18n.fr, '1', '1', @control_id, NOW(), NOW(), @user_id, @user_id
FROM structure_value_domains
INNER JOIN structure_value_domains_permissible_values ON structure_value_domains.id = structure_value_domains_permissible_values.structure_value_domain_id
INNER JOIN structure_permissible_values ON structure_permissible_values.id = structure_value_domains_permissible_values.structure_permissible_value_id
LEFT JOIN i18n ON i18n.id = structure_permissible_values.language_alias
WHERE structure_value_domains.domain_name="aliquot_in_stock_detail"
ORDER BY structure_permissible_values.language_alias DESC);

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue# 3651: Sops list limited to active sops for data creation/update
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE structure_formats SET `flag_search`='1', `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='sopmasters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='SopMaster' AND `tablename`='sop_masters' AND `field`='expiry_date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue#3690: newVersionSetup(): Configure ATiM when a consent control is defined as 'linkable' to study
-- Issue#3689: newVersionSetup(): Configure ATiM when a misc identifier control is defined as 'linkable' to study
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	
("structures 'miscidentifiers', 'miscidentifiers_for_participant_search' and 'datamart_browsing_controls' have been updated to not let people to create 'Study Participant Identifier' and search on this field.",
"The structures 'miscidentifiers', 'miscidentifiers_for_participant_search' and the data of the table 'datamart_browsing_controls' have been updated to not let people to create 'Participant Study Identifier' and search on data.",
"Les structures 'miscidentifiers', 'miscidentifiers_for_participant_search' et les données de la table 'datamart_browsing_controls' ont été mises à jour pour ne permettre pas aux utilisateurs de créer un 'Identifiant d'étude du participant' et d'effectuer une recherche sur cette donnée.");

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue#3708: newVersionSetup(): Configure ATiM when an model is defined as 'linkable' to study in databrowser
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	
("set up structures to display study fields in following structures [%s] based on datamart configuration.",
"The 'Study' fields of the following structures [%s] have been configured to be displayed in the structure based on 'Databrowser' structures links properties (active links)!",
"Les champs 'd'Étude' des structures suivantes [%s] ont été configurés pour être affichés selon les configurations des liens entre structures du 'Navigateur de données' (liens actifs)!"),

("set up structures to hide study fields in following structures [%s] based on datamart configuration.",
"The 'Study' fields of the following structures [%s] have been configured to be hidden in the structure based on 'Databrowser' structures links properties (inactive links)!",
"Les champs 'd'Étude' des structures suivantes [%s] ont été configurés pour être cachés selon les configurations des liens entre structures du 'Navigateur de données' (liens inactifs)!");   
      
-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue#3707: 	newVersionSetup(): Configure ATiM based on Order core (core.php) variables
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO i18n (id,en,fr)
VALUES 	
("'datamart_browsing_controls' have been updated to not let people to search on order line.",
"The table 'datamart_browsing_controls' has been updated to not let people to search on 'Order Line' from 'Study', 'Order' and 'Order Item' into the 'Data Browser' based on the value of the core variable 'order_item_to_order_objetcs_link_setting'.",
"La table 'datamart_browsing_controls' a été mise à jour pour ne pas permettre aux utilisateurs d'effectuer une recherche sur les 'Lignes de commande' à partir d'une 'Étude', 'Commande' ou d'un 'Article de commande' dans le 'Navigateur de données' en fonction de la valeur de la variable du core 'order_item_to_order_objetcs_link_setting'."),

("'datamart_browsing_controls' have been updated to let people to search on order line.",
"The table 'datamart_browsing_controls' has been updated to let people to search on 'Order Line' from 'Study', 'Order' and 'Order Item' into the 'Data Browser' based on the value of the core variable 'order_item_to_order_objetcs_link_setting'.",
"La table 'datamart_browsing_controls' a été mise à jour pour permettre aux utilisateurs d'effectuer une recherche sur les 'Lignes de commande' à partir d'une 'Étude', 'Commande' ou d'un 'Article de commande' dans le 'Navigateur de données' en fonction de la valeur de la variable du core 'order_item_to_order_objetcs_link_setting'."),

("'datamart_browsing_controls' have been updated to let people to search on order item from both tma slide and aliquot.",
"The table 'datamart_browsing_controls' has been updated to let people to search on 'Order Item' from both 'TMA Slide' and Aliquot into the 'Data Browser' based on the value of the core variable 'order_item_type_config'.",
"La table 'datamart_browsing_controls' a été mise à jour pour permettre aux utilisateurs d'effectuer une recherche sur les 'Articles de commande' à partir des 'Lames de TMA' ou des 'Aliquots' dans le 'Navigateur de données' en fonction de la valeur de la variable du core 'order_item_type_config'."),

("'datamart_browsing_controls' have been updated to let people to search on order item from aliquot only.",
"The table 'datamart_browsing_controls' has been updated to let people to search on 'Order Item' from 'Aliquot' only into the 'Data Browser' ('TMA Slide' un-linked) based on the value of the core variable 'order_item_type_config'.",
"La table 'datamart_browsing_controls' a été mise à jour pour permettre aux utilisateurs d'effectuer une recherche sur les 'Articles de commande' à partir des 'Aliquots' seulement dans le 'Navigateur de données' ('Lames de TMA' non liée) en fonction de la valeur de la variable du core 'order_item_type_config'."),

("'datamart_browsing_controls' have been updated to let people to search on order item from tma slide only.",
"The table 'datamart_browsing_controls' has been updated to let people to search on 'Order Item' from 'TMA Slide' only into the 'Data Browser' ('Aliquot' un-linked) based on the value of the core variable 'order_item_type_config'.",
"La table 'datamart_browsing_controls' a été mise à jour pour permettre aux utilisateurs d'effectuer une recherche sur les 'Articles de commande' à partir des 'Lames de TMA' seulement dans le 'Navigateur de données' ('Aliquot' non lié) en fonction de la valeur de la variable du core 'order_item_type_config'.");

INSERT IGNORE INTO i18n (id,en,fr)
VALUES 	
("'datamart_browsing_controls' have been updated to not let people to search on order.",
"The table 'datamart_browsing_controls' has been updated to not let people to search on 'Order'.",
"La table 'datamart_browsing_controls' a été mise à jour pour ne pas permettre aux utilisateurs d'effectuer une recherche sur les 'Commandes'.");

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue#3703: Make 'batch action' forms smaller
-- Create batch_process_view_sample_joined_to_collection from view_sample_joined_to_collection
-- ----------------------------------------------------------------------------------------------------------------------------------

-- batch_process_aliq_storage_and_in_stock_details

UPDATE structure_formats SET `flag_override_label`='1', `language_label`='', `flag_override_tag`='1', `language_tag`='aliquot in stock detail - new value' WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_aliq_storage_and_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='in_stock' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_in_stock_values') AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_label`='1', `language_label`='', `flag_override_tag`='1', `language_tag`='aliquot in stock detail - new value' WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_aliq_storage_and_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='in_stock' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_in_stock_values') AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_label`='1', `language_label`='', `flag_override_tag`='1', `language_tag`='detail' WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_aliq_storage_and_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='in_stock_detail' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_in_stock_detail') AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_tag`='1', `language_tag`='or erase detail' WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_aliq_storage_and_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_in_stock_detail' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_tag`='1', `language_tag`='remove storage data' WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_aliq_storage_and_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_from_storage' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_label`='0', `language_label`='', `flag_override_tag`='1', `language_tag`='remove storage data' WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_aliq_storage_and_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_from_storage' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats 
SET `flag_override_label`='1', `language_label`='', `flag_override_tag`='1', `language_tag`='new storage selection label' , `language_heading`='storage' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_aliq_storage_and_in_stock_details') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='recorded_storage_selection_label')
AND flag_add = '1';

-- Create batch_process_view_sample_joined_to_collection from view_sample_joined_to_collection

INSERT INTO structures(`alias`) VALUES ('batch_process_view_sample_joined_to_collection');
SET @structure_id = (SELECT id FROM structures WHERE alias = 'batch_process_view_sample_joined_to_collection');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
(SELECT @structure_id, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`
FROM structure_formats 
WHERE structure_id = (SELECT id FROM structures WHERE alias = 'view_sample_joined_to_collection')
AND flag_edit_readonly = '1' AND flag_edit_readonly = '1');
UPDATE structure_formats 
SET `flag_search`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' 
WHERE structure_id=@structure_id;

UPDATE structure_formats SET `display_order`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='acquisition_label' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='1', `display_order`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='2', `display_order`='0', `flag_override_label`='1', `language_label`='sample', `flag_override_tag`='1', `language_tag`='type' WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='sample_type' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='sample_type') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='2', `display_order`='1', `language_heading`='', `flag_override_label`='1', `language_label`='', `flag_override_tag`='1', `language_tag`='sample code' WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='sample_code' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES 	
("aliquot in stock detail - new value", "Stock Detail - New 'In Stock' Value", "Détail du stock - Nouvelle donnée 'En stock'"),
("or erase detail", "Or Erase Detail", "Ou effacer le détail")	,
("remove storage data", "Remove (Erase storage data)", "Enlver (supprimer données d'entreposage");

UPDATE structure_formats SET `display_order`='1', `flag_override_label`='1', `language_label`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='sample_type' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='sample_type') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='0', `flag_override_label`='1', `language_label`='sample' WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='sample_code' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='1', `flag_override_label`='1', `language_label`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='sample_code' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='0', `flag_override_label`='1', `language_label`='sample', `flag_override_tag`='0', `language_tag`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='sample_type' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='sample_type') AND `flag_confidential`='0');

-- used_aliq_in_stock_details

UPDATE structure_formats SET `display_order`='11', `flag_override_label`='1', `language_label`='', `flag_override_tag`='1', `language_tag`='barcode' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='barcode' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='12', `flag_override_label`='1', `language_label`='', `flag_override_tag`='1', `language_tag`='aliquot label' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='aliquot_label' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='13', `flag_override_label`='1', `language_label`='', `flag_override_tag`='1', `language_tag`='type' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='aliquot_type' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='20' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StorageMaster' AND `tablename`='storage_masters' AND `field`='selection_label' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='21' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='storage_coord_x' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='22' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='storage_coord_y' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='1', `display_order`='11', `flag_override_label`='1', `language_label`='aliquot in stock detail - new value' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='in_stock' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_in_stock_values') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='1', `display_order`='12', `flag_override_label`='1', `language_label`='', `flag_override_tag`='1', `language_tag`='detail' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='in_stock_detail' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_in_stock_detail') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='1', `display_order`='13', `flag_override_label`='1', `language_label`='remove storage data' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') 
AND structure_field_id IN (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_from_storage' AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_label`='1', `language_label`='aliquot' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='barcode' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_label`='1', `language_label`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='barcode' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='10', `flag_override_label`='1', `language_label`='aliquot', `flag_override_tag`='0', `language_tag`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='aliquot_type' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type') AND `flag_confidential`='0');

-- used_aliq_in_stock_detail_volume

UPDATE structure_formats SET `display_column`='2', `display_order`='11' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_detail_volume') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='current_volume' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='2', `display_order`='12' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_detail_volume') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit') AND `flag_confidential`='0');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue#3680 : change way we manage the quality controls used volume to simplify the customization
-- ----------------------------------------------------------------------------------------------------------------------------------

-- qualityctrls_volume_for_detail

DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='qualityctrls_volume_for_detail');
DELETE FROM structures WHERE alias='qualityctrls_volume_for_detail';

-- qualityctrls_volume

UPDATE structure_formats 
SET `flag_summary`='0' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='qualityctrls_volume') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='QualityCtrl' AND `tablename`='quality_ctrls' AND `field`='used_volume' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats 
SET `flag_detail`='1' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='qualityctrls_volume') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit') AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_edit`='1' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='qualityctrls_volume') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='QualityCtrl' AND `tablename`='quality_ctrls' AND `field`='used_volume' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_edit`='1', `flag_edit_readonly`='1' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='qualityctrls_volume') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit') AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_edit`='0', `flag_edit_readonly`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='qualityctrls_volume') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit') AND `flag_confidential`='0');

-- qualityctrls

UPDATE structure_formats SET `flag_edit`='0', `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='qualityctrls') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='QualityCtrl' AND `tablename`='quality_ctrls' AND `field`='used_volume' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_edit`='0', `flag_edit_readonly`='0', `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='qualityctrls') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='33', `language_heading`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='qualityctrls') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='QualityCtrl' AND `tablename`='quality_ctrls' AND `field`='used_volume' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='34' WHERE structure_id=(SELECT id FROM structures WHERE alias='qualityctrls') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit') AND `flag_confidential`='0');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='qualityctrls') AND structure_field_id IN (SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='aliquot_type');
DELETE FROM structure_validations WHERE structure_field_id IN (SELECT id FROM structure_fields WHERE (`public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='aliquot_type' AND `language_label`='aliquot type' AND `language_tag`='' AND `type`='input' AND `setting`='' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1'));
DELETE FROM structure_fields WHERE (`public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='aliquot_type' AND `language_label`='aliquot type' AND `language_tag`='' AND `type`='input' AND `setting`='' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue#3721 : Field with type different than 'select' linked structure_value_domain : Remove list
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE structure_fields 
SET  `structure_value_domain`= NULL  
WHERE `type`='checkbox' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='yes_no_checkbox');
UPDATE structure_fields 
SET  `structure_value_domain`= NULL  
WHERE `type`='input' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='colon_surgery_2016_margins_1');
UPDATE structure_fields 
SET  `structure_value_domain`= NULL  
WHERE `type`='integer_positive' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='datetime_accuracy_indicator');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue#3722 : Unable to save Password Reset Questions
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE structure_fields 
SET  `structure_value_domain`= NULL  
WHERE `type`='input' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='forgotten_password_reset_questions');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Replace wrong i18n value for 'storage (non tma block)'
-- ----------------------------------------------------------------------------------------------------------------------------------

REPLACE INTO i18n (id,en,fr) 
VALUES
('storage (non tma block)', 'Storage (Non TMA Block)', 'Entreposage (Bloc de TMA exclu)'),
('storage (non tma block) - value generated by newVersionSetup function', 'Storage (Non TMA Block)', 'Entreposage (Bloc de TMA exclu)');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Batch Actions - Validate that the submitted data are complete
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
("submitted data is not complete - max_input_vars issue",
"The submitted data is not complete. The max_input_vars variable is probably too small compared to the submitted data. Part of the data to save are missing. Please submit less data or contact your administrator.",
"Les données soumises ne sont pas complètes. La variable max_input_vars est probablement trop petite par rapport aux données soumises. Une partie des données à sauvegarder sont manquantes. Veuillez soumettre moins de données ou contactez votre administrateur.");

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Add missing i18n values
-- ----------------------------------------------------------------------------------------------------------------------------------

REPLACE INTO i18n (id,en,fr)
VALUES
('manage recipients' , 'Manage Recipients', "Gérer les destinataires");

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #3750 : Hide miscidentiers when no misc-identifier_controls exists
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO i18n (id,en,fr)
VALUES 	
("datamart_browsing_controls has been updated to not let people to search on misc identifier.",
"The table 'datamart_browsing_controls' has been updated to not let people to search on 'Participant Identifier' from 'Participant' and 'Study' into the 'Data Browser' based on the value of the 'misc_identifier_controls' data.",
"La table 'datamart_browsing_controls' a été mise à jour pour ne pas permettre aux utilisateurs d'effectuer une recherche sur les 'Identiant de participant' à partir d'une 'Étude', ou d'un 'patient' dans le 'Navigateur de données' en fonction des données de la table 'misc_identifier_controls'."),

("datamart_browsing_controls has been updated to let people to search on misc identifier.",
"The table 'datamart_browsing_controls' has been updated to let people to search on 'Participant Identifier' from 'Participant' and 'Study' into the 'Data Browser' based on the value of the 'misc_identifier_controls' data.",
"La table 'datamart_browsing_controls' a été mise à jour pour permettre aux utilisateurs d'effectuer une recherche sur les 'Identiant de participant' à partir d'une 'Étude', ou d'un 'patient' dans le 'Navigateur de données' en fonction des données de la table 'misc_identifier_controls'.");

-- ----------------------------------------------------------------------------------------------------------------------------------
-- " Fix the bug #3629: Rename Aliquot in French
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE i18n SET fr = REPLACE(fr, 'aliquot', 'aliquote');
UPDATE i18n SET fr = REPLACE(fr, 'aliquotee', 'aliquote');
UPDATE i18n SET fr = REPLACE(fr, 'Aliquot', 'Aliquote');
UPDATE i18n SET fr = REPLACE(fr, 'Aliquotee', 'Aliquote');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #3741 : Display nbr of participants / aliquots / available aliquots on home page
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO i18n (id,en,fr)
VALUES 	
('menu bank activity message %%s1%%, %%s2%% and %%s3%%', 
"Biobanking Activity Summary:
<br>- %%s1%% participants.
<br>- %%s2%% aliquots in bank.
<br>- %%s3%% referenced aliquots.",
"Résumé de l'activité de biobanque:
<br>- %%s1%% participants.
<br>- %%s2%% aliquotes en banque.
<br>- %%s3%% aliquotes référencées."),
('click here to see bank activit report for details', 'Click here for more details.', ' Cliquez ici pour plus de détails.');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- ATiM dashboard
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO `datamart_reports` VALUES 
(null,'report_atim_dashboard','report_atim_dashboard_summary','','report_atim_dashboard','detail','ctrnetAtimDashboard',1,NULL,0);

INSERT INTO structures(`alias`) VALUES ('report_atim_dashboard');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Datamart', '0', '', 'atim_dashboard_created_participants_total', 'input',  NULL , '0', '', '', '', 'created participants', ''), 
('Datamart', '0', '', 'atim_dashboard_created_participants_week', 'input',  NULL , '0', '', '', '', 'last 7 days', ''), 
('Datamart', '0', '', 'atim_dashboard_created_participants_month', 'input',  NULL , '0', '', '', '', 'last 4 weeks', ''), 
('Datamart', '0', '', 'atim_dashboard_created_participants_year', 'input',  NULL , '0', '', '', '', '12 months', ''), 
('Datamart', '0', '', 'atim_dashboard_updated_participants_total', 'input',  NULL , '0', '', '', '', 'updated participants', ''), 
('Datamart', '0', '', 'atim_dashboard_updated_participants_week', 'input',  NULL , '0', '', '', '', 'last 7 days', ''), 
('Datamart', '0', '', 'atim_dashboard_updated_participants_month', 'input',  NULL , '0', '', '', '', 'last 4 weeks', ''), 
('Datamart', '0', '', 'atim_dashboard_updated_participants_year', 'input',  NULL , '0', '', '', '', '12 months', ''), 
('Datamart', '0', '', 'atim_dashboard_consented_participants_total', 'input',  NULL , '0', '', '', '', 'consented participants', ''), 
('Datamart', '0', '', 'atim_dashboard_consented_participants_week', 'input',  NULL , '0', '', '', '', 'last 7 days', ''), 
('Datamart', '0', '', 'atim_dashboard_consented_participants_month', 'input',  NULL , '0', '', '', '', 'last 4 weeks', ''), 
('Datamart', '0', '', 'atim_dashboard_consented_participants_year', 'input',  NULL , '0', '', '', '', '12 months', ''), 
('Datamart', '0', '', 'atim_dashboard_participants_with_collection_total', 'input',  NULL , '0', '', '', '', 'participants with collected samples', ''), 
('Datamart', '0', '', 'atim_dashboard_participants_with_collection_week', 'input',  NULL , '0', '', '', '', 'last 7 days', ''), 
('Datamart', '0', '', 'atim_dashboard_participants_with_collection_month', 'input',  NULL , '0', '', '', '', 'last 4 weeks', ''), 
('Datamart', '0', '', 'atim_dashboard_participants_with_collection_year', 'input',  NULL , '0', '', '', '', '12 months', ''), 
('Datamart', '0', '', 'atim_dashboard_shipped_aliquots_total', 'input',  NULL , '0', '', '', '', 'shipped aliquots', ''), 
('Datamart', '0', '', 'atim_dashboard_shipped_aliquots_week', 'input',  NULL , '0', '', '', '', 'last 7 days', ''), 
('Datamart', '0', '', 'atim_dashboard_shipped_aliquots_month', 'input',  NULL , '0', '', '', '', 'last 4 weeks', ''), 
('Datamart', '0', '', 'atim_dashboard_shipped_aliquots_year', 'input',  NULL , '0', '', '', '', '12 months', ''), 
('Datamart', '0', '', 'atim_dashboard_internally_used_aliquots_total', 'input',  NULL , '0', '', '', '', 'aliquots internally used', ''), 
('Datamart', '0', '', 'atim_dashboard_internally_used_aliquots_week', 'input',  NULL , '0', '', '', '', 'last 7 days', ''), 
('Datamart', '0', '', 'atim_dashboard_internally_used_aliquots_month', 'input',  NULL , '0', '', '', '', 'last 4 weeks', ''), 
('Datamart', '0', '', 'atim_dashboard_internally_used_aliquots_year', 'input',  NULL , '0', '', '', '', '12 months', ''), 
('Datamart', '0', '', 'atim_dashboard_last_created_participant', 'date',  NULL , '0', '', '', '', 'last created participant', ''), 
('Datamart', '0', '', 'atim_dashboard_last_consented_participant', 'date',  NULL , '0', '', '', '', 'last consented participant', ''), 
('Datamart', '0', '', 'atim_dashboard_last_collection', 'date',  NULL , '0', '', '', '', 'last collection', '');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Datamart', '0', '', 'atim_dashboard_aliquots_in_stock', 'input',  NULL , '0', '', '', '', 'created aliquots', 'in stock'), 
('Datamart', '0', '', 'atim_dashboard_aliquots_total', 'input',  NULL , '0', '', '', '', '', '/ total'), 
('Datamart', '0', '', 'atim_dashboard_created_aliquots_week', 'input',  NULL , '0', '', '', '', 'last 7 days', ''), 
('Datamart', '0', '', 'atim_dashboard_created_aliquots_month', 'input',  NULL , '0', '', '', '', 'last 4 weeks', ''), 
('Datamart', '0', '', 'atim_dashboard_created_aliquots_year', 'input',  NULL , '0', '', '', '', '12 months', '');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Datamart', '0', '', 'atim_dashboard_created_studies', 'input',  NULL , '0', '', '', '', 'created studies', ''), 
('Datamart', '0', '', 'atim_dashboard_active_studies_week', 'input',  NULL , '0', '', '', 'ctrnet_help_atim_dashboard_active_studies', 'processed studies - last 7 days', ''), 
('Datamart', '0', '', 'atim_dashboard_active_studies_month', 'input',  NULL , '0', '', '', 'ctrnet_help_atim_dashboard_active_studies', 'processed studies - last 4 weeks', ''), 
('Datamart', '0', '', 'atim_dashboard_active_studies_year', 'input',  NULL , '0', '', '', 'ctrnet_help_atim_dashboard_active_studies', 'processed studies - 12 months', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_created_participants_total' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='created participants' AND `language_tag`=''), 
'0', '1', 'recruitments', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_created_participants_week' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last 7 days' AND `language_tag`=''), 
'0', '2', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_created_participants_month' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last 4 weeks' AND `language_tag`=''), 
'0', '3', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_created_participants_year' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='12 months' AND `language_tag`=''), 
'0', '4', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_updated_participants_total' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='updated participants' AND `language_tag`=''), 
'0', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_updated_participants_week' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last 7 days' AND `language_tag`=''), 
'0', '11', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_updated_participants_month' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last 4 weeks' AND `language_tag`=''), 
'0', '12', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_updated_participants_year' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='12 months' AND `language_tag`=''), 
'0', '13', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_consented_participants_total' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='consented participants' AND `language_tag`=''), 
'0', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_consented_participants_week' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last 7 days' AND `language_tag`=''), 
'0', '21', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_consented_participants_month' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last 4 weeks' AND `language_tag`=''), 
'0', '22', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_consented_participants_year' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='12 months' AND `language_tag`=''), 
'0', '23', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 

((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_participants_with_collection_total' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='participants with collected samples' AND `language_tag`=''), 
'1', '130', 'samples collections', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_participants_with_collection_week' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last 7 days' AND `language_tag`=''), 
'1', '131', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_participants_with_collection_month' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last 4 weeks' AND `language_tag`=''), 
'1', '132', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_participants_with_collection_year' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='12 months' AND `language_tag`=''), 
'1', '133', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_aliquots_in_stock' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='created aliquots' AND `language_tag`='in stock'), 
'1', '140', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_aliquots_total' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='/ total'), 
'1', '141', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_created_aliquots_week' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last 7 days' AND `language_tag`=''), 
'1', '142', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_created_aliquots_month' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last 4 weeks' AND `language_tag`=''), 
'1', '143', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_created_aliquots_year' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='12 months' AND `language_tag`=''), 
'1', '144', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'),

((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_shipped_aliquots_total' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='shipped aliquots' AND `language_tag`=''), 
'2', '340', 'aliquots used/shipped', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_shipped_aliquots_week' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last 7 days' AND `language_tag`=''), 
'2', '341', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_shipped_aliquots_month' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last 4 weeks' AND `language_tag`=''), 
'2', '342', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_shipped_aliquots_year' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='12 months' AND `language_tag`=''), 
'2', '343', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_internally_used_aliquots_total' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='aliquots internally used' AND `language_tag`=''), 
'2', '350', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_internally_used_aliquots_week' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last 7 days' AND `language_tag`=''), 
'2', '351', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_internally_used_aliquots_month' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last 4 weeks' AND `language_tag`=''), 
'2', '352', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_internally_used_aliquots_year' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='12 months' AND `language_tag`=''), 
'2', '353', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_created_studies' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='created studies' AND `language_tag`=''), 
'2', '400', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_active_studies_week' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='ctrnet_help_atim_dashboard_active_studies' AND `language_label`='processed studies - last 7 days' AND `language_tag`=''), 
'2', '401', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_active_studies_month' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='ctrnet_help_atim_dashboard_active_studies' AND `language_label`='processed studies - last 4 weeks' AND `language_tag`=''), 
'2', '403', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_active_studies_year' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='ctrnet_help_atim_dashboard_active_studies' AND `language_label`='processed studies - 12 months' AND `language_tag`=''), 
'2', '404', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'),

((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_last_created_participant' AND `type`='date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last created participant' AND `language_tag`=''), 
'3', '570', 'last activities', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_last_consented_participant' AND `type`='date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last consented participant' AND `language_tag`=''), 
'3', '580', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_atim_dashboard'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='atim_dashboard_last_collection' AND `type`='date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='last collection' AND `language_tag`=''), 
'3', '590', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) VALUES
('report_atim_dashboard', 'ATiM Dashboard (Activities Report)', 'Tableau de bord ATiM (rapport d''activités)'),
('report_atim_dashboard_summary', 'Dashboard proposed by CTRNet team fo follow-up your biobank activities.', 'Tableau de bord proposé par l''équipe de CTRNet pour suivre l''activité de votre biobanque'),
('atim dashboard - statistics', 'ATiM Dashboard - Statistics', 'Tableau de bord ATiM - Statistiques'),
('last 7 days', 'Last 7 Days', '7 derniers jours'),
('last 4 weeks', 'Last 4 Weeks', '4 dernières semaines'),
('12 months', '12 Months', '12 mois'),
('updated participants', 'Updated Participants', 'Participants mis à jour'),
('participants who consented', 'Participants Who Consented', 'Participants ayant consenti'),
('participants with collected samples', 'Participants With Collected Samples', 'Participants avec échantillons prélevés'),
('shipped aliquots', 'Shipped Aliquots', 'Aliquotes expédiées'),
('aliquots internally used', 'Aliquots Internally Used', 'Aliquotes utilisées en interne'),
('last created participant', 'Last Created Participant', 'Dernier participant créé'),
('last consented participant', 'Last Consented Participant', 'Dernier participant ayant consenti'),
('last collection', 'Last Collection', 'Dernière collection'),
('aliquots in stock counter', 'Aliquots In Stock', 'Aliquotes en stock'),
('all aliquots (in stock and not) counter', 'All Aliquots (In Stock and Sold Out)', 'Aliquotes (en stock et épuisées)'),
('recruitments of participants', 'Recruitments of Participants', 'Recrutements de participants'),
('recruitments', 'Recruitments', 'Recrutements'),
('number of participants', 'Nbr of Participants', 'Nbr de participants'),
('consented participants', 'Participants Who Consented', 'Participants ayant consenti'),
('atim dashboard - graphics', 'ATiM Dashboard', 'Tableau de bord ATiM'),
('atim dashboard decription', 'Dashboard proposed by CTRNet team fo follow-up your biobank activities.', 'Tableau de bord proposé par l''équipe de CTRNet pour suivre l''activité de votre biobanque'),
('last 12 months', 'last 12 months', '12 derniers mois'),
('all periods', 'All Periods', 'Toutes les periodes'),
('participants who withdrawn', 'Participants Who Withdrawn', 'Participants qui se sont retirés'),
('participants with collected specimens', 'Participants With Collected Samples', 'Participants avec pélevement d''échantillons'),
('aliquots used/shipped', "Aliquots Used/Shipped", "Aliquotes utilisées/expédiées"),
('samples collections', "Samples Collections", "Collection d'échantillons"),
('last activities', "Last Activities", "Dernières activités"),
('ctrnet_help_atim_dashboard_active_studies', "Studies linked to at least one aliquot shipped or one aliquot used (internally).", "Études liées à au moins une aliquote expédiée ou utilisée (en interne)"),
('created aliquots', "Created Aliquots", "Aliquotes créées"),
('/ total', "/ Total", "/ Total"),
('created studies', "Created Studies", "Études crées"),
('with shipped aliquots', 'Shipped Aliquots', "Aliquotes expédiées"),
('with aliquots locally used', 'Aliquots Internally Used', "Aliquotes utilisées en interne"),
('processed studies', 'Processed Studies', 'Études traitées'),
('processed studies - last 7 days', 'Processed Studies For Last 7 Days', 'Études traitées / 7 derniers jours'),
('processed studies - last 4 weeks', 'Processed Studies For Last 4 Weeks', 'Études traitées / 4 dernières semaines'),
('processed studies - 12 months', 'Processed Studies For last 12 months', 'Études traitées / 12 derniers mois');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- " Fix the bug #3629: Rename Aliquot in French
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE i18n SET fr = "Ajouter aliquotes sources" WHERE fr = "Ajouter aliquotes source";
UPDATE i18n SET fr = "Ajouter aliquotes testées" WHERE fr = "Ajouter aliquotes testés";
UPDATE i18n SET fr = "Aliquote testée" WHERE fr = "Aliquote testé";
UPDATE i18n SET fr = "Aliquote utilisée" WHERE fr = "Aliquote utilisé";
UPDATE i18n SET fr = "Aliquote utilisée" WHERE fr = "Aliquote utilisé";
UPDATE i18n SET fr = "Aliquote utilisée (pour mise à jour)" WHERE fr = "Aliquote utilisé (pour mise à jour)";
UPDATE i18n SET fr = "Aliquote(s) 'enfant'" WHERE fr = "Aliquote(s) 'enfant'";
UPDATE i18n SET fr = "Aliquotes créees" WHERE fr = "Aliquots crées";
UPDATE i18n SET fr = "Aliquotes en stock" WHERE fr = "Aliquots en stock";
UPDATE i18n SET fr = "Aliquotes expédiées" WHERE fr = "Aliquots expédiés";
UPDATE i18n SET fr = "Aliquotes sélectionnées" WHERE fr = "Aliquotes sélectionnés";
UPDATE i18n SET fr = "Aliquotes testées" WHERE fr = "Aliquotes testés";
UPDATE i18n SET fr = "Aliquotes utilisées en interne" WHERE fr = "Aliquots utilisées en interne";
UPDATE i18n SET fr = "Aliquotes utilisées en interne" WHERE fr = "Aliquots utilisés en interne";
UPDATE i18n SET fr = "Aliquots (en stock et épuisées)" WHERE fr = "Aliquots (en stock et épuisés)";
UPDATE i18n SET fr = "Aliquots expédiées" WHERE fr = "Aliquots expédiés";
UPDATE i18n SET fr = "Aliquots utilisée/expédiées" WHERE fr = "Aliquots utilisé/expédiés";
UPDATE i18n SET fr = "Au moins une aliquote doit être créée!" WHERE fr = "Au moins un aliquote doit être créé!";
UPDATE i18n SET fr = "Au moins une aliquote enfant doit être créée!" WHERE fr = "Au moins un aliquote enfant doit être créé!";
UPDATE i18n SET fr = "Aucune aliquote affichée" WHERE fr = "Aucun aliquote affiché";
UPDATE i18n SET fr = "Aucun cahier de laboratoire ne peut être attaché à ce processus de réaliquotage!" WHERE fr = "Aucun cahier de laboratoire ne peut être attaché à ce processus de réaliquoteage!";
UPDATE i18n SET fr = "Aucun volume doit être enregistré pour ce type d'aliquote!" WHERE fr = "Aucun volume doit être enregistré pour ce type d'aliquote!";
UPDATE i18n SET fr = "Aucune aliquote de l'échantillon n'a été définie comme testée." WHERE fr = "Aucun aliquote de l'échantillon n'a été défini comme testé";
UPDATE i18n SET fr = "Aucune aliquote n'a été définie comme aliquote enfant!" WHERE fr = "Aucun aliquote n'a été défini comme aliquote enfant!";
UPDATE i18n SET fr = "Aucune aliquote n'a été définie comme aliquote source!" WHERE fr = "Aucun aliquote n'a été défini comme aliquote source!";
UPDATE i18n SET fr = "Aucune aliquote n'est contenue dans cet entreposage" WHERE fr = "Aucun aliquote n'est contenu dans cet entreposage";
UPDATE i18n SET fr = "Aucune nouvelle aliquote de l'échantillon ne peut actuellement être définie comme aliquote ré-aliquoteé (enfant)!" WHERE fr = "Aucun nouvel aliquote de l'échantillon ne peut actuellement être défini comme aliquote ré-aliquoteé (enfant)!";
UPDATE i18n SET fr = "Aucune nouvelle aliquote de l'échantillon ne peut actuellement être définie comme aliquote source!" WHERE fr = "Aucun nouvel aliquote de l'échantillon ne peut actuellement être défini comme aliquote source!";
UPDATE i18n SET fr = "Aucune nouvelle aliquote ne peut actuellement être définie comme aliquote enfant pour les aliquotes parents suivantes." WHERE fr = "Aucun nouvel aliquote ne peut actuellement être défini comme aliquote enfant pour les aliquotes parents suivants";
UPDATE i18n SET fr = "Aucune nouvelle aliquote ne peut actuellement être définie comme aliquote 'testée'!" WHERE fr = "Aucun nouvel aliquote ne peut actuellement être défini comme aliquote 'testé'!";
UPDATE i18n SET fr = "Barcode de l'aliquote testée" WHERE fr = "Barcode de l'aliquote testé";
UPDATE i18n SET fr = "Cette aliquote est enregistrée dans un autre entreposage/position" WHERE fr = "Cet aliquote est enregistré dans un autre entreposage/position";
UPDATE i18n SET fr = "Cette aliquote n'a aucun volume enregistré" WHERE fr = "Cet aliquote n'a aucun volume enregistré";
UPDATE i18n SET fr = "Conflit de données : Au moins une aliquote mise à jour est définie comme 'non stockée'. Veuillez mettre a jour la valeur 'En Stock'." WHERE fr = "Conflit de données : Au moins un aliquote mis à jour est défini comme 'non stocké'. Veuillez mettre a jour la valeur 'En Stock'.";
UPDATE i18n SET fr = "Date (réaliquotage)" WHERE fr = "Date (réaliquoteage)";
UPDATE i18n SET fr = "Date à laquelle l'aliquote (tube, bloc, etc) a été entreposée (dans un congélateur, un réfrigérateur, un conteneur d'azote, etc) pour la première fois, quoi qu'il arrive à cette aliquote dans le futur (déplacemement de l'aliquote, utilisation, etc.)" WHERE fr = "Date à laquelle l'aliquote (tube, bloc, etc) a été entreposé (dans un congélateur, un réfrigérateur, un conteneur d'azote, etc) pour la première fois, quoi qu'il arrive à cet aliquote dans le futur (déplacemement de l'aliquote, utilisation, etc.)";
UPDATE i18n SET fr = "Date à laquelle l'aliquote d'un échantillon a été realiquotée afin de créer une ou plusieures aliquotes de ce même échantillon à partir de l'aliquote source (ex: création d'un tube d'ADN à partir d'un autre tube d'ADN entreposé pour une expédition, la création d'un bloc de tissu à partir d'un tube de tissu, etc)." WHERE fr = "Date à laquelle l'aliquote d'un échantillon a été realiquoteé afin de créer un ou plusieurs aliquotes de ce même échantillon à partir de l'aliquote source (ex: création d'un tube d'ADN à partir d'un autre tube d'ADN entreposé pour une expédition, la création d'un bloc de tissu à partir d'un tube de tissu, etc).";
UPDATE i18n SET fr = "Date à laquelle un échantillon et /ou une aliquote a été testé(e) pour évaluer la qualité, la concentration, etc." WHERE fr = "Date à laquelle un échantillon et /ou un aliquote a été testé pour évaluer la qualité, la concentration, etc.";
UPDATE i18n SET fr = "Date à laquelle une aliquote a été utilisée (contrôle de la qualité, 'realiquotage', usage interne (prêt à un autre laboratoire, etc), commande, etc)." WHERE fr = "Date à laquelle un aliquote a été utilisé (contrôle de la qualité, 'realiquoteage', usage interne (prêt à un autre laboratoire, etc), commande, etc).";
UPDATE i18n SET fr = "Définir comme ré-aliquotée" WHERE fr = "Définir comme ré-aliquoteé";
UPDATE i18n SET fr = "e" WHERE fr = "Vous êtes sur le point de créer un(e) utilisation/événement pour %d aliquotes";
UPDATE i18n SET fr = "Effacer les aliquotes téléchargées ou scannées" WHERE fr = "Effacer les aliquotes téléchargés ou scannés";
UPDATE i18n SET fr = "Études liées à au moins une aliquote expédiée ou utilisée (en interne)." WHERE fr = "Études liées à au moins un aliquot expédié ou utilisé (en interne";
UPDATE i18n SET fr = "La sélection du parent du réaliquote est requise" WHERE fr = "La sélection du parent du réaliquote est requise";
UPDATE i18n SET fr = "La table 'datamart_browsing_controls' a été mise à jour pour permettre aux utilisateurs d'effectuer une recherche sur les 'Articles de commande' à partir des 'Lames de TMA' seulement dans le 'Navigateur de données' ('Aliquote' non liée) en fonction de la valeur de la variable du core 'order_item_type_config'." WHERE fr = "La table 'datamart_browsing_controls' a été mise à jour pour permettre aux utilisateurs d'effectuer une recherche sur les 'Articles de commande' à partir des 'Lames de TMA' seulement dans le 'Navigateur de données' ('Aliquote' non lié) en fonction de la valeur de la variable du core 'order_item_type_config'.";
UPDATE i18n SET fr = "Le statu d'une aliquote définie comme 'Retournée' ne peut pas être changé à 'En attente' ou 'Envoyée' lorsque celle-ci est déjà liée à une autre commande avec ces 2 status." WHERE fr = "Le statu d'un aliquote défini comme 'Retourné' ne peut pas être changé à 'En attente' ou 'Envoyé' lorsque celui-ci est déjà lié à une autre commande avec ces 2 status.";
UPDATE i18n SET fr = "L'information sur les types d'échantillons et d'aliquotes est disponible <a href='%s' target='blank'>ici</a>." WHERE fr = "L'information sur les types d'échantillons et d'aliquotes est disponible <a href='%s' target='blank'>ici</a>.";
UPDATE i18n SET fr = "Nombre d'aliquotes: Analysées =% d, validées =% d, avec avertissement =% d, avec erreur =% d" WHERE fr = "Nombre d'aliquotes: Analysés =% d, validé =% d, avec avertissement =% d, avec erreur =% d";
UPDATE i18n SET fr = "Parent ré-aliquotée" WHERE fr = "Parent ré-aliquoteé";
UPDATE i18n SET fr = "Permet de définir si l'aliquote étudiée a été réaliquotée en une autre aliquote (parent) ou est une aliquote créée à partir d'une autre aliquote (enfant)." WHERE fr = "Permet de définir si l'aliquote étudié a été réaliquoteé en un autre aliquote (parent) ou est un aliquote créé à partir d'un autre aliquote (enfant).";
UPDATE i18n SET fr = "Permet l'enregistrement de données liées à des entités de l'inventaire créés durant le même processus (realiquotage en lot, dérivés créés en lot, etc) et qui s'appliquent à l'ensemble des entités. Cette outil peut être utilisé pour synchroniser les données de ces entités créés en lot." WHERE fr = "Permet l'enregistrement de données liées à des entités de l'inventaire créés durant le même processus (realiquoteiage en lot, dérivés créés en lot, etc) et qui s'appliquent à l'ensemble des entités. Cette outil peut être utilisé pour synchroniser les données de ces entités créés en lot.";
UPDATE i18n SET fr = "Processus de ré-aliquotage" WHERE fr = "Processus de ré-aliquoteage";
UPDATE i18n SET fr = "Re-aliquotage" WHERE fr = "Re-aliquoteage";
UPDATE i18n SET fr = "Réaliquotage" WHERE fr = "Réaliquoteage";
UPDATE i18n SET fr = "Ré-aliquotée en" WHERE fr = "Ré-aliquoteé en";
UPDATE i18n SET fr = "Ré-aliquotée par" WHERE fr = "Ré-aliquoteé par";
UPDATE i18n SET fr = "Réaliquoter" WHERE fr = "Réaliquoteter";
UPDATE i18n SET fr = "Résumé de l'activité de biobanque: <br>- %%s1%% participants. <br>- %%s2%% aliquotes en banque. <br>- %%s3%% aliquotes référencées." WHERE fr = "Résumé de l'activité de biobanque: <br>- %%s1%% participants. <br>- %%s2%% aliquotes en banque. <br>- %%s3%% aliquotes référencés.";
UPDATE i18n SET fr = "Retour d'aliquote envoyée" WHERE fr = "Retour d'aliquote envoyé";
UPDATE i18n SET fr = "Sélection aliquote testée" WHERE fr = "Sélection aliquote testé";
UPDATE i18n SET fr = "Sélection des aliquotes enfant" WHERE fr = "Sélection des aliquotes enfant";
UPDATE i18n SET fr = "Statut d'une aliquote : <br> - 'Oui & Disponible' => Aliquote présente physiquement dans la banque et disponible sans restriction. <br> - 'Oui & Non disponible' => Aliquote présente physiquement dans la banque mais une restriction existe (réservée pour une étude, etc). <br> 'Non' => L'aliquote n'existe plus dans la banque." WHERE fr = "Statut d'un aliquote : <br> - 'Oui & Disponible' => Aliquote présent physiquement dans la banque et disponible sans restriction. <br> - 'Oui & Non disponible' => Aliquote présent physiquement dans la banque mais une restriction existe (réservé pour une étude";
UPDATE i18n SET fr = "Temps écoulé entre la date de réception du spécimen dans la banque/laboratoire (l'échantillon est physiquement présent dans la banque ou une personne du laboratoire vient de prendre possession de l'échantillon) et la date à laquelle l'aliquote (tube, bloc, etc) a été entreposée (dans un congélateur, un réfrigérateur, un conteneur d'azote, etc) pour la première fois." WHERE fr = "Temps écoulé entre la date de réception du spécimen dans la banque/laboratoire (l'échantillon est physiquement présent dans la banque ou une personne du laboratoire vient de prendre possession de l'échantillon) et la date à laquelle l'aliquote (tube, bloc, etc) a été entreposé (dans un congélateur, un réfrigérateur, un conteneur d'azote, etc) pour la première fois.";
UPDATE i18n SET fr = "Temps écoulé entre la date du prélèvement du spécimen (ex: date de la chirurgie, date de la biopsie, date de la collection d'urine, etc) et la date à laquelle l'aliquote (tube, bloc, etc) a été entreposée (dans un congélateur, un réfrigérateur, un conteneur d'azote, etc) pour la première fois." WHERE fr = "Temps écoulé entre la date du prélèvement du spécimen (ex: date de la chirurgie, date de la biopsie, date de la collection d'urine, etc) la date à laquelle l'aliquote (tube, bloc, etc) a été entreposé (dans un congélateur, un réfrigérateur, un conteneur d'azote, etc) pour la première fois.";
UPDATE i18n SET fr = "Temps écoulé entre la date du processus qui a permis d'obtenir l'échantillon dérivé à partir d'un échantillon parent (extraction d'ADN, centrifugation de sang, extraction de protéines, amplification d'ARN, etc) et la date à laquelle l'aliquote (tube, bloc, etc) a été entreposée (dans un congélateur, un réfrigérateur, un conteneur d'azote, etc) pour la première fois." WHERE fr = "Temps écoulé entre la date du processus qui a permis d'obtenir l'échantillon dérivé à partir d'un échantillon parent (extraction d'ADN, centrifugation de sang, extraction de protéines, amplification d'ARN, etc) et la date à laquelle l'aliquote (tube, bloc, etc) a été entreposé (dans un congélateur, un réfrigérateur, un conteneur d'azote, etc) pour la première fois.";
UPDATE i18n SET fr = "Toutes les fonctions dont le nom contient add, batch, edit, define, delete, realiquote, remove, ou save ne pourront pas être utilisées." WHERE fr = "Toutes les fonctions dont le nom contient add, batch, edit, define, delete, realiquote, remove, ou save seront refusées d'accès.";
UPDATE i18n SET fr = "Un aliquote ne peut être mise que dans une seule commande!" WHERE fr = "Un aliquote ne peut être mis que dans une seule commande!";
UPDATE i18n SET fr = "Un aliquote ne peut pas être ajoutée à deux reprises à des commandes aussi longtemps que celle-ci n'a pas d'abord été définie comme 'retournée'." WHERE fr = "Un aliquote ne peut pas être ajouté à deux reprises à des commandes aussi longtemps que celui-ci n'a pas d'abord été défini comme 'retourné'.";
UPDATE i18n SET fr = "Un aliquote non en stock ne peut être attachée à un entreposage!" WHERE fr = "Un aliquote non en stock ne peut être attaché à un entreposage!";
UPDATE i18n SET fr = "Volume de l'aliquote parent utilisée pour créer l'aliquote enfant." WHERE fr = "Volume de l'aliquote parent utilisé pour créer l'aliquote enfant.";
UPDATE i18n SET fr = "Volume de l'aliquote source utilisée pour créer l'échantillon dérivé." WHERE fr = "Volume de l'aliquote source utilisé pour créer l'échantillon dérivé.";
UPDATE i18n SET fr = "Volume de l'aliquote utilisée pour le contrôle de qualité." WHERE fr = "Volume de l'aliquote utilisé pour le contrôle de qualité.";
UPDATE i18n SET fr = "Vos données ne peuvent être supprimées! Des aliquotes ont été définies pour votre contrôle de qualité." WHERE fr = "Vos données ne peuvent être supprimées! Des aliquotes ont été définis pour votre contrôle de qualité.";
UPDATE i18n SET fr = "Vos données ne peuvent être supprimées! Des aliquotes 'réaliquotées' sont attachées à votre cahier de laboratoire." WHERE fr = "Vos données ne peuvent être supprimées! Des aliquote 'réaliquoteés' sont attachés à votre cahier de laboratoire.";
UPDATE i18n SET fr = "Vos données ne peuvent être supprimées! Des aliquotes sont placées à cette position dans votre entreposage." WHERE fr = "Vos données ne peuvent être supprimées! Des aliquotes sont placés à cette position dans votre entreposage.";
UPDATE i18n SET fr = "Vos données ne peuvent être supprimées! Des données de réaliquotage existent pour votre aliquote." WHERE fr = "Vos données ne peuvent être supprimées! Des données de réaliquoteage existent pour votre aliquote.";
UPDATE i18n SET fr = "Vos données ne peuvent être supprimées! Un aliquote de l'échantillon parent est définie comme source." WHERE fr = "Vos données ne peuvent être supprimées! Un aliquote de l'échantillon parent est défini comme source.";
UPDATE i18n SET fr = "Vous êtes sur le point de créer un(e) événement/utilisation pour %d aliquotes contenues dans %s" WHERE fr = "Vous êtes sur le point de créer un(e) événement/utilisation pour %d aliquotes contenus dans %s";
UPDATE i18n SET fr = "Vous ne pouvez pas réaliquoter ces éléments ensembles car ce ne sont pas un même type d'échantillon et d'aliquote!" WHERE fr = "Vous ne pouvez pas réalqiuoter ces éléments ensembles car ce ne sont pas et le même type d'échantillon et le même type d'aliquote!";

UPDATE i18n SET fr = "Ajout des aliquotes à une commande : Aliquotes étudiées" WHERE fr = "Ajout des aliquotes à une commande : Aliquotes étudiés";
UPDATE i18n SET fr = "Aliquotes utilisées/expédiées" WHERE fr = "Aliquotes utilisé/expédiés";
UPDATE i18n SET fr = "Aliquotes (en stock et épuisées)" WHERE fr = "Aliquotes (en stock et épuisés)";
UPDATE i18n SET fr = "Nombre d'aliquotes disponibles" WHERE fr = "Nombre d'aliquote disponibles";
UPDATE i18n SET fr = "Aliquotes créées" WHERE fr = "Aliquotes crées";
UPDATE i18n SET fr = "Aliquotes expédiées" WHERE fr = "Aliquotes expédiés";
UPDATE i18n SET fr = "Détails dérivé & aliquotes" WHERE fr = "Détails dérivé & Aliquotes";
UPDATE i18n SET fr = "Plus d'une aliquote a le même code à barres" WHERE fr = "Plus d'un aliquote a le même code à barres";
UPDATE i18n SET fr = "Vous ne pouvez pas réaliquoter ces éléments ensembles car les types d'échantillon et d'aliquote sont différents!" WHERE fr = "Vous ne pouvez pas réaliquoter ces éléments ensembles car ce ne sont pas un même type d'échantillon et d'aliquote!";
UPDATE i18n SET fr = "Résumé de l'activité de biobanque:<br>- %%s1%% participants.<br>- %%s2%% aliquotes en banque.<br>- %%s3%% aliquotes référencées." WHERE fr LIKE "Résumé de l'activité de biobanque%participants%aliquot%s en banque%aliquot%s référencé%s.";

UPDATE i18n SET fr = "Les unités de volume des aliquotes sont différentes. Aucun volume ne pourra être défini." WHERE fr = "Les unités de volume des aliquotes sont différents. Aucun volume ne pourra être défini.";

UPDATE i18n SET fr = REPLACE(fr, 'un aliquote', 'une aliquote');
UPDATE i18n SET fr = REPLACE(fr, 'Un aliquote', 'Une aliquote');

UPDATE i18n SET fr = "Sélection des aliquotes enfants" WHERE fr = "Sélection des aliquotes enfant";

SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Aliquot In Stock Details');
UPDATE structure_permissible_values_customs SET `fr` = "Contaminée" WHERE fr = "Contaminé" AND control_id = @control_id;
UPDATE structure_permissible_values_customs SET `fr` = "Envoyée et retournée" WHERE fr = "Enovyé &amp; Retourné" AND control_id = @control_id;
UPDATE structure_permissible_values_customs SET `fr` = "Envoyée" WHERE fr = "Envoyé" AND control_id = @control_id;
UPDATE structure_permissible_values_customs SET `fr` = "Perdue" WHERE fr = "Perdu" AND control_id = @control_id;
UPDATE structure_permissible_values_customs SET `fr` = "Prêtée" WHERE fr = "Prêté" AND control_id = @control_id;
UPDATE structure_permissible_values_customs SET `fr` = "Réservée pour une commande" WHERE fr = "Réservé pour une commande" AND control_id = @control_id;
UPDATE structure_permissible_values_customs SET `fr` = "Réservé pour une étude" WHERE fr = "Réservé pour une Étude" AND control_id = @control_id;
UPDATE structure_permissible_values_customs SET `fr` = "Utilisée" WHERE fr = "Utilisé" AND control_id = @control_id;

UPDATE structure_permissible_values_customs SET `fr` = "Envoyée et retournée" WHERE fr LIKE "Enovyé % Retourné" AND control_id = @control_id;
UPDATE structure_permissible_values_customs SET `fr` = "Réservée pour une étude/projet" WHERE fr = "Réservé pour une Étude/Projet" AND control_id = @control_id;

-- ----------------------------------------------------------------------------------------------------------------------------------
--  Fix the bug #3755: Ignoring the value list if it's inactive and in edit mode
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO i18n (id, en, fr)
values
('the value [%s] is not used any more as input in the [%s] list',
'The value [%s] is not used any more as input in the [%s] list.',
"La valeur [%s] n'est plus utilisée comme entrée dans la liste [%s].");

-- -------------------------------------------------------------------------------------
--	Created sample type Cheek Swab
-- -------------------------------------------------------------------------------------

INSERT INTO `sample_controls` (`id`, `sample_type`, `sample_category`, `detail_form_alias`, `detail_tablename`, `display_order`, `databrowser_label`) VALUES
(null, 'cheek swab', 'specimen', 'specimens', 'sd_spe_cheek_swabs', 0, 'cheek swab');

CREATE TABLE IF NOT EXISTS `sd_spe_cheek_swabs` (
  `sample_master_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `sd_spe_cheek_swabs_revs` (
  `sample_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
ALTER TABLE `sd_spe_cheek_swabs`
  ADD CONSTRAINT `FK_sd_spe_cheek_swabs_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`);

INSERT IGNORE INTO i18n (id,en,fr) VALUES ('cheek swab','Cheek Swab','Frottis de joue');
INSERT INTO `parent_to_derivative_sample_controls` (`id`, `parent_sample_control_id`, `derivative_sample_control_id`, `flag_active`, `lab_book_control_id`) 
VALUES
(null, NULL, (SELECT id FROM sample_controls WHERE sample_type = 'cheek swab'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cheek swab'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL);
INSERT INTO `aliquot_controls` (`id`, `sample_control_id`, `aliquot_type`, `aliquot_type_precision`, `detail_form_alias`, `detail_tablename`, `volume_unit`, `flag_active`, `comment`, `display_order`, `databrowser_label`) VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cheek swab'), 'tube', '', 'ad_spec_tubes', 'ad_tubes', '', 0, '', 0, 'cheek swab|tube');
INSERT INTO `realiquoting_controls` (`id`, `parent_aliquot_control_id`, `child_aliquot_control_id`, `flag_active`, `lab_book_control_id`) VALUES
(null, (SELECT aliquot_controls.id FROM aliquot_controls INNER JOIN sample_controls ON sample_controls.id = sample_control_id AND sample_type = 'cheek swab'), 
(SELECT aliquot_controls.id FROM aliquot_controls INNER JOIN sample_controls ON sample_controls.id = sample_control_id AND sample_type = 'cheek swab'), 0, NULL);

-- ----------------------------------------------------------------------------------------------------------------------------------
-- "The message related to user_logs table
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO i18n (id, en, fr)
VALUES
    ('please wait while cleaning up the tables', 'Please wait while cleaning up the tables.', 'Veuillez patienter pendant le nettoyage des tables.'),
    ('the user logs table overflow warning message',
    "<b>Attention</b>: The user log table contains so much data, please contact the administrator to clean up unnecessary data.",
    "<b>Attention</b>: Attention: La table de journalisation de l'utilisateur contient beaucoup de données. Veuillez contacter l'administrateur pour nettoyer les données inutiles.");

-- ----------------------------------------------------------------------------------------------------------------------------------
-- 'Issue#3761 (Can not change the users setting)
-- ----------------------------------------------------------------------------------------------------------------------------------
UPDATE `configs` SET `modified` = '1111-11-11' WHERE  CAST(modified AS CHAR(20)) = '0000-00-00 00:00:00';
UPDATE `configs` SET `created` = '1111-11-11' WHERE  CAST(created AS CHAR(20)) = '0000-00-00 00:00:00';

ALTER TABLE `configs`
  CHANGE `created` `created` datetime NULL,
  CHANGE `modified` `modified` datetime NULL,
  CHANGE `define_time_format` `define_time_format` int NOT NULL DEFAULT 24;

UPDATE `configs` SET `define_time_format` = 24 WHERE  `define_time_format` = 2;
UPDATE `configs` SET `define_time_format` = 12 WHERE  `define_time_format` = 1;

UPDATE `configs` SET `modified` = null WHERE  `modified` = '1111-11-11';
UPDATE `configs` SET `created` = null WHERE  `created` = '1111-11-11';

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Add missing parent to derivative samples controls 
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO `parent_to_derivative_sample_controls` (`id`, `parent_sample_control_id`, `derivative_sample_control_id`, `flag_active`, `lab_book_control_id`) 
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'peritoneal wash'), (SELECT id FROM sample_controls WHERE sample_type = 'protein'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'peritoneal wash'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'peritoneal wash'), (SELECT id FROM sample_controls WHERE sample_type = 'cell culture'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'peritoneal wash'), (SELECT id FROM sample_controls WHERE sample_type = 'cell lysate'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'peritoneal wash'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL);

INSERT INTO `parent_to_derivative_sample_controls` (`id`, `parent_sample_control_id`, `derivative_sample_control_id`, `flag_active`, `lab_book_control_id`) 
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'csf'), (SELECT id FROM sample_controls WHERE sample_type = 'protein'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'csf'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'csf'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL),   
(null, (SELECT id FROM sample_controls WHERE sample_type = 'csf cells'), (SELECT id FROM sample_controls WHERE sample_type = 'protein'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'csf cells'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'csf cells'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL); 

INSERT INTO `parent_to_derivative_sample_controls` (`id`, `parent_sample_control_id`, `derivative_sample_control_id`, `flag_active`, `lab_book_control_id`) 
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pleural fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'protein'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pleural fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pleural fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'cell culture'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pleural fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'cell lysate'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pleural fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL);   
   
INSERT INTO `parent_to_derivative_sample_controls` (`id`, `parent_sample_control_id`, `derivative_sample_control_id`, `flag_active`, `lab_book_control_id`) 
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'ascite'), (SELECT id FROM sample_controls WHERE sample_type = 'protein'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'ascite'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'ascite'), (SELECT id FROM sample_controls WHERE sample_type = 'cell culture'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'ascite'), (SELECT id FROM sample_controls WHERE sample_type = 'cell lysate'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'ascite'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL);      
   
INSERT INTO `parent_to_derivative_sample_controls` (`id`, `parent_sample_control_id`, `derivative_sample_control_id`, `flag_active`, `lab_book_control_id`) 
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cystic fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'protein'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cystic fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cystic fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'cell culture'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cystic fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'cell lysate'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cystic fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL);      
      
INSERT INTO `parent_to_derivative_sample_controls` (`id`, `parent_sample_control_id`, `derivative_sample_control_id`, `flag_active`, `lab_book_control_id`) 
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bone marrow'), (SELECT id FROM sample_controls WHERE sample_type = 'protein'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bone marrow'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bone marrow'), (SELECT id FROM sample_controls WHERE sample_type = 'cell culture'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bone marrow'), (SELECT id FROM sample_controls WHERE sample_type = 'cell lysate'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bone marrow'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL);      
  
INSERT INTO `parent_to_derivative_sample_controls` (`id`, `parent_sample_control_id`, `derivative_sample_control_id`, `flag_active`, `lab_book_control_id`) 
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pericardial fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'protein'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pericardial fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pericardial fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'cell culture'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pericardial fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'cell lysate'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pericardial fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL);      

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Activate/Inactivate menus based on databrowser
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO i18n (id, en, fr)
VALUES   
("following menus have been inactivated : %s.", "Following menus have been inactivated : %s.", "Les menus suivants ont été désactivés: %s."),
("following menus have been activated : %s.", "Following menus have been activated : %s.", "Les menus suivants ont été activés: %s.");
UPDATE menus SET use_link = '/ClinicalAnnotation/EventMasters/listall/clinical/%%Participant.id%%' WHERE use_link = '/ClinicalAnnotation/EventMasters/listall/Clinical/%%Participant.id%%';

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Add  hook in debug kit
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT IGNORE INTO i18n (id, en, fr)
VALUES
('hook file name', 'Hook file name', 'Nom du fichier hook'),
('exists', 'Exists', 'Existe');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- mCode Coding Feature
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO i18n (id, en, fr)
VALUES
('mcode %s code picker', "mCode '%s' Code Picker", " Sélection mCode '%s' code"),
('select a mcode %s code', "Select a mCode '%s' Code", "Sélectioner mCode '%s' code"),
('search for a mcode %s code', "Search For a mCode '%s' Code", "Recherche d'un mCode '%s' code");
      
-- ----------------------------------------------------------------------------------------------------------------------------------
-- Added barcode generatio system
-- ----------------------------------------------------------------------------------------------------------------------------------
      
INSERT IGNORE INTO i18n (id, en, fr)
VALUES
("system has been configured to let atim to generate all aliquot barcodes", 
"The system has been configured to let ATiM to generate all aliquot barcodes.", 
"Le système a été configuré pour permettre à ATiM de générer tous les codes-barres des aliquotes."),
("system has been configured to let ATim to generate the un-entered aliquot barcodes", 
"The system has been configured to let ATiM to generate the un-entered barcodes.", 
"Le système a été configuré pour permettre à ATiM de générer tous les codes-barres des aliquotes non saisis.");
        
-- ----------------------------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;
INSERT INTO `versions` (version_number, date_installed, trunk_build_number, branch_build_number) 
VALUES
('2.7.2', NOW(),'to complete when install ATiM','to complete when install ATiM');
