<div align="center">
  <a href="https://atim-software.ca/en/home/" target="_blank">
      <img alt="ATiM | The Advanced Tissue Management Application" src="https://atim-software.ca/wp-content/uploads/2019/05/Logo_ATIM_CMYK.png" width="400" />

  </a>
</div>



# I - ATiM v2.7 Read Me

**Releases:**

* [v2.7.0](https://gitlab.com/ctrnet/atim-master/-/releases/v2.7.0) on 2018-00-00
* [v2.7.1](https://gitlab.com/ctrnet/atim-master/-/releases/v2.7.1) on 2018-09-04
* [v2.7.2](https://gitlab.com/ctrnet/atim-master/-/releases/v2.7.2) on 2019-12-04
* [v2.7.3](https://gitlab.com/ctrnet/atim-master/-/releases/v2.7.3) on 2020-12-00

**For more information:**

* ATiM Software Website [atim-software.ca](http://atim-software.ca) 

* ATiM Wiki [wiki.atim-software.ca](http://wiki.atim-software.ca) (username/password: ATiM/ATiMWiKi) 



# II - ATiM Database Creation/Upgrade

**Full installation**

```
mysql -u {username} -p{password} {atim_database} --default-character-set=utf8 < atim_v2.7.0_full_installation.sql
mysql -u {username} -p{password} {atim_database} --default-character-set=utf8 < atim_v2.7.1_upgrade.sql
mysql -u {username} -p{password} {atim_database} --default-character-set=utf8 < atim_v2.7.2_upgrade.sql 
mysql -u {username} -p{password} {atim_database} --default-character-set=utf8 < atim_v2.7.3_upgrade.sql 
```

**ATiM v2.6.8 upgrade**

```
mysql -u {username} -p{password} {atim_database} --default-character-set=utf8 < atim_v2.7.0_upgrade.sql
mysql -u {username} -p{password} {atim_database} --default-character-set=utf8 < atim_v2.7.1_upgrade.sql
mysql -u {username} -p{password} {atim_database} --default-character-set=utf8 < atim_v2.7.2_upgrade.sql
mysql -u {username} -p{password} {atim_database} --default-character-set=utf8 < atim_v2.7.3_upgrade.sql
```

**Demo installation**

Run the batch demo file for Windows: 

```
/scripts/v2.7.0/DemoData/atim_v2.7.3_demo.bat
```

Or run the shell script demo file for Linux:

```
/scripts/v2.7.0/DemoData/atim_v2.7.3_demo.sh
```



# III - ATiM Migration Details & Action Items

## ATiM v2.7.0 Release

See ATiM [v2.7.0](https://gitlab.com/ctrnet/atim-master/-/releases/v2.7.0) release notes.

### *v270 # 1* - CakePhp version upgrade and Reformatting source code
    
**DETAILS**

ATiM version 2.7.0 is built on the version 2.9.8 of CakePhp. 

In addition to this upgrade, the source code of ATiM has been reformatted to be more compliant with the [CakePhp coding standards](https://book.cakephp.org/2.0/en/contributing/cakephp-coding-conventions.html).

**TODO**

In any custom php files (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories), reformat custom code following the instructions listed below. Note that all php scripts listed into the instructions are located in `scripts/v2.7.0/CustomCodeSnifer/`.

* Reformat custom code to PSR-2 coding style

Instructions for eclipse:

```
a. Window>preferences>PHP>Formatter.
b. Choose PSR2>OK
c. Click on the app folder of project in PHP Explorer
d. Press Ctrl+Shift+F.
```

* Replace `flash()` function with either `atimFlashWarning()`, `atimFlashInfo()`, `atimFlashConfirm()` or `atimFlashError()`


* Remove php deprecated function in php 7.0

Use `explode()` instead of `split()`.

* Replace conditional statements `else if` with `elseif`

Use replacement function in editor to replace statements (Replace regular expression `else\s\s*if` with `elseif`).

* Delete `?>` at the end of pure PHP files

Note: If you don't add --commit option the changes will be logged in a file without effecting the codes.

Linux or mac:

```
php ./CustomCodeSnifer/RemovePhpTagAndSpaceAtTheEndOfFile-custom.php /.../atim/app --commit
```

Windows:

```
php CustomCodeSnifer\RemovePhpTagAndSpaceAtTheEndOfFile-custom.php ..\..\app --commit
```

* Change case (from uppercase to lowercase) for `NULL`, `FALSE` and `TRUE` using replacement function in editor


* Change the `function` to `public function` in the class (if you don't add `--commit` option the changes will be logged in a file without effecting the codes)

Linux or mac:

```
php ./CustomCodeSnifer/addPublic-custom.php /.../atim/app --commit
```

Windows: 

```
php CustomCodeSnifer\addPublic-custom.php ..\..\app --commit
```

* Change the `underscore_case` to `camelCase` (if you don't add `--commit` option the changes will be logged in a file without effecting the codes)

Linux or mac:

```
php ./CustomCodeSnifer/snakeToCamel-custom.php /.../atim --commit
```

Windows: 
	
```
php CustomCodeSnifer\snakeToCamel-custom.php ..\..\app --commit
```

### *v270 # 2* - The recursive model attribute is now type sensitive

**DETAILS**

Any code matching `'recursive' => 'integer_value'` should be replaced with `'recursive' => integer_value`.

For example:

```php
$this->Model->find('first', array(
	'conditions' => array(),
	'recursive' => '1'
));
```

Replaced by:

```php
$this->Model->find('first', array(
	'conditions' => array(),
	'recursive' => 1
));
```

**TODO**

Parse any custom code (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories), to apply the same change.

### *v270 # 3* - Custom autoComplete field functions

**DETAILS**

The autocomplete field did not work in the v2.6.8 version for submitted data having special characters like [`\%_`].

The following functions of the controllers and/or models (in the trunk source code) have been modified to fix this issue. All these functions were in charge of either:
1. Generating the autocomplete lists. 
1. Formatting the displayed values in an autocomplete list.
1. Or validating the selected values.

**Here are the lists of the updated functions:**

* Drug definition by selecting generic name

```
Correction : On autocomplete and validation functions
Field : Drug.generic_name
Function(s):
	DrugsController.autocompleteDrug() [controller]
	Drug.getDrugIdFromDrugDataAndCode() [model]
```

* Aliquot definition by selecting barcode

```
Correction : On autocomplete function only
Field : AliquotMaster.barcode
Functions(s):
	AliquotMastersController.autocompleteBarcode() [controller]
```

* Storage (box, freezer, etc.) definition by selecting storage container selection label

```
Correction : On autocomplete function only
Field : StorageMaster.Selection_label
Functions(s):
	StorageMastersController.autocompleteLabel() [controller]
	StorageMaster.getStorageDataFromStorageLabelAndCode() [model]
```

* TMA slide definition by selecting barcode

```
Correction : On autocomplete function only
Field : TmaSlide.barcode
Functions(s):
	TmaSlidesController.autocompleteBarcode() [controller]
```

* TMA slide immunochemistry definition by selecting exiting values

```
Correction : On autocomplete function only
Field : TmaSlide.immunochemistry
Functions(s):
	TmaSlidesController.autocompleteTmaSlideImmunochemistry() [controller]
```

* Study definition by selecting study title

```
Correction : On autocomplete and validation functions
Field : StudySummary.title
Functions(s):
	StudySummariesController.autocompleteStudy() [controller]
	StudySummary.getStudyIdFromStudyDataAndCode() [model]
```

**The following sections present the modifications done on controller functions (as for DrugsController.autocompleteDrug() function):**
  
* Special characters of the `$term` have been formatted using `str_replace()`

```php
$term = str_replace(array( "\\", '%', '_'), array("\\\\", '\%', '\_'), $_GET['term']);
```

* Search conditions have been changed  

From 

```php
"Model.Field LIKE '%" . trim($keyWord) . "%'",
```

To 

```php
"Model.Field LIKE " => '%' . trim($keyWord) . '%', 
```

* Returned value has been formatted using `str_replace()` 

```php
$result = "";
foreach ($data as $dataUnit) {
	 $result .= '"' . str_replace(array('\\', '"'), array('\\\\', '\"'), dataUnit . '", ';
}
```

**The following sections present the modifications done on model functions (as for Drug.getDrugIdFromDrugDataAndCode($drugDataAndCode) function):**

* Special characters of the submitted value (like `$drugDataAndCode`) have been formatted using `str_replace()`

```php
$submitedValue = str_replace(array( "\\", '%', '_'), array("\\\\", '\%', '\_'), $submittedValue);
```

* Search conditions have been changed

From

```php
"Model.Field LIKE '%" . trim($submittedValue) . "%'",
```
	
To 

```php
"Model.Field LIKE " => '%' . trim($submittedValue) . '%',
```

**TODO**
   
Check if any trunk function listed above has been overridden in custom code (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories) then clean up custom code function with fixes if applicable.

### *v270 # 4* - New content in collection tree view upgrade
    
**DETAILS**
   
Both Quality Control and the Path Review informations are now summarized into the collection tree view when these information is not specifically linked to a tested aliquot (meaning not displayed as an Aliquot Use under the attached aliquot).
   
**TODO**
   
Check if any customization exists on both the structure with alias `sample_uses_for_collection_tree_view` and the function `SampleMastersController.contentTreeView()` then validate and clean up customized structure and/or the custom function if applicable.


## ATiM v2.7.1 Release

See ATiM [v2.7.1](https://gitlab.com/ctrnet/atim-master/-/releases/v2.7.1) release notes.

### *v271 # 1* - Collection Templates improvement by the addition of a Collection Protocols tool

**DETAILS**

Improvement of the Collection Template tool allowing user to create Collection Protocols. 

A Collection Protocols is considered in ATiM as a list of collection events (or visits) a participant is supposed to participate. The tool lets users to define the time-line, the default values of each collection plus which collection templates to use for each visit.

**TODO 1 : To disable the feature**

Complete following steps:

* Create Hook file `app/Plugin/Tools/View/Template/Hook/listProtocolsAndTemplates_protocol.php` and copy paste following code to hide the Collection Protocols sub list and the Add Protocol button.

```php
$displayNextForm = false;
$links = array(
	'bottom' => array(
	'add' => '/Tools/Template/add/'
	)
);
```

* Hide all structure fields displaying protocol data into the `InventoryManagement` plugin pages.

```sql  
UPDATE structure_formats SET `flag_search`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='view_collection') 
AND structure_field_id=(
	SELECT id FROM structure_fields 
	WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='collection_protocol_id' 
	AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='collection_protocols') 
	AND `flag_confidential`='0'
);

UPDATE structure_formats SET `language_heading`='collection' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='clinicalcollectionlinks') 
AND structure_field_id=(
	SELECT id FROM structure_fields WHERE `model`='Collection' AND `tablename`='collections' AND `field`='acquisition_label' 
	AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'
);

UPDATE structure_formats SET `language_heading`='', `flag_index`='0' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='clinicalcollectionlinks') 
AND structure_field_id=(
	SELECT id FROM structure_fields WHERE `model`='Collection' AND `tablename`='collections' AND `field`='collection_protocol_id' 
	AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='collection_protocols') 
	AND `flag_confidential`='0'
);

UPDATE structure_formats SET `flag_index`='0', `flag_summary`='0' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='collections_for_collection_tree_view') 
AND structure_field_id=(
	SELECT id FROM structure_fields WHERE `model`='Collection' AND `tablename`='collections' AND `field`='collection_protocol_id'
	AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='collection_protocols')
	AND `flag_confidential`='0'
);
```

**TODO 2 : To validate there is no impact on custom code**

Validate that the new trunk code works correctly with local custom code.

* Check these following controller functions (and associated views) have not been overridden by custom code (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories) else validate then correct custom code if applicable.

```php
ClinicalCollectionLinksController.listall()
ClinicalCollectionLinksController.add()
```

* Check if these following custom hook files exist on the local installation then validate and correct these one if applicable. A specific attention should be paid on the `$addButtonLinks` variable.

The `listall()` functions

```
app/Plugin/ClinicalAnnotation/Controller/Hook/ClinicalCollectionLinks_listall_before_find.php
app/Plugin/ClinicalAnnotation/Controller/Hook/ClinicalCollectionLinks_listall_format.php
app/Plugin/ClinicalAnnotation/View/ClinicalCollectionLinks/Hook/listall.php
app/Plugin/ClinicalAnnotation/View/ClinicalCollectionLinks/Hook/listall_tree_view.php
```

The `add()` functions

```
app/Plugin/ClinicalAnnotation/Controller/Hook/ClinicalCollectionLinks_add_format.php
app/Plugin/ClinicalAnnotation/Controller/Hook/ClinicalCollectionLinks_add_initial_display.php
app/Plugin/ClinicalAnnotation/View/ClinicalCollectionLinks/Hook/add_collection_detail.php
app/Plugin/ClinicalAnnotation/View/ClinicalCollectionLinks/Hook/add_consent_detail.php
app/Plugin/ClinicalAnnotation/View/ClinicalCollectionLinks/Hook/add_diagnosis_detail.php
app/Plugin/ClinicalAnnotation/View/ClinicalCollectionLinks/Hook/add_trt_detail.php
app/Plugin/ClinicalAnnotation/View/ClinicalCollectionLinks/Hook/add_event_detail.php
```

### *v271 # 2* - Add persistent field to the database connection details

**DETAILS**

To guarantee that the right error page will be displayed when database connection fails, be sure that the `persistent` is set to `false`.

```php
public $default = array(
	'datasource' => 'Database/Mysql',
	'persistent' => false,
	'host' => '127.0.0.1',
	'login' => 'root',
	'password' => '',
	'database' => 'v27z',
	'prefix' => '',
	'encoding' => 'utf8',
	'port' => 3306
);
```

**TODO**

Set `persistent` variable to false.

### *v271 # 3* - ReportsControllerCustom & javascript:history.back()

**DETAILS**

The code `javascript:history.back()` does not work into `ReportsControllerCustom()` functions and should be replaced by `Router::url(null , true)`.

**TODO**

In custom controller `class ReportsControllerCustom extends ReportsController{}` replace

```php
javascript:history.back()
```

by

```php
Router::url(null , true)]
```

### *v271 # 4* - AnnouncementsController conflict

**DETAILS**

The `AnnouncementsController` of the `Customize` plugin has been renamed to `UserAnnouncementsController` to remove all conflicts with the `AnnouncementsController` of the `Administrate` plugin. 

**TODO**

Check if custom code (see below) has been developed for the `Announcements` controller on the local installation then update code if applicable by replacing `Announcements` by `UserAnnouncements`.

```
/app/Plugin/Customize/Controller/AnnouncementsController/Hook
/app/Plugin/Customize/Controller/AnnouncementsController/Custom

/app/Plugin/Customize/View/Announcements/Hook
```

### *v271 # 5* - New pre_search_handler Hooks

**DETAILS**

Many controller function hook calls (`$hookLink = $this->hook('pre_search_handler');`) have been added before `searchHandler()` function calls (see controller `search()` function).

```php
$hookLink = $this->hook('pre_search_handler');
if ($hookLink) {
	require ($hookLink);
}

$this->searchHandler()
```

**TODO**

Check if the following custom hook file `/app/Plugin/Study/Controller/Hook/StudySummaries_search_format.php` exists on the local ATiM then validate and correct code if applicable. The following (and obsolete) code lines of the controller function `StudySummariesController.search()` have been replaced by the new code displayed above.

```php
$hook_link = $this->hook('format');
if( $hook_link ) { require($hook_link); }
		
$this->searchHandler($search_id, $this->StudySummary, 'studysummaries', '/Study/StudySummaries/search');
```

### *v271 # 6* - Upload file feature

**DETAILS**

A new type of field has been created for downloading files on server (pdf, jpg, etc).

```php
structure_fields.type = 'file'
structure_fields.setting = 'class=upload'
```

**TODO**

If you your local installation required these kind of field:
* See [How to upload file on server with ATiM feature](http://atim-software.ca/en/quick-install/how-to-upload-file-on-server-with-atim-feature) to create specific fields.
* Set the core variable `uploadDirectory` with the path of the server directory keeping downloaded files (see `app/Config/core.php`).
* [Change server directory permissions](https://atim-software.ca/en/quick-install/#atimpermissions)

### *v271 # 7* - Charts feature to complete reports

**DETAILS**

A new feature has been developed to add charts to reports.

**TODO**

See page [How to create a report](http://wiki.atim-software.ca/index.php?title=How_to_create_a_report#Graphics) on ATiM Wiki (username/password: ATiM/ATiMWiKi) to create specific charts.

### *v271 # 8* - Option to copy user logs data on a server file

**DETAILS**

A new feature has been developed to copy part of the information of the `	user_logs`	 database table to a file created on the server to limit the number of rows of the table.

**We strongly recommend to set up your local installation with this new configuration**

**TODO**

* Set path of the server directory to the core variable `atim_user_log_output_path` (see `app/Config/core.php`).
* [Change server directory permissions](https://atim-software.ca/en/quick-install/#atimpermissions)
* Add scheduled task (`crontab` or `.bat`) on the server to zip file `user_logs.txt` every weeks (or months) and delete this one to limit the size of this output file.

### *v271 # 9* - ATiM compatible with the Directory Access Protocol (LDAP)

**DETAILS**

A new feature has been developed to use `LDAP` as the identification process.

**TODO**

If your installation required the `LDAP` identification process
* Set core variables `if_use_ldap_authentication` and more (see `app/Config/core.php`).
* See documentation [LOGIN AND PASSWORD RULES CONFIGURATION](http://atim-software.ca/en/quick-install/login-and-password-rules-configuration) to complete configuration.

### *v271 # 10* - Views update

**DETAILS**

Following views have been modified in trunk version.

```
ViewAliquot
ViewCollection
ViewSample
ViewStorageMaster
```

**TODO**

Check if customized views exist in both `app/Plugin/InventoryManagement/Model/Custom/` and `app/Plugin/StorageLayout/Model/Custom/`. 

```php
class ViewAliquotCustom extends ViewAliquot{}
class ViewCollectionCustom extends ViewCollection{}
class ViewSampleCustom extends ViewSample{}
class class StorageMasterCustom extends StorageMaster{}
```

For any custom view that overrides the `$tableQuery` variable of the parent view (trunk code), copy and paste the variable value (SQL statement) from the parent model (view of the trunk version) to the custom view plus add custom SQL code to the copied trunk SQL statement (custom selected fields, custom joins, custom conditions).

### *v271 # 11* - OrderItems.edit() function deprecated and replaced by OrderItems.editInBatch()

**DETAILS**

Function `OrderItems.edit()` is now deprecated and replaced by `OrderItems.editInBatch()`. The structure `orderitems_returned` has also been deleted.

**TODO**

* When the `OrderItems.edit()` function has been overridden in a `OrderItemsControllerCustom` controller (check if controller exists in `app/Plugin/Order/Controller/Custom/`), then add the custom function `editInBatch()` to implement the same custom business rules than these developed in the `edit()` custom function and remove the last one.

* When one of the following hook files have been developed, create the appropriated hook files associated with the `editInBatch()` function to implement the same custom business rules than these previously developed for the `edit()` function.

Obsolete Hooks:

```
app/Plugin/Order/Controller/Hook/OrderItems_edit_format.php
app/Plugin/Order/Controller/Hook/OrderItems_edit_presave.php
app/Plugin/Order/Controller/Hook/OrderItems_edit_postave.php
app/Plugin/Order/View/OrderItems/Hook/edit.php
```

New Hooks:

```
app/Plugin/Order/Controller/Hook/OrderItems_editInBatch_format.php
app/Plugin/Order/Controller/Hook/OrderItems_editInBatch_initial_display.php
app/Plugin/Order/Controller/Hook/OrderItems_editInBatch_presave_process.php
app/Plugin/Order/Controller/Hook/OrderItems_editInBatch_postsave_process.php
app/Plugin/Order/View/OrderItems/Hook/editInBatch.php
```

* Check that no custom code (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories) set the structure `orderitems_returned` else change the alias if applicable.

* Check that no custom custom structure fields has been attached to the deprecated structure `orderitems_returned` else transfer these fields to the structure `orderitems`.

### *v271 # 12* - Variable $structureOverride

**DETAILS**

Any variables `$override` have been renamed to `$structureOverride` to be consistent all over the code.

**TODO**

* In any custom code (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories) search on following variable using the search function in editor and replace the found one with `$structureOverride` variable.
    - Variable `$override` to replace with `$structureOverride`.
    - Variable `$overrideData` to replace with `$structureOverride`.
    - Variable `$createdSampleOverrideData` to replace with `$createdSampleStructureOverride`.
    - Variable `$createdAliquotOverrideData` to replace with `$createdAliquotStructureOverride`.

* In any custom code (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories) search on following `$this->set()` function calls using the search function in editor and replace the found one with `$this->set('structureOverride', ` call.
    - Function call `$this->set('override', ...)`	 to replace with `$this->set('structureOverride', ...)`.
    - Function call `$this->set('overrideData', ...)` to replace with `$this->set('structureOverride', ...)`.
    - Function call `$this->set('createdSampleOverrideData', ...)` to replace with `$this->set('createdSampleStructureOverride', ...)`.
    - Function call `$this->set('createdAliquotOverrideData', ...)` to replace with `$this->set('createdAliquotStructureOverride', ...)`.

### *v271 # 13* - Material tool clean up
    
**DETAILS**

The `Material` tool has been cleaned up and is now functional. Also the `Material` model in SOP plugin has been removed. 

**TODO**

Check that no custom code (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories) use `Material` model and functions using the search function in editor then correct code if applicable.      
   
### *v271 # 14* - Test version
    
**DETAILS**

Added a core variable `isTest` to display install name as a test version (change header color to red plus add suffix 'Test') or as a prod version (see `app/Config/core.php`). 

**TODO**

Set core variable `isTest` to 1 for any installation on test.
           
### *v271 # 15* - Collection Template Default Values
    
**DETAILS**

Added a new feature to set default values for each node of a Collection Template (see Collection Template tool).

**TODO**

Check if following custom code files exists on local installation. If one or both exist, check if these custom codes have been initially created to manage default values. If they were, check if the new feature can replace custom code (meaning that user himself can set the default value creating a Collection Template) then remove custom code and transfer knowledge to user. 

```
app/Plugin/InventoryManagement/Controller/Hook/AliquotMasters_add_initial_display.php
app/Plugin/InventoryManagement/Controller/Hook/SampleMasters_add_initial_display.php
```


## ATiM v2.7.2 Release

See ATiM [v2.7.2](https://gitlab.com/ctrnet/atim-master/-/releases/v2.7.2) release notes.

### *v272 # 1* - Set @user_id in upgrade SQL scripts

**DETAILS**

At the beginning of any `atim_vx.x.x_upgrade.sql` script, a `@user_id` variable is set to populate the `created_by` and `modified_by` fields of the ATiM data tables for any records inserted by the sql script.     

**TODO**

Replace the username by the appropriate `username` if applicable.

```sql
SET @user_id = (SELECT id FROM users WHERE username = 'System');
```

### *v272 # 2* - Replaced SampleMaster.parent\_id and $parentSampleDataForDisplay variable with variable Generated.parent_sample
 
**DETAILS**

The variable `$parentSampleDataForDisplay` previously used to display information about the parent sample of a created or displayed derivative sample is not used any more. This variable was used to populate, on a dynamic way, a string gathering parent sample information for field `SampleMaster.parent_id`.

A new structure field `Generated.parent_sample` has been created to simplify the process to display the name of a parent sample and will replace the old one.

These changes have an impact on following functions:

```php
SampleMasters.add()
SampleMasters.detail()
SampleMasters.batchDerivative()
SampleMasters.edit() 
```

**TODO**

Check that no custom code (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories) contains the variable `$parentSampleDataForDisplay` or overridde the field `SampleMaster.parent_id`. 

Search can be done using the search tool of the editor: 
* Search on `/parentSampleDataForDisplay/`.      
* Search on regular expression `/SampleMaster(.+)parent_id/`.

When custom code exists, each function listed above should be tested to validate that the parent sample information is correctly displayed in the form then custom code should be corrected if applicable.

### *v272 # 3* - Move the dropdown_options to controller
    
**DETAILS**

Added new code to validate submitted value in back end for any value selected from a drop down list.
      
**TODO 1 : Force system to validate submitted data for drop down field list overridden by custom code**
      
Check that no drop down list attached to a structure field with `select` type is overridden by custom code in a custom view searching following strings in custom view (see `app/Plugin/../View/../Hook` directories).

```php
dropdown_options
dropdownOptionValues
dropdownOption
dropdown
```

When one of the strings listed above is found into a custom view (hook file), meaning that the list has been overridden by the custom code of the view, then add code below in the hook file `format` embedded by the controller function attached to the view (`app/Plugin/../Controller/Hook/..._format.php`). This code lines will force framework to validate submitted value based on list of values passed in arguments.

```php
$this->Structures->setDropdownOptions($theListsOfValues, 'nameOfTheVariableUsedInViewFile')"
```

An example of code can be reviewed in the `SpecimenReviewsController.add()` function and its associated view file `app/Plugin/InventoryManagement/View/SpecimenReview/add.ctp` .

* SpecimenReviewsController (controller)

```php
public function add($collectionId, $sampleMasterId, $specimenReviewControlId)
{
	...
	// Get aliquots list in array
	$aliquotList = $this->AliquotReviewMaster->getAliquotListForReview(...);
	...
	$dropdownOptions = array(
		'AliquotReviewMaster.aliquot_master_id' => $aliquotList
	);
	$this->Structures->setDropdownOptions($dropdownOptions, 'dropdownOptions');
	...
}
```

* add.ctp (view)

```php
...
$finalOptions = array(
    ...
    'dropdown_options' => $dropdownOptions
);
...
$this->Structures->build($finalAtimStructure, $finalOptions);
...
```

**TODO 2 : Clean up the wrong defined non-drop down fields**
             
Check that no structure field exists with a type different than `select` but a list of values attached to this one by the `structure_fields.structure_value_domain`. Data entry will generated an error any time the user will submit data to record.

Structure field to correct can be listed executing the following query: 

```sql
SELECT structure_alias, type, structure_value_domain_name 
FROM view_structure_formats_simplified 
WHERE type != 'select' 
AND structure_value_domain IS NOT NULL
AND structure_value_domain !=(SELECT id FROM structure_value_domains WHERE domain_name='forgotten_password_reset_questions');
```

The bug can be fixed deleting the field link to the `structure_value_domains`:

```sql
UPDATE structure_fields SET structure_value_domain = NULL WHERE ...
```

### *v272 # 4* - The type of the aliquot In Stock Detail field list is changed from fix to custom drop down
    
**DETAILS**

Changed drop down list associated to the field `AliquotMaster.in_stock_detail` from fix list, to custom drop down list.

**TODO**
      
Comment appropriated SQL statements in `atim_v2.7.2_upgrade.sql` if this change has already be done on your local version. Statements can be found searching for the following string in the sql file.

```php
-- Issue# 3652: Change Aliquot In Stock Detail list from fix to custom' in  if this change has already be done on your local version.
```

### *v272 # 5* - Change of the batch actions form display
    
**DETAILS**

In any batch action forms, the `structures` (forms) used to display either the used samples or the used aliquots (displayed parent data) have been modified to make forms smaller. These changes have an impact on the following:

* Forms
    - Structure `view_sample_joined_to_collection` used previously in batch action forms and replaced by `batch_process_view_sample_joined_to_collection` in these forms.
    - Structure `used_aliq_in_stock_details`.
    - Structure `used_aliq_in_stock_detail_volume`.

* Functions
    - `SampleMaster.batchDerivative()` allows users to create derivatives in batch from a list of samples or aliquots.
    - `AliquotMaster.add()` allows users to create aliquots in batch from a list of samples.
    - `AliquotMaster.defineRealiquotedChildren()` allows users to define realiquoted children in batch from a list of aliquots.
    - `AliquotMaster.realiquot()` allows users to realiquot in batch a list of aliquots.
    - `AliquotMaster.addAliquotInternalUse)` allows users to create in batch a same uses/events for a list of aliquots.
    - `QualityCtrl.add()` allows users to create quality control from a list of samples or aliquots.

**TODO**

Launch all functions listed above and validate that the parent information is correctly displayed in following sub-sections forms `Aliquot Source (For update)`, `Sample`, `Parent Aliquot (For update)`, `Used Aliquot (For update)`, `Parent Sample`, etc.

Update structures (forms) `view_sample_joined_to_collection`, `used_aliq_in_stock_details` and `view_sample_joined_to_collection` if fields are missing or custom fields have to be re-ordered.

### *v272 # 6* - Record old user log data in text file

**DETAILS**

Added feature to limit the number of records tracked by the `user_logs` database table.

When the core variable `atim_user_log_output_path` (see `app/Config/core.php`) is set to a server directory path value, a check is done on the all content of the `user_logs` data table. If one log has been created more than one year ago, the system will copy all logs older than 6 months into a text file `user_logs.txt` recorded on the server in a folder as defined in the `atim_user_log_output_path`. Then all logs older than 6 months will be erased in the `user_logs` database table.

**TODO**

If you set the core variable to a server directory path value:

* Be sure that the path `atim_user_log_output_path` exists and the permissions are correct for writing into the directory.

* Validate the correct creation of the file `user_logs.txt` stored into the folder identified by the `atim_user_log_output_path` variable.

* Change code or core variable if you don't want to erase data of the `user_logs` table.

* Run the `/scripts/v2.7.0/ATiMDatabaseLogCleaner/backupAndClearOldUserLogs.php` to export all records older than 6 months into the `user_logs` table (don't forget to set the server, database, username, password and date).

### *v272 # 7* - Simplification of the permissions management tool

**DETAILS**

In ATiM Group Permissions form (`Administrate/Permissions/tree/%%Group.id%%/`), the list of actions that a user can have access or not have been limited to simplify the task of the ATim administrator. Most of the actions have been hidden in the tree and permissions have been matched to similar actions than could be activated or not.

As an example, the action `Controller/ClinicalAnnotation/FamilyHistories/listall` has been hidden and system will check permission on action `Controller/ClinicalAnnotation/FamilyHistories/detail` to let user access the `listall` page or not.

The association between hidden and displayed actions can be reviewed checking variable `$hiddenPermissionsAndSubstitutions` of the `PermissionManagerComponent`.

**TODO**

* Permissions tool can be tested.

* Content of the `$hiddenPermissionsAndSubstitutions` variable can be reviewed to validate the matches. 
   
### *v272 # 8* - Check notBlank in back end

**DETAILS**

Added new code to validate `notBlank` in back end.

This could have many impacts on data creation and update when a structure (form) contains at least one `structure_fields` linked to a validation `notBlank` but the field is not displayed on the page used to create or modify the record (`flag_add = '0`, `flag_edit = '0`, etc.). Although the field is not displayed, the user will get an error message similar these:
* `The barcode is required!`.
* `The acquisition label is required! (Acquisition Label)`.
* `The {field} is required!`.
* Etc.

As an example:

* When the `AliquotMaster.barcode` of your local version is generated by the system, then the new version code could have an impact on the `AliquotMaster.add()` and `AliquotMaster.realiquot()` functions and generate this message: `The barcode is required!`.

* When the `Collection.acquisition_label` of your local version is not displayed, then the new version code could have an impact on the `Collection.add()` function and generate this message: `The acquisition label is required! (Acquisition Label)`.

**TODO**

* List structures having at least one field hidden and linked to a structure_valdiations `notBlank` running following query: 

```sql      
SELECT DISTINCT structure_alias,
model,
field,
structure_value_domain_name,
type
FROM (
	SELECT structure_alias, plugin, model, field, structure_value_domain_name, type
	FROM view_structure_formats_simplified
	WHERE structure_id IN ( SELECT DISTINCT structure_id FROM view_structure_formats_simplified WHERE flag_add = '1')
	AND structure_field_id IN ( SELECT structure_field_id FROM structure_validations WHERE rule = 'notBlank')
	AND (flag_add = '0')
	UNION ALL
	SELECT structure_alias, plugin, model, field, structure_value_domain_name, type
	FROM view_structure_formats_simplified
	WHERE structure_id IN ( SELECT DISTINCT structure_id FROM view_structure_formats_simplified WHERE flag_addgrid = '1')
	AND structure_field_id IN ( SELECT structure_field_id FROM structure_validations WHERE rule = 'notBlank')
	AND (flag_addgrid = '0')
	UNION ALL
	SELECT structure_alias, plugin, model, field, structure_value_domain_name, type
	FROM view_structure_formats_simplified
	WHERE structure_id IN ( SELECT DISTINCT structure_id FROM view_structure_formats_simplified WHERE flag_edit = '1')
	AND structure_field_id IN ( SELECT structure_field_id FROM structure_validations WHERE rule = 'notBlank')
	AND (flag_edit = '0')
	UNION ALL
	SELECT structure_alias, plugin, model, field, structure_value_domain_name, type
	FROM view_structure_formats_simplified
	WHERE structure_id IN ( SELECT DISTINCT structure_id FROM view_structure_formats_simplified WHERE flag_editgrid = '1')
	AND structure_field_id IN ( SELECT structure_field_id FROM structure_validations WHERE rule = 'notBlank')
	AND (flag_editgrid = '0')
) AS res
WHERE 
!(structure_alias IN ('aliquot_master_edit_in_batchs', 'consent_masters', 'consent_masters_study', 'miscidentifiers',
	'aliquotinternaluses', 'aliquot_masters', 'miscidentifiers', 'miscidentifiers_study', 'orderlines', 'orders', 
	'tma_slides', 'tma_slide_uses') AND model = 'StudySummary' AND field = 'title')
AND !(
	structure_alias IN ('ctrnet_demo_txe_hormono_therapies', 'ctrnet_demo_txe_systemic_treatments', 'pe_chemos', 'txe_chemos')
	AND model = 'Drug' AND field = 'generic_name') 
AND !(structure_alias = 'collections' AND model = 'Generated' AND field = 'field1')
AND !(structure_alias = 'sourcealiquots' AND model = 'AliquotMaster' AND field = 'in_stock')
AND !(structure_alias IN ('querytool_batch_set') AND model = 'BatchSet' AND field = 'sharing_status')
AND !(structure_alias IN ('sample_masters_for_search_result') AND model = 'Collection' AND field = 'acquisition_label')
AND !(structure_alias = 'csv_popup')
AND !(structure_alias = 'users')
AND !(structure_alias = 'qualityctrls' AND model = 'AliquotMaster' AND field = 'barcode')
AND !(structure_alias = 'storagemasters' AND model = 'StorageMaster' AND field = 'barcode')
AND !(structure_alias = 'orderitems' AND model = 'AliquotMaster' AND field = 'barcode')
AND !(structure_alias = 'orderitems' AND model = 'Shipment' AND field = 'shipment_code')
AND !(structure_alias = 'orderitems' AND model = 'TmaSlide' AND field = 'barcode')
AND !(structure_alias = 'orderitems_and_lines' AND model = 'FunctionManagement' AND field = 'product_type')
AND !(structure_alias = 'aliquotinternaluses' AND model = 'AliquotMaster' AND field = 'barcode')
AND !(structure_alias = 'children_aliquots_selection' AND model = 'AliquotMaster' AND field = 'barcode')
AND !(structure_alias = 'children_aliquots_selection' AND model = 'AliquotMaster' AND field = 'in_stock')
AND !(structure_alias = 'children_storages' AND model = 'StorageMaster' AND field = 'barcode')
AND !(structure_alias = 'ctrnet_demo_txd_systemic_treatments' AND model = 'TreatmentDetail' AND field = 'type')
AND !(structure_alias = 'incrementedmiscidentifiers' AND model = 'MiscIdentifier' AND field = 'identifier_value')
AND !(structure_alias = 'in_stock_detail' AND model = 'AliquotMaster' AND field = 'barcode')
AND !(structure_alias = 'mcode_txd_cancer_related_radiation' AND model = 'TreatmentDetail' AND field = 'code')
AND !(structure_alias = 'aliquotinternaluses' AND model = 'AliquotInternalUse' AND field = 'type')
AND !(structure_alias = 'aliquotinternaluses' AND model = 'AliquotInternalUse' AND field = 'use_code')
AND !(structure_alias = 'storage_controls')
AND !(structure_alias = 'preferences')
AND !(structure_alias = 'orderitems')
AND !(structure_alias = 'in_stock_detail' AND model = 'AliquotMaster' AND field = 'in_stock')
AND !(structure_alias = 'qualityctrls' AND model = 'QualityCtrl' AND field = 'run_id')
ORDER BY structure_alias, model, field;
```

* Then launch the controller actions displaying structures returned by the query and test data record creation and/or update. Validate that no error message is displayed else fix the issue as described below:
    - Either add custom code to manage issue according to your ATiM customization.
    - Or remove `notBlank` validations.

```sql
DELETE FROM structure_validations 
WHERE rule = 'notBlank'
AND structure_field_id IN ( 
	SELECT id FROM structure_fields WHERE `model`='....' AND `tablename`='....' AND `field`='....'
);
```

### *v272 # 9* - Batch Process: Processed Items Limit
    
**DETAILS**
         
The limit of items that could be processed in batch have been tested on development server then reset (increased or decreased in v272) based on test results.

If you upgrade your ATiM to v2.7.3 and more you can skip this note and focus on upgrade message `v273 # 12`.
     
**TODO**

Review following core variables (see `app/Config/core.php`) and test the following actions by testing the maximal number of items a user can process all together according to this core variables. Change values assigned to each variable to increase or decrease the value if applicable. The limit could be increased according to your server configuration and performances.

* Core variables:

```php
ParticipantMessageCreation_processed_participants_limit
SampleDerivativeCreation_processed_items_limit
AliquotCreation_processed_items_limit
AliquotModification_processed_items_limit
AliquotInternalUseCreation_processed_items_limit
RealiquotedAliquotCreation_processed_items_limit
AliquotBarcodePrint_processed_items_limit
QualityCtrlsCreation_processed_items_limit
AddToOrder_processed_items_limit
AddToShipment_processed_items_limit
defineOrderItemsReturned_processed_items_limit
editocessed_items_limit
TmaSlideCreation_processed_items_limit
```

* Actions:

```php
ParticipantMessage.add()
SampleMasters.batchDerivative()
AliquotMasters.add()
AliquotMasters.editInBatch()
AliquotMasters.addAliquotInternalUse()
AliquotMasters.defineRealiquotedChildren()
AliquotMasters.realiquot()
AliquotMasters.printBarcodes()
QualityCtrls.add()
OrderItems.add()
OrderItems.addOrderItemsInBatch()
Shipments.addToShipment()
OrderItems.defineOrderItemsReturned()
OrderItems.editInBatch()
TmaSlides.add()
TmaSlides.editInBatch()
TmaSlideUses.add()
TmaSlideUses.editInBatch()
```

### *v272 # 10* - New Reports

**DETAILS**

Two new reports have been developed into the v272:
* `ATiM Dashboard` accessible from the home page to offer a tool to users to follow-up data records/updates in ATiM.
* `Unclassified storage List` to list all storages with unpositioned items.

**TODO**

Inactivate reports if these ones have not to be accessible to users running following queries:

```sql
UPDATE datamart_reports SET flag_active = 0 WHERE name = 'unclassified storage list';
UPDATE datamart_reports SET flag_active = 0 WHERE name = 'report_atim_dashboard';
```

### *v272 # 11* - French sentences spelling correction
    
**DETAILS**

The French word `Un Aliquot` has been corrected to `Une Aliquote`.

**TODO**

Check that no correction has to be done on custom i18n records running following query:

```sql
SELECT id, fr FROM i18n WHERE fr LIKE '%aliquot%' AND fr NOT LIKE '%aliquote%';
```

### *v272 # 12* - JavaScript update

**DETAILS**

Many of the JavaScript's files have been updated. All caches of web browsers have to be cleared.

**TODO**

All users have to clear the cache of their web browser.

### *v272 # 13* - ATiM Forms Library
    
**DETAILS**
         
A new library of forms created for different ATiMs projects is now accessible under script folder `/scripts/v2.7.0/CustomFormsLibrary`.

**TODO**

* Check with your users if some forms can be re-used with or without customization.

* Send [email](support@atim-software.ca) to CRCHUM ATiM Laboratory team to add your own custom forms if you want to share these forms to the community.

### *v272 # 14* - New Parent To Derivative Sample Control
    
**DETAILS**

Many missing `Parent To Derivative Sample Controls` have been added to the ATiM v272 but flagged as inactivated (see `parent_to_derivative_sample_controls` table). 
      
**TODO**

Comment lines under comment `Add missing parent to derivative samples controls` in `atim_v2.7.2_upgrade.sql` for links you already created into your local version to avoid duplicated records and database table constraints errors.

### *v272 # 15* - Options to let ATiM creating aliquots barcodes

**DETAILS**

A new code and function have been developed to let ATiM to create the aliquot barcode when the aliquot barcode field is either hidden or kept empty by user. 

Configuration can be done changing the values of core variables (see `app/Config/core.php`) as defined below if applicable.

**TODO**

* To hide barcode field in aliquot creation forms (creation from a sample or by realiquoting from a parent aliquot) and to allow ATiM to assign a new barcode to a created aliquot:
    - Set core variable `hideAliquotBarcodeFieldOfAliquotCreationForms` to `true` (the `AppController.newVersionSetup()` function will execute SQL statements to hide `barcode` field and remove the `notBlank` validation).
    - Set core variable `useATiMAliquotBarcodeGeneratorForEmptyBarcode` to `true` (the `AliquotMaster.generateNewAtimBarcode()` will be executed to generate a new barcode).

* To force user to enter an aliquot barcode:      
    - Set core variable `hideAliquotBarcodeFieldOfAliquotCreationForms` to `false`.
    - Set core variable `useATiMAliquotBarcodeGeneratorForEmptyBarcode` to `false`.

* To let user either to enter an aliquot barcode or to keep field empty to let ATiM to assign a new barcode to the created aliquot:      
    - Set core variable `hideAliquotBarcodeFieldOfAliquotCreationForms` to `false` (the `AppController.newVersionSetup()` function will execute SQL statements to remove the `notBlank` validation on `barcode`field).
    - Set core variable `useATiMAliquotBarcodeGeneratorForEmptyBarcode` to `true` (the `AliquotMaster.generateNewAtimBarcode()` will be executed to generate a new barcode when barcode field is not completed).

* To develop your own code generating the barcode:      
    - Override the `AliquotMaster.generateNewAtimBarcode()` in custom model.

### *v272 # 16* - Options to let ATiM creating default aliquot label

**DETAILS**

A new code and function have been developed to let ATiM generate a default aliquot label any time a new aliquot is created either from a sample or from a parent aliquot by realiquoting. This default aliquot label is displayed when the form used for data entry is displayed for the first time.

**TODO**

Set core variable `useATiMDefaultAliquotLabelSystem` to `true` (see `app/Config/core.php`) if applicable.

The format of the label can be customized by overridden the following functions:
* `AliquotMaster.setStructureOverrideForCreatedAliquotsOfOneSample()`.
* `AliquotMaster.completeSampleChildrenDataForAddAliquotInitialDisplay()`.
* `AliquotMaster.setStructureOverrideForRealiquotedChildrenAliquotsOfOneParent()`.
* `AliquotMaster.completeAliquotChildrenDataForRealiquotInitialDisplay()`.
 
Functions listed above are detailed below.

### *v272 # 17* - Functions to override structure fields when creating new aliquots

Two functions have been developed to populate an array that will be used by the views to override structure fields when creating aliquots from a sample (`AliquotMaster.add()`) or by realiquoting (`AliquotMaster.realiquot()`):

* Function `AliquotMaster.setStructureOverrideForCreatedAliquotsOfOneSample()` (aliquot creation from sample).
* Function `AliquotMaster.setStructureOverrideForRealiquotedChildrenAliquotsOfOneParent()` (aliquot creation by realiquoting). 

Any default value associated to a field into this array will be displayed in the appropriated field for any created records when form is displayed for the first time. These functions let ATiM to create the default values on a dynamic way: Creation of the default aliquot label based on participants and collections data, etc.

**TODO**

Override the functions to customize ATiM by overriding fields by custom values if applicable. 

If you already created custom code (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories) to populate these default values you can keep existing custom code or move your code into functions that override these two functions to simplify custom code and future ATiM upgrade.

### *v272 # 18* - Functions to complete the initial displayed aliquot data when creating new aliquots

Two functions have been developed to complete the initial data sent to the view for data entering when creating aliquots from a sample (`AliquotMaster.add()`) or by realiquoting (`AliquotMaster.realiquot()`):
* Function `AliquotMaster.completeSampleChildrenDataForAddAliquotInitialDisplay()` (aliquot creation from sample).
* Function `AliquotMaster.completeAliquotChildrenDataForRealiquotInitialDisplay()` (aliquot creation by realiquoting). 

These two functions are complementary to the previous ones (function overriding the structures fields) to let ATiM to generate default value specific to each new record to create. As examples, these functions can be used to add an incremental suffix to the default aliquot label created by the function overriding the fields, etc.

**TODO**

Override the functions to customize ATiM by creating dynamically the initial data to display if applicable. 

If you already created custom code (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories) to populate these default values you can keep existing custom code or move your code into functions that override these two functions to simplify custom code and future ATiM upgrade.

### *v272 # 19* - New notification and message mode

**DETAILS**

There are 3 changes related to the notifications and messages that are deactivated by default in `core.php` (see `app/Config/core.php`).

* Browser notification

Display all `atimFlash` message (`atimFlashWarning()`, `atimFlashInfo()`, `atimFlashConfirm()` or `atimFlashError()`) into the Browser Notification tool. To activate this feature you need change the variable `browser_notification` in `core.php` to `true`. 

```php
Configure::write('browser_notification', true);
```

* ATiM flash messages 

Use of small popups that will be disappear after 5 seconds for any messages. Set the variable `pop_up_flash_message` to `true`. 

```php
Configure::write('pop_up_flash_message', true);
```

* Alert messages

To make ATiM alert popups uniform to whatever the browser is, you need to set the variable `pop_up_alert_message` in `core.php` to `true`.

```php
Configure::write('pop_up_alert_message', true);
```

**TODO**

See above.

### *v272 # 20* - Automatic ATiM Customisation Feature: Hide unused sections of records linked to the study
    
**DETAILS**

When user clicks on the `Records linked to study` ATiM sub-menu (`Study/StudySummaries/listAllLinkedRecords/%%StudySummary.id%%/`) accessible in the `Study` tool, ATiM now automatically hide sections (sub-forms) listing records linked to the study when the type of records can never been attached to a study according to the ATiM custom configuration. This automatic feature is based on the databrowser configuration allowing to activate/inactivate databrowser structures (model) links between `Study` structure and the other structures (model) as consent, aliquot internal use, order, etc.

As an example, when the link between `Consent` and `Study` is disabled into the databrowser tool, then the `Consents` sub forms gathering all consents attached to the study will be hidden when user clicks on `Records linked to study`.

**TODO**

If you overridde variable `$linkedRecordsProperties` in a custom code (see `app/Plugin/Study/../Hook` and `app/Plugin/Study/../Custom` directories), the custom code can be erased because this one is not used anymore.

Searches can be done using the search tool of the editor: 
* Search on `/linkedRecordsProperties/`.

### *v272 # 21* - Automatic ATiM Customisation Feature: Hide fields and models of the collection context never linked to a collection in ClinicalCollectionLink

**DETAILS**

In ATiM `Clinical Annotation` plugin, the users can define the context of a participant collection selecting the specific diagnosis, consent, treatment and clinical event to attach to a participant collection (see sub menu `Clinical Annotation / Inventory` (`ClinicalAnnotation/ClinicalCollectionLinks/listall/%%Participant.id%%/`)). According to your local installation set up, these specific links between collection and the other data models can be disabled based on your user requirements to simplify the context of collection and the use of ATiM.  

To simplify the ATiM customization process and limit the creation of custom code and execution of custom installation SQL queries, some code and set up SQL queries (executed by the `AppController.newVersionSetup()` function):
* Hide structure fields of specific models attached to the `ClinicalCollectionLinks` structure setting `flag_index` to zero (all diagnosis fields as an example). 
    - See page `ClinicalAnnotation/ClinicalCollectionLinks/listall/%%Participant.id%%/` listing all participant collections.
* Remove sub-forms like consent, diagnosis, etc used to select/display the context of collection in `ClinicalCollectionLinks` pages when these collections to models (consent, treatment, etc) links are not requested by users:       
    - See page `ClinicalAnnotation/ClinicalCollectionLinks/add/%%Participant.id%%/` allowing user to define the context of a new collection.       
    - See page `ClinicalAnnotation/ClinicalCollectionLinks/detail/%%Participant.id%%/%%Collection.id%%/` allowing user to access the context of a collection.
    - See page `ClinicalAnnotation/ClinicalCollectionLinks/edit/%%Participant.id%%/%%Collection.id%%/` allowing user to modify the context of a collection.

This automatic feature is based on the databrowser configuration allowing to activate/inactivate databrowser structures (model) links between `Collection` structure and the other structures (model) as consent, diagnosis, treatment, etc.

As an example, when the link between `Consent` and `Collection` is disabled into the databrowser tool, then the `Consents` sub forms gathering all consents attached to the participants and the consent structure fields attached to the `ClinicalCollectionLinks` structure will be hidden when users is working on the collection context.

**TODO**

* Check all custom hook files that could exist in `app/Plugin/ClinicalAnnotation/View/ClinicalCollectionLinks/Hook` and erase these that won't be used anymore if exist.

* Validate all fields displayed in `Links to collections` sub-form to check that all expected fields are in (`ClinicalAnnotation/ClinicalCollectionLinks/listall/%%Participant.id%%/`).

### *v272 # 22* - Automatic ATiM Customisation Feature: Automatic configurations based on core variables and controls tables content to simplify local installation configuration

**DETAILS**

Here is the list of custom configurations managed automatically by ATiM and the `AppController.newVersionSetUp()` function based on core variables and controls tables content.

* Databrowser:
    - The databrowser relationships diagram is automatically generated by ATiM
    - The link between `ParticipantIdentifier` and `Participant` models is configured by ATiM based on the existing of active Participant Identifier in `misc_identifier_controls`.
    - The link between `ParticipantIdentifier` and `StudySummary` models is configured by ATiM based on the existing of Study Participant Identifier in `misc_identifier_controls` (values of `misc_identifier_controls.flag_link_to_study field` set to '1' for at least one identifier type).
    - The link between `Consent` and `StudySummary` models is configured by ATiM based on the existing of Study Consent in `consent_controls` (values of `consent_controls.flag_link_to_study` field set to '1' for at least one consent type).
    - The links between `OrderLine` and the models `Order`, `OrderItem`, `StudySummary` are configured by ATiM based on the core variable `order_item_to_order_objetcs_link_setting`:
	
        1. Link OrderItem to both Order and OrderLine (order line submodule available)
        2. link OrderItem to OrderLine only (order line submodule available)
        3. link OrderItem to Order only (order line submodule not available)
		
    - The links between `OrderItem`and the models `TmaSlide`, `Aliquot` are configured by ATiM based on the core variable `order_item_type_config`:
	
        1. both tma slide and aliquot
        2. aliquot only
        3. tma slide only

* Form fields displayed:
    - The display or not of the Participant Identifier section plus the `Add identifier` button in participant profile page is configured by ATiM based on the existing of active Participant Identifier in `misc_identifier_controls`.
    - The display or not of the Participant Identifier Study fields in `miscidentifiers`, `miscidentifiers_for_participant_search` structures is configured based on the existing of Study Participant Identifier in `misc_identifier_controls`.
    - The display or not of the Consent Study fields in `consent_masters` structures is configured based on the existing of Study Consent in `consent_controls`.
    - The display or not of the structure fields gathering study information or allowing users to select a study in different strutures is configured based on the databrowser links configuration allowing to activate/inactivate databrowser structures (model) links between `Study` structure and the other structures (model) as aliquot, etc:
        - `Study` to `Aliquot` : forms `aliquot_masters`, `aliquot_master_edit_in_batchs`, `view_aliquot_joined_to_sample_and_collection`.
        - `Study` to `AliquotUse` : structure `aliquotinternaluses` and ``viewaliquotuses` (partially).
        - `Study` to `TmaSlide` : structure `tma_slides`.
        - `Study` to `TmaSlideUse` : structure `tma_slide_uses`.
        - `Study` to `OrderLine` : structure `orderlines` and ``viewaliquotuses` (partially).
        - `Study` to `Order` : structure `orders`, `orders_short` and ``viewaliquotuses` (partially).

**TODO**

* Note that the files used for the Databrowser Relationships Diagram, accessible when user is launching a new search in the DataBrowser, are not used anymore. See following files in `/app/webroot/img/dataBrowser`:
    - `datamart_structures_relationships.vsd`.
    - `datamart_structures_relationships_en.png`.
    - `datamart_structures_relationships_fr.png`.  

* In the databrowser tool, click on 'New' to launch a new search (`/Datamart/Browser/browse/`) then click on the `Data Types Relationship Diagram` to validate that all data structures links are correctly activated/disabled.

* Test that you can or not work with study information according to your local installation setup when creating, editing or displaying following data type:
    - MiscIdentifier
    - Consent
    - Aliquot
    - AliquotUse
    - TmaSlide
    - TmaSlideUse
    - Order
    - OrderLine


## ATiM v2.7.3 Release

See ATiM [v2.7.3](https://gitlab.com/ctrnet/atim-master/-/releases/v2.7.3) release notes.

### *v273 # 1* - New sample type

**DETAILS**

Created new sample types:
* Bronchial washing
* BMMC (Bone marrow mononuclear cells)
* Red blood cells
* Viral RNA
* White blood cells
* PDX (Patient derived xenograft) and all PDX derivatives

**TODO**

Comment lines in `atim_v2.7.3_upgrade.sql` for any sample types already created on your local version to avoid script upload issue.

Activate any sample types having some interest for your local installation.

### *v273 # 2* - New Parent To Derivative Sample Control
    
**DETAILS**

Created missing Parent To Derivative Sample Controls (see `parent_to_derivative_sample_controls` table) flagged as inactivated (`flag_active = 0`):
* Bone Marrow to Buffy Coat.
* Bone Marrow to Plasma.
      
**TODO**

Comment SQL statements following comment `Issue #95: Add Plasma and Buffy Coat derivatvie from Bone Marrow` in the SQL file `atim_v2.7.3_upgrade.sql` for any link (above) you already created on your local version to avoid duplicated records and database table constraints errors.

### *v273 # 3* - Cheek Swab sample type changed to URT or Cheek Swab sample type
    
**DETAILS**

Changed `Cheek Swab` to `URT (upper respiratory tract) or Cheek Swab` sample type with a custom drop down list field `URT or Cheek Swab Sites` to specify the collection site.

**TODO**

If your local installation tracks `Cheek Swab` samples, then validate these samples have been correctly migrated to `URT or Cheek Swab` with a field `sd_spe_urt_cheek_swabs.collection_site` equal to `Cheek`.

Note that the table `sd_spe_cheek_swabs` has been renamed to `sd_spe_urt_cheek_swabs`.

If any custom code and hooks (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories) contain specific code lines linked to the `Cheek Swab` sample type, please correct your local custom code to be consistent with this change.

### *v273 # 4* - Non-neoplastic diagnosis feature
 
**DETAILS**

Added new diagnosis category `nonneoplastic` to allow the creation of non-neoplastic diagnosis type. 
* No `child` diagnosis can be created for this category of diagnosis as it could be for a primary diagnosis (creation of progressions, metastasis, etc).
* This category will also impact the labels displayed to identify a diagnosis into the menus, the headers and the views (remove/add of the tag 'New Primary', remove/add of the category when displaying the type of a diagnosis, etc).

**TODO**

If some diagnosis types in your local installation could be considered as `nonneoplastic` and custom code (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories) contains specific code to hide diagnosis category into menus, forms, etc or customize ATiM to support these types of diagnosis then review custom code and remove any unusefull code plus update `diagnosis_controls.category` to `nonneoplastic` for the diagnosis matching this definition. 

If some custom code in `app/Plugin/ClinicalAnnotation/View/DiagnosisMasters/Hook` have been developed for the `DiagnosisMasters.listall()` and `DiagnosisMasters.detail()` actions to manage data attached the `add` button, please review custom code because labels attached to the add buttons have been modified.

### *v273 # 5* - New rule on SOP creation to make sop code plus version unique (instead code only)
 
**DETAILS**

Changed `isUnique` constraints on fields `SopMaster.code` and `SopMaster.version` plus added validation rules in `SopMaster` model function `validates` to make this combination unique.

**TODO**

If your local installation tracks SOP then:
* Validate the creation and update of SOPs.
* Change code by adding custom codes, hooks, or removing `structure_validations` rules if you want to remove this new constraint.
* Validate that any custom code or hooks developed on your local version to apply specific rules on SOP have not been impacted by this change.

### *v273 # 6* - Realiquoting controls data removed
 
**DETAILS** 

There is no control anymore on the types of aliquots that could be realiquoted.

The `realiquoting_controls` table is not used anymore for the definition of the aliquot types that could be created by realiquoting from a specific aliquot type. New code allows a user to realiquot any aliquot types of one sample to any aliquot types of the same sample.

The table `realiquoting_controls` and model `RealiquotingControl` have not been removed to only keep `LabBook` features functional.

**TODO**

Check no custom functions or custom hook files contains code lines processing data linked to the `RealiquotingControl` model or recorded into the `realiquoting_controls` table.  

Search can be done using the search tool of the editor:
* Search on `/RealiquotingControl/`.      
* Search on regular expression `/realiquoting_controls/`.

### *v273 # 7* - Use of pagination for derivatives and sample's aliquots lists view
 
**DETAILS**

Use pagination to display:
* The list of derivative samples (`/InventoryManagement/SampleMasters/listAllDerivatives/%Collection.id%/%SampleMaster.id%`)
* The list of sample aliquots into the sample detail page (`/InventoryManagement/SampleMasters/detail/%Collection.id%/%SampleMaster.id%`)

**TODO**

Open both pages in ATiM to validate both lists are correctly displayed:
* Sample detail page with at least one aliquot.
* Derivative samples list.
	
Check there is no custom code (see below) processing data that will be displayed in these 2 sub-pages (lists / views). 

Here are the list of hook files that could be impacted:

* Derivative samples:

```
app/Plugin/InventoryManagement/Controller/Hook/SampleMasters_listAllDerivatives_format.php
app/Plugin/InventoryManagement/View/SampleMasters/Hook/listAllDerivatives_main.php
app/Plugin/InventoryManagement/View/SampleMasters/Hook/listAllDerivatives_specific_derivative_type.php
```

* Sample detail page:

```
app/Plugin/InventoryManagement/Controller/Hook/SampleMasters_detail_format.php
app/Plugin/InventoryManagement/Controller/Hook/SampleMasters_listAllSampleAliquots_format.php
app/Plugin/InventoryManagement/View/SampleMasters/Hook/detail.php
app/Plugin/InventoryManagement/View/SampleMasters/Hook/detail_aliquots.php
app/Plugin/InventoryManagement/View/SampleMasters/Hook/listAllSampleAliquots.php
```

### *v273 # 8* - New StructureField setting to disable copy/paste option on structure fields in addgrid or editgrid display modes
 
**DETAILS**

In the previous version, to disable the copy/paste option on structure fields, the model and field names have to be passed to the `Structures.build()` function adding this information to the `$option` argument as below. This customization required the creation of `hooks` files in views. 

```php
$options['settings']['paste_disabled_fields'] = array(
    'AliquotMaster.barcode',
    'AliquotMaster.aliquot_label'
);
```

With version v2.7.3 and more, the add of `class=pasteDisabled` to the setting of the field in the `structure_fields` table or of the `structure_formats` as below will have the same consequence with no hooks or custom files creation.

```sql
UPDATE structure_formats 
SET `flag_override_setting`='1', `setting`='class=pasteDisabled'
WHERE structure_id=(...) 
AND structure_field_id=(...);
```

**TODO**

If you disabled copy/paste option on specific structure fields of your local installation by creating hooks to set the information to the `$options` variable of the build function, you can either:
* Keep your local installation as is.
* Or remove these hooks file adding the `pasteDisabled` class to option of the new `structure_fields.setting` and then limit custom code you have to maintain in long terms.
	
### *v273 # 9* - Installation Mode
 
**DETAILS**

Added a core installation flag `installation_mode` displaying a message to user that maintenance on ATiM is in progress and blocking any access to ATiM.

**TODO**

Nothing to do.

### *v273 # 10* - Function StructureValueDomain.getDropDown($domainName) to return all translated values of a drop down list 

**DETAILS**

Created new function `StructureValueDomain.getDropDown($domainName)` to return all translated values of a drop down list whatever the type of list is (fix, custom, generated by function). Data are returned into an array with keys equal to the value in database and values equal to the translated values.

**TODO**

Simplify code replacing complex custom code querying database model attached to the structure field lists by the new function in any custom code (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories) that manages drop down list values for display as in a `Model.summary()` functions or participant history form.

* Example of code to replace - 1

```php
$this->StructurePermissibleValuesCustom = AppModel::getInstance("", "StructurePermissibleValuesCustom", true); 
$myList = $this->StructurePermissibleValuesCustom->getCustomDropdown(array($myCustomListName));
$myList = array_merge($myList['defined'], $myList['previously_defined']);
$myValueToDisplay = myList[$myDbValue];
```

* Example of code to replace - 2

```php
$structureValueDomainModel = new StructureValueDomain();
$conditions = array(
    'recursive' => 2,
    'conditions' => array(
        'StructureValueDomain.domain_name' => $myDomainName
    )
);
$myList = $structureValueDomainModel->find('first', $conditions);
if (isset($myList['StructurePermissibleValue'])) {
	$myFormatedList = array();
	foreach ($myList['StructurePermissibleValue'] as $newValue)
		$myFormatedList[$newValue['value']] = __($newValue['language_alias']);
	}
	$myValueToDisplay = $myFormatedList[$myDbValue];
}
```

### *v273 # 11* - Table field name datamart\_reports.limit\_access\_from\_datamart\_structure_function misspelled

**DETAILS**

The table field name `datamart_reports.limit_access_from_datamart_structure_function` has initialy been misspelled and corrected in the v2.7.3.

**TODO**

Check that no custom functions (reports) developed into the custom report controller (`class ReportsControllerCustom extends ReportsController {}`) contains the misspelled field name:
* Search on `/limit_access_from_datamart_structrue_function/`.
* Replace by `limit_access_from_datamart_structure_function`.

### *v273 # 12* - The isUnique StructureField validation with addgrid or editgrid display modes

**DETAILS**

A new function `AppModel.checkUniqueGridFields()` has been implemented to check the `isUnique` structure field constraint on data returned by a view when the view display mode is set to `addgrid`or `editgrid` (allowing user to create records in batch). This new function will check that a value entered in a structure field attached to a `isUnique` constraint cannot be entered more than once into the posted data array (gathering many records to save) else function will return the error message.

Note that the `AppModel.checkUniqueGridFields()` function does not validate data against existing data in the database. This validation process already exists in ATiM. This new function fixes an issue that existed in ATiM v2.7.2 and less.

An example of `AppModel.checkUniqueGridFields()` function use can be reviewed in `FamilyHistoriesController.add()`.

```php
$errors = array();
$lineCounter = 0;
foreach ($this->FamilyHistory->checkUniqueGridFields($this->request->data) as $isUniqueField => $isUniqueMessages) {
    foreach ($isUniqueMessages as $isUniqueMsg) {
        $errors[$isUniqueField][$isUniqueMsg][''] = '';
    }
}
```

**TODO**

When custom code (see `app/Plugin/../../Hook` and `app/Plugin/../../Custom` directories) has been developed on a local installation to correct the issue that existed on v2.7.2 and less, then this custom code can be removed.

### *v273 # 13* - The $structureBottomLinks variable definition on the top of the source code of the ClinicalCollectionLink detail view

**DETAILS**

The `$structureBottomLinks` variable definition has been moved to the top of the source code of the `ClinicalCollectionLink` detail view (`/app/Plugin/ClinicalAnnotation/View/ClinicalCollectionLinks/detail.ctp`).

**TODO**

Check that custom code file exists on the local installation and contains the `$structureBottomLinks` variable then update custom code (if applicable) when the change can simplify and reduce the custom code. Check has to be done on following hook files:

```
/app/Plugin/ClinicalAnnotation/Controller/Hook/ClinicalCollectionLinks_detail_format.php
/app/Plugin/ClinicalAnnotation/View/ClinicalCollectionLinks/Hook/detail_*_detail.php
```

### *v273 # 14* - Batch Process: Processed Items Limit and data compression
    
**DETAILS**
        
Added code to the batch action views to compress the posted data and let user to enter more records in one action.

Added a new core variable `RealiquotedAliquotCreation_processed_items_per_parent_limit` to limit the number of records that could be created per parent in realiquoting process.

The limit of items that could be processed in batch have been tested on development server then reset (increased or decreased in v273) based on test results.
      
**TODO**

Review following core variables (see `app/Config/core.php`) and test following actions testing the maximal number of items user can process all together according to this core variables. Change values assigned to each variable to increase or decrease the value if applicable. The limit could be increased according to your server configuration and performances.

* Core variables:

```php
ParticipantMessageCreation_processed_participants_limit
SampleDerivativeCreation_processed_items_limit
AliquotCreation_processed_items_limit
AliquotModification_processed_items_limit
AliquotInternalUseCreation_processed_items_limit
RealiquotedAliquotCreation_processed_items_limit

AliquotBarcodePrint_processed_items_limit
QualityCtrlsCreation_processed_items_limit
AddToOrder_processed_items_limit
AddToShipment_processed_items_limit
defineOrderItemsReturned_processed_items_limit
editocessed_items_limit
TmaSlideCreation_processed_items_limit
```

* Actions:

```php
ParticipantMessage.add()
SampleMasters.batchDerivative()
AliquotMasters.add()
AliquotMasters.editInBatch()
AliquotMasters.addAliquotInternalUse()
AliquotMasters.defineRealiquotedChildren()
AliquotMasters.realiquot()
AliquotMasters.printBarcodes()
QualityCtrls.add()
OrderItems.add()
OrderItems.addOrderItemsInBatch()
OrderItems.defineOrderItemsReturned()
OrderItems.editInBatch()
Shipments.addToShipment()
TmaSlides.add()
TmaSlides.editInBatch()
TmaSlideUses.add()
TmaSlideUses.editInBatch()
```
### *v273 # 15* - Multi/Single user login to ATiM
    
**DETAILS**
        
Added a new core variable `multi_user_login` (see `app/Config/core.php`) to define if a same user can login to the same ATiM instance with different web browsers or computers.
      
**TODO**

Set the right value to the core variable `multi_user_login` to be compliant with the policies of your institution.

### *v273 # 16* - Automatic ATiM Customisation Feature: Updated AppController.newVersionSetUp() code to group and clean up code dedicated to the ATiM Automatic Customisation 

**DETAILS**

All code of the `AppController.newVersionSetUp()` function dedicated to the ATiM Automatic Customisation has been grouped, reformated and commented.

This code let ATiM to customise ATiM based on controls tables content, core variables and databrowser configuration:
* Activate/inactivate menus.
* Hide/display structure fields.
* Activate/inactivate databrowser models links (`datamart_browsing_controls`).

For more details about the executed customisation please review comments attached to the `AUTOMATIC CUSTOMISATION` tag in `AppController.newVersionSetUp()`.

All the ATiM Automatic Customisation feature can now be activated or not by changing core variable `letNewVersionSetupCustomisePartiallyATiM` value (see `app/Config/core.php`). 

Note that the `configureMenusInNewVersionSetup` is not used anymore.

**TODO**

* Review comments attached to the `AUTOMATIC CUSTOMISATION` tag in `AppController.newVersionSetUp()` to learn more about the process.
* Change the core variable `letNewVersionSetupCustomisePartiallyATiM` value according to your ATiM customisation strategy.
