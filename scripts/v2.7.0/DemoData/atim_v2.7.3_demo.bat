CLS
@ECHO off
COLOR F
ECHO ==================================================================
ECHO ==   ATiM DEMO DATA                                             ==
ECHO ==     Version 2.7.3                                            ==
ECHO ==================================================================
ECHO -
ECHO All your data will be erased and unrecoverable.
SET /p confirm= "Are you sure to run it? if yes type YES or anything else for cancel. YES/NO "

IF NOT "%confirm%" == "YES" (
	EXIT
)

set /p databaseName=Database Name? 

set /p databaseUserName=Database databaseUserName (root)? 
if "%databaseUserName%" == "" (
	SET databaseUserName=root
)
set /p password=Database password? 
if NOT "%password%" == "" (
	SET password=-p%password%
)

@ECHO off
REM ===================================================================================================================================
REM  Create backup of the database
REM  version: all
REM ===================================================================================================================================
REM mysqldump -u %databaseUserName% %password% %databaseName% > the_last_backup_file.sql
REM ===================================================================================================================================
REM  DROP DATABASE AND ReCreate it
REM  version: all
REM ===================================================================================================================================

@ECHO on
mysql -u %databaseUserName% %password% -e "DROP DATABASE IF EXISTS %databaseName%"
mysql -u %databaseUserName% %password% -e "CREATE DATABASE %databaseName%"
@ECHO off

REM ===================================================================================================================================
REM  ATiM TRUNK CREATION AND UPDATE SCRIPTS
REM  version: v2.7.2 demo data
REM ===================================================================================================================================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../atim_v2.7.0_full_installation.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../atim_v2.7.1_upgrade.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../atim_v2.7.2_upgrade.sql
@ECHO off

REM ===================================================================================================================================
REM  CUSTOM FORM LIBRARIES : CLINICAL ANNOTATION
REM  version: v2.7.2 demo data
REM ===================================================================================================================================

REM  Profile & Misc Identifier
REM ==========================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_add_participant_identifier_csv_browsing_option.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_add_last_contact_field.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_display_fields_in_search_index_views.sql

mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_misc_identifier_hide_dates_notes_fields.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_misc_identifier_create_ctrnet_demo_identifiers.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_misc_identifier_create_ctrnet_demo_study_identifier.sql

@ECHO off

REM  Consent
REM ========

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_hide_consent_national_fields.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_create_ctrnet_demo_study_consent.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_create_ctrnet_demo_bank_consent.sql
@ECHO off
REM mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_add_consent_file_upload_field.sql

REM  Diagnosis
REM ==========

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_tfri_cpcbn_primary_prostate.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_tfri_coeur_primary_ovary.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_qbcf_secondary_breast_progression.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_ctrnet_demo_primary_breast.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_primary_diagnosis.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_progression_cns_replase.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_progression_histological_transformation.sql
@ECHO off

REM  Treatment
REM ==========

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_tfri_cpcbn_biopsy_turp.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_tfri_cpcbn_prostatectomy.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_crchum_hpb_liver_and_pancreas_surgery.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_radiotherapy.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_hormonotherapy.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_systemic_treatment.sql

mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_add_ctrnet_trunk_surgery_site_notes_fields.sql
@ECHO off

REM  Clinical Event
REM ===============

REM CLinical

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_followup.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_imaging.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_imaging.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_medical_history.sql
@ECHO off
	
REM Lab

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_biology.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_liver_metastases_lab_report.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_biology.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_biomarker.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_onco_axis_genetic_test.sql
@ECHO off

REM  Contact
REM ========

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_participant_contact_display_fields_in_search_index_views.sql
@ECHO off

REM  Message
REM ========

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_participant_message_display_fields_in_search_index_views.sql
@ECHO off

REM  Chronology
REM ===========

@ECHO on
REM  mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_chronology_display_time_column.sql
@ECHO off

REM ===================================================================================================================================
REM  CUSTOM FORM LIBRARIES : INVENTORY MANAGEMENT
REM  version: v2.7.2 demo data
REM ===================================================================================================================================

REM  Collection
REM ===========

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_collection_add_participant_identifier_csv_browsing_option.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_collection_add_fields_to_template_init_form.sql
@ECHO off
	
REM  Sample
REM =======
	
@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_sample_add_headings_to_view.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_sample_add_participant_identifier_csv_browsing_option.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_sample_display_fields_in_search_index_views.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_sample_link_tissue_source_field_to_icdo3_topo_categories.sql
@ECHO off
	
REM  Aliquot
REM ========
	
@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_aliquot_add_barcode_participant_identifier_csv_browsing_options.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_aliquot_add_headings_to_view.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_aliquot_add_tissue_tube_storage_solution_method_fields.sql
@ECHO off

REM ===================================================================================================================================
REM  CUSTOM FORM LIBRARIES : TOOLS
REM  version: v2.7.2 demo data
REM ===================================================================================================================================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/tool_drug_create_tfri_projects_drugs.sql

mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/tool_study_update_to_ctrnet_demo_study_forms.sql

mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/datamart_databrowser_activate_ctrnet_demo_links.sql
@ECHO off

REM ===================================================================================================================================
REM  CUSTOM FORM LIBRARIES : QUERY TOOL
REM  version: v2.7.2 demo data
REM ===================================================================================================================================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/datamart_report_add_ctrnet_demo_identifiers_to_identifiers_report.sql
@ECHO off

REM ===================================================================================================================================
REM  DEMO DATA
REM  version: v2.7.2 demo data
REM ===================================================================================================================================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ./atim_v2.7.2_demo_data.sql
@ECHO off

REM ===================================================================================================================================
REM  mCode () fields
REM  version: v2.7.2 demo data
REM ===================================================================================================================================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/coding_icd_mcode_codes.sql

mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_add_mcode_patient_fields.sql

mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_mcode_diseases.sql

mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_comorbid_condition.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_performance_status.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_outcome.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_tumor_marker_test.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_vital_sign.sql

mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_mcode_radiation.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_mcode_surgery.sql
@ECHO off

REM ===================================================================================================================================
REM  ATiM TRUNK CREATION AND UPDATE SCRIPTS
REM  version: v2.7.3 demo data
REM ===================================================================================================================================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../atim_v2.7.3_upgrade.sql
@ECHO off

REM ===================================================================================================================================
REM  CUSTOM FORM LIBRARIES : CLINICAL ANNOTATION
REM  version: v2.7.3 demo data
REM ===================================================================================================================================

REM  Profile & Misc Identifier
REM ==========================

REM  Consent
REM ========

REM  Diagnosis
REM ==========

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_ctrnet_demo_common_cardiovascular_disease.sql
@ECHO off

REM  Treatment
REM ==========

REM  Clinical Event
REM ===============

REM CLinical

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_treatment_history.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_isaric_covid19.sql
@ECHO off

REM Lab

REM TODO
REM Add Scores from HPB

REM  Contact
REM ========

REM  Message
REM ========

REM  Chronology
REM ===========

REM ===================================================================================================================================
REM  CUSTOM FORM LIBRARIES : INVENTORY MANAGEMENT
REM  version: v2.7.3 demo data
REM ===================================================================================================================================

REM  Collection
REM ===========

REM  Sample
REM =======
	
REM  Aliquot
REM ========
	
@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_aliquot_disable_barcode_copy_past.sql
@ECHO off

REM ===================================================================================================================================
REM  DEMO DATA
REM  version: v2.7.3 demo data
REM ===================================================================================================================================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ./atim_v2.7.3_demo_data_large_datasets.sql
@ECHO off

REM ==================================================================
REM  ATiM USERS UPDATE
REM  version: all
REM ==================================================================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE users SET flag_active=0, deleted = 1, username=CONCAT('user#', id),first_name=CONCAT('user#', id), last_name=CONCAT('user#', id), password='2939a463baec8a8b1f3cef69238e6bfea3795a5c', email = '' WHERE username != 'administrator'"
mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE users SET flag_active=1, deleted = 0, force_password_reset = 0, first_name='administrator', last_name='administrator', email = ''  WHERE username = 'administrator'"

mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE groups SET flag_show_confidential = '0'  WHERE name != 'Administrators'"
mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE groups SET flag_show_confidential = '1'  WHERE name = 'Administrators'"
@ECHO off

REM ===================================================================================================================================
REM ===================================================================================================================================


COLOR F
ECHO -
ECHO ==================================================================
ECHO ==   ATiM DEMO SET UP - DONE                                    ==
ECHO ==================================================================
ECHO -

ECHO Use the username 'administrator' and the password 'administrator' to log into ATiM.
ECHO -
ECHO To force password reset for the username 'administrator', execute following query:
ECHO "UPDATE users SET force_password_reset = 1 WHERE username = 'administrator'"
ECHO -
ECHO To change the username 'administrator', execute following query:
ECHO "UPDATE users SET username = 'my_new_username' WHERE username = 'administrator'"
ECHO -

ECHO -

PAUSE

@ECHO on
REM  MysqlDump Cmd to create the demo data satetments
REM -------------------------------------------------

REM mysqldump -u %databaseUserName% %password% %databaseName% participants participants_revs misc_identifiers misc_identifiers_revs > participant_and_identifier.dump.sql
REM mysqldump -u %databaseUserName% %password% %databaseName% consent_masters consent_masters_revs cd_nationals cd_nationals_revs ctrnet_demo_cd_banks ctrnet_demo_cd_banks_revs > consent.dump.sql
REM mysqldump -u %databaseUserName% %password% %databaseName% diagnosis_masters diagnosis_masters_revs dxd_bloods dxd_bloods_revs dxd_tissues dxd_tissues_revs dxd_primaries dxd_primaries_revs dxd_secondaries dxd_secondaries_revs dxd_remissions dxd_remissions_revs dxd_progressions dxd_progressions_revs dxd_recurrences dxd_recurrences_revs tfri_cpcbn_dxd_prostates tfri_cpcbn_dxd_prostates_revs tfri_coeur_dxd_ovaries tfri_coeur_dxd_ovaries_revs qbcf_dxd_breast_progressions qbcf_dxd_breast_progressions_revs ctrnet_demo_dxd_breasts ctrnet_demo_dxd_breasts_revs > diagnosis.dump.sql 
REM mysqldump -u %databaseUserName% %password% %databaseName% treatment_masters treatment_masters_revs txd_chemos txd_chemos_revs txd_radiations txd_radiations_revs txd_surgeries txd_surgeries_revs tfri_cpcbn_txd_biopsy_turps tfri_cpcbn_txd_biopsy_turps_revs tfri_cpcbn_txd_rps tfri_cpcbn_txd_rps_revs crchum_hpb_txd_surgery_livers crchum_hpb_txd_surgery_livers_revs crchum_hpb_txd_surgery_pancreas crchum_hpb_txd_surgery_pancreas_revs ctrnet_demo_txd_hormono_therapies ctrnet_demo_txd_hormono_therapies_revs ctrnet_demo_txd_systemic_treatments ctrnet_demo_txd_systemic_treatments_revs treatment_extend_masters treatment_extend_masters_revs txe_chemos txe_chemos_revs txe_surgeries txe_surgeries_revs crchum_hpb_txe_surgery_complications crchum_hpb_txe_surgery_complications_revs ctrnet_demo_txe_radiations ctrnet_demo_txe_radiations_revs ctrnet_demo_txe_hormono_therapies ctrnet_demo_txe_hormono_therapies_revs ctrnet_demo_txe_systemic_treatments ctrnet_demo_txe_systemic_treatments_revs > treatment.dump.sql
REM mysqldump -u %databaseUserName% %password% %databaseName% event_masters event_masters_revs ed_all_comorbidities ed_all_comorbidities_revs ed_cap_report_16_colon_resections ed_cap_report_16_colon_resections_revs ed_breast_lab_pathologies ed_breast_lab_pathologies_revs ed_all_clinical_followups ed_all_clinical_followups_revs ed_all_clinical_presentations ed_all_clinical_presentations_revs ed_all_lifestyle_smokings ed_all_lifestyle_smokings_revs ed_breast_screening_mammograms ed_breast_screening_mammograms_revs ed_all_study_researches ed_all_study_researches_revs crchum_hpb_ed_imagings crchum_hpb_ed_imagings_revs ctrnet_demo_ed_imagings ctrnet_demo_ed_imagings_revs crchum_hpb_ed_medical_histories crchum_hpb_ed_medical_histories_revs crchum_hpb_ed_lab_biologies crchum_hpb_ed_lab_biologies_revs crchum_hpb_ed_liver_metastasis_lab_reports crchum_hpb_ed_liver_metastasis_lab_reports_revs ctrnet_demo_ed_lab_biologies ctrnet_demo_ed_lab_biologies_revs ctrnet_demo_ed_lab_biomarkers ctrnet_demo_ed_lab_biomarkers_revs crchum_onco_axis_ed_lab_genetic_tests crchum_onco_axis_ed_lab_genetic_tests_revs > event.dump.sql
REM mysqldump -u %databaseUserName% %password% %databaseName% participant_messages_revs participant_messages participant_contacts participant_contacts_revs family_histories family_histories_revs > participant_other_data.dump.sql

REM mysqldump -u %databaseUserName% %password% %databaseName% collections collections_revs > collections.dump.sql
REM mysqldump -u %databaseUserName% %password% %databaseName% sample_masters sample_masters_revs specimen_details specimen_details_revs derivative_details derivative_details_revs sd_der_pbmcs sd_der_pbmcs_revs sd_spe_bloods sd_spe_bloods_revs sd_spe_tissues sd_spe_tissues_revs sd_der_blood_cells sd_der_blood_cells_revs sd_der_plasmas sd_der_plasmas_revs sd_der_serums sd_der_serums_revs sd_der_dnas sd_der_dnas_revs > sample.dump.sql
REM mysqldump -u %databaseUserName% %password% %databaseName% aliquot_masters aliquot_masters_revs aliquot_internal_uses aliquot_internal_uses_revs source_aliquots source_aliquots_revs realiquotings realiquotings_revs ad_tubes ad_tubes_revs ad_blocks ad_blocks_revs ad_tissue_slides ad_tissue_slides_revs ad_tissue_cores ad_tissue_cores_revs > aliquot.dump.sql
REM mysqldump -u %databaseUserName% %password% %databaseName% quality_ctrls quality_ctrls_revs specimen_review_masters specimen_review_masters_revs spr_breast_cancer_types spr_breast_cancer_types_revs aliquot_review_masters aliquot_review_masters_revs ar_breast_tissue_slides ar_breast_tissue_slides_revs > qc_and_review.dump.sql

REM mysqldump -u %databaseUserName% %password% %databaseName% study_fundings study_fundings_revs study_investigators study_investigators_revs study_summaries study_summaries_revs > study.dump.sql
REM mysqldump -u %databaseUserName% %password% %databaseName% protocol_masters protocol_masters_revs pd_chemos pd_chemos_revs protocol_extend_masters protocol_extend_masters_revs pe_chemos pe_chemos_revs > protocol.dump.sql
REM mysqldump -u %databaseUserName% %password% %databaseName% storage_masters storage_masters_revs std_rooms std_rooms_revs std_cupboards std_cupboards_revs std_nitro_locates std_nitro_locates_revs std_freezers std_freezers_revs std_boxs std_boxs_revs std_racks std_racks_revs std_shelfs std_shelfs_revs std_tma_blocks std_tma_blocks_revs tma_slides tma_slides_revs tma_slide_uses tma_slide_uses_revs > storage_and_tma_block_slide.dump.sql
REM mysqldump -u %databaseUserName% %password% %databaseName% orders orders_revs order_items order_items_revs order_lines order_lines_revs shipments shipments_revs > order.dump.sql
REM mysqldump -u %databaseUserName% %password% %databaseName% sop_masters sop_masters_revs sopd_inventory_alls sopd_inventory_alls_revs > sop.dump.sql
REM mysqldump -u %databaseUserName% %password% %databaseName% collection_protocols collection_protocol_visits templates template_nodes > template.dump.sql
