# The Script file to load the demo data for the Linux users.

WHITE='\033[1;37m'
CYAN='\033[1;36m'
YELLOW='\033[1;33m'
RED='\033[0;31m'
clear
echo -e "${YELLOW}"

COLUMNS=$(tput cols) 
title="ATiM DEMO DATA (Version 2.7.3)" 
printf "%*s\n\n\n" $(((${#title}+$COLUMNS)/2)) "$title"

echo -e "${RED}"
printf "All your data will be erased and unrecoverable."
echo -e "${WHITE}"
printf "Are you sure to run it? if yes type YES or anything else for cancel. YES/NO "

read confirm

if [ "$confirm" != "YES" ];
then
	exit
fi

printf "Database Name? "
read databaseName

printf "Database username (root)? "
read databaseUserName
if [ "$databaseUserName" == "" ];
then
	databaseUserName="root"
fi

printf "Database Password? "
read password
if [ "$password" == "" ];
then
	password=""
else
	password="-p$password"
fi


echo -e "\n${CYAN}\t\tCreating the last database backup"
echo -e "${WHITE}mysqldump -u $databaseUserName $password $databaseName > the_last_backup_file.sql"
mysqldump -u $databaseUserName $password $databaseName > the_last_backup_file.sql

echo -e "\n${CYAN}\t\tDrop and create the old database"
echo -e "${WHITE}mysql -u $databaseUserName $password -e 'DROP DATABASE IF EXISTS $databaseName'"
mysql -u $databaseUserName $password -e "DROP DATABASE IF EXISTS $databaseName"
echo -e "${WHITE}mysql -u $databaseUserName $password -e 'CREATE DATABASE $databaseName'"
mysql -u $databaseUserName $password -e "CREATE DATABASE $databaseName"

# -------------------------------------------------------------------------------------------------------------------
# v272
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${CYAN}\t\tATiM TRUNK CREATION AND UPDATE SCRIPTS"
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../atim_v2.7.0_full_installation.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../atim_v2.7.0_full_installation.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../atim_v2.7.1_upgrade.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../atim_v2.7.1_upgrade.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../atim_v2.7.2_upgrade.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../atim_v2.7.2_upgrade.sql

echo -e "\n${CYAN}\t\tCUSTOM FORM LIBRARIES : CLINICAL ANNOTATION"
echo -e "\n${CYAN}\t\tProfile & Misc Identifier"

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_add_participant_identifier_csv_browsing_option.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_add_participant_identifier_csv_browsing_option.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_add_last_contact_field.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_add_last_contact_field.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_display_fields_in_search_index_views.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_display_fields_in_search_index_views.sql

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_misc_identifier_hide_dates_notes_fields.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_misc_identifier_hide_dates_notes_fields.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_misc_identifier_create_ctrnet_demo_identifiers.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_misc_identifier_create_ctrnet_demo_identifiers.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_misc_identifier_create_ctrnet_demo_study_identifier.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_misc_identifier_create_ctrnet_demo_study_identifier.sql

echo -e "\n${CYAN}\t\tConsent"
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_hide_consent_national_fields.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_hide_consent_national_fields.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_create_ctrnet_demo_study_consent.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_create_ctrnet_demo_study_consent.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_create_ctrnet_demo_bank_consent.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_create_ctrnet_demo_bank_consent.sql

echo -e "\n${CYAN}\t\tmysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_add_consent_file_upload_field.sql"

echo -e "\n${CYAN}\t\tDiagnosis"
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_tfri_cpcbn_primary_prostate.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_tfri_cpcbn_primary_prostate.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_tfri_coeur_primary_ovary.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_tfri_coeur_primary_ovary.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_qbcf_secondary_breast_progression.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_qbcf_secondary_breast_progression.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_ctrnet_demo_primary_breast.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_ctrnet_demo_primary_breast.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_primary_diagnosis.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_primary_diagnosis.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_progression_cns_replase.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_progression_cns_replase.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_progression_histological_transformation.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_progression_histological_transformation.sql

echo -e "\n${CYAN}\t\tTreatment"
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_tfri_cpcbn_biopsy_turp.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_tfri_cpcbn_biopsy_turp.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_tfri_cpcbn_prostatectomy.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_tfri_cpcbn_prostatectomy.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_crchum_hpb_liver_and_pancreas_surgery.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_crchum_hpb_liver_and_pancreas_surgery.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_radiotherapy.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_radiotherapy.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_hormonotherapy.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_hormonotherapy.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_systemic_treatment.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_systemic_treatment.sql

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_add_ctrnet_trunk_surgery_site_notes_fields.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_add_ctrnet_trunk_surgery_site_notes_fields.sql

echo -e "\n${CYAN}\t\tClinical Event"
echo -e "\n${CYAN}\t\tCLinical"
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_followup.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_followup.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_imaging.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_imaging.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_imaging.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_imaging.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_medical_history.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_medical_history.sql

echo -e "\n${CYAN}\t\tLab"
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_biology.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_biology.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_liver_metastases_lab_report.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_liver_metastases_lab_report.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_biology.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_biology.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_biomarker.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_biomarker.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_onco_axis_genetic_test.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_onco_axis_genetic_test.sql
	
# echo -e "\n${CYAN}\t\tTODO"
# echo -e "\n${CYAN}\t\tAdd Scores from HPB"

echo -e "\n${CYAN}\t\tContact"

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_participant_contact_display_fields_in_search_index_views.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_participant_contact_display_fields_in_search_index_views.sql

echo -e "\n${CYAN}\t\tMessage"

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_participant_message_display_fields_in_search_index_views.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_participant_message_display_fields_in_search_index_views.sql

echo -e "\n${CYAN}\t\tChronology"

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_chronology_display_time_column.sql"
# mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_chronology_display_time_column.sql

echo -e "\n${CYAN}\t\tCUSTOM FORM LIBRARIES : INVENTORY MANAGEMENT"
echo -e "\n${CYAN}\t\tCollection"

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_collection_add_participant_identifier_csv_browsing_option.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_collection_add_participant_identifier_csv_browsing_option.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_collection_add_fields_to_template_init_form.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_collection_add_fields_to_template_init_form.sql

	
echo -e "\n${CYAN}\t\tSample"
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_sample_add_headings_to_view.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_sample_add_headings_to_view.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_sample_add_participant_identifier_csv_browsing_option.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_sample_add_participant_identifier_csv_browsing_option.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_sample_display_fields_in_search_index_views.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_sample_display_fields_in_search_index_views.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_sample_link_tissue_source_field_to_icdo3_topo_categories.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_sample_link_tissue_source_field_to_icdo3_topo_categories.sql

	
echo -e "\n${CYAN}\t\tAliquot"
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_aliquot_add_barcode_participant_identifier_csv_browsing_options.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_aliquot_add_barcode_participant_identifier_csv_browsing_options.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_aliquot_add_headings_to_view.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_aliquot_add_headings_to_view.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_aliquot_add_tissue_tube_storage_solution_method_fields.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_aliquot_add_tissue_tube_storage_solution_method_fields.sql

echo -e "\n${CYAN}\t\tCUSTOM FORM LIBRARIES : TOOLS"
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/tool_drug_create_tfri_projects_drugs.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/tool_drug_create_tfri_projects_drugs.sql

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/tool_study_update_to_ctrnet_demo_study_forms.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/tool_study_update_to_ctrnet_demo_study_forms.sql

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/datamart_databrowser_activate_ctrnet_demo_links.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/datamart_databrowser_activate_ctrnet_demo_links.sql

echo -e "\n${CYAN}\t\tCUSTOM FORM LIBRARIES : QUERY TOOL"
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/datamart_report_add_ctrnet_demo_identifiers_to_identifiers_report.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/datamart_report_add_ctrnet_demo_identifiers_to_identifiers_report.sql

echo -e "\n${CYAN}\t\tDEMO DATA"

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ./atim_v2.7.2_demo_data.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ./atim_v2.7.2_demo_data.sql

echo -e "\n${CYAN}\t\tmCode () fields"

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/coding_icd_mcode_codes.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/coding_icd_mcode_codes.sql

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_add_mcode_patient_fields.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_add_mcode_patient_fields.sql

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_mcode_diseases.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_mcode_diseases.sql

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_comorbid_condition.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_comorbid_condition.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_performance_status.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_performance_status.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_outcome.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_outcome.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_tumor_marker_test.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_tumor_marker_test.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_vital_sign.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_vital_sign.sql

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_mcode_radiation.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_mcode_radiation.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_mcode_surgery.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_mcode_surgery.sql

# -------------------------------------------------------------------------------------------------------------------
# v273
# -------------------------------------------------------------------------------------------------------------------

echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../atim_v2.7.3_upgrade.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../atim_v2.7.3_upgrade.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_ctrnet_demo_common_cardiovascular_disease.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_ctrnet_demo_common_cardiovascular_disease.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_treatment_history.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_treatment_history.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_isaric_covid19.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_isaric_covid19.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_aliquot_disable_barcode_copy_past.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/inventory_management_aliquot_disable_barcode_copy_past.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ./atim_v2.7.3_demo_data_large_datasets.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ./atim_v2.7.3_demo_data_large_datasets.sql

# -------------------------------------------------------------------------------------------------------------------
# Users
# -------------------------------------------------------------------------------------------------------------------

# echo -e "\n${CYAN}\t\tATiM USERS UPDATE"

mysql -u $databaseUserName $password $databaseName -e "UPDATE users SET flag_active=0, deleted = 1, username=CONCAT('user#', id),first_name=CONCAT('user#', id), last_name=CONCAT('user#', id), password='2939a463baec8a8b1f3cef69238e6bfea3795a5c', email = '' WHERE username != 'administrator'"
mysql -u $databaseUserName $password $databaseName -e "UPDATE users SET flag_active=1, deleted = 0, force_password_reset = 0, first_name='administrator', last_name='administrator', email = ''  WHERE username = 'administrator'"

mysql -u $databaseUserName $password $databaseName -e "UPDATE groups SET flag_show_confidential = '0'  WHERE name != 'Administrators'"
mysql -u $databaseUserName $password $databaseName -e "UPDATE groups SET flag_show_confidential = '1'  WHERE name = 'Administrators'"

COLUMNS=$(tput cols) 
echo -e "${YELLOW}"
title="ATiM DEMO SET UP - DONE" 
printf "%*s\n\n\n" $(((${#title}+$COLUMNS)/2)) "$title"
echo -e "${WHITE}"