-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- --------------------------------------------------------------------------------
-- Add institution to study summary form.
-- ................................................................................
-- Fields 'Institution' with custom drop down list 'Institutions'.
-- --------------------------------------------------------------------------------

ALTER TABLE study_summaries 
  ADD COLUMN ctrnet_demo_institution VARCHAR(50) DEFAULT NULL;
ALTER TABLE study_summaries_revs 
  ADD COLUMN ctrnet_demo_institution VARCHAR(50) DEFAULT NULL;

INSERT INTO structure_value_domains (domain_name, source) 
VALUES 
('ctrnet_demo_institutions', "StructurePermissibleValuesCustom::getCustomDropdown('Institutions')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
('Institutions', 1, 50, 'study / project');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Study', 'StudySummary', 'study_summaries', 'ctrnet_demo_institution', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_institutions') , '0', '', '', '', 'laboratory / institution', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES 
((SELECT id FROM structures WHERE alias='studysummaries'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='ctrnet_demo_institution'), 
'1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('laboratory / institution', 'Laboratory/Institution','Laboratoire/Institution');

-- --------------------------------------------------------------------------------
-- Add ethical information to study summary form.
-- ................................................................................
-- Fields 'Approved' and 'File Name' to track name of the Ethical file.
-- ................................................................................
-- 
-- Could be replaced by a browsing button to load file on the server following 
-- instruction below :
-- 
-- In core.php
-- -----------
-- 
-- The directory that contains the uploaded files is located beside the 'APP' directory of ATiM 
-- and should be defined in the core file (see example below with directory ./app/atimUploadDirectory). 
-- However you can move the directory somewhere else changing following core variables.
-- 
-- Permission on directory has to be changed. To setup permissions on directory, 
-- please see Quick Install Guide (Step by Step).
-- 
--     /**
--      * Let user to download a file on the server using a ATiM field with setting set to 'File'.
--      *
--      * By default, the file will be downloaded into the \atimUploadDirectory. Path of the file
--      * could be changed by another one changing the value of the $uploadDirectory variable.
--      */
-- 
--    $uploadDirectory = substr(APP, 0, strlen(APP) - 4);
-- 
--    Configure::write('uploadDirectory', $uploadDirectory . 'atimUploadDirectory');
--    Configure::write('deleteDirectory', 'deleteDirectory');
--    Configure::write('deleteUploadedFilePhysically', false);
--    Configure::write('maxUploadFileSize', 10 * 1024 * 1024);
-- 
-- If you want to keep the files for ever set the deleteUploadedFilePhysically to false.
-- The directory that will contain the deleted and replaced files is defined by deleteDirectory.
-- The maximum size of each file for upload is 10MB by default. See maxUploadFileSize.
-- 
-- In PHP.ini
-- -----------
-- 
-- The PHP.ini should changed but it can be done also in core.php using following line code
--    upload_max_filesize = 10.1M
--    post_max_size = 16M
-- 
-- Field specification
-- In structure_fields table set the type of the field to 'file' and the setting 
-- to 'class=upload'.
-- --------------------------------------------------------------------------------

ALTER TABLE study_summaries 
  ADD COLUMN ctrnet_demo_ethical_approved char(1) DEFAULT '', 
  ADD COLUMN ctrnet_demo_ethical_approval_file_name varchar(500) DEFAULT null;
ALTER TABLE study_summaries_revs 
  ADD COLUMN ctrnet_demo_ethical_approved char(1) DEFAULT '', 
  ADD COLUMN ctrnet_demo_ethical_approval_file_name varchar(500) DEFAULT null;
  
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Study', 'StudySummary', 'study_summaries', 'ctrnet_demo_ethical_approved', 'yes_no',  NULL , '0', '', '', '', 'ethic', ''), 
('Study', 'StudySummary', 'study_summaries', 'ctrnet_demo_ethical_approval_file_name', 'input',  NULL , '0', 'size=50', '', '', '', 'file name');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES 
((SELECT id FROM structures WHERE alias='studysummaries'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='ctrnet_demo_ethical_approved' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='ethic' AND `language_tag`=''), 
'2', '1', 'approval', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='studysummaries'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='ctrnet_demo_ethical_approval_file_name' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=50' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='file name'), 
'2', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('approval', 'Approval', 'Approbation'),
('ethic', 'Ethic', 'éthique'),
('file name', 'File Name', 'Nom du fichier');

-- --------------------------------------------------------------------------------
-- Add MTA information to study summary form.
-- ................................................................................
-- Fields 'Approved' and 'File Name' to track name of the MTA file.
-- ................................................................................
--
-- Could be replaced by a browsing button to load file on the server following 
-- instruction below :
-- 
-- In core.php
-- -----------
-- 
-- The directory that contains the uploaded files is located beside the 'APP' directory of ATiM 
-- and should be defined in the core file (see example below with directory ./app/atimUploadDirectory). 
-- However you can move the directory somewhere else changing following core variables.
-- 
-- Permission on directory has to be changed. To setup permissions on directory, 
-- please see Quick Install Guide (Step by Step).
-- 
--     /**
--      * Let user to download a file on the server using a ATiM field with setting set to 'File'.
--      *
--      * By default, the file will be downloaded into the \atimUploadDirectory. Path of the file
--      * could be changed by another one changing the value of the $uploadDirectory variable.
--      */
-- 
--    $uploadDirectory = substr(APP, 0, strlen(APP) - 4);
-- 
--    Configure::write('uploadDirectory', $uploadDirectory . 'atimUploadDirectory');
--    Configure::write('deleteDirectory', 'deleteDirectory');
--    Configure::write('deleteUploadedFilePhysically', false);
--    Configure::write('maxUploadFileSize', 10 * 1024 * 1024);
-- 
-- If you want to keep the files for ever set the deleteUploadedFilePhysically to false.
-- The directory that will contain the deleted and replaced files is defined by deleteDirectory.
-- The maximum size of each file for upload is 10MB by default. See maxUploadFileSize.
-- 
-- In PHP.ini
-- -----------
-- 
-- The PHP.ini should changed but it can be done also in core.php using following line code
--    upload_max_filesize = 10.1M
--    post_max_size = 16M
-- 
-- Field specification
-- In structure_fields table set the type of the field to 'file' and the setting 
-- to 'class=upload'.
-- --------------------------------------------------------------------------------

ALTER TABLE study_summaries 
  ADD COLUMN ctrnet_demo_mta_data_sharing_approved char(1) DEFAULT '', 
  ADD COLUMN ctrnet_demo_mta_data_sharing_approved_file_name varchar(500) DEFAULT null;
ALTER TABLE study_summaries_revs 
  ADD COLUMN ctrnet_demo_mta_data_sharing_approved char(1) DEFAULT '', 
  ADD COLUMN ctrnet_demo_mta_data_sharing_approved_file_name varchar(500) DEFAULT null; 
   
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Study', 'StudySummary', 'study_summaries', 'ctrnet_demo_mta_data_sharing_approved', 'yes_no',  NULL , '0', '', '', '', 'mta data sharing', ''), 
('Study', 'StudySummary', 'study_summaries', 'ctrnet_demo_mta_data_sharing_approved_file_name', 'input',  NULL , '0', 'size=50', '', '', '', 'file name');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES 
((SELECT id FROM structures WHERE alias='studysummaries'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='ctrnet_demo_mta_data_sharing_approved' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='mta data sharing' AND `language_tag`=''), 
'2', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='studysummaries'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='ctrnet_demo_mta_data_sharing_approved_file_name' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=50' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='file name'), 
'2', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('file name', 'File Name', 'Nom du fichier'),
('mta data sharing', 'MTA Data Sharing', 'Partage de matériels et de données');

-- --------------------------------------------------------------------------------
-- Add PubmedIds to track Pubmed references.
-- ................................................................................
-- Fields 'PubmedIds'.
-- --------------------------------------------------------------------------------

ALTER TABLE study_summaries 
  ADD COLUMN ctrnet_demo_pubmed_ids TEXT DEFAULT NULL;
ALTER TABLE study_summaries_revs 
  ADD COLUMN ctrnet_demo_pubmed_ids TEXT DEFAULT NULL;  

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Study', 'StudySummary', 'study_summaries', 'ctrnet_demo_pubmed_ids', 'textarea',  NULL , '0', 'cols=40,rows=1', '', '', 'pubmed ids', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES 
((SELECT id FROM structures WHERE alias='studysummaries'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='ctrnet_demo_pubmed_ids' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='cols=40,rows=1' AND `default`='' AND `language_help`='' AND `language_label`='pubmed ids' AND `language_tag`=''), 
'2', '20', 'literature', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('literature','Literature','Littérature'),
('pubmed ids','PubMed IDs','PubMed IDs');

-- --------------------------------------------------------------------------------
-- Change position of the 'Summary' field.
-- ................................................................................
-- N/A.
-- --------------------------------------------------------------------------------

UPDATE structure_formats 
SET `display_order`='30', `language_heading`='details' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='studysummaries') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- --------------------------------------------------------------------------------
-- Hide field 'Summary' Disease Site'.
-- ................................................................................
-- N/A.
-- --------------------------------------------------------------------------------

UPDATE structure_formats 
SET `flag_add`='0', `flag_edit`='0', `flag_search`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='studysummaries') 
AND structure_field_id IN (
  SELECT id 
  FROM structure_fields 
  WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='disease_site'
);  

-- --------------------------------------------------------------------------------
-- Redesign the Study Investigator sub-form.
-- ................................................................................
-- N/A.
-- --------------------------------------------------------------------------------

UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='middle_name' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='occupation' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='department' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='0', `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='organization' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='participation_start_date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='participation_end_date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='1', `display_order`='-1' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='role' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='role') AND `flag_confidential`='0');
UPDATE structure_fields SET  `language_label`='study_city',  `language_tag`='' WHERE model='StudyInvestigator' AND tablename='study_investigators' AND field='address_city' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_label`='study_province',  `language_tag`='' WHERE model='StudyInvestigator' AND tablename='study_investigators' AND field='address_province' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_label`='study_country',  `language_tag`='' WHERE model='StudyInvestigator' AND tablename='study_investigators' AND field='address_country' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_label`='study_street',  `language_tag`='' WHERE model='StudyInvestigator' AND tablename='study_investigators' AND field='address_street' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `display_column`='1', `display_order`='50' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='brief' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `language_heading`='study_address' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='address_street' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='organization' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='email' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyinvestigators') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyInvestigator' AND `tablename`='study_investigators' AND `field`='role' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='role') AND `flag_confidential`='0');

-- --------------------------------------------------------------------------------
-- Redesign the Study Funding sub-form.
-- ................................................................................
-- N/A.
-- --------------------------------------------------------------------------------

UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_addgrid`='0', `flag_editgrid`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='restrictions' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='amount_year_1' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='year_1' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='amount_year_2' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='year_2' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='amount_year_3' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='year_3' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='amount_year_4' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='year_4' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='amount_year_5' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='studyfundings') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudyFunding' AND `tablename`='study_fundings' AND `field`='year_5' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;