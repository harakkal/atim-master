-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CRCHUM Hepatopancreatobiliary Bank : Liver Surgery
-- ................................................................................
-- Form developped to capture data for the CRCHUM Hepatopancreatobiliary BioBank.
-- Designed and created by the BioBank PIs.
-- --------------------------------------------------------------------------------

-- Treatment Extend : Complication
-- -------------------------------

INSERT INTO treatment_extend_controls (id, detail_tablename, detail_form_alias, flag_active, `type`, databrowser_label) 
VALUES
(null, 'crchum_hpb_txe_surgery_complications', 'crchum_hpb_txe_surgery_complications', 1, 'crchum hpb - surgery complications', 'crchum hpb - surgery complications');

DROP TABLE IF EXISTS crchum_hpb_txe_surgery_complications;
CREATE TABLE IF NOT EXISTS crchum_hpb_txe_surgery_complications (
  type varchar(250) DEFAULT NULL,
  other_type_from_icd10_who varchar(50) DEFAULT NULL,
  date date DEFAULT NULL,
  date_accuracy char(1) DEFAULT '',
  notes text,
  treatment_extend_master_id int(11) NOT NULL,
  KEY FK_crchum_hpb_txe_surgery_complications_treatment_extend_masters (treatment_extend_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS crchum_hpb_txe_surgery_complications_revs;
CREATE TABLE IF NOT EXISTS crchum_hpb_txe_surgery_complications_revs (
  type varchar(250) DEFAULT NULL,
  other_type_from_icd10_who varchar(50) DEFAULT NULL,
  date date DEFAULT NULL,
  date_accuracy char(1) DEFAULT '',
  notes text,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  treatment_extend_master_id int(11) NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE crchum_hpb_txe_surgery_complications
  ADD CONSTRAINT FK_crchum_hpb_txe_surgery_complications_treatment_extend_masters FOREIGN KEY (treatment_extend_master_id) REFERENCES treatment_extend_masters (id);

INSERT INTO structures(`alias`) VALUES ('crchum_hpb_txe_surgery_complications');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'TreatmentExtendDetail', 'crchum_hpb_txe_surgery_complications', 'type', 'input',  NULL , '0', '', '', '', 'surgery complication type', ''), 
('ClinicalAnnotation', 'TreatmentExtendDetail', 'crchum_hpb_txe_surgery_complications', 'other_type_from_icd10_who', 'autocomplete',  NULL , '0', 'size=10,url=/CodingIcd/CodingIcd10s/autocomplete/ca,tool=/CodingIcd/CodingIcd10s/tool/ca', '', '', 'other type precision', ''), 
('ClinicalAnnotation', 'TreatmentExtendDetail', 'crchum_hpb_txe_surgery_complications', 'date', 'date',  NULL , '0', '', '', '', 'date', ''), 
('ClinicalAnnotation', 'TreatmentExtendDetail', 'crchum_hpb_txe_surgery_complications', 'notes', 'input',  NULL , '0', 'rows=1,cols=30', '', '', 'notes', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchum_hpb_txe_surgery_complications'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='crchum_hpb_txe_surgery_complications' AND `field`='type' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='surgery complication type' AND `language_tag`=''), 
'1', '0', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '0', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txe_surgery_complications'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='crchum_hpb_txe_surgery_complications' AND `field`='other_type_from_icd10_who' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10,url=/CodingIcd/CodingIcd10s/autocomplete/ca,tool=/CodingIcd/CodingIcd10s/tool/ca' AND `default`='' AND `language_help`='' AND `language_label`='other type precision' AND `language_tag`=''), 
'1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '0', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txe_surgery_complications'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='crchum_hpb_txe_surgery_complications' AND `field`='date' AND `type`='date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='date' AND `language_tag`=''), 
'1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '0', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txe_surgery_complications'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='crchum_hpb_txe_surgery_complications' AND `field`='notes' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='rows=1,cols=30' AND `default`='' AND `language_help`='' AND `language_label`='notes' AND `language_tag`=''), 
'1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0');

INSERT INTO structure_validations(structure_field_id, rule, language_message) VALUES
((SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='crchum_hpb_txe_surgery_complications' AND `field`='type' AND `type`='input'), 'notBlank', ''),
((SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='crchum_hpb_txe_surgery_complications' AND `field`='other_type_from_icd10_who' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10,url=/CodingIcd/CodingIcd10s/autocomplete/ca,tool=/CodingIcd/CodingIcd10s/tool/ca' AND `default`='' AND `language_help`='' AND `language_label`='other type precision' AND `language_tag`=''), 'validateIcd10CaCode', 'invalid disease code');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("crchum hpb - surgery complications", "Surgery Complications (CRCHUM Hpb)", "Complication Chirurgie (CRCHUM Hpb)"); 

-- Treatment : Surgery
-- -------------------

INSERT INTO treatment_controls (id, tx_method, disease_site, flag_active, detail_tablename, detail_form_alias, display_order, applied_protocol_control_id, extended_data_import_process, databrowser_label, flag_use_for_ccl, treatment_extend_control_id, use_addgrid, use_detail_form_for_index) 
VALUES
(null, 'crchum hpb - liver surgery', 'liver', 1, 'crchum_hpb_txd_surgery_livers', 'crchum_hpb_txd_surgery_livers', 0, NULL, NULL, 'liver|crchum hpb - liver surgery', 
1, (SELECT id FROM treatment_extend_controls WHERE detail_tablename = 'crchum_hpb_txe_surgery_complications' AND detail_form_alias = 'crchum_hpb_txe_surgery_complications' AND type = 'crchum hpb - surgery complications'), 0, 0);

DROP TABLE IF EXISTS crchum_hpb_txd_surgery_livers;
CREATE TABLE IF NOT EXISTS crchum_hpb_txd_surgery_livers (
  treatment_master_id int(11) NOT NULL,
  principal_surgery varchar(50) NOT NULL DEFAULT '',
  associated_surgery_1 varchar(50) NOT NULL DEFAULT '',
  associated_surgery_2 varchar(50) NOT NULL DEFAULT '',
  associated_surgery_3 varchar(50) NOT NULL DEFAULT '',
  other_organ_resection_1 varchar(50) NOT NULL DEFAULT '',
  other_organ_resection_2 varchar(50) NOT NULL DEFAULT '',
  other_organ_resection_3 varchar(50) NOT NULL DEFAULT '',
  surgeon varchar(50) NOT NULL DEFAULT '',
  operative_time smallint(5) UNSIGNED DEFAULT NULL,
  laparoscopy char(1) DEFAULT '',
  phlebotomy char(1) DEFAULT '',
  phlebotomy_volume_ml decimal(6,2) DEFAULT NULL,
  operative_bleeding smallint(5) UNSIGNED DEFAULT NULL,
  cell_saver char(1) DEFAULT '',
  cell_saver_volume_ml decimal(6,2) DEFAULT NULL,
  transfusions char(1) DEFAULT '',
  rbc_units smallint(5) UNSIGNED DEFAULT NULL,
  plasma_units smallint(5) UNSIGNED DEFAULT NULL,
  platelets_units smallint(5) UNSIGNED DEFAULT NULL,
  drainage char(1) DEFAULT '',
  liver_appearance varchar(50) NOT NULL DEFAULT '',
  vascular_occlusion char(1) DEFAULT '',
  resected_liver_weight smallint(5) UNSIGNED DEFAULT NULL,
  liver_size_x int(3) DEFAULT NULL,
  liver_size_y int(3) DEFAULT NULL,
  liver_size_z int(3) DEFAULT NULL,
  segment_1_resection varchar(10) NOT NULL DEFAULT '',
  segment_2_resection varchar(10) NOT NULL DEFAULT '',
  segment_3_resection varchar(10) NOT NULL DEFAULT '',
  segment_4a_resection varchar(10) NOT NULL DEFAULT '',
  segment_4b_resection varchar(10) NOT NULL DEFAULT '',
  segment_5_resection varchar(10) NOT NULL DEFAULT '',
  segment_6_resection varchar(10) NOT NULL DEFAULT '',
  segment_7_resection varchar(10) NOT NULL DEFAULT '',
  segment_8_resection varchar(10) NOT NULL DEFAULT '',
  gastric_tube_duration_in_days int(4) DEFAULT NULL,
  KEY FK_crchum_hpb_txd_surgery_livers_treatment_masters (treatment_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS crchum_hpb_txd_surgery_livers_revs;
CREATE TABLE IF NOT EXISTS crchum_hpb_txd_surgery_livers_revs (
  treatment_master_id int(11) NOT NULL,
  principal_surgery varchar(50) NOT NULL DEFAULT '',
  associated_surgery_1 varchar(50) NOT NULL DEFAULT '',
  associated_surgery_2 varchar(50) NOT NULL DEFAULT '',
  associated_surgery_3 varchar(50) NOT NULL DEFAULT '',
  other_organ_resection_1 varchar(50) NOT NULL DEFAULT '',
  other_organ_resection_2 varchar(50) NOT NULL DEFAULT '',
  other_organ_resection_3 varchar(50) NOT NULL DEFAULT '',
  surgeon varchar(50) NOT NULL DEFAULT '',
  operative_time smallint(5) UNSIGNED DEFAULT NULL,
  laparoscopy char(1) DEFAULT '',
  phlebotomy char(1) DEFAULT '',
  phlebotomy_volume_ml decimal(6,2) DEFAULT NULL,
  operative_bleeding smallint(5) UNSIGNED DEFAULT NULL,
  cell_saver char(1) DEFAULT '',
  cell_saver_volume_ml decimal(6,2) DEFAULT NULL,
  transfusions char(1) DEFAULT '',
  rbc_units smallint(5) UNSIGNED DEFAULT NULL,
  plasma_units smallint(5) UNSIGNED DEFAULT NULL,
  platelets_units smallint(5) UNSIGNED DEFAULT NULL,
  drainage char(1) DEFAULT '',
  liver_appearance varchar(50) NOT NULL DEFAULT '',
  vascular_occlusion char(1) DEFAULT '',
  resected_liver_weight smallint(5) UNSIGNED DEFAULT NULL,
  liver_size_x int(3) DEFAULT NULL,
  liver_size_y int(3) DEFAULT NULL,
  liver_size_z int(3) DEFAULT NULL,
  segment_1_resection varchar(10) NOT NULL DEFAULT '',
  segment_2_resection varchar(10) NOT NULL DEFAULT '',
  segment_3_resection varchar(10) NOT NULL DEFAULT '',
  segment_4a_resection varchar(10) NOT NULL DEFAULT '',
  segment_4b_resection varchar(10) NOT NULL DEFAULT '',
  segment_5_resection varchar(10) NOT NULL DEFAULT '',
  segment_6_resection varchar(10) NOT NULL DEFAULT '',
  segment_7_resection varchar(10) NOT NULL DEFAULT '',
  segment_8_resection varchar(10) NOT NULL DEFAULT '',
  gastric_tube_duration_in_days int(4) DEFAULT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE crchum_hpb_txd_surgery_livers
  ADD CONSTRAINT FK_crchum_hpb_txd_surgery_livers_treatment_masters FOREIGN KEY (treatment_master_id) REFERENCES treatment_masters (id);

INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("totalsss", "total");
INSERT INTO structure_value_domains (domain_name, override, category, source) VALUES ("crchum_hpb_total_partial", "", "", NULL);
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("total", "total"),("partial", "partial");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="crchum_hpb_total_partial"), (SELECT id FROM structure_permissible_values WHERE value="total" AND language_alias="total"), "0", "1");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="crchum_hpb_total_partial"), (SELECT id FROM structure_permissible_values WHERE value="partial" AND language_alias="partial"), "0", "1");

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_principal_surgery', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : Principal surgery\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM HPB : Principal surgery', 1, 20, 'clinical - treatment');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : Principal surgery');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("alcoholization", "Alcoholization", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("beger procedure", "Beger Procedure", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("bile duct resection", "Bile Duct Resection", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("biliary drainage", "Biliary Drainage", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("bisegmentectomy", "Bisegmentectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("brain surgery", "Brain Surgery", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("central pancreatectomy", "Central Pancreatectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("cholangiography", "Cholangiographie", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("cholangioscopy", "Cholangioscopie", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("cholecystectomy", "Cholecystectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("cholecystostomy", "Cholecystostomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("choledocoduodenostomy", "Choledocoduodenostomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("choledocojejunostomy", "Choledocojejunostomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("choledocoscopy", "Choledocoscopy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("colon resection", "Colon Resection", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("cysto-duodenectomy", "Cysto-Duodenectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("cysto-gastrectomy", "Cysto-Gastrectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("cysto-jejunostomy", "Cysto-Jejunostomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("cytoreductive surg", "Cytoreductive Surg", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("distal pancreatectomy", "Distal Pancreatectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("distal pancreatectomy + splenectomy", "Distal Pancreatectomy + Splenectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("distal splenorenal shunt", "Distal Splenorenal Shunt", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("duodenectomy", "Duodenectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("enucleation", "Enucleation", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("frey procedure", "Frey Procedure", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("gastrectomy", "Gastrectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("gastrojejunostomy", "Gastrojejunostomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("gynecologic surgery", "Gynecologic Surgery", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("hepaticojejunostomy", "Hepaticojejunostomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("hepaticojejunostomy + gastrojejunostomy", "Hepaticojejunostomy + Gastrojejunostomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("hilar lymphadenectomy", "Hilar Lymphadenectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("ileum resection", "Ileum Resection", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("jejunostomy", "Jejunostomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("jejunum resection", "Jejunum Resection", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("laparotomy", "Laparotomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("left hemicolectomy", "Left Hemicolectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("left hepatectomy", "Left Hepatectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("left hepatectomy + segment i", "Left Hepatectomy + Segment I", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("left lateral sectionectomy", "Left Lateral Sectionectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("left medial sectionectomy", "Left Medial Sectionectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("left trisectionectomy", "Left Trisectionectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("lithotrypsy", "Lithotrypsy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("liver biopsy", "Liver Biopsy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("liver cyst fenestration", "Liver Cyst Fenestration", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("liver cyst resection", "Liver Cyst Resection", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("liver transplant", "Liver Transplant", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("lung surgery", "Lung Surgery", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("mesenteric artery resection", "Mesenteric Artery Resection", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("mesocaval shunt", "Mesocaval Shunt", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("microwaves ablation", "Microwaves Ablation", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("nephrectomy (left)", "Nephrectomy (Left)", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("nephrectomy (right)", "Nephrectomy (Right)", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("other", "Other", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("pancreatoscopy", "Pancreatoscopy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("partial nephrectomyl", "Partial Nephrectomyl", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("partial nephrectomyr", "Partial Nephrectomyr", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("peritoneal resection", "Peritoneal Resection", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("portacaval shunt", "Portacaval Shunt", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("portal vein resection", "Portal Vein Resection", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("radiofrequency ablation", "Radiofrequency Ablation", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("rectal resection", "Rectal Resection", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("right anterior sectionectomy", "Right Anterior Sectionectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("right hemicolectomy", "Right Hemicolectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("right hepatectomy", "Right Hepatectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("right hepatectomy + segment i", "Right Hepatectomy + Segment I", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("right posterior sectionectomy", "Right Posterior Sectionectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("right trisectionectomy", "Right Trisectionectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("segmentectomy", "Segmentectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("splenectomy", "Splenectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("splenectomy (partial)", "Splenectomy (Partial)", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("stomy closure", "Stomy Closure", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("sub-segmentectomy", "Sub-Segmentectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("sub-total pancreatectomy", "Sub-Total Pancreatectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("sugiura procedure", "Sugiura Procedure", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("total pancreatectomy", "Total Pancreatectomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("transduodenal sphincteroplasty", "Transduodenal Sphincteroplasty", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("transduodenal vater papilla resection", "Transduodenal Vater Papilla Resection", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("umbilical hernia rep", "Umbilical Hernia Rep", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("vena cava resection", "Vena Cava Resection", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("whipple", "Whipple", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("wirsungojejunostomy", "Wirsungojejunostomy", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_other_organ_resection', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : Other organ resection\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM HPB : Other organ resection', 1, 20, 'clinical - treatment');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : Other organ resection');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("abdominal wall", "Abdominal Wall", "Paroi Abdominale", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("adrenal", "Adrenal", "Surrénal", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("appendix", "Appendix", "Appendice", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("colon", "Colon", "Côlon", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("diaphragm", "Diaphragm", "Diaphragme", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("duodenum", "Duodenum", "Duodénum", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("ileum", "Ileum", "Iléon", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("jejunum", "Jejunum", "Jéjunum", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("kidney", "Kidney", "Rein", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("lymph node", "Lymph Node", "Ganglion Lymphatique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("ovary", "Ovary", "Ovaire", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("pancreas", "Pancreas", "Pancréas", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("rectum", "Rectum", "Rectum", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("stomach", "Stomach", "Estomac", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_surgery_liver_appearance', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRHCUM HPB : Liver appearance\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRHCUM HPB : Liver appearance', 1, 20, 'clinical - treatment');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRHCUM HPB : Liver appearance');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("cirrhosis", "", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("fibrosis", "", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("normal", "", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("peliosis", "peliosis", "péliose", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("sinusoidal obstruction", "", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("steatohepatitis", "", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("steatosis", "", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structures(`alias`) VALUES ('crchum_hpb_txd_surgery_livers');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'principal_surgery', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery') , '0', '', '', '', 'principal surgery', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'associated_surgery_1', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery') , '0', '', '', '', 'associated surgery 1', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'associated_surgery_2', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery') , '0', '', '', '', 'associated surgery 2', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'associated_surgery_3', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery') , '0', '', '', '', 'associated surgery 3', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'other_organ_resection_1', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_other_organ_resection') , '0', '', '', '', 'other organ resection 1', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'other_organ_resection_2', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_other_organ_resection') , '0', '', '', '', 'other organ resection 2', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'other_organ_resection_3', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_other_organ_resection') , '0', '', '', '', 'other organ resection 3', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'surgeon', 'input',  NULL , '0', '', '', '', 'surgeon', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'operative_time', 'integer_positive',  NULL , '0', '', '', '', 'operative time (mn)', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'laparoscopy', 'yes_no',  NULL , '0', '', '', '', 'laparoscopy', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'phlebotomy', 'yes_no',  NULL , '0', '', '', '', 'phlebotomy', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'phlebotomy_volume_ml', 'float_positive',  NULL , '0', '', '', '', '', 'vol (ml)'), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'operative_bleeding', 'integer_positive',  NULL , '0', '', '', '', 'operative bleeding', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'cell_saver', 'yes_no',  NULL , '0', '', '', '', 'cell_saver', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'cell_saver_volume_ml', 'float_positive',  NULL , '0', '', '', '', '', 'vol (ml)'), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'transfusions', 'yes_no',  NULL , '0', '', '', '', 'transfusions', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'rbc_units', 'integer_positive',  NULL , '0', '', '', '', 'rbc units', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'plasma_units', 'integer_positive',  NULL , '0', '', '', '', 'plasma units', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'platelets_units', 'integer_positive',  NULL , '0', '', '', '', 'platelets units', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'drainage', 'yes_no',  NULL , '0', '', '', '', 'drainage', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'liver_appearance', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_surgery_liver_appearance') , '0', '', '', '', 'liver appearance', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'vascular_occlusion', 'yes_no',  NULL , '0', '', '', '', 'vascular occlusion', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'resected_liver_weight', 'integer_positive',  NULL , '0', '', '', '', 'resected liver weight (gr)', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'liver_size_x', 'integer',  NULL , '0', 'size=3', '', '', 'liver size x', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'liver_size_y', 'integer',  NULL , '0', 'size=3', '', '', '', '-'), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'liver_size_z', 'integer',  NULL , '0', 'size=3', '', '', '', '-'), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'segment_1_resection', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial') , '0', '', '', '', 'segment 1 resection', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'segment_2_resection', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial') , '0', '', '', '', 'segment 2 resection', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'segment_3_resection', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial') , '0', '', '', '', 'segment 3 resection', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'segment_4a_resection', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial') , '0', '', '', '', 'segment 4a resection', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'segment_4b_resection', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial') , '0', '', '', '', 'segment 4b resection', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'segment_5_resection', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial') , '0', '', '', '', 'segment 5 resection', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'segment_6_resection', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial') , '0', '', '', '', 'segment 6 resection', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'segment_7_resection', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial') , '0', '', '', '', 'segment 7 resection', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'segment_8_resection', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial') , '0', '', '', '', 'segment 8 resection', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_livers', 'gastric_tube_duration_in_days', 'integer',  NULL , '0', 'size=5', '', '', 'gastric tube duration in days', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='principal_surgery' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='principal surgery' AND `language_tag`=''), 
'1', '12', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='associated_surgery_1' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='associated surgery 1' AND `language_tag`=''), 
'1', '13', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='associated_surgery_2' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='associated surgery 2' AND `language_tag`=''), 
'1', '14', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='associated_surgery_3' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='associated surgery 3' AND `language_tag`=''), 
'1', '15', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='other_organ_resection_1' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_other_organ_resection')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='other organ resection 1' AND `language_tag`=''), 
'1', '18', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='other_organ_resection_2' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_other_organ_resection')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='other organ resection 2' AND `language_tag`=''), 
'1', '19', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='other_organ_resection_3' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_other_organ_resection')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='other organ resection 3' AND `language_tag`=''), 
'1', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='surgeon' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='surgeon' AND `language_tag`=''), 
'1', '21', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='operative_time' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='operative time (mn)' AND `language_tag`=''), 
'1', '22', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='laparoscopy' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='laparoscopy' AND `language_tag`=''), 
'1', '23', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='phlebotomy' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='phlebotomy' AND `language_tag`=''), 
'1', '23', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='phlebotomy_volume_ml' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='vol (ml)'), 
'1', '23', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='operative_bleeding' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='operative bleeding' AND `language_tag`=''), 
'1', '24', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='cell_saver' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='cell_saver' AND `language_tag`=''), 
'1', '24', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='cell_saver_volume_ml' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='vol (ml)'), 
'1', '24', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='transfusions' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='transfusions' AND `language_tag`=''), 
'1', '25', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='rbc_units' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='rbc units' AND `language_tag`=''), 
'1', '26', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='plasma_units' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='plasma units' AND `language_tag`=''), 
'1', '27', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='platelets_units' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='platelets units' AND `language_tag`=''), 
'1', '28', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='drainage' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='drainage' AND `language_tag`=''), 
'1', '31', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='liver_appearance' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_surgery_liver_appearance')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='liver appearance' AND `language_tag`=''), 
'2', '11', 'specific liver surgery data', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='vascular_occlusion' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='vascular occlusion' AND `language_tag`=''), 
'2', '12', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='resected_liver_weight' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='resected liver weight (gr)' AND `language_tag`=''), 
'2', '14', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='liver_size_x' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='liver size x' AND `language_tag`=''), 
'2', '14', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='liver_size_y' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='-'), 
'2', '14', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='liver_size_z' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='-'), 
'2', '14', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='segment_1_resection' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='segment 1 resection' AND `language_tag`=''), 
'2', '15', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='segment_2_resection' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='segment 2 resection' AND `language_tag`=''), 
'2', '16', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='segment_3_resection' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='segment 3 resection' AND `language_tag`=''), 
'2', '17', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='segment_4a_resection' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='segment 4a resection' AND `language_tag`=''), 
'2', '18', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='segment_4b_resection' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='segment 4b resection' AND `language_tag`=''), 
'2', '19', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='segment_5_resection' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='segment 5 resection' AND `language_tag`=''), 
'2', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='segment_6_resection' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='segment 6 resection' AND `language_tag`=''), 
'2', '21', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='segment_7_resection' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='segment 7 resection' AND `language_tag`=''), 
'2', '22', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='segment_8_resection' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_total_partial')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='segment 8 resection' AND `language_tag`=''), 
'2', '23', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_livers' AND `field`='gastric_tube_duration_in_days' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='gastric tube duration in days' AND `language_tag`=''), 
'2', '33', 'gastric tube', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_livers'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentMaster' AND `tablename`='treatment_masters' AND `field`='notes' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='rows=3,cols=30' AND `default`='' AND `language_help`='help_notes' AND `language_label`='notes' AND `language_tag`=''), 
'2', '100', 'other', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

REPLACE INTO i18n (id,en,fr)
VALUES
("crchum hpb - liver surgery", "Liver Surgery (CRCHUM Hpb)", "Chirurgie du foie (CRCHUM Hpb)"), 
("-", "-", "-"),
("associated surgery 1", "1st Associated Surgery", "1er Chirurgie associée"),
("associated surgery 2", "2nd Associated Surgery", "2eme Chirurgie associée"),
("associated surgery 3", "3th Associated Surgery", "3eme Chirurgie associée"),
("cell_saver", "Cell Saver", ""),
("drainage", "Drainage", "Drainage"),
("gastric tube", "Gastric tube", "Tube gastrique"),
("gastric tube duration in days", "Gastric Tube Use (Days)", "Utilisation Sonde Gastrique (Jours)"),
("laparoscopy", "Laparoscopy", "Laparoscopie"),
("liver appearance", "Liver Appearance", "Apparence du foie"),
("liver size x", "Liver Size (cm x cm x cm)", "Taille foie (cm x cm x cm)"),
("notes", "Notes", "Notes"),
("operative bleeding", "Operative bleeding", "Saignement opératoire"),
("operative time (mn)", "Operative Time (min)", "Temps d'opération (min)"),
("other", "Other", "Autre"),
("other organ resection 1", "Other Organ Resection 1", "Autre résection d'organe 1"),
("other organ resection 2", "Other Organ Resection 2", "Autre résection d'organe 2"),
("other organ resection 3", "Other Organ Resection 3", "Autre résection d'organe 3"),
("phlebotomy", "Phlebotomy", "Phlebotomie"),
("plasma units", "Plasma Units", "Unités Plasma"),
("platelets units", "Platelets Units", "Unités Plaquettes"),
("principal surgery", "Principal surgery", "Chirurgie principale"),
("rbc units", "RBC Units", "Units RBC"),
("resected liver weight (gr)", "Resected Liver Weight (gr)", "Poids du foie (résection - gr)"),
("segment 1 resection", "Segment 1 Resection", "Résection segment 1"),
("segment 2 resection", "Segment 2 Resection", "Résection segment 2"),
("segment 3 resection", "Segment 3 Resection", "Résection segment 3"),
("segment 4a resection", "Segment 4a Resection", "Résection segment 4a"),
("segment 4b resection", "Segment 4b Resection", "Résection segment 4b"),
("segment 5 resection", "Segment 5 Resection", "Résection segment 5"),
("segment 6 resection", "Segment 6 Resection", "Résection segment 6"),
("segment 7 resection", "Segment 7 Resection", "Résection segment 7"),
("segment 8 resection", "Segment 8 Resection", "Résection segment 8"),
("specific liver surgery data", "Liver Surgery Data", "Données - chirurgie du foie"),
("surgeon", "Surgeon", "Chirurgien"),
("transfusions", "Transfusions", "Transfusions"),
("vascular occlusion", "Vascular occlusion", "Occlusion vasculaire"),
("surgery complication type", "Type", "Type"),    	
("other type precision", "Precision", "Précision"),    
("vol (ml)", "Vol (ml)", "Vol (ml)");
 
-- --------------------------------------------------------------------------------
-- CRCHUM Hepatopancreatobiliary Bank : Pancreas Surgery
-- ................................................................................
-- Form developped to capture data for the CRCHUM Hepatopancreatobiliary BioBank.
-- Designed and created by the BioBank PIs.
-- --------------------------------------------------------------------------------

INSERT INTO treatment_controls (id, tx_method, disease_site, flag_active, detail_tablename, detail_form_alias, display_order, applied_protocol_control_id, extended_data_import_process, databrowser_label, flag_use_for_ccl, treatment_extend_control_id, use_addgrid, use_detail_form_for_index) 
VALUES
(null, 'crchum hpb - pancreas surgery', 'pancreas', 1, 'crchum_hpb_txd_surgery_pancreas', 'crchum_hpb_txd_surgery_pancreas', 0, NULL, NULL, 'pancreas|crchum hpb - pancreas surgery', 
1, (SELECT id FROM treatment_extend_controls WHERE detail_tablename = 'crchum_hpb_txe_surgery_complications' AND detail_form_alias = 'crchum_hpb_txe_surgery_complications' AND type = 'crchum hpb - surgery complications'), 0, 0);

DROP TABLE IF EXISTS crchum_hpb_txd_surgery_pancreas;
CREATE TABLE IF NOT EXISTS crchum_hpb_txd_surgery_pancreas (
  treatment_master_id int(11) NOT NULL,
  principal_surgery varchar(50) NOT NULL DEFAULT '',
  associated_surgery_1 varchar(50) NOT NULL DEFAULT '',
  associated_surgery_2 varchar(50) NOT NULL DEFAULT '',
  associated_surgery_3 varchar(50) NOT NULL DEFAULT '',
  other_organ_resection_1 varchar(50) NOT NULL DEFAULT '',
  other_organ_resection_2 varchar(50) NOT NULL DEFAULT '',
  other_organ_resection_3 varchar(50) NOT NULL DEFAULT '',
  surgeon varchar(50) NOT NULL DEFAULT '',
  operative_time smallint(5) UNSIGNED DEFAULT NULL,
  laparoscopy char(1) DEFAULT '',
  phlebotomy char(1) DEFAULT '',
  phlebotomy_volume_ml decimal(6,2) DEFAULT NULL,
  operative_bleeding smallint(5) UNSIGNED DEFAULT NULL,
  cell_saver char(1) DEFAULT '',
  cell_saver_volume_ml decimal(6,2) DEFAULT NULL,
  transfusions char(1) DEFAULT '',
  rbc_units smallint(5) UNSIGNED DEFAULT NULL,
  plasma_units smallint(5) UNSIGNED DEFAULT NULL,
  platelets_units smallint(5) UNSIGNED DEFAULT NULL,
  drainage char(1) DEFAULT '',
  pancreas_appearance varchar(50) NOT NULL DEFAULT '',
  wirsung_diameter decimal(7,2) DEFAULT NULL,
  recoupe_pancreas varchar(3) NOT NULL DEFAULT '',
  portal_vein_resection varchar(3) NOT NULL DEFAULT '',
  arterial_resection varchar(3) NOT NULL DEFAULT '',
  pancreas_anastomosis varchar(3) NOT NULL DEFAULT '',
  pylori_preservation varchar(3) NOT NULL DEFAULT '',
  preoperative_sandostatin varchar(3) NOT NULL DEFAULT '',
  gastric_tube_duration_in_days int(4) DEFAULT NULL,
  KEY FK_crchum_hpb_txd_surgery_pancreas_treatment_masters (treatment_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS crchum_hpb_txd_surgery_pancreas_revs;
CREATE TABLE IF NOT EXISTS crchum_hpb_txd_surgery_pancreas_revs (
  treatment_master_id int(11) NOT NULL,
  principal_surgery varchar(50) NOT NULL DEFAULT '',
  associated_surgery_1 varchar(50) NOT NULL DEFAULT '',
  associated_surgery_2 varchar(50) NOT NULL DEFAULT '',
  associated_surgery_3 varchar(50) NOT NULL DEFAULT '',
  other_organ_resection_1 varchar(50) NOT NULL DEFAULT '',
  other_organ_resection_2 varchar(50) NOT NULL DEFAULT '',
  other_organ_resection_3 varchar(50) NOT NULL DEFAULT '',
  surgeon varchar(50) NOT NULL DEFAULT '',
  operative_time smallint(5) UNSIGNED DEFAULT NULL,
  laparoscopy char(1) DEFAULT '',
  phlebotomy char(1) DEFAULT '',
  phlebotomy_volume_ml decimal(6,2) DEFAULT NULL,
  operative_bleeding smallint(5) UNSIGNED DEFAULT NULL,
  cell_saver char(1) DEFAULT '',
  cell_saver_volume_ml decimal(6,2) DEFAULT NULL,
  transfusions char(1) DEFAULT '',
  rbc_units smallint(5) UNSIGNED DEFAULT NULL,
  plasma_units smallint(5) UNSIGNED DEFAULT NULL,
  platelets_units smallint(5) UNSIGNED DEFAULT NULL,
  drainage char(1) DEFAULT '',
  pancreas_appearance varchar(50) NOT NULL DEFAULT '',
  wirsung_diameter decimal(7,2) DEFAULT NULL,
  recoupe_pancreas varchar(3) NOT NULL DEFAULT '',
  portal_vein_resection varchar(3) NOT NULL DEFAULT '',
  arterial_resection varchar(3) NOT NULL DEFAULT '',
  pancreas_anastomosis varchar(3) NOT NULL DEFAULT '',
  pylori_preservation varchar(3) NOT NULL DEFAULT '',
  preoperative_sandostatin varchar(3) NOT NULL DEFAULT '',
  gastric_tube_duration_in_days int(4) DEFAULT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE crchum_hpb_txd_surgery_pancreas
  ADD CONSTRAINT FK_crchum_hpb_txd_surgery_pancreas_treatment_masters FOREIGN KEY (treatment_master_id) REFERENCES treatment_masters (id);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_tx_surgery_pancreas_appearance', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : Pancreas appearance\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM HPB : Pancreas appearance', 1, 20, 'clinical - treatment');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : Pancreas appearance');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("firm", "", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("soft", "", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structures(`alias`) VALUES ('crchum_hpb_txd_surgery_pancreas');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'principal_surgery', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery') , '0', '', '', '', 'principal surgery', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'associated_surgery_1', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery') , '0', '', '', '', 'associated surgery 1', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'associated_surgery_2', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery') , '0', '', '', '', 'associated surgery 2', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'associated_surgery_3', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery') , '0', '', '', '', 'associated surgery 3', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'other_organ_resection_1', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_other_organ_resection') , '0', '', '', '', 'other organ resection 1', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'other_organ_resection_2', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_other_organ_resection') , '0', '', '', '', 'other organ resection 2', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'other_organ_resection_3', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_other_organ_resection') , '0', '', '', '', 'other organ resection 3', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'surgeon', 'input',  NULL , '0', '', '', '', 'surgeon', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'operative_time', 'integer_positive',  NULL , '0', '', '', '', 'operative time (mn)', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'laparoscopy', 'yes_no',  NULL , '0', '', '', '', 'laparoscopy', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'phlebotomy', 'yes_no',  NULL , '0', '', '', '', 'phlebotomy', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'phlebotomy_volume_ml', 'float_positive',  NULL , '0', '', '', '', '', 'vol (ml)'), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'operative_bleeding', 'integer_positive',  NULL , '0', '', '', '', 'operative bleeding', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'cell_saver', 'yes_no',  NULL , '0', '', '', '', 'cell_saver', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'cell_saver_volume_ml', 'float_positive',  NULL , '0', '', '', '', '', 'vol (ml)'), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'transfusions', 'yes_no',  NULL , '0', '', '', '', 'transfusions', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'rbc_units', 'integer_positive',  NULL , '0', '', '', '', 'rbc units', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'plasma_units', 'integer_positive',  NULL , '0', '', '', '', 'plasma units', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'platelets_units', 'integer_positive',  NULL , '0', '', '', '', 'platelets units', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'drainage', 'yes_no',  NULL , '0', '', '', '', 'drainage', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'pancreas_appearance', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_tx_surgery_pancreas_appearance') , '0', '', '', '', 'pancreas appearance', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'wirsung_diameter', 'float_positive',  NULL , '0', '', '', '', 'wirsung diameter', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'recoupe_pancreas', 'yes_no',  NULL , '0', '', '', '', 'transection pancreas', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'portal_vein_resection', 'yes_no',  NULL , '0', '', '', '', 'portal vein resection', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'arterial_resection', 'yes_no',  NULL , '0', '', '', '', 'arterial resection', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'pancreas_anastomosis', 'yes_no',  NULL , '0', '', '', '', 'pancreas anastomosis', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'pylori_preservation', 'yes_no',  NULL , '0', '', '', '', 'pylori preservation', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'preoperative_sandostatin', 'yes_no',  NULL , '0', '', '', '', 'preoperative sandostatin', ''), 
('ClinicalAnnotation', 'TreatmentDetail', 'crchum_hpb_txd_surgery_pancreas', 'gastric_tube_duration_in_days', 'integer',  NULL , '0', 'size=5', '', '', 'gastric tube duration in days', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='principal_surgery' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='principal surgery' AND `language_tag`=''), 
'1', '12', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='associated_surgery_1' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='associated surgery 1' AND `language_tag`=''), 
'1', '13', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='associated_surgery_2' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='associated surgery 2' AND `language_tag`=''), 
'1', '14', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='associated_surgery_3' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_principal_surgery')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='associated surgery 3' AND `language_tag`=''), 
'1', '15', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='other_organ_resection_1' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_other_organ_resection')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='other organ resection 1' AND `language_tag`=''), 
'1', '18', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='other_organ_resection_2' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_other_organ_resection')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='other organ resection 2' AND `language_tag`=''), 
'1', '19', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='other_organ_resection_3' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_other_organ_resection')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='other organ resection 3' AND `language_tag`=''), 
'1', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='surgeon' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='surgeon' AND `language_tag`=''), 
'1', '21', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='operative_time' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='operative time (mn)' AND `language_tag`=''), 
'1', '22', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='laparoscopy' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='laparoscopy' AND `language_tag`=''), 
'1', '23', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='phlebotomy' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='phlebotomy' AND `language_tag`=''), 
'1', '23', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='phlebotomy_volume_ml' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='vol (ml)'), 
'1', '23', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='operative_bleeding' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='operative bleeding' AND `language_tag`=''), 
'1', '24', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='cell_saver' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='cell_saver' AND `language_tag`=''), 
'1', '24', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='cell_saver_volume_ml' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='vol (ml)'), 
'1', '24', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='transfusions' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='transfusions' AND `language_tag`=''), 
'1', '25', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='rbc_units' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='rbc units' AND `language_tag`=''), 
'1', '26', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='plasma_units' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='plasma units' AND `language_tag`=''), 
'1', '27', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='platelets_units' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='platelets units' AND `language_tag`=''), 
'1', '28', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='drainage' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='drainage' AND `language_tag`=''), 
'1', '31', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='pancreas_appearance' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_tx_surgery_pancreas_appearance')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='pancreas appearance' AND `language_tag`=''), 
'2', '11', 'specific pancreas surgery data', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='wirsung_diameter' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='wirsung diameter' AND `language_tag`=''), 
'2', '12', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='recoupe_pancreas' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='transection pancreas' AND `language_tag`=''), 
'2', '13', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='portal_vein_resection' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='portal vein resection' AND `language_tag`=''), 
'2', '14', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='arterial_resection' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='arterial resection' AND `language_tag`=''), 
'2', '14', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='pancreas_anastomosis' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='pancreas anastomosis' AND `language_tag`=''), 
'2', '15', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'),
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='pylori_preservation' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='pylori preservation' AND `language_tag`=''), 
'2', '17', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='preoperative_sandostatin' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='preoperative sandostatin' AND `language_tag`=''), 
'2', '18', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'),
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='crchum_hpb_txd_surgery_pancreas' AND `field`='gastric_tube_duration_in_days' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='gastric tube duration in days' AND `language_tag`=''), 
'2', '33', 'gastric tube', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_txd_surgery_pancreas'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentMaster' AND `tablename`='treatment_masters' AND `field`='notes' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='rows=3,cols=30' AND `default`='' AND `language_help`='help_notes' AND `language_label`='notes' AND `language_tag`=''), 
'2', '100', 'other', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("crchum hpb - pancreas surgery", "Pancreas Surgery (CRCHUM Hpb)", "Chirurgie du pancréas (CRCHUM Hpb)"), 
("specific pancreas surgery data", "Pancreas Surgery Data", "Données - chirurgie du pancréas"),
("pancreas appearance", "Pancreas Appearance", ""),
("wirsung diameter", "Wirsung Diameter", "Diam. Wirsung"),
("transection pancreas", "Transection Pancreas", ""),
("arterial resection", "Arterial Resection", ""),
("pancreas anastomosis", "Pancreas Anastomosis", ""),
("pylori preservation", "Pylori Preservation", ""),
("portal vein resection", "Portal Vein resection", ""),
("preoperative sandostatin", "Preoperative Sandostatin", "");

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;