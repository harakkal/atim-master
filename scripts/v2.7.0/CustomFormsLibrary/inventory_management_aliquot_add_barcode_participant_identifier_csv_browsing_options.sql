-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- Aliquot Barcode CSV Browsing Icon
-- ................................................................................
-- Add the 'Aliquot Barcode CSV Browsing' icon to let users to look for
-- aliquots based on Barcode from a CSV file.
-- --------------------------------------------------------------------------------

UPDATE structure_formats 
SET `flag_override_setting`='1', `setting`='size=30,class=range file' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='barcode' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- --------------------------------------------------------------------------------
-- Aliquot Participant Identifier CSV Browsing Icon
-- ................................................................................
-- Add the 'participant_identifier CSV Browsing' icon to let users to look for
-- aliquots based on Participant Identifier from a CSV file.
-- --------------------------------------------------------------------------------

UPDATE structure_formats 
SET `flag_override_setting`='1', `setting`='size=30,class=file range' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;