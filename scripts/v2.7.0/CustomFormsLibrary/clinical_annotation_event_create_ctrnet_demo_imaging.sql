-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CRCHUM Hepatopancreatobiliary Bank : Imaging
-- ................................................................................
-- Form developped to capture data for the CRCHUM Hepatopancreatobiliary BioBank.
-- Designed and created by the BioBank PIs.
-- --------------------------------------------------------------------------------

INSERT INTO event_controls (id, disease_site, event_group, event_type, flag_active, detail_form_alias, detail_tablename, display_order, databrowser_label, flag_use_for_ccl, use_addgrid, use_detail_form_for_index) VALUES
(null, '', 'clinical', 'ctrnet demo - imaging', 1, 'ctrnet_demo_ed_imaging', 'ctrnet_demo_ed_imagings', 0, 'clinical|ctrnet demo - imaging', 0, 1, 1);

DROP TABLE IF EXISTS ctrnet_demo_ed_imagings;
CREATE TABLE IF NOT EXISTS ctrnet_demo_ed_imagings  (
  event_master_id int(11) NOT NULL,
  imaging_type varchar(100)  DEFAULT NULL,
  topography varchar(100) DEFAULT NULL,
  result varchar(100) NOT NULL DEFAULT '',
  interpretation varchar(100) NOT NULL DEFAULT '',
  KEY event_master_id (event_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS ctrnet_demo_ed_imagings_revs;
CREATE TABLE IF NOT EXISTS ctrnet_demo_ed_imagings_revs (
  event_master_id int(11) NOT NULL,
  imaging_type varchar(100)  DEFAULT NULL,
  topography varchar(100) DEFAULT NULL,
  result varchar(100) NOT NULL DEFAULT '',
  interpretation varchar(100) NOT NULL DEFAULT '',
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE ctrnet_demo_ed_imagings
  ADD CONSTRAINT ctrnet_demo_ed_imagings_ibfk_1 FOREIGN KEY (event_master_id) REFERENCES event_masters (id);

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES
('ctrnet_demo_imaging_types', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CTRNet Demo : Imaging Types\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('CTRNet Demo : Imaging Types', 1, 100, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CTRNet Demo : Imaging Types');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("bone scintigraphy", "Bone scintigraphy", "Scintigraphie Osseuse", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("CT-scan", "CT-Scan", "CT-Scan", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("mri", "MRI", "IRM", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("PET-scan", "PET-Scan", "PET-Scan", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES
('ctrnet_demo_imaging_results', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CTRNet Demo : Imaging Results\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('CTRNet Demo : Imaging Results', 1, 100, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CTRNet Demo : Imaging Results');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("negative", "Negative", "Negatif", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("positive", "Positive", "Positif", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("suspicious", "Suspicious", "Suspicieux", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES
('ctrnet_demo_imaging_interpretations', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CTRNet Demo : Imaging Clinical Diagnosis\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('CTRNet Demo : Imaging Clinical Diagnosis', 1, 100, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CTRNet Demo : Imaging Clinical Diagnosis');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("none", "None", "Aucune", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("disease", "Disease", "Maladie", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("tumor", "Tumor", "Tumeur", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("progression", "Progression", "Progression", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("metastasis", "Metastasis", "Métastase", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("recurrence", "Recurrence", "Récidive", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("remission", "Remission", "Rémission", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structures(`alias`) VALUES ('ctrnet_demo_ed_imaging');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_imagings', 'imaging_type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_imaging_types') , '0', '', '', '', 'imaging', ''), 
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_imagings', 'topography', 'autocomplete',  NULL , '0', 'size=10,url=/CodingIcd/CodingIcdo3s/autocomplete/topo,tool=/CodingIcd/CodingIcdo3s/tool/topo', '', '', 'site', ''), 
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_imagings', 'result', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_imaging_results') , '0', '', '', '', 'result', ''), 
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_imagings', 'interpretation', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_imaging_interpretations') , '0', '', '', '', 'clinical diagnosis', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_imaging'), 
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '100', 'summary', '0', '0', '', '0', '', '0', '', '0', '', '1', 'cols=40,rows=1', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_imaging'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_imagings' AND `field`='imaging_type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_imaging_types')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='imaging' AND `language_tag`=''), 
'1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_imaging'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_imagings' AND `field`='topography' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10,url=/CodingIcd/CodingIcdo3s/autocomplete/topo,tool=/CodingIcd/CodingIcdo3s/tool/topo' AND `default`='' AND `language_help`='' AND `language_label`='site' AND `language_tag`=''), 
'1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_imaging'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_imagings' AND `field`='result' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_imaging_results')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='result' AND `language_tag`=''), 
'1', '7', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_imaging'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_imagings' AND `field`='interpretation' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_imaging_interpretations')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='clinical diagnosis' AND `language_tag`=''), 
'1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_validations(structure_field_id, rule, language_message) VALUES
((SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_imagings' AND `field`='topography' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10,url=/CodingIcd/CodingIcdo3s/autocomplete/topo,tool=/CodingIcd/CodingIcdo3s/tool/topo' AND `default`='' AND `language_help`='' AND `language_label`='site' AND `language_tag`=''), 'validateIcdo3TopoCode', 'invalid topography code'),
((SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_imagings' AND `field`='imaging_type' AND `type`='select'), 'notBlank', '');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("ctrnet demo - imaging", "Imaging (CTRNet Demo)", "Imagerie (CTRNet Demo)"),
("clinical diagnosis", "Clinical Diagnosis", "Diagnostic clinique"),
("imaging", "Imaging", "Imagerie");
 
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;