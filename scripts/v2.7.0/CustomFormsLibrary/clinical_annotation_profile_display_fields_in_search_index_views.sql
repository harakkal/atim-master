-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- --------------------------------------------------------------------------------
-- Participant Fields
-- ................................................................................
-- Display all fields used to create a participant in both search view and index
-- view.
-- --------------------------------------------------------------------------------

UPDATE structure_formats 
SET `flag_search`='1', `flag_index`='1' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='participants') 
AND flag_add = '1'
AND structure_field_id NOT IN (SELECT id FROM structure_fields WHERE `model`='Participant' AND `field` IN ('ids', 'notes'));

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;