-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CTRNet Demo : Biology
-- ................................................................................
-- Form developped to capture data of biology report based on the form
-- developped by the CRCHUM Hepatopancreatobiliary BioBank.
-- --------------------------------------------------------------------------------

INSERT INTO event_controls (id, disease_site, event_group, event_type, flag_active, detail_form_alias, detail_tablename, display_order, databrowser_label, flag_use_for_ccl, use_addgrid, use_detail_form_for_index) VALUES
(null, '', 'lab', 'ctrnet demo - biology', 1, 'ctrnet_demo_ed_lab_biology', 'ctrnet_demo_ed_lab_biologies', 0, 'lab|ctrnet demo - biology', 0, 1, 1);

DROP TABLE IF EXISTS ctrnet_demo_ed_lab_biologies;
CREATE TABLE IF NOT EXISTS ctrnet_demo_ed_lab_biologies (
  event_master_id int(11) NOT NULL,
  biology_test varchar(100) DEFAULT NULL,
  biology_test_precision varchar(250) DEFAULT NULL,
  biology_test_result decimal(10,2) DEFAULT NULL,
  biology_test_unit varchar(20) DEFAULT NULL,
  biology_test_result_precision varchar(250) DEFAULT NULL,
  KEY event_master_id (event_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS ctrnet_demo_ed_lab_biologies_revs;
CREATE TABLE IF NOT EXISTS ctrnet_demo_ed_lab_biologies_revs (
  event_master_id int(11) NOT NULL,
  biology_test varchar(100) DEFAULT NULL,
  biology_test_precision varchar(250) DEFAULT NULL,
  biology_test_result decimal(10,2) DEFAULT NULL,
  biology_test_unit varchar(20) DEFAULT NULL,
  biology_test_result_precision varchar(250) DEFAULT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE ctrnet_demo_ed_lab_biologies
  ADD CONSTRAINT ctrnet_demo_ed_lab_biologies_ibfk_1 FOREIGN KEY (event_master_id) REFERENCES event_masters (id);

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES
('ctrnet_demo_biology_tests', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CTRNet Demo : Biology Tests\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('CTRNet Demo : Biology Tests', 1, 100, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CTRNet Demo : Biology Tests');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("5 HIAA", "5 HIAA", "5 HIAA", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("a fp", "Alpha-FP", "Alpha-FP", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("albumin", "Albumin", "Albumine", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("alkalin _phosphatase", "Alkalin Phosphatase", "Phosphatase alcaline", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("alt", "ALT", "ALT", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("amylase", "Amylase", "Amylase", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ast", "AST", "AST", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ca125", "Ca125", "Ca125", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ca2", "Ca2", "Ca2", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("b hcg", "Beta HCG", "Beta HCG", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("bilirubin", "Bilirubin", "Bilirubine", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ca 125", "Ca 125", "Ca 125", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ca 15 3", "Ca 15-3", "Ca 15-3", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ca 19 9", "Ca 19-9", "Ca 19-9", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ca total", "Ca Total", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ca2+", "Ca2+", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("cea", "CEA", "CEA", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("cholesterol", "Cholesterol", "Cholestérol", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("chromogranine", "Chromogranine", "Chromogranine", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("cl", "Cl", "Cl", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("creatinine", "Creatinine", "Créatinine", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("direct bilirubin", "Direct Bilirubin", "Bilirubine directe", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("glycemia", "Glycemia", "Glycémie", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("hb", "Hb", "Hb", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ht", "Ht", "Ht", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("indirec _bilirubin", "Indirect Bilirubin", "Bilirubine indirecte", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("inr", "INR", "INR", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("k", "K", "K", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("lipase", "Lipase", "Lipase", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("mg", "Mg", "Mg", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("na", "Na", "Na", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("p", "P", "P", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("psa", "PSA", "APS", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("platelets", "Platelets", "Plaquettes", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("protein", "Protein", "Protéine", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ptt", "PTT", "PTT", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("rbc", "RBC", "NGR", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("total bilirubin", "Total Bilirubin", "Bilirubine totale", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("triglycerides", "Triglycerides", "Triglycéride", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("urea", "Urea", "Urée", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("uric acid", "Uric Acid", "Acide urique", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("wbc", "WBC", "GB", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES
('ctrnet_demo_biology_test_units', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CTRNet Demo : Biology Test Units\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('CTRNet Demo : Biology Test Units', 1, 20, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CTRNet Demo : Biology Test Units');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("ng/ml", "", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("u/L", "", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("umol/L", "", "", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structures(`alias`) VALUES ('ctrnet_demo_ed_lab_biology');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_lab_biologies', 'biology_test', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_biology_tests') , '0', '', '', '', 'test', ''), 
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_lab_biologies', 'biology_test_precision', 'input',  NULL , '0', 'size=20', '', '', '', 'precision'), 
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_lab_biologies', 'biology_test_result', 'float',  NULL , '0', 'size=5', '', '', 'result', ''), 
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_lab_biologies', 'biology_test_unit', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_biology_test_units') , '0', '', '', '', '', ''), 
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_lab_biologies', 'biology_test_result_precision', 'input',  NULL , '0', 'size=20', '', '', '', 'precision');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_lab_biology'), 
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '100', 'summary', '0', '0', '', '0', '', '0', '', '0', '', '1', 'cols=40,rows=1', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_lab_biology'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_lab_biologies' AND `field`='biology_test' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_biology_tests')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='test' AND `language_tag`=''), 
'1', '5', 'test', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_lab_biology'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_lab_biologies' AND `field`='biology_test_precision' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='precision'), 
'1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_lab_biology'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_lab_biologies' AND `field`='biology_test_result' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='result' AND `language_tag`=''), 
'1', '7', 'result', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_lab_biology'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_lab_biologies' AND `field`='biology_test_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_biology_test_units')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_lab_biology'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_lab_biologies' AND `field`='biology_test_result_precision' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='precision'), 
'1', '9', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("ctrnet demo - biology", "Biology (CTRNet Demo)", "Biologie (CTRNet Demo)");
 
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;