-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- --------------------------------------------------------------------------------
-- Hide Misc Identifier dates and notes fields
-- --------------------------------------------------------------------------------

UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_detail`='0' 
WHERE structure_id IN (SELECT id FROM structures WHERE alias IN ('incrementedmiscidentifiers', 'miscidentifiers'))
AND structure_field_id IN (SELECT id FROM structure_fields WHERE `model`='MiscIdentifier' AND `tablename`='misc_identifiers' AND `field` IN ('effective_date', 'expiry_date','notes'));

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;