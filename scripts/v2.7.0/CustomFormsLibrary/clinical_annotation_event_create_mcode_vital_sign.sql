-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- mCode ((Minimal Common Oncology Data Elements) initiative : 
--   Create outcome form based on 'Labs/Vitals' group
--   and 'vital signs' profile fields  
--   of the mCODE™ DataDictionnary Version 0.9.1.
-- ................................................................................

--   List based on :
--    - height
--       mCODE™ Value Set Name : BodyHeightLengthUnitsVS
--    - weight
--       mCODE™ Value Set Name : BodyWeightUnitsVS
-- --------------------------------------------------------------------------------

INSERT INTO event_controls (id, disease_site, event_group, event_type, flag_active, detail_form_alias, detail_tablename, display_order, databrowser_label, flag_use_for_ccl, use_addgrid, use_detail_form_for_index) VALUES
(null, '', 'lab', 'mcode - vital signs', 1, 'mcode_ed_lab_vital_sign', 'mcode_ed_lab_vital_signs', 0, 'lab|mcode - vital signs', 0, 1, 1);

DROP TABLE IF EXISTS mcode_ed_lab_vital_signs;
CREATE TABLE IF NOT EXISTS mcode_ed_lab_vital_signs (
  event_master_id int(11) NOT NULL,
  height decimal(6,2) DEFAULT NULL,
  height_unit varchar(50) default NULL,
  weight decimal(6,2) DEFAULT NULL,
  weight_unit varchar(50) default NULL,
  diastolic_pressure_mmhg decimal(6,2) DEFAULT NULL, 
  systolic_pressure_mmhg decimal(6,2) DEFAULT NULL,
  KEY event_master_id (event_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS mcode_ed_lab_vital_signs_revs;
CREATE TABLE IF NOT EXISTS mcode_ed_lab_vital_signs_revs (
  event_master_id int(11) NOT NULL,
  height decimal(6,2) DEFAULT NULL,
  height_unit varchar(50) default NULL,
  weight decimal(6,2) DEFAULT NULL,
  weight_unit varchar(50) default NULL,
  diastolic_pressure_mmhg decimal(6,2) DEFAULT NULL, 
  systolic_pressure_mmhg decimal(6,2) DEFAULT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE mcode_ed_lab_vital_signs
  ADD CONSTRAINT mcode_ed_lab_vital_signs_ibfk_1 FOREIGN KEY (event_master_id) REFERENCES event_masters (id);

INSERT INTO structures(`alias`) VALUES ('mcode_ed_lab_vital_sign');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_lab_vital_signs', 'height', 'float',  NULL , '0', 'size=5', '', '', 'height', ''), 
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_lab_vital_signs', 'height_unit', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_BodyHeightLengthUnitsVS') , '0', '', '', 'help_mcode_height_unit', '', ''), 
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_lab_vital_signs', 'weight', 'float',  NULL , '0', 'size=5', '', '', 'weight', ''), 
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_lab_vital_signs', 'weight_unit', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_BodyWeightUnitsVS') , '0', '', '', 'help_mcode_weight_unit', '', ''), 
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_lab_vital_signs', 'diastolic_pressure_mmhg', 'float',  NULL , '0', 'size=5', '', '', 'diastolic pressure mmhg', ''), 
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_lab_vital_signs', 'systolic_pressure_mmhg', 'float',  NULL , '0', 'size=5', '', '', 'systolic pressure mmhg', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES 
((SELECT id FROM structures WHERE alias='mcode_ed_lab_vital_sign'),
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_lab_vital_signs' AND `field`='height' AND `type`='float'), 
'1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_ed_lab_vital_sign'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_lab_vital_signs' AND `field`='height_unit' AND `type`='select'), 
'1', '9', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_ed_lab_vital_sign'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_lab_vital_signs' AND `field`='weight' AND `type`='float'), 
'1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_ed_lab_vital_sign'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_lab_vital_signs' AND `field`='weight_unit' AND `type`='select'), 
'1', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_ed_lab_vital_sign'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_lab_vital_signs' AND `field`='diastolic_pressure_mmhg' AND `type`='float'), 
'1', '12', 'pressure (mmHg)', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_ed_lab_vital_sign'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_lab_vital_signs' AND `field`='systolic_pressure_mmhg' AND `type`='float'), 
'1', '13', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='mcode_ed_lab_vital_sign'), 
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '100', 'summary', '0', '0', '', '0', '', '0', '', '0', '', '1', 'cols=40,rows=1', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('mcode - vital signs', 'Vital Signs (mCODE™)', 'Signes vitaux (mCODE™)'),
('pressure', 'Pressure', 'Pression'), 
('diastolic pressure mmhg', 'Diastolic', 'Diastolique'), 
('systolic pressure mmhg', 'Systolic', 'Systolique'),
('help_mcode_height_unit',
"The unit of the patient's height as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the BodyHeightLengthUnitsVS Value Set details of the mCODE Data Dictionnary version 0.9.1.", 
"L'unité de la taille du patient telle que définie par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la liste des codes BodyHeightLengthUnitsVS de la version 0.9.1 du dictionnaire de données mCode."),
('help_mcode_weight_unit',
"The unit of the patient's weight as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the BodyWeightUnitsVS Value Set details of the mCODE Data Dictionnary version 0.9.1.", 
"L'unité du poids du patient telle que définie par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la liste des codes BodyWeightUnitsVS de la version 0.9.1 du dictionnaire de données mCode.");

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;