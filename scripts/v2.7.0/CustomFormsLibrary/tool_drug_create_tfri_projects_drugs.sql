-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user id to define

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- TFRI COEUR & CPCBN Drugs and more...
-- ................................................................................
-- List of drugs created to capture data of prostat and ovarian cancer 
-- for the central ATiM of the Canadian Prostate Cancer Biomarker Network (CPCBN)
-- and the central ATiM of the Canadian Ovarian Experimental Unified Resource 
-- Projects of the Terry Fox Research Institute (TFRI).
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS `drugs_tmp`;
CREATE TABLE IF NOT EXISTS `drugs_tmp` (
  `generic_name` varchar(50) NOT NULL DEFAULT '',
  `trade_name` varchar(50) NOT NULL DEFAULT '',
  `type` varchar(50) DEFAULT NULL,
  `to_delete` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `drugs_tmp` (`generic_name`, `trade_name`, `type`) 
VALUES
('5-ARI', '', 'hormonal'),
('Abiraterone', '', 'hormonal'),
('Bicalutamide', '', 'hormonal'),
('Buserelin', '', 'hormonal'),
('Cabazitaxel', '', 'chemotherapy'),
('Carboplatin', '', 'chemotherapy'),
('Cisplatin', '', 'chemotherapy'),
('Cyclophosphamide', '', 'chemotherapy'),
('Cyproterone', '', 'hormonal'),
('Dasatinib', '', 'chemotherapy'),
('Degarelix', '', 'hormonal'),
('Dexamethasone', '', 'chemotherapy'),
('Docetaxel', '', 'chemotherapy'),
('Doxorubicin', '', 'chemotherapy'),
('Dutasteride ', '', 'hormonal'),
('Enzalutamide', '', 'hormonal'),
('Enzastaurin', '', 'chemotherapy'),
('Etoposide', '', 'chemotherapy'),
('Finasteride', '', 'hormonal'),
('Flutamide', '', 'hormonal'),
('Gefitinib', '', 'chemotherapy'),
('Gemcitabine', '', 'chemotherapy'),
('Goserelin', '', 'hormonal'),
('Leuprorelin', '', 'hormonal'),
('LHRH agonist', '', 'hormonal'),
('Olaparib', '', 'chemotherapy'),
('Oxaliplatine', '', 'chemotherapy'),
('Paclitaxel', '', 'chemotherapy'),
('Risedronic acid', '', 'other'),
('Tamoxifen', '', 'chemotherapy'),
('Tamsulosin HCl', '', 'hormonal'),
('Topotecan', '', 'chemotherapy'),
('Vinorelbine', '', 'chemotherapy');

UPDATE drugs_tmp, drugs
SET drugs_tmp.to_delete = 1
WHERE drugs.deleted <> 1
AND drugs.generic_name = drugs_tmp.generic_name
AND drugs.type = drugs_tmp.type;

INSERT INTO `drugs` (`generic_name`, `trade_name`, `type`, `created`, `created_by`, `modified`, `modified_by`) 
(SELECT `generic_name`, `trade_name`, `type`, NOW(), @user_id, NOW(), @user_id FROM drugs_tmp WHERE to_delete = 0);

DROP TABLE IF EXISTS `drugs_tmp`;