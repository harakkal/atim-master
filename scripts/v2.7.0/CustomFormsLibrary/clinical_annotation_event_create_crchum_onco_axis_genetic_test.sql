-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CRCHUM Oncology Axis Banks : Genetic Test
-- ................................................................................
-- Form developped to capture data for the CRCHUM Oncology Axis Banks.
-- Designed and created by the BioBank Laboratory People.
-- --------------------------------------------------------------------------------

INSERT INTO event_controls (id, disease_site, event_group, event_type, flag_active, detail_form_alias, detail_tablename, display_order, databrowser_label, flag_use_for_ccl, use_addgrid, use_detail_form_for_index) VALUES
(null, '', 'lab', 'crchum oncology axis - genetic test', 1, 'crchum_onco_axis_ed_lab_genetic_test', 'crchum_onco_axis_ed_lab_genetic_tests', 0, 'lab|crchum oncology axis - genetic test', 0, 1, 1);

DROP TABLE IF EXISTS crchum_onco_axis_ed_lab_genetic_tests;
CREATE TABLE IF NOT EXISTS crchum_onco_axis_ed_lab_genetic_tests (
  event_master_id int(11) NOT NULL,
  test varchar(100) DEFAULT NULL,
  result varchar(100) DEFAULT NULL,
  detail varchar(250) DEFAULT NULL,
  site varchar(150) DEFAULT NULL,
  simplified_result varchar(20) DEFAULT NULL,
  KEY event_master_id (event_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS crchum_onco_axis_ed_lab_genetic_tests_revs;
CREATE TABLE IF NOT EXISTS crchum_onco_axis_ed_lab_genetic_tests_revs (
  event_master_id int(11) NOT NULL,
  test varchar(100) DEFAULT NULL,
  result varchar(100) DEFAULT NULL,
  detail varchar(250) DEFAULT NULL,
  site varchar(150) DEFAULT NULL,
  simplified_result varchar(20) DEFAULT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE crchum_onco_axis_ed_lab_genetic_tests
  ADD CONSTRAINT crchum_onco_axis_ed_lab_genetic_tests_ibfk_1 FOREIGN KEY (event_master_id) REFERENCES event_masters (id);

INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM Onco. Axis : Genetic Tests', 1, 100, 'clinical - annotation');
INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('qcrchum_onco_axis_genetic_tests', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM Onco. Axis : Genetic Tests\')');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM Onco. Axis : Genetic Tests');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("AKT1", "AKT1", "AKT1", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("APC", "APC", "APC", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("ATM-Ataxia Telangiectasia", "ATM (Ataxia Telangiectasia)", "ATM (Ataxia Telangiectasia)", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("AXIN2", "AXIN2", "AXIN2", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("B-RAF", "B-RAF", "B-RAF", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("BARD1", "BARD1", "BARD1", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("BMPR1A (ALK3/CD292/SKR5)", "BMPR1A (ALK3/CD292/SKR5)", "BMPR1A (ALK3/CD292/SKR5)", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("BRCA 1-2", "BRCA 1-2", "BRCA 1-2", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("BRIP1", "BRIP1", "BRIP1", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("CDC73", "CDC73", "CDC73", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("CDH1 (E-Cadherin)", "CDH1 (E-Cadherin)", "CDH1 (E-Cadherin)", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("CDK4", "CDK4", "CDK4", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("CDKN2A", "CDKN2A", "CDKN2A", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("CHEK2 (Checkpoint Kinase 2)", "CHEK2 (Checkpoint Kinase 2)", "CHEK2 (Checkpoint Kinase 2)", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("COL5A1", "COL5A1", "COL5A1", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("DICER1", "DICER1", "DICER1", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("EPCAM", "EPCAM (deletion/duplication)", "EPCAM (délétion/duplication)", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("FAM175A", "FAM175A", "FAM175A", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("GREM1", "GREM1", "GREM1", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("KIT", "KIT", "KIT", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("KRAS", "KRAS", "KRAS", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("MEN1", "MEN1", "MEN1", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("MET", "MET", "MET", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("MRE11A", "MRE11A", "MRE11A", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("MUTYH", "MUTYH", "MUTYH", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("NBN", "NBN (Nibrin)", "NBN (Nibrin)", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("NF1 (Neurofibromin)", "NF1 (Neurofibromin)", "NF1 (Neurofibromin)", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("PALB2", "PALB2", "PALB2", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("PALLD", "PALLD", "PALLD", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("PDGFRA", "PDGFRA", "PDGFRA", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("PIK3CA", "PIK3CA", "PIK3CA", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("POLD1", "POLD1", "POLD1", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("POLE", "POLE", "POLE", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("PRKAR1A", "PRKAR1A", "PRKAR1A", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("PTCH1", "PTCH1", "PTCH1", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("PTEN", "PTEN", "PTEN", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("RAD50", "RAD50", "RAD50", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("RAD51", "RAD51 C-D", "RAD51 C-D", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("RET", "RET", "RET", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("RINT-1", "RINT-1", "RINT-1", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("SDHB", "SDHB", "SDHB", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("SDHC", "SDHC", "SDHC", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("SDHD", "SDHD", "SDHD", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("SMAD4", "SMAD4", "SMAD4", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("SMARC4 (BRG1)", "SMARCA4 (BRG1)", "SMARCA4 (BRG1)", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("SMARCA4", "SMARCA4", "SMARCA4", "0", "0", @control_id, NOW(), NOW(), @user_id, @user_id), 
("STK11 (LKB1)", "STK11 (LKB1)", "STK11 (LKB1)", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("TSC1", "TSC1", "TSC1", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("TSC2", "TSC2", "TSC2", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("VHL (von Hippel-Lindau syndrome)", "VHL (von Hippel-Lindau syndrome)", "VHL (von Hippel-Lindau syndrome)", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("XRCC2", "XRCC2", "XRCC2", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source) VALUES ("crchum_onco_axis_genetic_test_results", "open", "", NULL);
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("positive", "positive"), ("variant", "variant"), ("negative", "negative"), ("unknown", "unknown"), ("not done", "not done"), ("waiting", "waiting");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="crchum_onco_axis_genetic_test_results"), (SELECT id FROM structure_permissible_values WHERE value="positive" AND language_alias="positive"), "1", "1");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="crchum_onco_axis_genetic_test_results"), (SELECT id FROM structure_permissible_values WHERE value="variant" AND language_alias="variant"), "2", "1");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="crchum_onco_axis_genetic_test_results"), (SELECT id FROM structure_permissible_values WHERE value="negative" AND language_alias="negative"), "3", "1");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="crchum_onco_axis_genetic_test_results"), (SELECT id FROM structure_permissible_values WHERE value="unknown" AND language_alias="unknown"), "4", "1");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="crchum_onco_axis_genetic_test_results"), (SELECT id FROM structure_permissible_values WHERE value="not done" AND language_alias="not done"), "5", "1");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="crchum_onco_axis_genetic_test_results"), (SELECT id FROM structure_permissible_values WHERE value="waiting" AND language_alias="waiting"), "6", "1");

INSERT INTO structures(`alias`) VALUES ('crchum_onco_axis_ed_lab_genetic_test');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchum_onco_axis_ed_lab_genetic_tests', 'site', 'input',  NULL , '1', '', '', '', 'laboratory', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_onco_axis_ed_lab_genetic_tests', 'test', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='qcrchum_onco_axis_genetic_tests') , '1', '', '', '', 'test', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_onco_axis_ed_lab_genetic_tests', 'simplified_result', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_onco_axis_genetic_test_results') , '1', '', 'positive', '', 'result', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_onco_axis_ed_lab_genetic_tests', 'result', 'input',  NULL , '1', 'size=50', '', '', '', '-'), 
('ClinicalAnnotation', 'EventDetail', 'crchum_onco_axis_ed_lab_genetic_tests', 'detail', 'input',  NULL , '1', 'size=50', '', '', 'detail', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchum_onco_axis_ed_lab_genetic_test'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_onco_axis_ed_lab_genetic_tests' AND `field`='site' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='1' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='laboratory' AND `language_tag`=''), 
'2', '25', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_onco_axis_ed_lab_genetic_test'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_onco_axis_ed_lab_genetic_tests' AND `field`='test' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='qcrchum_onco_axis_genetic_tests')  AND `flag_confidential`='1' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='test' AND `language_tag`=''), 
'2', '29', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_onco_axis_ed_lab_genetic_test'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_onco_axis_ed_lab_genetic_tests' AND `field`='simplified_result' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_onco_axis_genetic_test_results')  AND `flag_confidential`='1' AND `setting`='' AND `default`='positive' AND `language_help`='' AND `language_label`='result' AND `language_tag`=''), 
'2', '30', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_onco_axis_ed_lab_genetic_test'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_onco_axis_ed_lab_genetic_tests' AND `field`='result' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='1' AND `setting`='size=50' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='-'), 
'2', '31', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_onco_axis_ed_lab_genetic_test'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_onco_axis_ed_lab_genetic_tests' AND `field`='detail' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='1' AND `setting`='size=50' AND `default`='' AND `language_help`='' AND `language_label`='detail' AND `language_tag`=''), 
'2', '32', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_onco_axis_ed_lab_genetic_test'), 
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='cols=40,rows=6' AND `default`='' AND `language_help`='' AND `language_label`='summary' AND `language_tag`=''), 
'2', '33', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("conclusion", "Conclusion", "Conclusion"),
("crchum oncology axis - genetic test", "Genetic Test (CRCHUM Onco. Axis)", "Test génétique (CRCHUM Axe Onco.)");
 
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;