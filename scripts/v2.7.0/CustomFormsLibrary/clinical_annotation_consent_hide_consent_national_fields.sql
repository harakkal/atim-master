-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- --------------------------------------------------------------------------------
-- Hide 'Consent National' fields
-- ................................................................................
-- Fields :
--     - Person Handling Consent
--     - Process Status
--     - Facilty 
--     - Facility Other
--     - Operation Date
-- --------------------------------------------------------------------------------

UPDATE structure_formats SET `flag_add`='0', `flag_edit`='0', `flag_search`='0', `flag_index`='0', `flag_detail`='0' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='cd_nationals') 
AND structure_field_id IN (
  SELECT id FROM structure_fields 
  WHERE `model`='ConsentMaster' AND `tablename`='consent_masters' AND `field` IN ('operation_date', 'process_status', 'consent_person', 'facility', 'facility_other')
);

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;