-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- --------------------------------------------------------------------------------
-- Participant Chronology : Display Time Field
-- ................................................................................
-- Display Time Field in the participant chronology form.
-- --------------------------------------------------------------------------------
-- Participant Chronology 

UPDATE structure_formats 
SET `flag_index`='1' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='chronology') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='custom' AND `tablename`='' AND `field`='time' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;