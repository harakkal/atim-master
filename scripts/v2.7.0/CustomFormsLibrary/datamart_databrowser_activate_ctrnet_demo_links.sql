-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- --------------------------------------------------------------------------------
-- Create missing databrowser link
-- ................................................................................
-- -
-- --------------------------------------------------------------------------------

-- TMA Slide
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------

UPDATE datamart_browsing_controls SET flag_active_1_to_2 = 1, flag_active_2_to_1 = 1 
WHERE id1 = (SELECT id FROM datamart_structures WHERE model = 'OrderItem') 
AND id2 = (SELECT id FROM datamart_structures WHERE model = 'TmaSlide');

UPDATE datamart_browsing_controls SET flag_active_1_to_2 = 1, flag_active_2_to_1 = 1 
WHERE id1 = (SELECT id FROM datamart_structures WHERE model = 'TmaSlideUse') 
AND id2 = (SELECT id FROM datamart_structures WHERE model = 'TmaSlide');

UPDATE datamart_browsing_controls SET flag_active_1_to_2 = 1, flag_active_2_to_1 = 1 
WHERE id1 = (SELECT id FROM datamart_structures WHERE model = 'TmaSlideUse') 
AND id2 = (SELECT id FROM datamart_structures WHERE model = 'StudySummary');

-- Order Line 
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------

UPDATE datamart_browsing_controls SET flag_active_1_to_2 = 1, flag_active_2_to_1 = 1
WHERE id1 = (SELECT id FROM datamart_structures WHERE model = 'OrderItem') 
AND id2 = (SELECT id FROM datamart_structures WHERE model = 'OrderLine'); 

UPDATE datamart_browsing_controls SET flag_active_1_to_2 = 1, flag_active_2_to_1 = 1
WHERE id1 = (SELECT id FROM datamart_structures
 WHERE model = 'OrderLine') 
AND id2 = (SELECT id FROM datamart_structures WHERE model = 'Order');

UPDATE datamart_browsing_controls SET flag_active_1_to_2 = 1, flag_active_2_to_1 = 1
WHERE id1 = (SELECT id FROM datamart_structures WHERE model = 'OrderLine') 
AND id2 = (SELECT id FROM datamart_structures WHERE model = 'StudySummary');

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;