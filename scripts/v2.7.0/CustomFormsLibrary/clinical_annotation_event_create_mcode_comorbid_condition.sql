-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- mCode ((Minimal Common Oncology Data Elements) initiative : 
--   Create comorbid condition form based on 'Patient' group 
--   'Comorbid Condition' profile fields of the mCODE™ Data 
--   Dictionnary Version 0.9.1.
-- ................................................................................

--   List based on :
--    - Code
--       mCODE™ Value Set Name : ComorbidConditionVS
--    - Status
--       HL7 FHIR codes version 4.0.0 : ConditionClinicalStatusCodes
--       (http://hl7.org/fhir/valueset-condition-clinical.html)
-- --------------------------------------------------------------------------------

INSERT INTO event_controls (id, disease_site, event_group, event_type, flag_active, detail_form_alias, detail_tablename, display_order, databrowser_label, flag_use_for_ccl, use_addgrid, use_detail_form_for_index) VALUES
(null, '', 'clinical', 'mcode - comorbid condition', 1, 'mcode_ed_clinical_comorbid_condition', 'mcode_ed_clinical_comorbid_conditions', 0, 'clinical|mcode - comorbid condition', 0, 1, 1);

DROP TABLE IF EXISTS mcode_ed_clinical_comorbid_conditions;
CREATE TABLE IF NOT EXISTS mcode_ed_clinical_comorbid_conditions (
  event_master_id int(11) NOT NULL,
  mcode_comorbid_condition_clinical_status varchar(50) default NULL,
  mcode_comorbid_condition_code varchar(50) default NULL,
  KEY event_master_id (event_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS mcode_ed_clinical_comorbid_conditions_revs;
CREATE TABLE IF NOT EXISTS mcode_ed_clinical_comorbid_conditions_revs (
  event_master_id int(11) NOT NULL,
  mcode_comorbid_condition_clinical_status varchar(50) default NULL,
  mcode_comorbid_condition_code varchar(50) default NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE mcode_ed_clinical_comorbid_conditions
  ADD CONSTRAINT mcode_ed_clinical_comorbid_conditions_ibfk_1 FOREIGN KEY (event_master_id) REFERENCES event_masters (id);

INSERT INTO structures(`alias`) VALUES ('mcode_ed_clinical_comorbid_condition');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_clinical_comorbid_conditions', 'mcode_comorbid_condition_clinical_status', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_ConditionClinicalStatusCodes') , '0', '', '', 'help_mcode_comorbid_condition_clinical_status', 'clinical status', ''), 
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_clinical_comorbid_conditions', 'mcode_comorbid_condition_code', 'autocomplete',  NULL , '0', 'size=10,url=/CodingIcd/CodingMCodes/autocomplete/ComorbidConditionVS,tool=/CodingIcd/CodingMCodes/tool/ComorbidConditionVS', '', 'help_mcode_comorbid_condition_code', 'comorbid condition code', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='mcode_ed_clinical_comorbid_condition'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_clinical_comorbid_conditions' AND `field`='mcode_comorbid_condition_clinical_status'), 
'1', '7', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_ed_clinical_comorbid_condition'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_clinical_comorbid_conditions' AND `field`='mcode_comorbid_condition_code'), 
'1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_ed_clinical_comorbid_condition'), 
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '100', 'summary', '0', '0', '', '0', '', '0', '', '0', '', '1', 'cols=40,rows=1', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_validations (structure_field_id, rule, language_message)
VALUES
((SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_clinical_comorbid_conditions' AND `field`='mcode_comorbid_condition_code' AND `type`='autocomplete'),
'validateMcodeCode,ComorbidConditionVS', 'invalid mcode code'),
((SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_clinical_comorbid_conditions' AND `field`='mcode_comorbid_condition_clinical_status' AND `type`='select'),
'notBlank', '');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('mcode - comorbid condition', 'Comorbid Condition (mCODE™)', 'Comorbidité (mCODE™)'),

('clinical status', 'Clinical Status', 'Statut clinique'),
('help_mcode_comorbid_condition_clinical_status',
"A flag indicating whether the condition is active or inactive, recurring, in remission, or resolved (as of the last update of the Condition) as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the ConditionClinicalStatusCodes code version 4.0.0 of the HL7 FHIR standard (http://hl7.org/fhir/ValueSet/condition-clinical).", 
"Une valeur indiquant si la condition clinique est active ou inactive, récurrente, en remission ou résolue (lors de la dernière mise à jour de la condition) telle que définie par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la version 4.0.0 du code ConditionClinicalStatusCodes de la norme FHIR HL7 (http://hl7.org/fhir/ValueSet/condition-clinical)."),

('comorbid condition code', 'Code', 'Code'),
('help_mcode_comorbid_condition_code',
"The code representing the comorbid condition as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the ComorbidConditionCode Value Set details of the mCODE Data Dictionnary version 0.9.1.", 
"Le code définissant le 'statut de la comorbidité' tel que défini par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la liste des codes ComorbidConditionCode de la version 0.9.1 du dictionnaire de données mCode.");
 
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;