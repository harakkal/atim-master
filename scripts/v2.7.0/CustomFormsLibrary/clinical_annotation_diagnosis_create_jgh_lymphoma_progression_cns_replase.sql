-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- Jewish General Hospital : Lymphoma CNS Relapse Diagnosis
-- ................................................................................
-- CNS relapse diagnosis forms developped to capture data of lymphoma CNS relapse 
-- for the ATiM of Lymphoma bank at the Lady Davis Institute (Jewish 
-- General Hospital).
-- --------------------------------------------------------------------------------

INSERT INTO `diagnosis_controls` (`id`, `category`, `controls_type`, `flag_active`, `detail_form_alias`, `detail_tablename`, `display_order`, `databrowser_label`, `flag_compare_with_cap`) 
VALUES
(null, 'progression - locoregional', 'jgh lymphoma - cns relapse', 1, 'jgh_lymphoma_dxd_cns_replapse', 'jgh_lymphoma_dxd_cns_replapses', 0, 'progression - locoregional|jgh lymphoma - cns relapse', 0);

DROP TABLE IF EXISTS `jgh_lymphoma_dxd_cns_replapses`;
CREATE TABLE IF NOT EXISTS `jgh_lymphoma_dxd_cns_replapses` (
  `diagnosis_master_id` int(11) NOT NULL,
  KEY `diagnosis_master_id` (`diagnosis_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `jgh_lymphoma_dxd_cns_replapses_revs`;
CREATE TABLE IF NOT EXISTS `jgh_lymphoma_dxd_cns_replapses_revs` (
  `diagnosis_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3739 DEFAULT CHARSET=latin1;

ALTER TABLE `jgh_lymphoma_dxd_cns_replapses`
  ADD CONSTRAINT `FK_jgh_lymphoma_dxd_cns_replapses_diagnosis_masters` FOREIGN KEY (`diagnosis_master_id`) REFERENCES `diagnosis_masters` (`id`);

INSERT INTO structures(`alias`) VALUES ('jgh_lymphoma_dxd_cns_replapse');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES 
("jgh lymphoma - cns relapse", "CNS Relapse (JGH Lymphoma)", "Rechute CNS (JGH Lymphome)");
 
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;