-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- mCode ((Minimal Common Oncology Data Elements) initiative : 
--   Create surgery form based on 'Treatment' group
--   and 'Cancer Related Surgical Procedure' profile fields  
--   of the mCODE™ DataDictionnary Version 0.9.1.
-- ................................................................................

--   List based on :
--    - Code
--       mCODE™ Value Set Name : surgeryProcedureVS
--    - Target Body Site
--       mCODE™ Value Set Name : surgeryTargetBodySiteVS
-- --------------------------------------------------------------------------------

INSERT INTO `treatment_controls` (`id`, `tx_method`, `disease_site`, `flag_active`, `detail_tablename`, `detail_form_alias`, `display_order`, `applied_protocol_control_id`, `extended_data_import_process`, `databrowser_label`, `flag_use_for_ccl`, `treatment_extend_control_id`, `use_addgrid`, `use_detail_form_for_index`) 
VALUES
(null, 'mcode - cancer related surgery procedure', '', 1, 'mcode_txd_cancer_related_surgeries', 'mcode_txd_cancer_related_surgery', 0, NULL, NULL, 'mcode - cancer related surgery procedure', 1, NULL, 1, 1);

DROP TABLE IF EXISTS mcode_txd_cancer_related_surgeries;
CREATE TABLE IF NOT EXISTS mcode_txd_cancer_related_surgeries (
  treatment_master_id int(11) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  KEY tx_master_id (treatment_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS mcode_txd_cancer_related_surgeries_revs;
CREATE TABLE IF NOT EXISTS mcode_txd_cancer_related_surgeries_revs (
  treatment_master_id int(11) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE mcode_txd_cancer_related_surgeries
  ADD CONSTRAINT mcode_txd_cancer_related_surgeries_ibfk_1 FOREIGN KEY (treatment_master_id) REFERENCES treatment_masters (id);

INSERT INTO structures(`alias`) VALUES ('mcode_txd_cancer_related_surgery');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
-- ('ClinicalAnnotation', 'TreatmentMaster', 'treatment_masters', 'tx_intent', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_TreatmentIntentVS') , '0', '', '', 'help_mcode_tx_cancer_related_surgery_intent', 'treatment intent', ''),
('ClinicalAnnotation', 'TreatmentDetail', 'mcode_txd_cancer_related_surgeries', 'code', 'autocomplete',  NULL , '0', 'size=10,url=/CodingIcd/CodingMCodes/autocomplete/CancerRelatedSurgicalProcedureVS,tool=/CodingIcd/CodingMCodes/tool/CancerRelatedSurgicalProcedureVS', '', 'help_mcode_tx_cancer_related_surgery_code', 'code', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='mcode_txd_cancer_related_surgery'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentMaster' AND `tablename`='treatment_masters' AND `field`='tx_intent' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_TreatmentIntentVS')), 
'1', '19', '', '0', '0', '', '0', '', '1', 'help_mcode_tx_cancer_related_surgery_intent', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='mcode_txd_cancer_related_surgery'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='mcode_txd_cancer_related_surgeries' AND `field`='code' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10,url=/CodingIcd/CodingMCodes/autocomplete/CancerRelatedSurgicalProcedureVS,tool=/CodingIcd/CodingMCodes/tool/CancerRelatedSurgicalProcedureVS' AND `default`='' AND `language_help`='help_mcode_tx_cancer_related_surgery_code' AND `language_label`='code' AND `language_tag`=''), 
'1', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_txd_cancer_related_surgery'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentMaster' AND `tablename`='treatment_masters' AND `field`='notes' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='rows=3,cols=30' AND `default`='' AND `language_help`='help_notes' AND `language_label`='notes' AND `language_tag`=''), 
'1', '99', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_validations (structure_field_id, rule, language_message)
VALUES
((SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='mcode_txd_cancer_related_surgeries' AND `field`='code'),
'validateMcodeCode,CancerRelatedSurgicalProcedureVS', 'invalid mcode code'),
((SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='mcode_txd_cancer_related_surgeries' AND `field`='code'),
'notBlank', '');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('mcode - cancer related surgery procedure', "Cancer Related surgery (mCODE™)", "surgery du cancer (mCODE™)"),
('treatment intent', 'Treatment Intent', 'Objectif du traitement'),
('help_mcode_tx_cancer_related_surgery_code', 
"The code for the surgery therapy procedure performed as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the surgeryProcedureVS Value Set details of the mCODE Data Dictionnary version 0.9.1.", 
"Le code définissant la 'procédure de chirurgie' telle que définie par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la liste des codes surgeryProcedureVS de la version 0.9.1 du dictionnaire de données mCode."),
('help_mcode_tx_cancer_related_surgery_intent', 
"The code for the purpose of a treatment as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the TreatmentIntentVS Value Set details of the mCODE Data Dictionnary version 0.9.1.", 
"Le code définissant 'l'objectif de la chirurgie' tel que défini par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la liste des codes TreatmentIntentVS de la version 0.9.1 du dictionnaire de données mCode.");

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;