-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- --------------------------------------------------------------------------------
-- Let people to synchronize data creation using LabBook features for:
-- ................................................................................
--     - DNA creation
--     - Tissue slide creation from block (realiquoting)
-- --------------------------------------------------------------------------------

UPDATE menus SET flag_active = '1' WHERE use_link LIKE '/Labbook/%';

UPDATE parent_to_derivative_sample_controls
SET lab_book_control_id = (select id from lab_book_controls WHERE book_type = 'dna extraction')
WHERE derivative_sample_control_id = (SELECT id FROM sample_controls WHERE sample_type = 'dna');

UPDATE realiquoting_controls
SET lab_book_control_id = (
  SELECT id FROM lab_book_controls WHERE book_type = 'slide creation'
) 
WHERE child_aliquot_control_id = (
  SELECT aliquot_controls.id 
  FROM sample_controls INNER JOIN aliquot_controls 
  WHERE sample_type = 'tissue' AND sample_control_id = sample_controls.id AND aliquot_type = 'slide'
);

UPDATE lab_book_controls SET flag_active = 1;

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;