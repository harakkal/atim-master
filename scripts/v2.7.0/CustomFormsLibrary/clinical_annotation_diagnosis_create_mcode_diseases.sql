-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- mCode ((Minimal Common Oncology Data Elements) initiative : 
--   Create diagnosis forms based on 'Disease' group fields of the
--    mCODE™ Data Dictionnary Version 0.9.1.
-- ................................................................................
-- Create form 
--    - 'Primary Cancer Condition' joined to 'TNM ...' fields.
--    - 'Secondary Cancer Condition'.
-- --------------------------------------------------------------------------------

-- Primary Cancer Condition & TNM 
--   List based on :
--    - Body Location Code
--       ICD-O-3 topological codes from 2009 version of Stats Canada.
--    - Clinical Status
--       ConditionClinicalStatusCodes codes version 4.0.0 of the HL7 FHIR standard.
--       http://hl7.org/fhir/valueset-condition-clinical.html
--    - Condition Code
--       mCODE™ Value Set Name : PrimaryOrUncertainBehaviorCancerDisorderVS
--    - Histology Morphology Behavior
--       ICD-O-3 morphological codes from december 2010 version of the CIHI publications department.
--    - Staging System
--       mCODE™ Value Set Name : CancerStagingSystemVS
-- --------------------------------------------------------------------------------

INSERT INTO `diagnosis_controls` (`id`, `category`, `controls_type`, `flag_active`, `detail_form_alias`, `detail_tablename`, `display_order`, `databrowser_label`, `flag_compare_with_cap`) 
VALUES
(null, 'primary', 'mcode - primary cancer condition and tnm', 1, 'mcode_dxd_primary_cancer_condition', 'mcode_dxd_primary_cancer_conditions', 0, 'primary|mcode - primary cancer condition and tnm', 0);

-- Primary Cancer Condition

DROP TABLE IF EXISTS `mcode_dxd_primary_cancer_conditions`;
CREATE TABLE IF NOT EXISTS `mcode_dxd_primary_cancer_conditions` (
  `diagnosis_master_id` int(11) NOT NULL,
  `clinical_status` varchar(50) DEFAULT NULL,
  `condition_code` varchar(50) DEFAULT NULL,
  KEY `diagnosis_master_id` (`diagnosis_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mcode_dxd_primary_cancer_conditions_revs`;
CREATE TABLE IF NOT EXISTS `mcode_dxd_primary_cancer_conditions_revs` (
  `diagnosis_master_id` int(11) NOT NULL,
  `clinical_status` varchar(50) DEFAULT NULL,
  `condition_code` varchar(50) DEFAULT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3739 DEFAULT CHARSET=latin1;

ALTER TABLE `mcode_dxd_primary_cancer_conditions`
  ADD CONSTRAINT `FK_mcode_dxd_primary_cancer_conditions_diagnosis_masters` FOREIGN KEY (`diagnosis_master_id`) REFERENCES `diagnosis_masters` (`id`);

INSERT INTO structures(`alias`) VALUES ('mcode_dxd_primary_cancer_condition');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'DiagnosisDetail', 'mcode_dxd_primary_cancer_conditions', 'clinical_status', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_ConditionClinicalStatusCodes') , '0', '', '', 'help_mcode_dx_clinical_status', 'clinical status', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'mcode_dxd_primary_cancer_conditions', 'condition_code', 'autocomplete',  NULL , '0', 'setting`=\'size=10,url=/CodingIcd/CodingMCodes/autocomplete/PrimaryOrUncertainBehaviorCancerDisorderVS,tool=/CodingIcd/CodingMCodes/tool/PrimaryOrUncertainBehaviorCancerDisorderVS', '', 'help_mcode_dx_condition_code', 'condition code', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='topography' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '5', '', '0', '1', 'body location code', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='mcode_dxd_primary_cancer_conditions' AND `field`='clinical_status' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_ConditionClinicalStatusCodes')), 
'1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='mcode_dxd_primary_cancer_conditions' AND `field`='condition_code' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='setting`=\'size=10,url=/CodingIcd/CodingMCodes/autocomplete/PrimaryOrUncertainBehaviorCancerDisorderVS,tool=/CodingIcd/CodingMCodes/tool/PrimaryOrUncertainBehaviorCancerDisorderVS'), 
'1', '7', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='morphology' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '10', '', '0', '1', 'histology morphology behavior', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');
INSERT INTO structure_validations (structure_field_id, rule, language_message)
VALUES
((SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='mcode_dxd_primary_cancer_conditions' AND `field`='condition_code' AND `type`='autocomplete' ),
'validateMcodeCode,PrimaryOrUncertainBehaviorCancerDisorderVS', 'invalid mcode code');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('mcode - primary cancer condition and tnm', "Cancer Condition & TNM (mCODE™)", "Condition du cancer & TNM (mCODE™)"),
('body location code', 'Body Location Code', 'Code de localisation dans corps'),
('help_mcode_dx_clinical_status', 
"A flag indicating whether the condition is active or inactive, recurring, in remission, or resolved (as of the last update of the Condition) as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the ConditionClinicalStatusCodes code version 4.0.0 of the HL7 FHIR standard (http://hl7.org/fhir/ValueSet/condition-clinical).", 
"Une valeur indiquant si la condition clinique est active ou inactive, récurrente, en remission ou résolue (lors de la dernière mise à jour de la condition) telle que définie par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la version 4.0.0 du code ConditionClinicalStatusCodes de la norme FHIR HL7 (http://hl7.org/fhir/ValueSet/condition-clinical)."),
('condition code', 'Condition Code', 'Condition (code)'), 
('help_mcode_dx_condition_code', 
"The code representing the type of malignant neoplastic disease as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the PrimaryOrUncertainBehaviorCancerDisorder Value Set details of the mCODE Data Dictionnary version 0.9.1.", 
"Le code définissant le 'type de néoplasie maligne' tel que défini par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la liste des codes PrimaryOrUncertainBehaviorCancerDisorder de la version 0.9.1 du dictionnaire de données mCode."),
('histology morphology behavior', 'Histology Morphology Behavior', "Comportement histologique morphologique");

-- TNM

ALTER TABLE diagnosis_masters 
  ADD COLUMN mcode_clinical_tstage_staging_system varchar(50) DEFAULT NULL AFTER clinical_tstage,
  ADD COLUMN mcode_clinical_nstage_staging_system varchar(50) DEFAULT NULL AFTER clinical_nstage,
  ADD COLUMN mcode_clinical_mstage_staging_system varchar(50) DEFAULT NULL AFTER clinical_mstage,
  ADD COLUMN mcode_clinical_stage_summary_staging_system varchar(50) DEFAULT NULL AFTER clinical_stage_summary,
  ADD COLUMN mcode_path_tstage_staging_system varchar(50) DEFAULT NULL AFTER path_tstage,
  ADD COLUMN mcode_path_nstage_staging_system varchar(50) DEFAULT NULL AFTER path_nstage,
  ADD COLUMN mcode_path_mstage_staging_system varchar(50) DEFAULT NULL AFTER path_mstage,
  ADD COLUMN mcode_path_stage_summary_staging_system varchar(50) DEFAULT NULL AFTER path_stage_summary;
ALTER TABLE diagnosis_masters_revs
  ADD COLUMN mcode_clinical_tstage_staging_system varchar(50) DEFAULT NULL AFTER clinical_tstage,
  ADD COLUMN mcode_clinical_nstage_staging_system varchar(50) DEFAULT NULL AFTER clinical_nstage,
  ADD COLUMN mcode_clinical_mstage_staging_system varchar(50) DEFAULT NULL AFTER clinical_mstage,
  ADD COLUMN mcode_clinical_stage_summary_staging_system varchar(50) DEFAULT NULL AFTER clinical_stage_summary,
  ADD COLUMN mcode_path_tstage_staging_system varchar(50) DEFAULT NULL AFTER path_tstage,
  ADD COLUMN mcode_path_nstage_staging_system varchar(50) DEFAULT NULL AFTER path_nstage,
  ADD COLUMN mcode_path_mstage_staging_system varchar(50) DEFAULT NULL AFTER path_mstage,
  ADD COLUMN mcode_path_stage_summary_staging_system varchar(50) DEFAULT NULL AFTER path_stage_summary;

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='clinical_stage_summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '40', 'clinical stage', '0', '1', 'group', '1', '', '1', 'help_mcode_staging_group', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='clinical_tstage' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '42', '', '0', '1', 't stage', '1', '', '1', 'help_mcode_tnm_t', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='clinical_nstage' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '44', '', '0', '1', 'n stage', '1', '', '1', 'help_mcode_tnm_n', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='clinical_mstage' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '46', '', '0', '1', 'm stage', '1', '', '1', 'help_mcode_tnm_m', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'),
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='path_stage_summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '50', 'pathological stage', '0', '1', 'group', '1', '', '1', 'help_mcode_staging_group', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='path_tstage' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '52', '', '0', '1', 't stage', '1', '', '1', 'help_mcode_tnm_t', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='path_nstage' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '54', '', '0', '1', 'n stage', '1', '', '1', 'help_mcode_tnm_n', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='path_mstage' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '56', '', '0', '1', 'm stage', '1', '', '1', 'help_mcode_tnm_m', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'DiagnosisMaster', 'diagnosis_masters', 'mcode_clinical_tstage_staging_system', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS') , '0', '', '', 'help_mcode_staging_system', '', 'mcode staging system'), 
('ClinicalAnnotation', 'DiagnosisMaster', 'diagnosis_masters', 'mcode_clinical_nstage_staging_system', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS') , '0', '', '', 'help_mcode_staging_system', '', 'mcode staging system'), 
('ClinicalAnnotation', 'DiagnosisMaster', 'diagnosis_masters', 'mcode_clinical_mstage_staging_system', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS') , '0', '', '', 'help_mcode_staging_system', '', 'mcode staging system'), 
('ClinicalAnnotation', 'DiagnosisMaster', 'diagnosis_masters', 'mcode_clinical_stage_summary_staging_system', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS') , '0', '', '', 'help_mcode_staging_system', '', 'mcode staging system'), 
('ClinicalAnnotation', 'DiagnosisMaster', 'diagnosis_masters', 'mcode_path_tstage_staging_system', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS') , '0', '', '', 'help_mcode_staging_system', '', 'mcode staging system'), 
('ClinicalAnnotation', 'DiagnosisMaster', 'diagnosis_masters', 'mcode_path_nstage_staging_system', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS') , '0', '', '', 'help_mcode_staging_system', '', 'mcode staging system'), 
('ClinicalAnnotation', 'DiagnosisMaster', 'diagnosis_masters', 'mcode_path_mstage_staging_system', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS') , '0', '', '', 'help_mcode_staging_system', '', 'mcode staging system'), 
('ClinicalAnnotation', 'DiagnosisMaster', 'diagnosis_masters', 'mcode_path_stage_summary_staging_system', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS') , '0', '', '', 'help_mcode_staging_system', '', 'mcode staging system');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='mcode_clinical_tstage_staging_system' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_mcode_staging_system' AND `language_label`='' AND `language_tag`='mcode staging system'), 
'2', '43', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='mcode_clinical_nstage_staging_system' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_mcode_staging_system' AND `language_label`='' AND `language_tag`='mcode staging system'), 
'2', '45', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='mcode_clinical_mstage_staging_system' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_mcode_staging_system' AND `language_label`='' AND `language_tag`='mcode staging system'), 
'2', '47', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='mcode_clinical_stage_summary_staging_system' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_mcode_staging_system' AND `language_label`='' AND `language_tag`='mcode staging system'), 
'2', '41', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='mcode_path_tstage_staging_system' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_mcode_staging_system' AND `language_label`='' AND `language_tag`='mcode staging system'), 
'2', '53', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='mcode_path_nstage_staging_system' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_mcode_staging_system' AND `language_label`='' AND `language_tag`='mcode staging system'), 
'2', '55', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='mcode_path_mstage_staging_system' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_mcode_staging_system' AND `language_label`='' AND `language_tag`='mcode staging system'), 
'2', '57', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_primary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='mcode_path_stage_summary_staging_system' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_CancerStagingSystemVS')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_mcode_staging_system'), 
'2', '51', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('mcode staging system', 'System', 'Sytème'),
('help_mcode_staging_system',
"Method or staging system used to stage the cancer.",
""),
('help_mcode_staging_group', 
"The category describing the extent of the disease, according to the TNM staging rules, typically taking into account several anatomic and prognostic elements.",
""),
('help_mcode_tnm_t',
"Category of the primary tumor according to TNM staging rules.",
""),
('help_mcode_tnm_n',
"Category of regional lymph nodes according to TNM staging rules.",
""),
('help_mcode_tnm_m',
"Category of distant metastases according to TNM staging rules.",
"");

-- UPDATE structure_fields SET language_label = language_tag WHERE `field` LIKE 'mcode_clinical_%staging_system' OR `field` LIKE 'mcode_path_%staging_system';

-- Secondary Cancer Condition
--   List based on :
--    - Clinical Status
--       ConditionClinicalStatusCodes codes version 4.0.0 of the HL7 FHIR standard.
--       http://hl7.org/fhir/valueset-condition-clinical.html
--    - Condition Code
--       mCODE™ Value Set Name : PrimaryOrUncertainBehaviorCancerDisorderVS
--    - Histology Morphology Behavior
--       ICD-O-3 morphological codes from december 2010 version of the CIHI publications department.
--    - Staging System
--       mCODE™ Value Set Name : CancerStagingSystemVS
-- --------------------------------------------------------------------------------

INSERT INTO `diagnosis_controls` (`id`, `category`, `controls_type`, `flag_active`, `detail_form_alias`, `detail_tablename`, `display_order`, `databrowser_label`, `flag_compare_with_cap`) 
VALUES
(null, 'secondary - distant', 'mcode - secondary cancer condition', 1, 'mcode_dxd_secondary_cancer_condition', 'mcode_dxd_secondary_cancer_conditions', 0, 'secondary - distant|mcode - secondary cancer condition', 0);

DROP TABLE IF EXISTS `mcode_dxd_secondary_cancer_conditions`;
CREATE TABLE IF NOT EXISTS `mcode_dxd_secondary_cancer_conditions` (
  `diagnosis_master_id` int(11) NOT NULL,
  `clinical_status` varchar(50) DEFAULT NULL,
  `condition_code` varchar(50) DEFAULT NULL,
  KEY `diagnosis_master_id` (`diagnosis_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mcode_dxd_secondary_cancer_conditions_revs`;
CREATE TABLE IF NOT EXISTS `mcode_dxd_secondary_cancer_conditions_revs` (
  `diagnosis_master_id` int(11) NOT NULL,
  `clinical_status` varchar(50) DEFAULT NULL,
  `condition_code` varchar(50) DEFAULT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3739 DEFAULT CHARSET=latin1;

ALTER TABLE `mcode_dxd_secondary_cancer_conditions`
  ADD CONSTRAINT `FK_mcode_dxd_secondary_cancer_conditions_diagnosis_masters` FOREIGN KEY (`diagnosis_master_id`) REFERENCES `diagnosis_masters` (`id`);

INSERT INTO structures(`alias`) VALUES ('mcode_dxd_secondary_cancer_condition');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'DiagnosisDetail', 'mcode_dxd_secondary_cancer_conditions', 'clinical_status', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_ConditionClinicalStatusCodes') , '0', '', '', 'help_mcode_dx_clinical_status', 'clinical status', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'mcode_dxd_secondary_cancer_conditions', 'condition_code', 'autocomplete',  NULL , '0', 'setting`=\'size=10,url=/CodingIcd/CodingMCodes/autocomplete/PrimaryOrUncertainBehaviorCancerDisorderVS,tool=/CodingIcd/CodingMCodes/tool/PrimaryOrUncertainBehaviorCancerDisorderVS', '', 'help_mcode_dx_condition_code', 'condition code', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='mcode_dxd_secondary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='mcode_dxd_secondary_cancer_conditions' AND `field`='clinical_status' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_ConditionClinicalStatusCodes')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_mcode_dx_clinical_status' AND `language_label`='clinical status' AND `language_tag`=''), 
'1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_secondary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='mcode_dxd_secondary_cancer_conditions' AND `field`='condition_code' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='setting`=\'size=10,url=/CodingIcd/CodingMCodes/autocomplete/PrimaryOrUncertainBehaviorCancerDisorderVS,tool=/CodingIcd/CodingMCodes/tool/PrimaryOrUncertainBehaviorCancerDisorderVS' AND `default`='' AND `language_help`='help_mcode_dx_condition_code' AND `language_label`='condition code' AND `language_tag`=''), 
'1', '7', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='mcode_dxd_secondary_cancer_condition'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='morphology' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '10', '', '0', '1', 'histology morphology behavior', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('mcode - secondary cancer condition', "Cancer Condition (mCODE™)", "Condition du cancer (mCODE™)");

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;