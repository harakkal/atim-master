-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- --------------------------------------------------------------------------------
-- Update Participant Identifiers Report
-- ................................................................................
-- To be compatible with the MiscIdentifierControl created for the demo data.
-- --------------------------------------------------------------------------------

DELETE FROM structure_formats WHERE structure_id = (SELECT id FROM structures WHERE alias = 'report_participant_identifiers_result');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Datamart', '0', '', 'ctrnet_demo_hospital_number', 'input',  NULL , '1', 'size=20', '', '', 'ctrnet demo - hospital number', ''), 
('Datamart', '0', '', 'ctrnet_demo_health_card', 'input',  NULL , '1', 'size=20', '', '', 'ctrnet demo - health card', ''), 
('Datamart', '0', '', 'ctrnet_demo_care_card', 'input',  NULL , '1', 'size=20', '', '', 'ctrnet demo - care card', ''), 
('Datamart', '0', '', 'ctrnet_demo_ramq', 'input',  NULL , '1', 'size=20', '', '', 'ctrnet demo - ramq', ''), 
('Datamart', '0', '', 'ctrnet_demo_br_nbr', 'input',  NULL , '0', 'size=20', '', '', 'ctrnet demo - br nbr', ''), 
('Datamart', '0', '', 'ctrnet_demo_pr_nbr', 'input',  NULL , '0', 'size=20', '', '', 'ctrnet demo - pr nbr', ''), 
('Datamart', '0', '', 'ctrnet_demo_ovary_bank_nbr', 'input',  NULL , '0', 'size=20', '', '', 'ctrnet demo - ovary bank nbr', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='report_participant_identifiers_result'), 
(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='participant_identifier' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='help_participant identifier' AND `language_label`='participant identifier' AND `language_tag`=''), 
'0', '0', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_participant_identifiers_result'), 
(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='first_name' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='1'), 
'0', '1', '', '0', '1', 'name', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_participant_identifiers_result'), 
(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='last_name' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='1' AND `setting`='size=30' AND `default`='' AND `language_help`='help_last_name' AND `language_label`='' AND `language_tag`=''), 
'0', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_participant_identifiers_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='ctrnet_demo_hospital_number' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='1' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='ctrnet demo - hospital number' AND `language_tag`=''), 
'0', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_participant_identifiers_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='ctrnet_demo_health_card' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='1' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='ctrnet demo - health card' AND `language_tag`=''), 
'0', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_participant_identifiers_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='ctrnet_demo_care_card' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='1' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='ctrnet demo - care card' AND `language_tag`=''), 
'0', '12', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_participant_identifiers_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='ctrnet_demo_ramq' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='1' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='ctrnet demo - ramq' AND `language_tag`=''), 
'0', '13', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_participant_identifiers_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='ctrnet_demo_br_nbr' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='ctrnet demo - br nbr' AND `language_tag`=''), 
'0', '21', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_participant_identifiers_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='ctrnet_demo_pr_nbr' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='ctrnet demo - pr nbr' AND `language_tag`=''), 
'0', '22', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='report_participant_identifiers_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='ctrnet_demo_ovary_bank_nbr' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='ctrnet demo - ovary bank nbr' AND `language_tag`=''), 
'0', '23', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;