-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CTRNet Demo : Consent Master - Consent File Upload
-- ................................................................................
-- Add field to any consent form to upload consent file in pdf format, word, etc.
-- --------------------------------------------------------------------------------

-- In core.php
-- -----------
-- 
-- The directory that contains the uploaded files is located beside the 'APP' directory of ATiM 
-- and should be defined in the core file (see example below with directory ./app/atimUploadDirectory). 
-- However you can move the directory somewhere else changing following core variables.
-- 
-- Permission on directory has to be changed. To setup permissions on directory, 
-- please see Quick Install Guide (Step by Step).
-- 
--     /**
--      * Let user to download a file on the server using a ATiM field with setting set to 'File'.
--      *
--      * By default, the file will be downloaded into the \atimUploadDirectory. Path of the file
--      * could be changed by another one changing the value of the $uploadDirectory variable.
--      */
-- 
--    $uploadDirectory = substr(APP, 0, strlen(APP) - 4);
-- 
--    Configure::write('uploadDirectory', $uploadDirectory . 'atimUploadDirectory');
--    Configure::write('deleteDirectory', 'deleteDirectory');
--    Configure::write('deleteUploadedFilePhysically', false);
--    Configure::write('maxUploadFileSize', 10 * 1024 * 1024);
-- 
-- If you want to keep the files for ever set the deleteUploadedFilePhysically to false.
-- The directory that will contain the deleted and replaced files is defined by deleteDirectory.
-- The maximum size of each file for upload is 10MB by default. See maxUploadFileSize.
-- 
-- In PHP.ini
-- -----------
-- 
-- The PHP.ini should changed but it can be done also in core.php using following line code
--    upload_max_filesize = 10.1M
--    post_max_size = 16M
-- 
-- Field specification
-- In structure_fields table set the type of the field to 'file' and the setting 
-- to 'class=upload'.
-- --------------------------------------------------------------------------------

ALTER TABLE consent_masters 
  ADD COLUMN ctrnet_demo_consent_file_name varchar(500) DEFAULT null;
ALTER TABLE consent_masters_revs
  ADD COLUMN ctrnet_demo_consent_file_name varchar(500) DEFAULT null;
  
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'ConsentMaster', 'consent_masters', 'ctrnet_demo_consent_file_name', 'file',  NULL , '0', 'class=upload', '', '', 'file', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='consent_masters'), 
(SELECT id FROM structure_fields WHERE `model`='ConsentMaster' AND `tablename`='consent_masters' AND `field`='ctrnet_demo_consent_file_name' AND `type`='file' AND `structure_value_domain`  IS NULL  AND `setting`='class=upload' AND `default`='' AND `language_help`='' AND `language_label`='file' AND `language_tag`=''), 
'1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('file', 'File', 'Fichier');

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;