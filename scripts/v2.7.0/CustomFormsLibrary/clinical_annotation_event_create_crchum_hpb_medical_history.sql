-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CRCHUM Hepatopancreatobiliary Bank : Medical History
-- ................................................................................
-- Form developped to capture data for the CRCHUM Hepatopancreatobiliary BioBank.
-- Designed and created by the BioBank PIs.
-- --------------------------------------------------------------------------------

INSERT INTO event_controls (id, disease_site, event_group, event_type, flag_active, detail_form_alias, detail_tablename, display_order, databrowser_label, flag_use_for_ccl, use_addgrid, use_detail_form_for_index) VALUES
(null, 'liver', 'clinical', 'crchum hpb - medical history', 1, 'crchum_hpb_ed_medical_history', 'crchum_hpb_ed_medical_histories', 0, 'clinical|crchum hpb - medical history', 0, 1, 1);

DROP TABLE IF EXISTS crchum_hpb_ed_medical_histories ;
CREATE TABLE IF NOT EXISTS crchum_hpb_ed_medical_histories (
  event_master_id int(11) NOT NULL,
  type varchar(100)  DEFAULT NULL,
  who_icd10_code varchar(50) DEFAULT NULL,
  KEY event_master_id (event_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS crchum_hpb_ed_medical_histories_revs;
CREATE TABLE IF NOT EXISTS crchum_hpb_ed_medical_histories_revs (
  event_master_id int(11) NOT NULL,
  type varchar(100)  DEFAULT NULL,
  who_icd10_code varchar(50) DEFAULT NULL,  
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE crchum_hpb_ed_medical_histories
  ADD CONSTRAINT crchum_hpb_ed_medical_histories_ibfk_1 FOREIGN KEY (event_master_id) REFERENCES event_masters (id);
  
INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_ed_medical_history', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : Medical History Types\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM HPB : Medical History Types', 1, 100, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : Medical History Types');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("asa", "Asa", "Asa", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("heart disease", "Heart Disease", "Maladie cardiaque", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("vascular disease", "Vascular Disease", "Maladie vasculaire", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("respiratory disease", "Respiratory Disease", "Maladie respiratoire", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("neural vascular disease", "Neural Vascular Disease", "Maladie vasculaire neurale", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("endocrine disease", "Endocrine Disease", "Maladie endocrinienne", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("urinary disease", "Urinary Disease", "Maladie urinaire", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("gastro-intestinal disease", "Gastro-Intestinal Disease", "Maladie gastro-intestinale", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("gynecologic disease", "Gynecologic Disease", "Maladie gynécologique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("other disease", "Other Disease", "Autre maladie", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("diabetes", "Diabetes", "Diabète", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("dyslipidemia", "Dyslipidemia", "Dyslipidémie", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("hepatitis", "Hepatitis", "Hépatite", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("cirrhosis", "Cirrhosis", "Cirrhose", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("hepatic vein embolization", "Hepatic Vein Embolization", "Embolisation des veines hépatiques", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structures(`alias`) VALUES ('crchum_hpb_ed_medical_history');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_medical_histories', 'who_icd10_code', 'autocomplete',  NULL , '0', 'size=10,url=/CodingIcd/CodingIcd10s/autocomplete/ca,tool=/CodingIcd/CodingIcd10s/tool/ca', '', 'help_cause_of_death_icd10_code_who', 'precision', ''), 
('ClinicalAnnotation', 'EventMaster', 'crchum_hpb_ed_medical_histories', 'type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_ed_medical_history') , '0', '', '', '', 'type', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_medical_history'), 
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='crchum_hpb_ed_medical_histories' AND `field`='type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_ed_medical_history')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='type' AND `language_tag`=''), 
'1', '9', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_medical_history'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_medical_histories' AND `field`='who_icd10_code' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10,url=/CodingIcd/CodingIcd10s/autocomplete/ca,tool=/CodingIcd/CodingIcd10s/tool/ca' AND `default`='' AND `language_help`='help_cause_of_death_icd10_code_who' AND `language_label`='precision' AND `language_tag`=''), 
'1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_medical_history'), 
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '1', 'cols=40,rows=1', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_validations(structure_field_id, rule, language_message) VALUES
((SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_medical_histories' AND `field`='who_icd10_code' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10,url=/CodingIcd/CodingIcd10s/autocomplete/ca,tool=/CodingIcd/CodingIcd10s/tool/ca' AND `default`='' AND `language_help`='help_cause_of_death_icd10_code_who' AND `language_label`='precision' AND `language_tag`=''), 'validateIcd10CaCode', 'invalid disease code'),
((SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='crchum_hpb_ed_medical_histories' AND `field`='type'), 'notBlank', '');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("crchum hpb - medical history", "Medical History (CRCHUM Hpb)", "Historique médicale (CRCHUM Hpb)");

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;