-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- mCode ((Minimal Common Oncology Data Elements) initiative : 
--   Add 'Patient' group fields
-- ................................................................................
-- Change participant profile structure to support Patient Group fields excepted 
-- Address Zip Code (use contact form).
-- --------------------------------------------------------------------------------

-- Race 
--   Based on :
--   - RaceAndEthnicityCDC codes version 3.0.0 of the HL7 FHIR standard.
--     http://hl7.org/fhir/us/core/ValueSet/omb-race-category
--     http://hl7.org/fhir/us/core/STU3/CodeSystem-cdcrec.html#cdcrec-1002-5
-- --------------------------------------------------------------------------------

UPDATE structure_fields 
SET  `type`='autocomplete', `structure_value_domain`= NULL , `setting`='size=10,url=/CodingIcd/CodingMCodes/autocomplete/RaceCdc,tool=/CodingIcd/CodingMCodes/tool/RaceCdc', `language_label`='race code', `language_help`='help_mcode_races' 
WHERE model='Participant' AND tablename='participants' AND field='race' AND `type`='select';
UPDATE structure_formats 
SET `flag_override_help`='1', `language_help`='help_mcode_races' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='participants') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='race');
INSERT INTO structure_validations (structure_field_id, rule, language_message)
VALUES
((SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='race' AND `type`='autocomplete'),
'validateMcodeCode,RaceCdc', 'invalid mcode code');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('race code', 'Race code', 'Code de la race'),
('help_mcode_races',
"A code for the person's race as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the RaceAndEthnicityCDC code version 3.0.0 of the HL7 FHIR standard (http://hl7.org/fhir/us/core/ValueSet/omb-race-category).", 
"Un code pour définir la race de la personne telle que définie par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la version 3.0.0 du code RaceAndEthnicityCDC de la norme FHIR HL7 (http://hl7.org/fhir/us/core/ValueSet/omb-race-category).");

-- Ethnicity 
--   Based on :
--   - RaceAndEthnicityCDC codes version 3.0.0 of the HL7 FHIR standard.
--     http://hl7.org/fhir/us/core/ValueSet/omb-race-category
--     http://hl7.org/fhir/us/core/STU3/CodeSystem-cdcrec.html#cdcrec-1002-5
-- --------------------------------------------------------------------------------

ALTER TABLE participants
   ADD COLUMN mcode_ethnicity_code varchar(50) default NULL AFTER race;
ALTER TABLE participants_revs
   ADD COLUMN mcode_ethnicity_code varchar(50) default NULL AFTER race;

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'Participant', 'participants', 'mcode_ethnicity_code', 'autocomplete', NULL , '0', 'size=10,url=/CodingIcd/CodingMCodes/autocomplete/EthnicityCDC,tool=/CodingIcd/CodingMCodes/tool/EthnicityCDC', '', 'help_mcode_ethnicities', 'ethnicity code', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='participants'), 
(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='mcode_ethnicity_code' AND `type`='autocomplete'), 
'1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
INSERT INTO structure_validations (structure_field_id, rule, language_message)
VALUES
((SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='mcode_ethnicity_code' AND `type`='autocomplete'),
'validateMcodeCode,EthnicityCDC', 'invalid mcode code');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('ethnicity code', 'Ethnicity code', 'Code de l''éthnicité'),
('help_mcode_ethnicities',
"A code for the person's ethniciy as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the RaceAndEthnicityCDC code version 3.0.0 of the HL7 FHIR standard (http://hl7.org/fhir/us/core/ValueSet/omb-race-category).", 
"Un code pour définir l'éthnicité de la personne telle que définie par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la version 3.0.0 du code RaceAndEthnicityCDC de la norme FHIR HL7 (http://hl7.org/fhir/us/core/ValueSet/omb-race-category).");

-- Administrative Gender 
--   Based on :
--   - AdministrativeGender codes version 4.0.0 of the HL7 FHIR standard.
--     http://hl7.org/fhir/valueset-administrative-gender.html
-- --------------------------------------------------------------------------------

UPDATE structure_fields 
SET  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_AdministrativeGenderCodes') ,  
`language_help`='help_mcode_administrative_genders',  
`language_label`='administrative gender' 
WHERE model='Participant' AND tablename='participants' AND field='sex' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='sex');
UPDATE structure_formats SET `flag_override_help`='0', `language_help`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='participants') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='sex' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_AdministrativeGenderCodes') AND `flag_confidential`='0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('administrative gender', 'Administrative Gender', 'Genre administratif'),
('help_mcode_administrative_genders',
"A gender classification used for administrative purposes as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Administrative gender is not necessarily the same as a biological description or a gender identity. This attribute does not include terms related to clinical gender.
Based on the AdministrativeGender code version 4.0.0 of the HL7 FHIR standard (http://hl7.org/fhir/valueset-administrative-gender.html).", 
"Une classification par sexe utilisée à des fins administratives telle que définie par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Le genre administratif n'est pas nécessairement similaire à la description biologique ou l'identité de genre. Cet attribut n'inclut pas les termes liés au genre clinique.
Basé sur la version 4.0.0 du code AdministrativeGender de la norme FHIR HL7 (http://hl7.org/fhir/valueset-administrative-gender.html).");

UPDATE participants
SET sex = 'female' WHERE sex = 'f';
UPDATE participants
SET sex = 'male' WHERE sex = 'm';
UPDATE participants
SET sex = 'unknown' WHERE sex = 'u';

UPDATE participants_revs
SET sex = 'female' WHERE sex = 'f';
UPDATE participants_revs
SET sex = 'male' WHERE sex = 'm';
UPDATE participants_revs
SET sex = 'unknown' WHERE sex = 'u';

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;