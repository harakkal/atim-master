CLS
@ECHO off
COLOR F
ECHO ==================================================================
ECHO ==   ATiM DATABASE SET UP                                       ==
ECHO ==     Version 2.7.3                                            ==
ECHO ==================================================================
ECHO -
ECHO All your data will be erased and unrecoverable.
SET /p confirm= "Are you sure to run it? if yes type YES or anything else for cancel. YES/NO "

IF NOT "%confirm%" == "YES" (
	EXIT
)

set /p databaseName=Database Name? 

REM ==================================================================
REM  DROP DATABASE AND RECREATE IT
REM ==================================================================

@ECHO on
mysql -u root -e "DROP DATABASE IF EXISTS %databaseName%"
mysql -u root -e "CREATE DATABASE %databaseName%"
@ECHO off

REM ==================================================================
REM  ATiM TRUNK CREATION AND UPDATE SCRIPTS
REM ==================================================================

@ECHO on
mysql -u root %databaseName% --default-character-set=utf8 < ./atim_v2.7.0_full_installation.sql
mysql -u root %databaseName% --default-character-set=utf8 < ./atim_v2.7.1_upgrade.sql
mysql -u root %databaseName% --default-character-set=utf8 < ./atim_v2.7.2_upgrade.sql
mysql -u root %databaseName% --default-character-set=utf8 < ./atim_v2.7.3_upgrade.sql
@ECHO off

REM ==================================================================
REM  ATiM USERS UPDATE
REM ==================================================================

@ECHO on
mysql -u root %databaseName% -e"UPDATE users SET flag_active=0, deleted = 1, username=CONCAT('user#', id),first_name=CONCAT('user#', id), last_name=CONCAT('user#', id), password='2939a463baec8a8b1f3cef69238e6bfea3795a5c', email = '' WHERE username != 'administrator'"
mysql -u root %databaseName% -e"UPDATE users SET flag_active=1, deleted = 0, first_name='administrator', last_name='administrator', email = ''  WHERE username = 'administrator'"

mysql -u root %databaseName% -e"UPDATE groups SET flag_show_confidential = '0'  WHERE name != 'Administrators'"
mysql -u root %databaseName% -e"UPDATE groups SET flag_show_confidential = '1'  WHERE name = 'Administrators'"
@ECHO off

COLOR F
ECHO -
ECHO ==================================================================
ECHO ==   ATiM DATABASE SET UP - DONE                                ==
ECHO ==================================================================
ECHO -

ECHO Use the username 'administrator' and the password 'administrator' to log into ATiM.
ECHO -
ECHO To force password reset for the username 'administrator', execute following query:
ECHO "UPDATE users SET force_password_reset = 1 WHERE username = 'administrator'"
ECHO -
ECHO To change the username 'administrator', execute following query:
ECHO "UPDATE users SET username = 'my_new_username' WHERE username = 'administrator'"
ECHO -

PAUSE
